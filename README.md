# GeoPortal Data Entry App

Geoportal data-entry technology allows the actors who use it, the project management of spatio-temporal documents specified by meta-documents called "Use Case Descriptor" (UCD) which determine the document model, management, life cycle, etc. The system allows you to: (i) access and search for published projects by role for VRE, (ii) access the project publication report, view the publication status (SUCCESS, WARNING, ERROR), etc., (iii ) generate links for visualization on the map, (iv) update the contents of the registered projects through the workflow, (v) manage the relationships (quantity and quality temporal links) between the documents. In general, the technology allows the data entry of any document having spatio-temporal characteristics, whose metadata and payload are specified by one or more "gCube Profile" profiles which determine the structure of the resulting document (JSON).

## Built With

* [OpenJDK](https://openjdk.java.net/) - The JDK used
* [Maven](https://maven.apache.org/) - Dependency Management

**Uses**

* GWT v.2.10.0. [GWT](http://www.gwtproject.org) is licensed under [Apache License 2.0](http://www.gwtproject.org/terms.html)
* GWT-Bootstrap v.2.3.2.0. [GWT-Bootstrap](https://github.com/gwtbootstrap) is licensed under [Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0)
* pretty-print-json v.1.1. [pretty-print-json](https://github.com/center-key/pretty-print-json) is licensed under [MIT](https://github.com/center-key/pretty-print-json/blob/main/LICENSE.txt)
* jsoneditor v.9.5.5. [jsoneditor](https://github.com/josdejong/jsoneditor) is licensed under [Apache License 2.0](https://github.com/josdejong/jsoneditor/blob/master/LICENSE)

## Architecture

<img src="https://gcube.wiki.gcube-system.org/images_gcube/8/8e/GeoPortalDataEntry_Architecture.png" style="max-width:800px;" alt="GeoPortal Data-Entry - Architecture" />

## Showcase

##### D4GNA instance of Geoportal D4Science

see at [Dataset per il Geoportale Nazionale per l’Archeologia (D4GNA)](https://gna.d4science.org/)

**New Project facility**

<img src="https://gcube.wiki.gcube-system.org/images_gcube/c/c0/D4GNA_New_Project.png" style="max-width:800px;" alt="Workspace Home" />

**List of Projects facility**

<img src="https://gcube.wiki.gcube-system.org/images_gcube/6/67/D4GNA_List_Of_Projects.png" style="max-width:800px;" alt="List of Projects" />


**UnPublish facility**

<img src="https://gcube.wiki.gcube-system.org/images_gcube/6/6a/D4GNA_Unpublish.png" style="max-width:800px;" alt="UnPublish" />


**View Relations facility**

<img src="https://gcube.wiki.gcube-system.org/images_gcube/7/76/D4GNA_ViewRelations.png" style="max-width:800px;" alt="View Relations" />



## Documentation

Geoportal Service Documentation is available at [gCube CMS Suite](https://geoportal.d4science.org/geoportal-service/docs/index.html)

User Guide (DRAFT ITA) is available at [Guida al Sistema D4GNA (DRAFT-ITA)](https://gcube.wiki.gcube-system.org/images_gcube/b/b7/D4science_Guida_al_Sistema_D4GNA_bozza.pdf)

D4GNA Use Case -  3 Phase Lifecycle

<img src="https://gcube.wiki.gcube-system.org/images_gcube/4/46/D4GNA_Workflow_Phases_and_Operations.png" style="max-width:800px;" alt="GeoPortal Data-Entry - Workflow & Phases & Operations" />

## Change log

See the [Releases](https://code-repo.d4science.org/gCubeSystem/geoportal-data-entry-app/releases)

## Authors

* **Francesco Mangiacrapa** ([ORCID](https://orcid.org/0000-0002-6528-664X)) Computer Scientist at [ISTI-CNR Infrascience Group](http://nemis.isti.cnr.it/groups/infrascience)

## License

This project is licensed under the EUPL V.1.1 License - see the [LICENSE.md](LICENSE.md) file for details.


## About the gCube Framework
This software is part of the [gCubeFramework](https://www.gcube-system.org/ "gCubeFramework"): an
open-source software toolkit used for building and operating Hybrid Data
Infrastructures enabling the dynamic deployment of Virtual Research Environments
by favouring the realisation of reuse oriented policies.
 
The projects leading to this software have received funding from a series of European Union programmes including:

- the Sixth Framework Programme for Research and Technological Development
    - DILIGENT (grant no. 004260).
- the Seventh Framework Programme for research, technological development and demonstration 
    - D4Science (grant no. 212488);
    - D4Science-II (grant no.239019);
    - ENVRI (grant no. 283465);
    - EUBrazilOpenBio (grant no. 288754);
    - iMarine(grant no. 283644).
- the H2020 research and innovation programme 
    - BlueBRIDGE (grant no. 675680);
    - EGIEngage (grant no. 654142);
    - ENVRIplus (grant no. 654182);
    - PARTHENOS (grant no. 654119);
    - SoBigData (grant no. 654024);
    - DESIRA (grant no. 818194);
    - ARIADNEplus (grant no. 823914);
    - RISIS2 (grant no. 824091);
    - PerformFish (grant no. 727610);
    - AGINFRAplus (grant no. 731001).


