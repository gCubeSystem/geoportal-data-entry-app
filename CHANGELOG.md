
# Changelog

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v3.3.0] - 2024-10-15

- Implemented the init facility to resolve a public link on an item [#27120]
- Integrated new Uri-Resolver-Manager [#27160]
- Added Get Shareable Link facility [#27120]
- Added optional message when performing lifecycle step [#27192]
- Enforced deleteProject method/UX
- The save operation is now monitored asynchronously [#28268]
- Integrated in the report UI the link of project just saved

## [v3.2.2] - 2024-01-11

- Improved display of results on multiple fields (in the table) [#26372]

## [v3.2.1] - 2023-06-16

- Fixed issue in the Search facility [#25265]

## [v3.2.0] - 2023-05-12

- Implemented the Update facility [#24166]
- Integrated with the geoportal-data-mapper library [#24244]
- Integrated the Geoportal Data-Viewer Widget [#25015] 
- Passed to Geoportal_Resolver service [#25031]
- Provided the "View Document" and "View As JSON" facilities
- DELETE relation operation allowed only in DRAFT phase [#25104]

## [v3.1.0] - 2023-03-06

#### Enhancements

- [#24569] The Edit operation is available only in the "DRAFT" phase
- [#24571] The "Create Relation" operation is available only in the "DRAFT" phase

## [v3.0.2] - 2023-02-02

#### Fixes

- [#24520] Added parameter "force=true" to Delete Project
- [#24475] Propagated the Access Policy in the fileset 

## [v3.0.1] - 2023-01-19

#### Fixes

- [#24281] Fixed filtering selection label
- [#24049] Fixed "Show on Map" facility vs Chrome browser
- [#24432] Fixing serialization issue using LinkedHashMap<String, String> instead of LinkedHashMap<String, Object>

## [v3.0.0] - 2022-11-09

#### Enhancements

- [#22684] Migrated to geoportal-data-entry-app configuration for UCD
- [#23587] GUI model viewer passed to tree data structure
- [#22883] Integrated with (the new) geoportal-client (>= 1.1.0-SNAPSHOT)
- [#22685] Migrated to geoportal-data-list configuration for UCD
- [#23784] Migrated list and reload, searching and ordering functionalities
- [#23785] Migrated the GNA functionalities
- [#23834] Integrated with the create/view/delete Relationship facility
- [#23913] Integrated with GUI presentation configurations read from IS
- [#23926] Integrated a Post Creation Action in the UCD and manage it
- [#24136] Integrated the temporal dimension on the front-end side
- [#24458] Published projects cannot be edited/updated


## [v2.2.1] - 2022-06-29

#### Enhancements

- [#23593] Shown the published/unpublished field in the table
- Passed to maven-portal-bom 3.6.4

## [v2.2.0] - 2022-06-08

#### Enhancements

- [#23390] Implemented facility: "Clone Project"
- [#23457] Implemented the "Publish/UnPublish Project" facility

## [v2.1.0] - 2021-11-24

#### Enhancements

- [#22455] Integrated with roles: (Data-Member as default), Data-Manager, Data-Editor
- [#22287] Integrated with base search, ordering and filtering facility provided by MongoDB
- [#22506] Re-engineered the common utilities


## [v2.0.1] - 2021-11-17

- [#22369] Just to include the bug fix for Policy and LicenseID in the geoportal-common


## [v2.0.0] - 2021-09-29

#### Enhancements

- [#20435] Client integration with MongoConcessioni 
- Moved to maven-portal-bom 3.6.3
- [#21856] Implemented new user feedback
- [#21890] Passed to mongoID
- [#20599] Get List of Records
- [#22002] Integrated with ValidationReport and status
- [#21990] Provide the (first version of) edit mode
- [#22040] Revisited the "Abstract Relazione di Scavo"


## [v1.2.0] - 2020-12-18

#### Enhancements

- [#20357] new requirements


## [v1.1.0] - 2020-12-1

#### Enhancements

- [#20210] Updating required for data entry facility after the first feedback by domain experts
        

## [v1.0.2] - 2020-11-09

#### Bug fixes

- [#20092] Repeatible form: validate the card after the create event is fired


## [v1.0.1] - 2020-11-04

#### Bug fixes

- [#20063] Fixes for data-entry components


## [v1.0.0] - 2020-10-07

- [#19916] First release
