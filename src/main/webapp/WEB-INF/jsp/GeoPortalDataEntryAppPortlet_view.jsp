<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>

<%-- Uncomment below lines to add portlet taglibs to jsp
<%@ page import="javax.portlet.*"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>

<portlet:defineObjects />
--%>
<script
	src='<%=request.getContextPath()%>/GeoPortalDataEntryApp/js/jquery-1.10.1.min.js'></script>

<script
	src='<%=request.getContextPath()%>/GeoPortalDataEntryApp/js/bootstrap.min.js'></script>

<link
	href="//cdn.jsdelivr.net/npm/pretty-print-json@1.1/dist/pretty-print-json.css"
	rel="stylesheet" type="text/css">
<script
	src="//cdn.jsdelivr.net/npm/pretty-print-json@1.1/dist/pretty-print-json.min.js"
	type="text/javascript"></script>

<link
	href="//cdnjs.cloudflare.com/ajax/libs/jsoneditor/9.5.5/jsoneditor.min.css"
	rel="stylesheet" type="text/css">

<script
	src="//cdnjs.cloudflare.com/ajax/libs/jsoneditor/9.5.5/jsoneditor.min.js"
	type="text/javascript"></script>

<link type="text/css" rel="stylesheet"
	href="<%=request.getContextPath()%>/GeoPortalDataEntryApp/css/ol.css">

<script type="text/javascript"
	src="<%=request.getContextPath()%>/GeoPortalDataEntryApp/js/ol.js"></script>

<link rel="stylesheet"
	href="<%=request.getContextPath()%>/GeoPortalDataEntryApp.css"
	type="text/css">


<script type="text/javascript"
	src="<%=request.getContextPath()%>/GeoPortalDataEntryApp/GeoPortalDataEntryApp.nocache.js"></script>

<!-- <script type="text/javascript"
	src='<%=request.getContextPath()%>/js/jquery.autosize.js'></script> -->

<div id="geoportal-loaders"></div>
<div id="geoportal-data-entry"></div>