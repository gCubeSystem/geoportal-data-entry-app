package org.gcube.portlets.user.geoportaldataentry.client.ui.action;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.gcube.application.geoportalcommon.shared.config.GcubeUserRole;
import org.gcube.application.geoportalcommon.shared.geoportal.ResultDocumentDV;
import org.gcube.application.geoportalcommon.shared.geoportal.config.ActionDefinitionDV;
import org.gcube.portlets.user.geoportaldataentry.client.ConstantsGeoPortalDataEntryApp;
import org.gcube.portlets.user.geoportaldataentry.client.events.WorkflowActionOnSelectedItemEvent;

import com.github.gwtbootstrap.client.ui.Button;
import com.github.gwtbootstrap.client.ui.ButtonGroup;
import com.github.gwtbootstrap.client.ui.constants.ButtonType;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;

public class ActionListPanel extends Composite {

	private static ActionListPanelUiBinder uiBinder = GWT.create(ActionListPanelUiBinder.class);

	interface ActionListPanelUiBinder extends UiBinder<Widget, ActionListPanel> {
	}

	private String projectName;
	private String profileID;
	private List<ActionDefinitionDV> listActionDefinition;
	private HandlerManager appManagerBus;

	public ActionListPanel(HandlerManager appManagerBus, String projectName, String profileID,
			List<ActionDefinitionDV> listActionDef) {
		initWidget(uiBinder.createAndBindUi(this));
		GWT.log("Adding list of actions: " + listActionDef);
		this.appManagerBus = appManagerBus;
		this.projectName = projectName;
		this.profileID = profileID;
		this.listActionDefinition = listActionDef;
		initActions(listActionDefinition);
		actionListBasePanel.setVisible(false);
	}

	@UiField
	HTMLPanel actionListContainer;

	@UiField
	HTMLPanel actionListBasePanel;

	private Map<String, List<ActionDefButton>> mapPhaseListButtons;

	private void initActions(List<ActionDefinitionDV> listActionDef) {

		if (listActionDef.size() > 0) {
			// actionListBasePanel.setVisible(true);

			ButtonGroup buttonGroup = new ButtonGroup();
			buttonGroup.getElement().addClassName("actions-button-group");

			mapPhaseListButtons = new LinkedHashMap<String, List<ActionDefButton>>();

			for (ActionDefinitionDV actionDefinitionDV : listActionDef) {

				// skipping the special workflow action
				if (actionDefinitionDV.getId()
						.equals(ConstantsGeoPortalDataEntryApp.WORKFLOW_ACTION_POST_CREATION_ACTION_ID)) {
					continue;
				}

				Button butt = new Button();
				butt.setText(actionDefinitionDV.getTitle());
				butt.setTitle(actionDefinitionDV.getDescription());
				butt.setType(ButtonType.INFO);
				butt.addClickHandler(new ClickHandler() {

					@Override
					public void onClick(ClickEvent event) {

						appManagerBus
								.fireEvent(new WorkflowActionOnSelectedItemEvent<ResultDocumentDV>(actionDefinitionDV));

					}
				});

				String[] displayOnPhases = actionDefinitionDV.getDisplayOnPhase();

				for (String displayOnPhase : displayOnPhases) {
					List<ActionDefButton> mapPhase = mapPhaseListButtons.get(displayOnPhase);
					if (mapPhase == null)
						mapPhase = new ArrayList<ActionDefButton>();

					mapPhase.add(new ActionDefButton(actionDefinitionDV, butt));
					mapPhaseListButtons.put(displayOnPhase, mapPhase);
				}
				buttonGroup.add(butt);
			}

			actionListContainer.add(buttonGroup);
		} else {
			actionListBasePanel.setVisible(false);
		}

	}

	public <T> void showActionsOnSelected(List<T> listSelected, GcubeUserRole userRole) {
		setAllActionsVisible(false);

		if (listSelected.size() == 1) {

			for (T item : listSelected) {

				if (item instanceof ResultDocumentDV) {

					String itemPhase = ((ResultDocumentDV) item).getLifecycleInfo().getPhase();

					List<ActionDefButton> listButtons = mapPhaseListButtons.get(itemPhase);

					if (listButtons != null) {

						for (ActionDefButton actionDefButton : listButtons) {

							Set<String> roles = actionDefButton.getActionDefinitionDV().getRoles();

							// No role/s defined means enable the action by default
							if (roles.isEmpty()) {
								actionDefButton.getButton().setVisible(true);
								actionListBasePanel.setVisible(true);
							} else {
								// Checking if the userRole is matching the role defined in the ActionDefinition
								boolean isRoleIntoActionDef = roles.stream()
										.anyMatch(userRole.getName()::equalsIgnoreCase);
								if (isRoleIntoActionDef) {
									actionDefButton.getButton().setVisible(true);
									actionListBasePanel.setVisible(true);
								}
							}
						}
					}
				}
			}
		}

	}

	private void setAllActionsVisible(boolean bool) {

		if (mapPhaseListButtons != null && mapPhaseListButtons.values().size() > 0) {

			Iterator<List<ActionDefButton>> collIterator = mapPhaseListButtons.values().iterator();
			if (collIterator != null) {
				while (collIterator.hasNext()) {
					List<ActionDefButton> listButton = collIterator.next();
					for (ActionDefButton actionDefButton : listButton) {
						actionDefButton.getButton().setVisible(bool);
					}

				}
			}
		}
	}
}
