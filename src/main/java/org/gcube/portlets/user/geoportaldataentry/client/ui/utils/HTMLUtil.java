package org.gcube.portlets.user.geoportaldataentry.client.ui.utils;

import org.gcube.application.geoportalcommon.shared.geoportal.project.TemporalReferenceDV;
import org.gcube.portlets.user.geoportaldataentry.client.ConstantsGeoPortalDataEntryApp;

/**
 * The Class HTMLUtil.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 *         Jan 26, 2021
 */
public class HTMLUtil {

	/**
	 * The Enum HTML_TAG.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
	 * 
	 *         Jan 26, 2021
	 */
	public static enum HTML_TAG {
		p, div, span
	}

	/**
	 * Gets the HTML element.
	 *
	 * @param tag        the tag
	 * @param textSize   the text size
	 * @param rgb        the rgb
	 * @param fontWeight the font weight
	 * @param text       the text
	 * @return the HTML element
	 */
	public static String getHTMLElement(HTML_TAG tag, Integer textSize, String rgb, String fontWeight, String text) {
		StringBuilder htmlEl = new StringBuilder();

		if (textSize != null || rgb != null || fontWeight != null) {
			String style = "";
			if (textSize != null) {
				style += "font-size:" + textSize + ";";
			}
			if (rgb != null) {
				style += "color:#" + rgb + ";";
			}
			if (fontWeight != null) {
				style += "font-weight:" + fontWeight + ";";
			}
			htmlEl.append("<" + tag.name() + " style='" + style + "'>");
		} else {
			htmlEl.append(tagOPEN(tag));
		}

		if (text != null) {
			htmlEl.append(text);
		}

		htmlEl.append(tagCLosed(tag));

		return htmlEl.toString();
	}

	/**
	 * Tag OPEN.
	 *
	 * @param tag the tag
	 * @return the string
	 */
	public static String tagOPEN(HTML_TAG tag) {
		return "<" + tag.name() + ">";
	}

	/**
	 * Tag C losed.
	 *
	 * @param tag the tag
	 * @return the string
	 */
	public static String tagCLosed(HTML_TAG tag) {
		return "</" + tag.name() + ">";
	}

	/**
	 * Json to HTML.
	 *
	 * @param jsonTxt         the json txt
	 * @param cssClassToTable the css class to table
	 * @return the element
	 */
	public static native String jsonToHTML(String jsonTxt, String cssClassToTable)/*-{
		try {

			var jsonObj = JSON.parse(jsonTxt);
			//console.log(jsonObj.length)

			if (jsonObj.length == undefined)
				jsonObj = [ jsonObj ]
				//console.log(jsonObj.length)

				// EXTRACT VALUE FOR HTML HEADER.
			var col = [];
			for (var i = 0; i < jsonObj.length; i++) {
				for ( var key in jsonObj[i]) {
					//console.log('key json' +key)
					if (col.indexOf(key) === -1) {
						col.push(key);
					}
				}
			}

			// CREATE DYNAMIC TABLE.
			var table = document.createElement("table");
			var addDefaultCss = "json-to-html-table-column";
			if (cssClassToTable) {
				addDefaultCss = cssClassToTable;
			}
			try {
				table.classList.add(addDefaultCss);
			} catch (e) {
				console.log('invalid css add', e);
			}

			// ADD JSON DATA TO THE TABLE AS ROWS.
			for (var i = 0; i < col.length; i++) {

				tr = table.insertRow(-1);
				var firstCell = tr.insertCell(-1);
				//firstCell.style.cssText="font-weight: bold; text-align: center; vertical-align: middle;";
				firstCell.innerHTML = col[i];
				for (var j = 0; j < jsonObj.length; j++) {
					var tabCell = tr.insertCell(-1);
					var theValue = jsonObj[j][col[i]];
					tabCell.innerHTML = theValue;
				}
			}

			return table.outerHTML;

		} catch (e) {
			console.log('invalid json', e);
		}

	}-*/;
	
	public static String toHTMLCode(TemporalReferenceDV tempRef) {

		String htmlCode = "<span class='display-date'>";

		if (tempRef != null) {

			String dateToString = "";
			if (tempRef.getStart() != null) {
				dateToString += ConstantsGeoPortalDataEntryApp.DATE_TIME_FORMAT.format(tempRef.getStart());

			}
			dateToString += " / ";
			if (tempRef.getStart() != null) {
				dateToString += ConstantsGeoPortalDataEntryApp.DATE_TIME_FORMAT.format(tempRef.getEnd());

			}

			htmlCode += dateToString;
		}
		htmlCode += "</span>";

		return htmlCode;
	}

}
