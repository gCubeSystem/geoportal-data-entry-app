package org.gcube.portlets.user.geoportaldataentry.client.ui.projects;

import java.util.List;

import org.gcube.application.geoportalcommon.shared.config.GcubeUserRole;
import org.gcube.application.geoportalcommon.shared.geoportal.ConfigurationDV;
import org.gcube.application.geoportalcommon.shared.geoportal.config.ActionDefinitionDV;
import org.gcube.application.geoportalcommon.shared.geoportal.ucd.GEOPORTAL_DATA_HANDLER;
import org.gcube.application.geoportalcommon.shared.geoportal.ucd.HandlerDeclarationDV;
import org.gcube.application.geoportalcommon.shared.geoportal.ucd.UseCaseDescriptorDV;
import org.gcube.portlets.user.geoportaldataentry.client.ui.action.ActionListPanel;
import org.gcube.portlets.user.geoportaldataentry.client.ui.utils.UCD_Util;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * The Class ListOfProjectWorkflowActionsPanel.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Sep 21, 2022
 */
public class ListOfProjectWorkflowActionsPanel extends Composite {

	@UiField
	FlowPanel actionListMainPanel;

	private ActionListPanel actionListPanel;

	private HandlerManager appManagerBus;

	private static ListOfProjectWorkflowActionsPanelUiBinder uiBinder = GWT
			.create(ListOfProjectWorkflowActionsPanelUiBinder.class);

	/**
	 * The Interface ListOfProjectWorkflowActionsPanelUiBinder.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
	 * 
	 *         Sep 21, 2022
	 */
	interface ListOfProjectWorkflowActionsPanelUiBinder extends UiBinder<Widget, ListOfProjectWorkflowActionsPanel> {
	}

	/**
	 * Instantiates a new list of project workflow actions panel.
	 *
	 * @param appManagerBus the app manager bus
	 */
	public ListOfProjectWorkflowActionsPanel(HandlerManager appManagerBus) {
		initWidget(uiBinder.createAndBindUi(this));
		this.appManagerBus = appManagerBus;
	}

	/**
	 * Inits the action list panel.
	 *
	 * @param ucd the ucd
	 */
	public void initActionListPanel(UseCaseDescriptorDV ucd) {
		GWT.log("Init ActionListPanel for UCD: " + ucd);

		// Setting Project type having HANDLER WORKFLOW ACTION
		HandlerDeclarationDV dataListHandler = UCD_Util.getHandlerDeclarationFor(ucd,
				GEOPORTAL_DATA_HANDLER.geoportal_workflow_action_list);

		if (dataListHandler != null) {
			actionListMainPanel.setVisible(true);
			actionListMainPanel.clear();
			ConfigurationDV<?> config = dataListHandler.getConfiguration();
			List<ActionDefinitionDV> listActionDef = toListActionDefinition(config);
			actionListPanel = new ActionListPanel(appManagerBus, ucd.getName(), ucd.getProfileID(), listActionDef);
			actionListMainPanel.add(actionListPanel);

		} else {
			actionListMainPanel.clear();
			actionListMainPanel.setVisible(false);
		}
	}

	/**
	 * To list action definition.
	 *
	 * @param config the config
	 * @return the list
	 */
	private List<ActionDefinitionDV> toListActionDefinition(ConfigurationDV<?> config) {

		try {
			return (List<ActionDefinitionDV>) config.getConfiguration();
		} catch (Exception e) {
			GWT.log("Error on casting " + ConfigurationDV.class.getName() + " to List of "
					+ ActionDefinitionDV.class.getName());
		}

		return null;
	}

	/**
	 * Show actions on selected.
	 *
	 * @param <T>         the generic type
	 * @param selectItems the select items
	 * @param userRole    the user role
	 */
	public <T> void showActionsOnSelected(List<T> selectItems, GcubeUserRole userRole) {
		actionListPanel.showActionsOnSelected(selectItems, userRole);

	}

}
