package org.gcube.portlets.user.geoportaldataentry.client.events;

import com.google.gwt.event.shared.EventHandler;


/**
 * The Interface CloseCreateRelationGUIEventHandler.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 * Sep 19, 2022
 */
public interface CloseCreateRelationGUIEventHandler extends EventHandler {

	/**
	 * On close.
	 *
	 * @param closeCreateRelationGUI the close create relation GUI
	 */
	void onClose(CloseCreateRelationGUIEvent closeCreateRelationGUI);

}