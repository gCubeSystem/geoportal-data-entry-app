package org.gcube.portlets.user.geoportaldataentry.server.test;

import javax.servlet.http.HttpServletRequest;

import org.gcube.portlets.user.geoportaldataentry.server.MonitoringActionsOnServer;
import org.gcube.portlets.user.geoportaldataentry.shared.CommitReport;
import org.gcube.portlets.user.geoportaldataentry.shared.monitoring.MonitoringAction;
import org.gcube.portlets.user.geoportaldataentry.shared.monitoring.MonitoringAction.STATUS;

/**
 * The Class MockData.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 * Oct 14, 2024
 */
public class MockData {

	/**
	 * Mock monitor.
	 *
	 * @param uuidAsString the uuid as string
	 * @param request the request
	 * @throws InterruptedException the interrupted exception
	 */
	public static final void mockMonitor(String uuidAsString, HttpServletRequest request) throws InterruptedException {

		final MonitoringActionsOnServer monitoringActionsOnServer = new MonitoringActionsOnServer(request,
				uuidAsString);

		// 1. Action to Monitoring
		MonitoringAction action = new MonitoringAction(STATUS.IN_PROGESS,
				"Converting prject metadata to Geoportal data model");
		action = monitoringActionsOnServer.pushAction(action);

		// 1. Action completed
		action.setMsg("Converted prject metadata to Geoportal data model");
		action.setStatus(STATUS.DONE);
		monitoringActionsOnServer.overrideAction(action);

		// 2. Action to Monitoring
		MonitoringAction action2 = new MonitoringAction(STATUS.IN_PROGESS,
				"Creating project on Geoportal Storage service");
		action2 = monitoringActionsOnServer.pushAction(action2);

		Thread.sleep(5000);

		// 2. Action completed
		action2.setMsg("Created project on Geoportal Storage service");
		action2.setStatus(STATUS.DONE);
		monitoringActionsOnServer.overrideAction(action2);

		// 3. Action to Monitoring
		MonitoringAction action3 = new MonitoringAction(STATUS.IN_PENDING, "Going to register the file/s...");
		action3 = monitoringActionsOnServer.pushAction(action3);
		
		for (int i = 0; i < 5; i++) {

			String fileName = "File " + i;
			// 1..N Monitoring Uploading Fileset
			MonitoringAction actionI = new MonitoringAction(STATUS.IN_PROGESS, "Registering the fileset: " + fileName);
			actionI = monitoringActionsOnServer.pushAction(actionI);

			Thread.sleep(5000);
			// 1..N Monitoring Fileset uploaded action completed
			actionI.setMsg("Fileset " + fileName + " registered correclty");
			actionI.setStatus(STATUS.DONE);
			monitoringActionsOnServer.overrideAction(actionI);

		}
		
		monitoringActionsOnServer.setMonitoringTerminated(true);
		Thread.sleep(5000);
		
		CommitReport commitReport = new CommitReport("Mock projectId", "Mock profileId", null, null);
		monitoringActionsOnServer.setCommitReport(commitReport);
	}

}
