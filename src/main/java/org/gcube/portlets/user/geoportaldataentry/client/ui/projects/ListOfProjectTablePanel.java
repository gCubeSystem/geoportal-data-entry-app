package org.gcube.portlets.user.geoportaldataentry.client.ui.projects;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.gcube.application.geoportalcommon.shared.SearchingFilter;
import org.gcube.application.geoportalcommon.shared.SearchingFilter.LOGICAL_OP;
import org.gcube.application.geoportalcommon.shared.SearchingFilter.ORDER;
import org.gcube.application.geoportalcommon.shared.WhereClause;
import org.gcube.application.geoportalcommon.shared.geoportal.config.ItemFieldDV;
import org.gcube.application.geoportalcommon.shared.geoportal.ucd.UseCaseDescriptorDV;
import org.gcube.portlets.user.geoportaldataentry.client.GeoPortalClientCaches.CacheSearchingFilterParametersFromConfig;
import org.gcube.portlets.user.geoportaldataentry.client.events.GetListOfRecordsEvent;
import org.gcube.portlets.user.geoportaldataentry.client.ui.GeonaRecordsPaginatedView;

import com.github.gwtbootstrap.client.ui.Alert;
import com.github.gwtbootstrap.client.ui.Button;
import com.github.gwtbootstrap.client.ui.Dropdown;
import com.github.gwtbootstrap.client.ui.Hero;
import com.github.gwtbootstrap.client.ui.NavLink;
import com.github.gwtbootstrap.client.ui.TextBox;
import com.github.gwtbootstrap.client.ui.constants.AlertType;
import com.github.gwtbootstrap.client.ui.constants.ButtonType;
import com.github.gwtbootstrap.client.ui.constants.IconSize;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * The Class ListOfProjectTablePanel.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Sep 21, 2022
 */
public class ListOfProjectTablePanel extends Composite {

	@UiField
	Dropdown dropdownSortBy;

	@UiField
	Dropdown dropdownSearchFor;

	@UiField
	Alert alertSortBy;

	@UiField
	Alert alertProjectType;

	@UiField
	Alert alertSearchFor;

	@UiField
	NavLink buttonReloadProjects;

	@UiField
	HTMLPanel geonaListOfRecordsPanel;

	@UiField
	HTMLPanel contTabGetListOfProjects;

	@UiField
	TextBox searchField;

	@UiField
	Button resetSearch;

	@UiField
	Dropdown ddProjectType;

	@UiField
	Hero noProjectSelectionMessage;

	private CacheSearchingFilterParametersFromConfig cacheSearchingFilterParameters;

	private SearchingFilter currentSearchingFilter;

	private List<UseCaseDescriptorDV> ucdProjectTypesForListingDataView;

	protected static final int MIN_LENGHT_SERCHING_STRING = 3;

	private static final String LABEL_FILTER_SEPARATOR = " - ";

	private HandlerManager appManagerBus;

	private GeonaRecordsPaginatedView grpw;

	private static ListOfProjectTablePanelUiBinder uiBinder = GWT.create(ListOfProjectTablePanelUiBinder.class);

	/**
	 * The Interface ListOfProjectTablePanelUiBinder.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
	 * 
	 *         Sep 21, 2022
	 */
	interface ListOfProjectTablePanelUiBinder extends UiBinder<Widget, ListOfProjectTablePanel> {
	}

	/**
	 * Instantiates a new list of project table panel.
	 *
	 * @param appManagerBus the app manager bus
	 */
	public ListOfProjectTablePanel(HandlerManager appManagerBus) {
		initWidget(uiBinder.createAndBindUi(this));
		this.appManagerBus = appManagerBus;

		alertSortBy.setType(AlertType.INFO);
		alertSortBy.setClose(false);

		alertSearchFor.setType(AlertType.INFO);
		alertSearchFor.setClose(false);

		alertProjectType.setType(AlertType.INFO);
		alertProjectType.setClose(false);

		resetSearch.setIconSize(IconSize.TWO_TIMES);
		resetSearch.setType(ButtonType.LINK);

		bindEvents();
	}

	/**
	 * Bind events.
	 */
	private void bindEvents() {

		buttonReloadProjects.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {

				UseCaseDescriptorDV singleUCD = getSelectProjectType();
				if (singleUCD != null) {
					appManagerBus.fireEvent(new GetListOfRecordsEvent(false, singleUCD.getProfileID(),
							getCurrentSearchingFilter(), true));
				}

			}
		});

		searchField.addKeyPressHandler(new KeyPressHandler() {

			@Override
			public void onKeyPress(KeyPressEvent event) {
				GWT.log("CHAR CODE: " + event.getCharCode());
				if (com.google.gwt.event.dom.client.KeyCodes.KEY_ENTER == event.getCharCode()) {
					GWT.log(searchField.getText());
					doSearchEvent();
				}

			}
		});

		resetSearch.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				searchField.setText("");

				UseCaseDescriptorDV singleUCD = getSelectProjectType();
				if (singleUCD != null) {
					appManagerBus.fireEvent(new GetListOfRecordsEvent(false, singleUCD.getProfileID(),
							getCurrentSearchingFilter(), false));
				}

				resetSearch.setVisible(false);

			}
		});
	}

	/**
	 * Sets the filtering parameters.
	 *
	 * @param sfp the sfp
	 */
	public void setFilteringParameters(CacheSearchingFilterParametersFromConfig sfp) {
		GWT.log("ListOfProjectTablePanel setFilteringParameters called");
		this.cacheSearchingFilterParameters = sfp;
		List<ItemFieldDV> sortByFields = sfp.getOrderByFields();
		List<ItemFieldDV> searchForFields = sfp.getSearchByFields();
		// currentSearchingFilter = new SearchingFilter(sortByFields, null);

		alertSearchFor.clear();
		dropdownSearchFor.clear();

		alertSearchFor.setText(searchForFields.get(0).getDisplayName());
		alertSortBy.setText(toLabelFilter((sortByFields.get(0)), SearchingFilter.ORDER.ASC));

		for (final ItemFieldDV record_FIELD : searchForFields) {

			final NavLink navSearch = new NavLink(record_FIELD.getDisplayName());
			dropdownSearchFor.add(navSearch);

			navSearch.addClickHandler(new ClickHandler() {

				@Override
				public void onClick(ClickEvent event) {
					GWT.log("Search for: " + record_FIELD.getDisplayName());
					alertSearchFor.setText(record_FIELD.getDisplayName());

					if (searchField.getText().length() >= MIN_LENGHT_SERCHING_STRING) {
						doSearchEvent();
					}

				}
			});
		}

		dropdownSortBy.clear();

		for (ItemFieldDV record_FIELD : sortByFields) {

//			if (record_FIELD.equals(RECORD_FIELD.RECORD_STATUS))
//				continue;

			// ASC
			final String labelASC = toLabelFilter(record_FIELD, ORDER.ASC);
			final NavLink nav = new NavLink(labelASC);
			dropdownSortBy.add(nav);

			nav.addClickHandler(new ClickHandler() {

				@Override
				public void onClick(ClickEvent event) {
					GWT.log("Sort by: " + labelASC);
					alertSortBy.setText(labelASC);
					UseCaseDescriptorDV singleUCD = getSelectProjectType();
					if (singleUCD != null) {
						appManagerBus.fireEvent(new GetListOfRecordsEvent(false, singleUCD.getProfileID(),
								getCurrentSearchingFilter(), false));
					}
				}
			});

//			//DESC
			final String labelDESC = toLabelFilter(record_FIELD, ORDER.DESC);
			final NavLink nav2 = new NavLink(labelDESC);
			dropdownSortBy.add(nav2);

			nav2.addClickHandler(new ClickHandler() {

				@Override
				public void onClick(ClickEvent event) {
					GWT.log("Sort by: " + labelDESC);
					alertSortBy.setText(labelDESC);
					UseCaseDescriptorDV singleUCD = getSelectProjectType();
					if (singleUCD != null) {
						appManagerBus.fireEvent(new GetListOfRecordsEvent(false, singleUCD.getProfileID(),
								getCurrentSearchingFilter(), false));
					}
				}
			});
		}
	}

	/**
	 * To sort filter.
	 *
	 * @param labelFilter the label filter
	 * @return the searching filter
	 */
	private SearchingFilter toSortFilter(String labelFilter) {
		GWT.log("ListOfProjectTablePanel toSortFilter for label " + labelFilter);
		String[] array = labelFilter.split(LABEL_FILTER_SEPARATOR);

		GWT.log("label filter: " + array[0]);
		SearchingFilter sortFilter = null;
		try {
			ItemFieldDV recordField = null;
			GWT.log("cacheSearchingFilterParameters.getOrderByFields() is: "
					+ cacheSearchingFilterParameters.getOrderByFields());
			for (ItemFieldDV value : cacheSearchingFilterParameters.getOrderByFields()) {
				if (array[0].trim().compareTo(value.getDisplayName()) == 0) {
					recordField = value;
					break;
				}
			}
			ORDER orderField = ORDER.valueOf(array[1].trim());
			sortFilter = new SearchingFilter(Arrays.asList(recordField), orderField);
		} catch (Exception e) {

		}
		GWT.log("toSortFilter got " + sortFilter);
		return sortFilter;

	}

	/**
	 * Dirty solution. I created this one because I had problem on firing click
	 * event for Tab element
	 *
	 * @param ucdProjectTypesForListingDataView the ucd project types for listing
	 *                                          data view
	 */
	public void instanceAndShowListOfProjects(List<UseCaseDescriptorDV> ucdProjectTypesForListingDataView) {
		GWT.log("ListOfProjectTablePanel instanceAndShowListOfProjects called");
		this.ucdProjectTypesForListingDataView = ucdProjectTypesForListingDataView;

		Scheduler.get().scheduleDeferred(new ScheduledCommand() {

			@Override
			public void execute() {

				// noProjectSelectionMessage.setVisible(true);
				// Selecting directly the unique UCD type.
				if (ucdProjectTypesForListingDataView.size() == 1) {
					// noProjectSelectionMessage.setVisible(false);
					UseCaseDescriptorDV singleUCD = ucdProjectTypesForListingDataView.get(0);
					GetListOfRecordsEvent event = new GetListOfRecordsEvent(true, singleUCD.getProfileID(),
							getCurrentSearchingFilter(), true);
					setSearchTypeAndFire(singleUCD.getName(), event);
				}

			}
		});

	}
	
	public void setSearchTypeAndFire(String ucdName, GetListOfRecordsEvent event) {
		alertProjectType.setText(ucdName);
		
//		if(event.getSearchingFilter().getProfileID()!=null && event.getSearchingFilter().getProjectID()) {
//			
//		}
//		
		appManagerBus.fireEvent(event);
	}

	/**
	 * Built searching filter.
	 *
	 * @return the searching filter
	 */
	public SearchingFilter builtSearchingFilter() {
		SearchingFilter searchingFilter = toSortFilter(alertSortBy.getText());
		String searchText = searchField.getText();
		if (searchText != null && !searchText.isEmpty()) {
			Map<String, Object> searchInto = new HashMap<String, Object>();

			List<String> listOfSeachingFields = new ArrayList<String>();

			for (ItemFieldDV recordField : cacheSearchingFilterParameters.getSearchByFields()) {
				if (recordField.getDisplayName().equals(alertSearchFor.getText())) {
					listOfSeachingFields = recordField.getJsonFields();
					continue;
				}
			}

			for (String fieldname : listOfSeachingFields) {
				searchInto.put(fieldname, searchText);
			}

			WhereClause where = new WhereClause();
			where.setSearchInto(searchInto);
			where.setOperator(LOGICAL_OP.OR);

			searchingFilter.setConditions(Arrays.asList(where));
		}
		return searchingFilter;
	}

	/**
	 * Sets the internal height.
	 *
	 * @param height the new internal height
	 */
	public void setInternalHeight(int height) {
		contTabGetListOfProjects.asWidget().setHeight(height + "px");
	}

	/**
	 * Gets the current sort filter.
	 *
	 * @return the current sort filter
	 */
	public SearchingFilter getCurrentSearchingFilter() {
		currentSearchingFilter = builtSearchingFilter();
		GWT.log("ListOfProjectTablePanel getCurrentSearchingFilter: " + currentSearchingFilter);
		return currentSearchingFilter;
	}

	/**
	 * Gets the select project type.
	 *
	 * @return the select project type
	 */
	public UseCaseDescriptorDV getSelectProjectType() {
		String projectName = alertProjectType.getText();
		for (UseCaseDescriptorDV usdDV : ucdProjectTypesForListingDataView) {
			if (usdDV.getName().compareTo(projectName) == 0) {
				GWT.log("Selected project type is: " + usdDV);
				return usdDV;
			}
		}

		return null;
	}

	/**
	 * Do search event.
	 */
	private void doSearchEvent() {
		String searchText = searchField.getText();
		if (searchText.length() < MIN_LENGHT_SERCHING_STRING) {
			Window.alert("Please enter at least " + MIN_LENGHT_SERCHING_STRING + " characters");
			return;
		}

		resetSearch.setVisible(true);

		UseCaseDescriptorDV singleUCD = getSelectProjectType();
		if (singleUCD != null) {
			appManagerBus.fireEvent(
					new GetListOfRecordsEvent(false, singleUCD.getProfileID(), getCurrentSearchingFilter(), false));
		}
	}

	public void showListOfProjectsView(GeonaRecordsPaginatedView grpw) {
		GWT.log("showListOfProjectsView for: " + grpw);
		this.grpw = grpw;
		geonaListOfRecordsPanel.clear();
		VerticalPanel verticalPanel = new VerticalPanel();
		verticalPanel.add(this.grpw.getCellPanel());
		verticalPanel.add(this.grpw.getPagerPanel());
		geonaListOfRecordsPanel.add(verticalPanel);
	}

	/**
	 * To label filter.
	 *
	 * @param itemField the item field
	 * @param direction the direction
	 * @return the string
	 */
	private String toLabelFilter(ItemFieldDV itemField, ORDER direction) {
		String labelFilter = itemField.getDisplayName() + LABEL_FILTER_SEPARATOR + direction.name();
		return labelFilter;
	}

	/**
	 * Project type reset.
	 */
	public void projectTypeReset() {
		ddProjectType.clear();

	}

	public void addProjectType(final UseCaseDescriptorDV ucd) {

		NavLink link = new NavLink(ucd.getName());
		link.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				alertProjectType.setText(ucd.getName());
				appManagerBus
						.fireEvent(new GetListOfRecordsEvent(false, ucd.getProfileID(), builtSearchingFilter(), true));
			}
		});

		ddProjectType.add(link);

	}
	
	
	public String getSelectedProjectType() {
		return alertProjectType.getText();
	}

}
