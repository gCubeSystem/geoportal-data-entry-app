package org.gcube.portlets.user.geoportaldataentry.client.events;

import java.util.List;

import org.gcube.application.geoportalcommon.shared.config.OPERATION_ON_ITEM;
import org.gcube.application.geoportalcommon.shared.geoportal.DocumentDV;

import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.user.client.Element;

/**
 * The Class OperationOnItemEvent.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Sep 15, 2021
 * @param <T> the generic type
 */
public class OperationOnItemEvent<T extends DocumentDV> extends GwtEvent<OperationOnItemEventHandler> {
	public static Type<OperationOnItemEventHandler> TYPE = new Type<OperationOnItemEventHandler>();
	private List<T> selectItems;
	private OPERATION_ON_ITEM action;
	private Element sourceElement;
	private String ucdName;

	/**
	 * Instantiates a new action on item event.
	 *
	 * @param selectItems the select items
	 * @param doAction    the do action
	 * @param profileID   the profile ID
	 */
	public OperationOnItemEvent(List<T> selectItems, OPERATION_ON_ITEM doAction) {
		this.selectItems = selectItems;
		this.action = doAction;
	}

	/**
	 * Gets the associated type.
	 *
	 * @return the associated type
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.google.gwt.event.shared.GwtEvent#getAssociatedType()
	 */
	@Override
	public Type<OperationOnItemEventHandler> getAssociatedType() {
		return TYPE;
	}

	/**
	 * Dispatch.
	 *
	 * @param handler the handler
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.google.gwt.event.shared.GwtEvent#dispatch(com.google.gwt.event.shared.
	 * EventHandler)
	 */
	@Override
	protected void dispatch(OperationOnItemEventHandler handler) {
		handler.onDoActionFired(this);
	}
	
	public String getUcdName() {
		return ucdName;
	}

	/**
	 * Gets the select items.
	 *
	 * @return the select items
	 */
	public List<T> getSelectItems() {
		return selectItems;
	}

	/**
	 * Gets the action.
	 *
	 * @return the action
	 */
	public OPERATION_ON_ITEM getAction() {
		return action;
	}

	public void setSourceElement(Element element) {
		this.sourceElement = element;

	}

	public Element getSourceElement() {
		return sourceElement;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("OperationOnItemEvent [selectItems=");
		builder.append(selectItems);
		builder.append(", action=");
		builder.append(action);
		builder.append("]");
		return builder.toString();
	}
}
