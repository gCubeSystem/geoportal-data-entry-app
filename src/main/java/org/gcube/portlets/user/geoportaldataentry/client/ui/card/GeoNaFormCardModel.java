package org.gcube.portlets.user.geoportaldataentry.client.ui.card;

import org.gcube.application.geoportalcommon.shared.geoportal.config.GcubeProfileDV;
import org.gcube.portlets.user.geoportaldataentry.client.ProjectFormCard;
import org.gcube.portlets.widgets.mpformbuilder.client.form.generic.CreateMetadataForm;
import org.gcube.portlets.widgets.mpformbuilder.shared.metadata.MetaDataProfileBean;

public class GeoNaFormCardModel<T extends MetaDataProfileBean> {

	private T metadataProfileBean;
	private CreateMetadataForm metadataForm;
	private ProjectFormCard formCard; // matching with metadata profile
	private GcubeProfileDV gcubeProfile;

	public GeoNaFormCardModel() {

	}

	public GeoNaFormCardModel(T metadataProfileBean, CreateMetadataForm metadataForm, ProjectFormCard formCard,
			GcubeProfileDV gcubeProfile) {
		super();
		this.metadataProfileBean = metadataProfileBean;
		this.metadataForm = metadataForm;
		this.formCard = formCard;
		this.gcubeProfile = gcubeProfile;
	}

	public T getMetadataProfileBean() {
		return metadataProfileBean;
	}

	public void setMetadataProfileBean(T metadataProfileBean) {
		this.metadataProfileBean = metadataProfileBean;
	}

	public CreateMetadataForm getMetadataForm() {
		return metadataForm;
	}

	public void setMetadataForm(CreateMetadataForm metadataForm) {
		this.metadataForm = metadataForm;
	}

	public ProjectFormCard getFormCard() {
		return formCard;
	}

	public void setFormCard(ProjectFormCard formCard) {
		this.formCard = formCard;
	}

	public GcubeProfileDV getGcubeProfile() {
		return gcubeProfile;
	}

	public void setGcubeProfile(GcubeProfileDV gcubeProfile) {
		this.gcubeProfile = gcubeProfile;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("GeoNaFormCardModel [metadataProfileBean=");
		builder.append(metadataProfileBean);
		builder.append(", metadataForm=");
		builder.append(metadataForm);
		builder.append(", formCard=");
		builder.append(formCard);
		builder.append(", gcubeProfile=");
		builder.append(gcubeProfile);
		builder.append("]");
		return builder.toString();
	}

}
