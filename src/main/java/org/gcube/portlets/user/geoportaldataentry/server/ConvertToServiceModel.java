package org.gcube.portlets.user.geoportaldataentry.server;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.gcube.application.geoportal.common.model.legacy.AbstractRelazione;
import org.gcube.application.geoportal.common.model.legacy.AccessPolicy;
import org.gcube.application.geoportal.common.model.legacy.Concessione;
import org.gcube.application.geoportal.common.model.legacy.LayerConcessione;
import org.gcube.application.geoportal.common.model.legacy.RelazioneScavo;
import org.gcube.application.geoportal.common.model.legacy.UploadedImage;
import org.gcube.portlets.user.geoportaldataentry.client.ConstantsGeoPortalDataEntryApp;
import org.gcube.portlets.widgets.mpformbuilder.shared.GenericDatasetBean;
import org.gcube.vomanagement.usermanagement.model.GCubeUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class ConvertToServiceModel.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 *         Oct 21, 2020
 */
public class ConvertToServiceModel {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(ConvertToServiceModel.class);

	public static final String HOURS_MINUTES_SEPARATOR = ConstantsGeoPortalDataEntryApp.HOURS_MINUTES_SEPARATOR;

	public static final String DATE_FORMAT = ConstantsGeoPortalDataEntryApp.DATE_FORMAT;

	public static final String TIME_FORMAT = ConstantsGeoPortalDataEntryApp.TIME_FORMAT;

	/**
	 * To concessione.
	 *
	 * @param gdb  the gdb
	 * @param user the user
	 * @return the concessione
	 * @throws Exception the exception
	 */
	public static Concessione toConcessione(GenericDatasetBean gdb, GCubeUser user) throws Exception {

		Map<String, List<String>> mapFields = gdb.getFormDataEntryFields();

		Concessione concessione = new Concessione();

//		List<String> authors = mapFields.get("Autore");
//		if(authors!=null) {
//			concessione.setAuthors(authors);
//		}

		List<String> authors = mapFields.get("Nome Autore, Email, Ruolo");
		if (authors != null) {
			concessione.setAuthors(authors);
		}

		List<String> contributors = mapFields.get("Contributore");
		if (contributors != null) {
			for (String contributor : contributors) {
				concessione.setContributore(contributor);
			}
		}

		// concessione.setCreationTime(Instant.now());
		concessione.setCreationUser(user.getUsername());

		List<String> dataInizProgettoList = mapFields.get("Data inizio Progetto");
		if (dataInizProgettoList != null && dataInizProgettoList.size() > 0) {
			String inizioProgetto = dataInizProgettoList.get(0);
			LocalDateTime theLDT = toLocalDateTime(inizioProgetto);
			concessione.setDataInizioProgetto(theLDT);
		}

		List<String> dataFineProgettoList = mapFields.get("Data fine Progetto");
		if (dataFineProgettoList != null && dataFineProgettoList.size() > 0) {
			String fineProgetto = dataFineProgettoList.get(0);
			LocalDateTime theLDT = toLocalDateTime(fineProgetto);
			concessione.setDataFineProgetto(theLDT);
		}

		List<String> descrizioneLst = mapFields.get("Descrizione del contenuto");
		if (descrizioneLst != null && descrizioneLst.size() > 0) {
			concessione.setDescrizioneContenuto(descrizioneLst.get(0));
		}

		List<String> editors = mapFields.get("Editore");
		if (editors != null && editors.size() > 0) {
			concessione.setEditore(editors.get(0));
		}

		List<String> fontiFinanziamento = mapFields.get("Fonte del finanziamento");
		if (fontiFinanziamento != null) {
			concessione.setFontiFinanziamento(fontiFinanziamento);
		}

		List<String> licenzaLst = mapFields.get("ID Licenza");
		if (licenzaLst != null && licenzaLst.size() > 0) {
			concessione.setLicenzaID(licenzaLst.get(0));
		}

		List<String> introduzioneLst = mapFields.get("Introduzione");
		if (introduzioneLst != null && introduzioneLst.size() > 0) {
			concessione.setIntroduzione(introduzioneLst.get(0));
		}

		List<String> nomeLst = mapFields.get("Nome del progetto");
		if (nomeLst != null && nomeLst.size() > 0) {
			concessione.setNome(nomeLst.get(0));
		}

		List<String> paroleChiaveLibereLst = mapFields.get("Parola chiave a scelta libera");
		// LOG.debug("Parola chiave a scelta libera: "+paroleChiaveLibereLst);
		if (paroleChiaveLibereLst != null) {
			concessione.setParoleChiaveLibere(paroleChiaveLibereLst);
		}

		List<String> paroleChiaveICCDLst = mapFields.get("Parola chiave relativa alla cronologia");
		// LOG.debug("Parola chiave relativa alla cronologia: "+paroleChiaveICCDLst);
		if (paroleChiaveICCDLst != null) {
			concessione.setParoleChiaveICCD(paroleChiaveICCDLst);
		}

		List<String> risorsaCorrelataLst = mapFields.get("Risorsa correlata");
		if (risorsaCorrelataLst != null) {
			concessione.setRisorseCorrelate(risorsaCorrelataLst);
		}

		List<String> responsabileLst = mapFields.get("Responsabile");
		if (responsabileLst != null && responsabileLst.size() > 0) {
			concessione.setResponsabile(responsabileLst.get(0));
		}

		List<String> soggettoLst = mapFields.get("Soggetto");
		if (soggettoLst != null) {
			concessione.setSoggetto(soggettoLst);
		}

		List<String> titolareCopyrightLst = mapFields.get("Titolare Copyright");
		if (titolareCopyrightLst != null) {
			concessione.setTitolareCopyright(titolareCopyrightLst);
		}

		List<String> titolareLicenzaLst = mapFields.get("Titolare Licenza");
		if (titolareLicenzaLst != null) {
			concessione.setTitolareLicenza(titolareLicenzaLst);
		}

		List<String> titolareDatiLst = mapFields.get("Titolare dei dati");
		if (titolareDatiLst != null) {
			concessione.setTitolari(titolareDatiLst);
		}

		List<String> latList = mapFields.get("Latitudine Centroide");
		if (latList != null && latList.size() > 0) {
			try {
				String theLat = latList.get(0);
				if (theLat != null && !theLat.isEmpty()) {
					Double centroidLat = Double.parseDouble(theLat);
					concessione.setCentroidLat(centroidLat);
				}
			} catch (Exception e) {
				throw new Exception("Unable to parse " + latList.get(0) + " as valid latitude");
			}
		}

		List<String> longList = mapFields.get("Longitudine Centroide");
		if (longList != null && longList.size() > 0) {
			try {
				String theLong = longList.get(0);
				if (theLong != null && !theLong.isEmpty()) {
					Double centroidLong = Double.parseDouble(theLong);
					concessione.setCentroidLong(centroidLong);
				}
			} catch (Exception e) {
				throw new Exception("Unable to parse " + longList.get(0) + " as valid longitude");
			}
		}

		return concessione;

	}

	/**
	 * To relazione scavo.
	 *
	 * @param gdb the gdb
	 * @return the relazione scavo
	 */
	public static RelazioneScavo toRelazioneScavo(GenericDatasetBean gdb) {

		Map<String, List<String>> mapFields = gdb.getFormDataEntryFields();

		RelazioneScavo relazioneScavo = new RelazioneScavo();

		List<String> responsabiliLst = mapFields.get("Responsabile del documento");
		if (responsabiliLst != null) {
			relazioneScavo.setResponsabili(responsabiliLst);
		}

		List<String> autoreList = mapFields.get("Autore del documento");
		if (autoreList != null) {
			// TODO
		}

		List<String> politicaDiAccessoLst = mapFields.get("Politica di accesso");
		LOG.debug("Relazione di Scavo: Politica di accesso lst: "+politicaDiAccessoLst);
		if (politicaDiAccessoLst != null && politicaDiAccessoLst.size() > 0) {
			try {
				AccessPolicy ap = AccessPolicy.valueOf(politicaDiAccessoLst.get(0));
				LOG.debug("Relazione di Scavo: AccessPolicy: "+ap);
				relazioneScavo.setPolicy(ap);
			} catch (Exception e) {
				LOG.warn("I cannot cast " + politicaDiAccessoLst.get(0) + " to " + AccessPolicy.values(), e);
			}
		}

		List<String> licenzaIdList = mapFields.get("ID Licenza");
		if (licenzaIdList != null && licenzaIdList.size() > 0) {
			LOG.debug("Relazione di Scavo: ID Licenza lst: "+licenzaIdList);
			relazioneScavo.setLicenseID(licenzaIdList.get(0));
			LOG.debug("Relazione di Scavo: ID Licenza: "+relazioneScavo.getLicenseID());
		}

		// TODO
		List<String> periodoDiEmbargo = mapFields.get("Periodo di embargo");
		if (periodoDiEmbargo != null && periodoDiEmbargo.size() > 0) {
			String dateFromTo = periodoDiEmbargo.get(0);
			String[] dates = dateFromTo.split(",");
			String dateFrom = dates[0];
			String dateTo = dates[0];
			// TODO MUST BE INTENGRATED IN THE SERVICE
		}

		return relazioneScavo;
	}

	/**
	 * To abstract relazione scavo.
	 *
	 * @param gdb the gdb
	 * @return the abstract relazione
	 */
	public static AbstractRelazione toAbstractRelazioneScavo(GenericDatasetBean gdb) {

		Map<String, List<String>> mapFields = gdb.getFormDataEntryFields();

		AbstractRelazione abstractrelazione = new AbstractRelazione();

		List<String> abstractLstIta = mapFields.get("Abstract in Italiano");
		if (abstractLstIta != null && abstractLstIta.size() > 0) {
			abstractrelazione.setAbstractIta(abstractLstIta.get(0));
		}

		List<String> abstractLstEng = mapFields.get("Abstract in Inglese");
		if (abstractLstEng != null && abstractLstEng.size() > 0) {
			abstractrelazione.setAbstractEng(abstractLstEng.get(0));
		}

		// THIS SHOULD BE ALWAYS "OPEN"
		List<String> politicaDiAccessoLst = mapFields.get("Politica di accesso");
		if (politicaDiAccessoLst != null && politicaDiAccessoLst.size() > 0) {
			try {
				AccessPolicy ap = AccessPolicy.valueOf(politicaDiAccessoLst.get(0));
				abstractrelazione.setPolicy(ap);
			} catch (Exception e) {
				LOG.warn("I cannot cast " + politicaDiAccessoLst.get(0) + " to " + AccessPolicy.values(), e);
			}
		}

		// THIS SHOULD BE ALWAYS "CC-BY-4.0"
		List<String> licenzaIdList = mapFields.get("ID Licenza");
		if (licenzaIdList != null && licenzaIdList.size() > 0) {
			abstractrelazione.setLicenseID(licenzaIdList.get(0));
		}

		return abstractrelazione;
	}

	/**
	 * To immagini rappresentative.
	 *
	 * @param gdb the gdb
	 * @return the uploaded image
	 */
	public static UploadedImage toImmaginiRappresentative(GenericDatasetBean gdb) {

		Map<String, List<String>> mapFields = gdb.getFormDataEntryFields();

		UploadedImage uplaodedImage = new UploadedImage();

//		List<String> titoloLst = mapFields.get("Titolo");
//		if(titoloLst!=null && titoloLst.size()>0) {
//			uplaodedImage.setTitolo(titoloLst.get(0));
//		}
//		
		List<String> didascaliaLst = mapFields.get("Didascalia");
		if (didascaliaLst != null && didascaliaLst.size() > 0) {
			uplaodedImage.setDidascalia(didascaliaLst.get(0));
		}

		// NB here is setResponsabili but should be setAuthor
		List<String> responsabileLst = mapFields.get("Autore");
		if (responsabileLst != null) {
			uplaodedImage.setResponsabili(responsabileLst);
			// uplaodedImage.setAuthor is missing!!!
		}

//		List<String> autoreList = mapFields.get("Autore");
//		if(autoreList!=null) {
//			//TODO
//		}

		List<String> politicaDiAccessoLst = mapFields.get("Politica di accesso");
		if (politicaDiAccessoLst != null && politicaDiAccessoLst.size() > 0) {
			try {
				AccessPolicy ap = AccessPolicy.valueOf(politicaDiAccessoLst.get(0));
				uplaodedImage.setPolicy(ap);
			} catch (Exception e) {
				LOG.warn("I cannot cast " + politicaDiAccessoLst.get(0) + " to " + AccessPolicy.values(), e);
			}
		}

		List<String> licenzaIdList = mapFields.get("ID Licenza");
		if (licenzaIdList != null && licenzaIdList.size() > 0) {
			uplaodedImage.setLicenseID(licenzaIdList.get(0));
		}

		return uplaodedImage;
	}

	/**
	 * To layer concessione.
	 *
	 * @param gdb the gdb
	 * @return the layer concessione
	 */
	public static LayerConcessione toLayerConcessione(GenericDatasetBean gdb) {

		Map<String, List<String>> mapFields = gdb.getFormDataEntryFields();

		LayerConcessione layerConcessione = new LayerConcessione();

		List<String> titoloLst = mapFields.get("Titolo");
		if (titoloLst != null && titoloLst.size() > 0) {
			layerConcessione.setTitolo(titoloLst.get(0));
		}

		List<String> politicaDiAccessoLst = mapFields.get("Politica di accesso");
		if (politicaDiAccessoLst != null && politicaDiAccessoLst.size() > 0) {
			try {
				AccessPolicy ap = AccessPolicy.valueOf(politicaDiAccessoLst.get(0));
				layerConcessione.setPolicy(ap);
			} catch (Exception e) {
				LOG.warn("I cannot cast " + politicaDiAccessoLst.get(0) + " to " + AccessPolicy.values(), e);
			}
		}

		List<String> valutazioneQualitaLst = mapFields.get("Valutazione della qualità");
		if (valutazioneQualitaLst != null && valutazioneQualitaLst.size() > 0) {
			layerConcessione.setValutazioneQualita(valutazioneQualitaLst.get(0));
		}

		List<String> metodoRaccoltaDatiLst = mapFields.get("Metodo di raccolta dei dati");
		if (metodoRaccoltaDatiLst != null && metodoRaccoltaDatiLst.size() > 0) {
			layerConcessione.setMetodoRaccoltaDati(metodoRaccoltaDatiLst.get(0));
		}

		List<String> scalaAcquisizioneLst = mapFields.get("Scala di acquisizione dei dati");
		if (scalaAcquisizioneLst != null && scalaAcquisizioneLst.size() > 0) {
			layerConcessione.setScalaAcquisizione(scalaAcquisizioneLst.get(0));
		}

		// TODO
		List<String> periodoDiEmbargo = mapFields.get("Periodo di embargo");
		if (periodoDiEmbargo != null && periodoDiEmbargo.size() > 0) {
			String dateFromTo = periodoDiEmbargo.get(0);
			String[] dates = dateFromTo.split(",");
			String dateFrom = dates[0];
			String dateTo = dates[0];
			// TODO MUST BE INTENGRATED IN THE SERVICE
		}

		List<String> licenzaIdList = mapFields.get("ID Licenza");
		if (licenzaIdList != null && licenzaIdList.size() > 0) {
			layerConcessione.setLicenseID(licenzaIdList.get(0));
		}

		List<String> autoreLst = mapFields.get("Autore");
		if (autoreLst != null) {
			layerConcessione.setAuthors(autoreLst);
		}

		return layerConcessione;
	}

	/**
	 * To local date time.
	 *
	 * @param date the date
	 * @return the local date time
	 */
	public static LocalDateTime toLocalDateTime(String date) {
		LocalDateTime theLocalDT = null;
		try {
			date = date.trim();
			SimpleDateFormat dateFormat = null;
			if (date.contains(HOURS_MINUTES_SEPARATOR)) {
				dateFormat = new SimpleDateFormat(DATE_FORMAT + " " + TIME_FORMAT);
			} else
				dateFormat = new SimpleDateFormat(DATE_FORMAT);

			Date theDate = dateFormat.parse(date);
			theLocalDT = convertToLocalDateTimeViaInstant(theDate);
		} catch (ParseException e) {
			LOG.error("No able to parse: " + date, e);
		}
		return theLocalDT;
	}

	/**
	 * Convert to local date time via instant.
	 *
	 * @param dateToConvert the date to convert
	 * @return the local date time
	 */
	public static LocalDateTime convertToLocalDateTimeViaInstant(Date dateToConvert) {
		return dateToConvert.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
	}

}
