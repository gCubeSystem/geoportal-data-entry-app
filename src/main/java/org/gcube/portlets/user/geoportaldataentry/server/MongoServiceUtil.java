package org.gcube.portlets.user.geoportaldataentry.server;

import static org.gcube.application.geoportal.client.plugins.GeoportalAbstractPlugin.projects;
import static org.gcube.application.geoportal.client.plugins.GeoportalAbstractPlugin.useCaseDescriptors;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import org.bson.Document;
import org.gcube.application.geoportal.client.utils.Serialization;
import org.gcube.application.geoportal.common.faults.InvalidRequestException;
import org.gcube.application.geoportal.common.model.document.Project;
import org.gcube.application.geoportal.common.model.document.access.Access;
import org.gcube.application.geoportal.common.model.rest.RegisterFileSetRequest;
import org.gcube.application.geoportal.common.model.rest.TempFile;
import org.gcube.application.geoportal.common.rest.MongoConcessioni;
import org.gcube.application.geoportal.common.rest.Projects;
import org.gcube.application.geoportal.common.rest.UseCaseDescriptorsI;
import org.gcube.application.geoportal.common.utils.FileSets;
import org.gcube.application.geoportal.common.utils.StorageUtils;
import org.gcube.contentmanagement.blobstorage.transport.backend.RemoteBackendException;
import org.gcube.portlets.widgets.mpformbuilder.shared.upload.FileUploaded;
import org.gcube.portlets.widgets.mpformbuilder.shared.upload.FileUploadedRemote;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;

/**
 * The Class MongoServiceCommon.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Sep 16, 2021
 */
public class MongoServiceUtil {

	private static Logger LOG = LoggerFactory.getLogger(MongoServiceUtil.class);

	UseCaseDescriptorsI client = null;

	/**
	 * Use case descriptors client.
	 *
	 * @return the use case descriptors I
	 */
	public UseCaseDescriptorsI useCaseDescriptorsClient() {
		LOG.debug("useCaseDescriptorsClient called");
		return useCaseDescriptors().build();
	}

	/**
	 * Gets the projects client.
	 *
	 * @param profileID the profile ID
	 * @return the projects client
	 */
	public Projects<Project> getProjectsClient(String profileID) {
		LOG.debug("getProjectsClient called for profileID: " + profileID);
		return projects(profileID).build();
	}

	/**
	 * Creates the new.
	 *
	 * @param profileID the profile ID
	 * @param jsonDocument the json document
	 * @return the project
	 * @throws RemoteException the remote exception
	 */
	public Project createNew(String profileID, String jsonDocument) throws RemoteException {
		LOG.debug("createNew called for profileID: " + profileID);
		Document myDocument = Document.parse(jsonDocument);
		Projects<Project> client = getProjectsClient(profileID);
		// Create project
		Project project = client.createNew(myDocument);

		return project;
	}

	/**
	 * Register file set.
	 *
	 * @param profileID the profile ID
	 * @param project the project
	 * @param parentPath the parent path
	 * @param fieldName the field name
	 * @param fieldDefinition the field definition
	 * @param access the access
	 * @param files the files
	 * @throws RemoteException the remote exception
	 * @throws FileNotFoundException the file not found exception
	 * @throws JsonProcessingException the json processing exception
	 * @throws InvalidRequestException the invalid request exception
	 */
	public void registerFileSet(String profileID, Project project, String parentPath, String fieldName,
			String fieldDefinition, Access access, File... files)
			throws RemoteException, FileNotFoundException, JsonProcessingException, InvalidRequestException {
		LOG.info("registerFileSet called for profileID: " + profileID);
		LOG.info("and for parentPath: " + parentPath);
		LOG.info("and for fieldName: " + fieldName);
		LOG.info("and for fieldDefinition: " + fieldDefinition);
		LOG.info("and for access: " + access);

		Projects<Project> client = getProjectsClient(profileID);
		// Prepare request
		RegisterFileSetRequest fsRequest = FileSets.prepareRequest(new StorageUtils(), parentPath, fieldName,
				fieldDefinition, files);
		fsRequest.setToSetAccess(access);
		
		project = client.registerFileSet(project.getId(), fsRequest);
		
		LOG.trace("Resulting Project : " + project);
		LOG.debug("Resulting Project as JSON: " + Serialization.write(project));
	}


	/**
	 * Gets the instance mongo concessioni.
	 *
	 * @return the instance mongo concessioni
	 */
	public MongoConcessioni getInstanceMongoConcessioni() {
		// return mongoConcessioni().build();
		LOG.warn("\n\n\nMUST BE PORTED TO NEW MONGO CLIENT");
		return null;
	}

	/**
	 * Creates the temp file on storage.
	 *
	 * @param is       the is
	 * @param fileName the file name
	 * @return the temp file
	 */
	public TempFile createTempFileOnStorage(InputStream is, String fileName) {
		LOG.debug("createTempFileOnStorage called");
		StorageUtils storage = new StorageUtils();
		TempFile toUpload = null;
		try {
			LOG.info("calling putOntoStorage the stream with the fileName: " + fileName);
			toUpload = storage.putOntoStorage(is, fileName);
		} catch (RemoteBackendException | FileNotFoundException e) {
			LOG.error("Error when uploading stream on Storage: ", e);
		}
		return toUpload;
	}

	/**
	 * To tem files.
	 *
	 * @param listFileUploaded the list file uploaded
	 * @return the list
	 */
	public List<TempFile> toTemFiles(List<FileUploaded> listFileUploaded) {
		LOG.debug("toTemFiles called");
		if (listFileUploaded == null || listFileUploaded.isEmpty())
			return null;

		// Building TempFile
		List<TempFile> files = new ArrayList<TempFile>(listFileUploaded.size());
		for (FileUploaded fileUploaded : listFileUploaded) {
			FileInputStream fis;
			try {
				fis = new FileInputStream(fileUploaded.getTempSystemPath());
				// Creating TempFile
				TempFile storageTempFile = createTempFileOnStorage(fis, fileUploaded.getFileName());
				files.add(storageTempFile);
			} catch (FileNotFoundException e) {
				LOG.error("Error on loading temp file with path: " + fileUploaded.getTempSystemPath(), e);
			}
		}
		return files;
	}


	/**
	 * To temp file from remote.
	 *
	 * @param file the file
	 * @return the temp file
	 */
	public TempFile toTempFileFromRemote(FileUploadedRemote file) {
		LOG.debug("toTemFilesFromRemote called");
		if (file == null)
			return null;

		// Building TempFile
		TempFile storageTempFile = null;
		try {
			InputStream is = new URL(file.getUrl()).openStream();
			// Creating TempFile
			storageTempFile = createTempFileOnStorage(is, file.getFileName());
		} catch (IOException e) {
			LOG.error("Error on creating temp file from URL: " + file.getUrl(), e);
		}
		
		return storageTempFile;
	}


	/**
	 * To JSON.
	 *
	 * @param theObj the the obj
	 * @return the string
	 */
	public String toJSON(Object theObj) {
		LOG.debug("toJSON called");
		try {

//			if (theObj instanceof Serializable) {
			return org.gcube.application.geoportal.client.utils.Serialization.write(theObj);
//			}
//			throw new Exception("The input object is not serializable");

		} catch (Exception e) {
			LOG.warn("Error on deserializing: ", e);
			return null;
		}
	}

}
