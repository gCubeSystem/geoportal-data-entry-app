package org.gcube.portlets.user.geoportaldataentry.client.ui.edit;

import java.util.Arrays;
import java.util.List;

import org.gcube.application.geoportalcommon.shared.geoportal.DocumentDV;
import org.gcube.application.geoportalcommon.shared.geoportal.ResultDocumentDV;
import org.gcube.application.geoportalcommon.shared.geoportal.project.ProjectDV;
import org.gcube.portlets.user.geoportaldataentry.client.ConstantsGeoPortalDataEntryApp.ACTION_PERFORMED_ON_ITEM;
import org.gcube.portlets.user.geoportaldataentry.client.GeoPortalDataEntryApp;
import org.gcube.portlets.user.geoportaldataentry.client.events.OperationPerformedOnItemEvent;
import org.gcube.portlets.user.geoportaldataentry.client.events.OperationPerformedOnItemEventHandler;
import org.gcube.portlets.user.geoportaldataentry.client.ui.edit.jseditor.JSONEditorWrapper;
import org.gcube.portlets.user.geoportaldataentry.client.ui.utils.DialogInform;
import org.gcube.portlets.user.geoportaldataentry.client.ui.utils.LoaderIcon;

import com.github.gwtbootstrap.client.ui.Alert;
import com.github.gwtbootstrap.client.ui.Button;
import com.github.gwtbootstrap.client.ui.Tab;
import com.github.gwtbootstrap.client.ui.constants.AlertType;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.dom.client.Style.Visibility;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Random;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * The Class EditModeRecord.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Sep 17, 2021
 */
public class EditModeRecord extends Composite {

	private static EditModeRecordUiBinder uiBinder = GWT.create(EditModeRecordUiBinder.class);

	@UiField
	Tab tabRawUpdate;

	@UiField
	Tab tabUploadFiles;

	@UiField
	FlowPanel rawUpdatePanel;

	@UiField
	HTMLPanel filesUpdatePanel;

	@UiField
	Button buttonJSONUpdate;

	private ResultDocumentDV selectedProject;

	private JSONEditorWrapper jsEditor;

	private HandlerManager appManagerBus;

	private final HandlerManager editorManagerBus = new HandlerManager(null);

	private int modalHeight;

	/**
	 * The Interface EditModeRecordUiBinder.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
	 * 
	 *         Sep 17, 2021
	 */
	interface EditModeRecordUiBinder extends UiBinder<Widget, EditModeRecord> {
	}

	public EditModeRecord(HandlerManager appManagerBus, ResultDocumentDV selectedProject, int modalHeight) {
		initWidget(uiBinder.createAndBindUi(this));
		this.modalHeight = modalHeight;
		this.selectedProject = selectedProject;
		this.appManagerBus = appManagerBus;
		this.filesUpdatePanel.setHeight((modalHeight - 50) + "px");
		// filesUpdatePanel.getElement().modalHeight().setProperty("maxHeight",
		// "550px");

		// TODO Must be instanceUpdateFilesetEditor
		tabUploadFiles.asWidget().getElement().getStyle().setVisibility(Visibility.HIDDEN);

		instanceJSONEditor();
		instanceUpdateFilesetEditor();
		bindEvents();
	}

	private void instanceUpdateFilesetEditor() {

		// Window.alert("instanceUpdateFilesetEditor must be revisited");

		/*
		 * GeoPortalDataEntryApp.geoportalDataEntryService.readFileSetPaths(new
		 * AsyncCallback<FileSetPathsDV>() {
		 * 
		 * @Override public void onFailure(Throwable caught) { // TODO Auto-generated
		 * method stub
		 * 
		 * }
		 * 
		 * @Override public void onSuccess(FileSetPathsDV fileSetPaths) { UpdateFileset
		 * updateFileset = new UpdateFileset(editorManagerBus, selectedProject,
		 * recordType, fileSetPaths.getFileSetPaths());
		 * filesUpdatePanel.add(updateFileset); } });
		 */

	}

	public void noUpdateMode() {
		buttonJSONUpdate.setVisible(false);
	}

	private void instanceJSONEditor() {

		rawUpdatePanel.clear();

		final HorizontalPanel hpLoader = new HorizontalPanel();
		final LoaderIcon lc = new LoaderIcon("Loading Project... please wait");
		hpLoader.add(lc);
		rawUpdatePanel.add(hpLoader);
		final FlowPanel fp = new FlowPanel();
		fp.getElement().setId("jsoneditor" + Random.nextInt());
		fp.setHeight((modalHeight - 160) + "px");
		rawUpdatePanel.add(fp);

		Scheduler.get().scheduleDeferred(new ScheduledCommand() {
			public void execute() {

				GeoPortalDataEntryApp.geoportalDataEntryService.getJSONDocumentInTheProject(
						selectedProject.getProfileID(), selectedProject.getId(), new AsyncCallback<String>() {

							@Override
							public void onSuccess(String jsonData) {
								hpLoader.clear();
								GWT.log("Instance JSON Editor with: " + jsonData);
								jsEditor = JSONEditorWrapper.init(fp.getElement().getId());
								jsEditor.setName(selectedProject.getId());
								jsEditor.set(jsonData);

								new Timer() {

									@Override
									public void run() {
										jsEditor.setMode("tree");

									}
								}.schedule(200);

							}

							@Override
							public void onFailure(Throwable caught) {
								rawUpdatePanel.clear();
								Alert alert = new Alert(
										"Sorry, I cannot show the source Project with id '" + selectedProject.getId()
												+ "' Refresh an try again. Error: " + caught.getMessage(),
										AlertType.ERROR);
								alert.setClose(false);
								rawUpdatePanel.add(alert);

							}
						});
			};
		});

	}

	/**
	 * Bind events.
	 */
	private void bindEvents() {

		buttonJSONUpdate.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				rawUpdatePanel.getElement().addClassName("disable-div");
				buttonJSONUpdate.setEnabled(false);
				final LoaderIcon loader = new LoaderIcon("Updating the project...");
				tabRawUpdate.add(loader);
				GeoPortalDataEntryApp.geoportalDataEntryService.updateRecord(selectedProject.getProfileID(),
						selectedProject.getId(), jsEditor.getText(), new AsyncCallback<ProjectDV>() {

							@Override
							public void onFailure(Throwable caught) {
								buttonJSONUpdate.setEnabled(true);
								try {
									rawUpdatePanel.getElement().removeClassName("disable-div");
									tabRawUpdate.remove(loader);
								} catch (Exception e) {
									// TODO: handle exception
								}

								editorManagerBus.fireEvent(new OperationPerformedOnItemEvent<ResultDocumentDV>(
										selectedProject.getProfileID(), Arrays.asList(selectedProject),
										ACTION_PERFORMED_ON_ITEM.UPDATED_PROJECT));

							}

							@Override
							public void onSuccess(ProjectDV result) {
								buttonJSONUpdate.setEnabled(true);
								try {
									rawUpdatePanel.getElement().removeClassName("disable-div");
									tabRawUpdate.remove(loader);
								} catch (Exception e) {
									// TODO: handle exception
								}

								DialogInform di = new DialogInform(null, "Project updated!",
										"Project ID '" + result.getId() + "' updated correctly");
								di.setZIndex(100000);
								di.center();

								editorManagerBus.fireEvent(new OperationPerformedOnItemEvent<ResultDocumentDV>(
										selectedProject.getProfileID(), Arrays.asList(selectedProject),
										ACTION_PERFORMED_ON_ITEM.UPDATED_PROJECT));

							}
						});

			}
		});

		editorManagerBus.addHandler(OperationPerformedOnItemEvent.TYPE, new OperationPerformedOnItemEventHandler() {

			@Override
			public <T extends DocumentDV> void onDoActionPerformedFired(
					OperationPerformedOnItemEvent<T> actionPerformedOnItemEvent) {
				ACTION_PERFORMED_ON_ITEM action = actionPerformedOnItemEvent.getAction();
				List<T> items = actionPerformedOnItemEvent.getSelectItems();

				if (items != null) {
					instanceJSONEditor();

					appManagerBus.fireEvent(new OperationPerformedOnItemEvent<ResultDocumentDV>(
							selectedProject.getProfileID(), (List<ResultDocumentDV>) items, action));
				}

			}
		});
	}
}
