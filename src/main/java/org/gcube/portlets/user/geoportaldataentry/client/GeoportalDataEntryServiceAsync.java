package org.gcube.portlets.user.geoportaldataentry.client;

import java.util.List;

import org.gcube.application.geoportalcommon.shared.GeoportalItemReferences;
import org.gcube.application.geoportalcommon.shared.GeoportalItemReferences.SHARE_LINK_TO;
import org.gcube.application.geoportalcommon.shared.ResultSetPaginatedData;
import org.gcube.application.geoportalcommon.shared.SearchingFilter;
import org.gcube.application.geoportalcommon.shared.geoportal.ResultDocumentDV;
import org.gcube.application.geoportalcommon.shared.geoportal.config.ActionDefinitionDV;
import org.gcube.application.geoportalcommon.shared.geoportal.config.FilePathDV;
import org.gcube.application.geoportalcommon.shared.geoportal.project.LifecycleInformationDV;
import org.gcube.application.geoportalcommon.shared.geoportal.project.ProjectDV;
import org.gcube.application.geoportalcommon.shared.geoportal.project.TemporalReferenceDV;
import org.gcube.application.geoportalcommon.shared.geoportal.step.StepPerformedResultDV;
import org.gcube.application.geoportalcommon.shared.geoportal.ucd.RelationshipDefinitionDV;
import org.gcube.application.geoportalcommon.shared.geoportal.ucd.UseCaseDescriptorDV;
import org.gcube.application.geoportalcommon.shared.geoportal.view.ProjectView;
import org.gcube.application.geoportaldatamapper.shared.ProjectEdit;
import org.gcube.portlets.user.geoportaldataentry.shared.CommitReport;
import org.gcube.portlets.user.geoportaldataentry.shared.GNADataEntryExtendedConfigProfile;
import org.gcube.portlets.user.geoportaldataentry.shared.GeoNaFormDataObject;
import org.gcube.portlets.user.geoportaldataentry.shared.GeoportalISConfig;
import org.gcube.portlets.user.geoportaldataentry.shared.Tree_Node;
import org.gcube.portlets.user.geoportaldataentry.shared.monitoring.MonitoringActionsOnClient;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * The Interface GeoportalDataEntryServiceAsync.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Sep 1, 2022
 */
public interface GeoportalDataEntryServiceAsync {

	/**
	 * Utility class to get the RPC Async interface from client-side code
	 */
	public static final class Util {
		private static GeoportalDataEntryServiceAsync instance;

		public static final GeoportalDataEntryServiceAsync getInstance() {
			if (instance == null) {
				instance = (GeoportalDataEntryServiceAsync) GWT.create(GeoportalDataEntryService.class);
			}
			return instance;
		}

		private Util() {
			// Utility class should not be instantiated
		}
	}

	void saveGeonaDataForms(String uuid, String profileID, Tree_Node<GeoNaFormDataObject> tree_Node,
			List<String> stepsOnPostCreation, AsyncCallback<Void> callback);
	
	void updateGeportalDataForm(String profileID, String projectID, GeoNaFormDataObject section,
			String sectionPath, List<FilePathDV> listFilePaths, AsyncCallback<CommitReport> callback);

	void getGeonaInitConfig(AsyncCallback<GeoportalISConfig> callback);

	void getLinksFor(String itemId, String profileID, SHARE_LINK_TO shareLinkTo,
			AsyncCallback<GeoportalItemReferences> callback);

	void getListProjects(String theProfileID, Integer start, Integer limit, SearchingFilter filter,
			boolean reloadFromService, AsyncCallback<ResultSetPaginatedData> callback);

	void deleteProject(String profileID, String projectID, AsyncCallback<Boolean> callback);

	void getJSONDocumentInTheProject(String profileID, String projectID, AsyncCallback<String> callback);

	void updateRecord(String profileID, String projectID, String jsonUpdate, AsyncCallback<ProjectDV> callback);

	void readDataEntryPresentationConfig(AsyncCallback<GNADataEntryExtendedConfigProfile> asyncCallback);

	void getListUseCaseDescriptors(List<String> handlersIds, AsyncCallback<List<UseCaseDescriptorDV>> callback);

	void getLifecycleInfoForProjectId(String profileID, String projectID,
			AsyncCallback<LifecycleInformationDV> callback);

	void performActionSteps(String profileID, String projectID, String optionalMessage, ActionDefinitionDV action,
			AsyncCallback<StepPerformedResultDV> callback);

	void getRelationshipsDefinition(String profileID, AsyncCallback<List<RelationshipDefinitionDV>> callback);

	void createRelationship(String fromProfileID, String fromProjectID, String relationshipID, String toProfileID,
			String toProjectID, AsyncCallback<Void> callback);

	void getProjectByID(String profileID, String projectID, AsyncCallback<ProjectDV> callback);

	void getResultDocumentFoProjectByID(String profileID, String projectID, AsyncCallback<ResultDocumentDV> callback);

	void deleteRelationship(String fromProfileID, String fromProjectID, String id, String toProfileID,
			String toProjectID, AsyncCallback<ResultDocumentDV> asyncCallback);

	void temporalReferenceForProject(String profileID, String projectID, AsyncCallback<TemporalReferenceDV> callback);

	void getProjectEdit(String profileID, String projectID, AsyncCallback<ProjectEdit> callback);

	void getProjectView(String profileID, String projectID, AsyncCallback<ProjectView> callback);

	void getSavingProjectMonitorStatus(String monitorUUID, boolean isMonitoringTerminatedClientConsumed,
			AsyncCallback<MonitoringActionsOnClient> callback);

}
