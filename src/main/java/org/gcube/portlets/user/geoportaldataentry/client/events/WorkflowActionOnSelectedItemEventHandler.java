package org.gcube.portlets.user.geoportaldataentry.client.events;

import org.gcube.application.geoportalcommon.shared.geoportal.DocumentDV;

import com.google.gwt.event.shared.EventHandler;

/**
 * The Interface WorkflowActionOnSelectedItemEventHandler.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Sep 13, 2022
 */
public interface WorkflowActionOnSelectedItemEventHandler extends EventHandler {

	/**
	 * On do action fired.
	 *
	 * @param <T>                       the generic type
	 * @param workflowActionOnItemEvent the workflow action on item event
	 */
	<T extends DocumentDV> void onDoActionFired(WorkflowActionOnSelectedItemEvent<T> workflowActionOnItemEvent);
}