package org.gcube.portlets.user.geoportaldataentry.shared;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * The Class Tree_Node.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Jul 7, 2022
 * @param <T> the generic type
 */
public class Tree_Node<T> implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7319055031988865761L;
	private String name;
	private T data;
	private List<Tree_Node<T>> children = new ArrayList<Tree_Node<T>>();
	private Tree_Node<T> parent;
	private boolean isRoot;

	/**
	 * Instantiates a new tree node.
	 */
	public Tree_Node() {

	}

	/**
	 * Instantiates a new tree node.
	 *
	 * @param name the name
	 * @param data the data
	 */
	public Tree_Node(String name, T data) {
		this.name = name;
		this.data = data;
	}

	/**
	 * Checks if is root.
	 *
	 * @return true, if is root
	 */
	public boolean isRoot() {
		return isRoot;
	}

	/**
	 * Sets the root.
	 *
	 * @param isRoot the new root
	 */
	public void setRoot(boolean isRoot) {
		this.isRoot = isRoot;
	}

	/**
	 * Adds the child.
	 *
	 * @param child the child
	 */
	public void addChild(Tree_Node<T> child) {
		child.setParent(this);
		children.add(child);
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the data.
	 *
	 * @return the data
	 */
	public T getData() {
		return this.data;
	}

	/**
	 * Sets the data.
	 *
	 * @param data the new data
	 */
	public void setData(T data) {
		this.data = data;
	}

	/**
	 * Gets the parent.
	 *
	 * @return the parent
	 */
	public Tree_Node<T> getParent() {
		return this.parent;
	}

	/**
	 * Sets the parent.
	 *
	 * @param parent the new parent
	 */
	public void setParent(Tree_Node<T> parent) {
		this.parent = parent;
	}

	/**
	 * Gets the children.
	 *
	 * @return the children
	 */
	public List<Tree_Node<T>> getChildren() {
		return this.children;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Tree_Node [name=");
		builder.append(name);
		builder.append(", data=");
		builder.append(data);
		builder.append(", isRoot=");
		builder.append(isRoot);
		builder.append("]");
		return builder.toString();
	}

}