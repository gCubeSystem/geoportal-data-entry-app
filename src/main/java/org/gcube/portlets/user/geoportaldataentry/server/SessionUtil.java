/**
 *
 */
package org.gcube.portlets.user.geoportaldataentry.server;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.gcube.application.geoportal.common.model.legacy.Concessione;
import org.gcube.application.geoportal.common.rest.MongoConcessioni;
import org.gcube.application.geoportalcommon.shared.GNADataEntryConfigProfile;
import org.gcube.application.geoportalcommon.shared.geoportal.project.ProjectDV;
import org.gcube.application.geoportalcommon.shared.geoportal.ucd.RelationshipDefinitionDV;
import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.common.portal.PortalContext;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.portlets.user.geoportaldataentry.shared.ResultSetSorted;
import org.gcube.vomanagement.usermanagement.GroupManager;
import org.gcube.vomanagement.usermanagement.exception.GroupRetrievalFault;
import org.gcube.vomanagement.usermanagement.exception.UserManagementSystemException;
import org.gcube.vomanagement.usermanagement.impl.LiferayGroupManager;
import org.gcube.vomanagement.usermanagement.model.GCubeGroup;
import org.gcube.vomanagement.usermanagement.model.GCubeUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.liferay.portal.service.UserLocalServiceUtil;

/**
 * The Class SessionUtil.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 *         Oct 20, 2020
 */
public class SessionUtil {

	private static final String GNA_DATAENTRY_CONFIG_PROFILE = "GNA_DATAENTRY_CONFIG_PROFILE";
	private static final String LATEST_RESULT_SET_SORTED = "LATEST_RESULT_SET_SORTED";
	// private static final String GEONA_DATAVIEWER_PROFILE =
	// "GEONA_DATAVIEWER_PROFILE";
	private static final String LIST_OF_CONCESSIONI = "LIST_OF_CONCESSIONI";

	private static final String LIST_OF_RELATIONSHIP_DEFINITION = "LIST_OF_RELATIONSHIP_DEFINITION";

	private static final String LIST_OF_PROJECTS = "LIST_OF_PROJECTS";
	private static final String COUNT_DOCS_FOR_PROFILE_ID = "COUNT_DOCS_FOR_PROFILE_ID";
	private static final String TIMELINE_JSON_TEMPLATE = "DATA_ENTRY_TIMELINE_JSON_TEMPLATE";
	/** The log. */
	private static Logger LOG = LoggerFactory.getLogger(SessionUtil.class);

	/**
	 * Checks if is into portal.
	 *
	 * @return true, if is into portal
	 */
	public static boolean isIntoPortal() {
		try {
			UserLocalServiceUtil.getService();
			return true;
		} catch (Exception ex) {
			LOG.debug("Development Mode ON");
			return false;
		}
	}

	/**
	 * Retrieve the current user by using the portal manager.
	 *
	 * @param request the request
	 * @return a GcubeUser object
	 */
	public static GCubeUser getCurrentUser(HttpServletRequest request) {

		if (request == null)
			throw new IllegalArgumentException("HttpServletRequest is null!");

		PortalContext pContext = PortalContext.getConfiguration();
		GCubeUser user = pContext.getCurrentUser(request);
		LOG.debug("Returning user " + user);
		return user;
	}

	/**
	 * Retrieve the current scope by using the portal manager.
	 *
	 * @param request             the request
	 * @param setContextsInThread set the scope and the security token into current
	 *                            thread
	 * @return a GcubeUser object
	 */
	public static String getCurrentContext(HttpServletRequest request, boolean setContextsInThread) {

		if (request == null)
			throw new IllegalArgumentException("HttpServletRequest is null!");

		PortalContext pContext = PortalContext.getConfiguration();
		String context = pContext.getCurrentScope(request);

		if (setContextsInThread) {
			GCubeUser user = getCurrentUser(request);
			String token = pContext.getCurrentUserToken(context, user.getUsername());

			if (context != null)
				ScopeProvider.instance.set(context);

			if (token != null)
				SecurityTokenProvider.instance.set(token);
		}

		return context;
	}

	/**
	 * Retrieve the group given the scope.
	 *
	 * @param scope the scope
	 * @return the group from scope
	 * @throws UserManagementSystemException the user management system exception
	 * @throws GroupRetrievalFault           the group retrieval fault
	 */
	public static GCubeGroup getGroupFromScope(String scope) throws UserManagementSystemException, GroupRetrievalFault {

		if (scope == null || scope.isEmpty())
			throw new IllegalArgumentException("Scope is missing here!!");

		GroupManager gm = new LiferayGroupManager();
		long groupId = gm.getGroupIdFromInfrastructureScope(scope);
		return gm.getGroup(groupId);

	}

	/**
	 * Gets the list of projects for profile ID.
	 *
	 * @param httpServletRequest the http servlet request
	 * @param profileID          the profile ID
	 * @return the list of projects for profile ID
	 * @throws Exception the exception
	 */
	public static List<ProjectDV> getListOfProjectsForProfileID(HttpServletRequest httpServletRequest, String profileID)
			throws Exception {
		HttpSession session = httpServletRequest.getSession();
		return (List<ProjectDV>) session.getAttribute(LIST_OF_PROJECTS + profileID);
	}

	/**
	 * Gets the total document for profile ID.
	 *
	 * @param httpServletRequest the http servlet request
	 * @param theProfileID       the the profile ID
	 * @return the total document for profile ID
	 */
	public static Integer getTotalDocumentForProfileID(HttpServletRequest httpServletRequest, String theProfileID) {
		HttpSession session = httpServletRequest.getSession();
		return (Integer) session.getAttribute(COUNT_DOCS_FOR_PROFILE_ID + theProfileID);

	}

	/**
	 * Sets the total document for profile ID.
	 *
	 * @param httpServletRequest the http servlet request
	 * @param theProfileID       the the profile ID
	 * @param countForProfileId  the count for profile id
	 */
	public static void setTotalDocumentForProfileID(HttpServletRequest httpServletRequest, String theProfileID,
			Integer countForProfileId) {
		HttpSession session = httpServletRequest.getSession();
		session.setAttribute(COUNT_DOCS_FOR_PROFILE_ID + theProfileID, countForProfileId);

	}

	/**
	 * Sets the list of projects for profile ID.
	 *
	 * @param httpServletRequest the http servlet request
	 * @param profileID          the profile ID
	 * @param listOfProjects     the list of projects
	 * @throws Exception the exception
	 */
	public static void setListOfProjectsForProfileID(HttpServletRequest httpServletRequest, String profileID,
			List<ProjectDV> listOfProjects) throws Exception {
		HttpSession session = httpServletRequest.getSession();
		session.setAttribute(LIST_OF_PROJECTS + profileID, listOfProjects);
	}

	/**
	 * Gets the list of concessioni.
	 *
	 * @param httpServletRequest the http servlet request
	 * @param reloadFromService  the reload from service
	 * @return the list of concessioni
	 * @throws Exception the exception
	 */
	public static List<Concessione> getListOfConcessioni(HttpServletRequest httpServletRequest,
			boolean reloadFromService) throws Exception {
		HttpSession session = httpServletRequest.getSession();
		List<Concessione> listOfConcessioni = (List<Concessione>) session.getAttribute(LIST_OF_CONCESSIONI);

		// setting null to force reloading from service
		if (reloadFromService)
			listOfConcessioni = null;

		if (listOfConcessioni == null) {
			listOfConcessioni = new ArrayList<Concessione>();
			LOG.info("Loading list of concessione from client mongo");
			SessionUtil.getCurrentContext(httpServletRequest, true);
			MongoServiceUtil serviceUtil = new MongoServiceUtil();
			MongoConcessioni clientMongo = serviceUtil.getInstanceMongoConcessioni();

			Iterator<Concessione> concessioni = clientMongo.getList();
			if (concessioni != null) {
				while (concessioni.hasNext()) {
					Concessione concessione = (Concessione) concessioni.next();
					listOfConcessioni.add(concessione);

				}
			}
			// LOG.debug("Got list of concessioni from client mongo: " + listOfConcessioni);
			session.setAttribute(LIST_OF_CONCESSIONI, listOfConcessioni);
			LOG.info("Saved in session list of concessioni from client mongo with size: " + listOfConcessioni.size());
		} else
			LOG.info("list of concessioni presents in session, using it");

		LOG.info("read list of concessioni with size: " + listOfConcessioni.size());
		return listOfConcessioni;
	}

//	/**
//	 * Gets the geportal viewer resource profile.
//	 *
//	 * @param httpServletRequest the http servlet request
//	 * @return the geportal viewer resource profile
//	 * @throws Exception the exception
//	 */
//	public static GNADataViewerConfigProfile getGeportalViewerResourceProfile(HttpServletRequest httpServletRequest)
//			throws Exception {
//		HttpSession session = httpServletRequest.getSession();
//		GNADataViewerConfigProfile geoNaDataViewerProfile = (GNADataViewerConfigProfile) session
//				.getAttribute(GEONA_DATAVIEWER_PROFILE);
//
//		if (geoNaDataViewerProfile == null) {
//			GeoportalCommon gc = new GeoportalCommon();
//			geoNaDataViewerProfile = gc.readGNADataViewerConfig(null);
//			session.setAttribute(GEONA_DATAVIEWER_PROFILE, geoNaDataViewerProfile);
//		}
//
//		return geoNaDataViewerProfile;
//
//	}

	/**
	 * Gets the latest result set sorted.
	 *
	 * @param httpServletRequest the http servlet request
	 * @return the latest result set sorted
	 */
	public static ResultSetSorted getLatestResultSetSorted(HttpServletRequest httpServletRequest) {
		HttpSession session = httpServletRequest.getSession();
		return (ResultSetSorted) session.getAttribute(LATEST_RESULT_SET_SORTED);
	}

	/**
	 * Gets the latest sort filter.
	 *
	 * @param httpServletRequest the http servlet request
	 * @param rsSorted           the rs sorted
	 * @return the latest sort filter
	 */
	public static void setLatestResultSetSorted(HttpServletRequest httpServletRequest, ResultSetSorted rsSorted) {
		HttpSession session = httpServletRequest.getSession();
		session.setAttribute(LATEST_RESULT_SET_SORTED, rsSorted);
	}

	/**
	 * Gets the GNA data entry config profile.
	 *
	 * @param httpServletRequest the http servlet request
	 * @return the GNA data entry config profile
	 */
	public static GNADataEntryConfigProfile getGNADataEntryConfigProfile(HttpServletRequest httpServletRequest) {
		HttpSession session = httpServletRequest.getSession();
		return (GNADataEntryConfigProfile) session.getAttribute(GNA_DATAENTRY_CONFIG_PROFILE);
	}

	/**
	 * Sets the GNA data entry config profile.
	 *
	 * @param httpServletRequest the http servlet request
	 * @param gNADEConfigProfile the g NADE config profile
	 */
	public static void setGNADataEntryConfigProfile(HttpServletRequest httpServletRequest,
			GNADataEntryConfigProfile gNADEConfigProfile) {
		HttpSession session = httpServletRequest.getSession();
		session.setAttribute(GNA_DATAENTRY_CONFIG_PROFILE, gNADEConfigProfile);
	}

	/**
	 * Gets the relationships definition.
	 *
	 * @param httpServletRequest the http servlet request
	 * @param profileID          the profile ID
	 * @return the relationships definition
	 */
	public static List<RelationshipDefinitionDV> getRelationshipsDefinition(HttpServletRequest httpServletRequest,
			String profileID) {
		HttpSession session = httpServletRequest.getSession();
		return (List<RelationshipDefinitionDV>) session.getAttribute(LIST_OF_RELATIONSHIP_DEFINITION + profileID);

	}

	/**
	 * Sets the relationship definition.
	 *
	 * @param threadLocalRequest    the thread local request
	 * @param profileID             the profile ID
	 * @param listRelationshipNames the list relationship names
	 */
	public static void setRelationshipDefinition(HttpServletRequest threadLocalRequest, String profileID,
			List<RelationshipDefinitionDV> listRelationshipNames) {
		HttpSession session = threadLocalRequest.getSession();
		session.setAttribute(LIST_OF_RELATIONSHIP_DEFINITION + profileID, listRelationshipNames);
	}

	/**
	 * Gets the JSON timeline template.
	 *
	 * @param httpServletRequest the http servlet request
	 * @param profileID          the profile ID
	 * @return the JSON timeline template
	 */
	public static String getJSONTimelineTemplate(HttpServletRequest httpServletRequest, String profileID) {
		HttpSession session = httpServletRequest.getSession();
		return (String) session.getAttribute(TIMELINE_JSON_TEMPLATE + profileID);
	}

	/**
	 * Sets the JSON timeline template.
	 *
	 * @param httpServletRequest   the http servlet request
	 * @param profileID            the profile ID
	 * @param jsonTimelineTemplate the json timeline template
	 */
	public static void setJSONTimelineTemplate(HttpServletRequest httpServletRequest, String profileID,
			String jsonTimelineTemplate) {
		HttpSession session = httpServletRequest.getSession();
		session.setAttribute(TIMELINE_JSON_TEMPLATE + profileID, jsonTimelineTemplate);
	}

	public static void setMonitorStatus(HttpServletRequest httpServletRequest, String uuidAsString,
			MonitoringActionsOnServer monitorActions) {
		HttpSession session = httpServletRequest.getSession();
		String monitorID = getUniqueMonitorID(uuidAsString);
		session.setAttribute(monitorID, monitorActions);
	}
	
	public static void removeMonitorStatus(HttpServletRequest httpServletRequest, String uuidAsString) {
		HttpSession session = httpServletRequest.getSession();
		String monitorID = getUniqueMonitorID(uuidAsString);
		session.removeAttribute(monitorID);
	}

	public static MonitoringActionsOnServer getMonitorStatus(HttpServletRequest httpServletRequest, String uuidAsString) {
		HttpSession session = httpServletRequest.getSession();
		String monitorID = getUniqueMonitorID(uuidAsString);
		try {
			return (MonitoringActionsOnServer) session.getAttribute(monitorID);
		}catch (Exception e) {
			LOG.warn("No monitor in session for id: "+monitorID);
			return null;
		}
	}

	public static final String getUniqueMonitorID(String uuid) {
		return uuid + "_MONITOR";
	}

}
