package org.gcube.portlets.user.geoportaldataentry.client;

/**
 * The Class ProjectFormCard.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Mar 4, 2022
 */
public class ProjectFormCard {

	private String key;
	private String title; //is the profile title (read from geoportal config)
	private Integer order;
	private boolean internalRepeatibleForm;
	private Integer maxFormRepeatability;
	private Integer minFormRepeatability;

	/**
	 * Instantiates a new project form card.
	 *
	 * @param key                    the key
	 * @param title                  the title
	 * @param order                  the order
	 * @param internalRepeatibleForm the internal repeatible form
	 * @param minFormRepeatability   the min form repeatability
	 * @param maxFormRepeatability   the max form repeatability
	 */
	public ProjectFormCard(String key, String title, int order, boolean internalRepeatibleForm,
			Integer minFormRepeatability, Integer maxFormRepeatability) {
		this.key = key;
		this.title = title;
		this.order = order;
		this.internalRepeatibleForm = internalRepeatibleForm;
		this.minFormRepeatability = minFormRepeatability;
		this.maxFormRepeatability = maxFormRepeatability;
	}

	/**
	 * Gets the key.
	 *
	 * @return the key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Gets the order.
	 *
	 * @return the order
	 */
	public Integer getOrder() {
		return order;
	}

	/**
	 * Checks if is internal repeatible form.
	 *
	 * @return true, if is internal repeatible form
	 */
	public boolean isInternalRepeatibleForm() {
		return internalRepeatibleForm;
	}

	/**
	 * Gets the max form repeatability.
	 *
	 * @return the max form repeatability
	 */
	public Integer getMaxFormRepeatability() {
		return maxFormRepeatability;
	}

	/**
	 * Gets the min form repeatability.
	 *
	 * @return the min form repeatability
	 */
	public Integer getMinFormRepeatability() {
		return minFormRepeatability;
	}

	/**
	 * Sets the min form repeatability.
	 *
	 * @param minFormRepeatability the new min form repeatability
	 */
	public void setMinFormRepeatability(Integer minFormRepeatability) {
		this.minFormRepeatability = minFormRepeatability;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ProjectFormCard [key=");
		builder.append(key);
		builder.append(", title=");
		builder.append(title);
		builder.append(", order=");
		builder.append(order);
		builder.append(", internalRepeatibleForm=");
		builder.append(internalRepeatibleForm);
		builder.append(", maxFormRepeatability=");
		builder.append(maxFormRepeatability);
		builder.append(", minFormRepeatability=");
		builder.append(minFormRepeatability);
		builder.append("]");
		return builder.toString();
	}
	
	

}
