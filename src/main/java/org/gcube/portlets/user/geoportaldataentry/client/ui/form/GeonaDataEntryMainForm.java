package org.gcube.portlets.user.geoportaldataentry.client.ui.form;

import java.util.Arrays;
import java.util.LinkedHashMap;

import org.gcube.portlets.user.geoportaldataentry.client.events.SaveGeonaDataFormsEvent;
import org.gcube.portlets.user.geoportaldataentry.client.events.TreeItemEvent;
import org.gcube.portlets.user.geoportaldataentry.client.events.TreeItemEvent.ACTION;
import org.gcube.portlets.user.geoportaldataentry.client.ui.card.GeoNaFormCardModel;
import org.gcube.portlets.user.geoportaldataentry.client.ui.card.MetadataFormCard;
import org.gcube.portlets.user.geoportaldataentry.client.ui.tree.NodeItem;
import org.gcube.portlets.user.geoportaldataentry.client.ui.tree.TreeItemPanel;
import org.gcube.portlets.user.geoportaldataentry.client.ui.utils.TreeVisitUtil;
import org.gcube.portlets.user.geoportaldataentry.shared.GeoNaFormDataObject;
import org.gcube.portlets.user.geoportaldataentry.shared.Tree_Node;
import org.gcube.portlets.widgets.mpformbuilder.client.form.generic.CreateMetadataForm;

import com.github.gwtbootstrap.client.ui.AlertBlock;
import com.github.gwtbootstrap.client.ui.Button;
import com.github.gwtbootstrap.client.ui.FormActions;
import com.github.gwtbootstrap.client.ui.NavList;
import com.github.gwtbootstrap.client.ui.constants.AlertType;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.TreeItem;
import com.google.gwt.user.client.ui.Widget;

/**
 * The Class GeonaDataEntryMainForm.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Mar 4, 2022
 */
public class GeonaDataEntryMainForm extends Composite {

	private static GeonaDataEntryMainFormUiBinder uiBinder = GWT.create(GeonaDataEntryMainFormUiBinder.class);

	/**
	 * The Interface GeonaDataEntryMainFormUiBinder.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
	 * 
	 *         Mar 4, 2022
	 */
	interface GeonaDataEntryMainFormUiBinder extends UiBinder<Widget, GeonaDataEntryMainForm> {
	}

	private Tree_Node<GeoNaFormCardModel> nodes = new Tree_Node<GeoNaFormCardModel>();

	@UiField
	HTMLPanel mainHTMLPanel;

	@UiField
	ScrollPanel treePanel;

	@UiField
	NavList navbarTree;

	@UiField
	ScrollPanel inputPanel;

	@UiField
	AlertBlock alertFormAction;

	@UiField
	Button duplicateSelected;

	@UiField
	Button removeSelected;

	@UiField
	Button buttonSave;

	@UiField
	FormActions formActions;

	private LinkedHashMap<String, MetadataFormCard> mapForms = new LinkedHashMap<String, MetadataFormCard>();

	private HandlerManager appManagerBus;

	private TreeItemPanel treeItemPanel;

	private boolean canSave = true;

	/**
	 * Instantiates a new geona data entry main form.
	 *
	 * @param appManagerBus the app manager bus
	 */
	public GeonaDataEntryMainForm(HandlerManager appManagerBus) {
		initWidget(uiBinder.createAndBindUi(this));
		this.appManagerBus = appManagerBus;

		duplicateSelected.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				appManagerBus.fireEvent(new TreeItemEvent(null, ACTION.DUPLICATE));

			}
		});

		removeSelected.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				appManagerBus.fireEvent(new TreeItemEvent(null, ACTION.DELETE));

			}
		});
	}

	/**
	 * Adds the tree.
	 *
	 * @param treeItemPanel the tree item panel
	 */
	public void addTree(TreeItemPanel treeItemPanel) {
		this.treeItemPanel = treeItemPanel;
		navbarTree.setVisible(true);
		treePanel.add(treeItemPanel.getTree());
	}

	public void removeTree(TreeItemPanel treeItemPanel) {
		try {
			treePanel.remove(treeItemPanel.getTree());
		} catch (Exception e) {
			GWT.log("error: "+e.getMessage());
		}

	}

	/**
	 * Gets the tree item panel.
	 *
	 * @return the tree item panel
	 */
	public TreeItemPanel getTreeItemPanel() {
		return treeItemPanel;
	}

	/**
	 * Sets the selected form.
	 *
	 * @param form the new selected form
	 */
	public void setSelectedForm(CreateMetadataForm form) {
		inputPanel.clear();
		GWT.log("Showing form: " + form);
		
		form.getElement().addClassName("gna-dataentry-form-fieldset");
		inputPanel.add(form);
	}

	/**
	 * Reset.
	 */
	public void resetUI() {
		navbarTree.setVisible(false);
		mapForms.clear();
		// mainHTMLPanel.clear();
		inputPanel.clear();
		//inputPanel = new ScrollPanel();
		//removeTree(treeItemPanel);
		treePanel.clear();
		alertFormAction.clear();
		alertFormAction.setVisible(false);
		//treePanel = new ScrollPanel();
		// treePanel = new ScrollPanel();
		// listTabs.clear();
	}

	/**
	 * Reset.
	 */
	public void resetInputPanel() {
		inputPanel.clear();
	}

	public void validateFormsIntoTree(TreeItem root) {
		GWT.log("validateFormsIntoTree called");

		if (root == null) {
			return;
		}

		GWT.log("validateFormsIntoTree called for: " + root.getText());

		for (int i = 0; i < root.getChildCount(); i++) {
			TreeItem child = root.getChild(i);
			validateFormsIntoTree(child);
		}

		NodeItem node = (NodeItem) root.getWidget();
		GWT.log("validateFormsIntoTree called for: " + root.getText() + " node: " + node);

		// Is root
		if (node.isRoot()) {
			return;
		}

		boolean isValid = node.validateForm();
		GWT.log("validateFormsIntoTree called for: " + root.getText() + " is valid: " + isValid);
		node.setValidCard(isValid);
		if (!isValid) {
			canSave = false;
		}
	}

	/**
	 * Save data.
	 *
	 * @param e the e
	 */
	@UiHandler("buttonSave")
	void saveData(ClickEvent e) {

		canSave = true;
		final TreeItem root = treeItemPanel.getRoot();
		validateFormsIntoTree(root);

		if (canSave) {

			Tree_Node<GeoNaFormDataObject> treeNode = buildTree(root);
			GWT.log("Tree node built correctly!");
			// System.out.println(treeNode);

//			for (String metadataType : mapForms.keySet()) {
//				MetadataFormCard card = mapForms.get(metadataType);
//				List<GenericDatasetBean> listGDB = new ArrayList<GenericDatasetBean>(card.getListForms().size());
//				for (CreateMetadataForm form : card.getListForms()) {
//					listGDB.add(form.getFormDataBean());
//
//				}
//				listGeonaFormObjects.add(new GeoNaFormDataObject(listGDB, card.getGeonaFormModel().getGcubeProfile()));
//			}
//			appManagerBus.fireEvent(new SaveGeonaDataFormsEvent(profileID, listGeonaFormObjects));

			appManagerBus.fireEvent(new SaveGeonaDataFormsEvent(treeItemPanel.getProfileID(), treeNode));
			TreeVisitUtil.preOrderVisit(treeNode);
			//showAlertOnSaveAction("Andiamo a salvare", AlertType.INFO, true);

		} else {
			showAlertOnSaveAction("Detected errors, please fix it/them", AlertType.ERROR, true);
		}

		// Window.alert("I can save: "+listGeonaFormObjects);

	}

	public <T> void printTree(Tree_Node<T> node) {

		if (node == null) {
			return;
		}

		System.out.println(node);

		for (Tree_Node<T> children : node.getChildren()) {
			printTree(children);
		}

	}

	private Tree_Node<GeoNaFormDataObject> buildTree(TreeItem root) {
		GWT.log("buildTree called");

		if (root == null) {
			GWT.log("buildTree returns, node is null");
			return null;
		}

		GWT.log("buildTree called on: " + root.getText());

		// Cloning
		NodeItem rootNodeItem = (NodeItem) root.getWidget();
		GWT.log("buildTree called on: " + root.getText() + ", node json section: "
				+ rootNodeItem.getJsonSectionFullPath());

		Tree_Node<GeoNaFormDataObject> tNode;

		// Children
		if (!rootNodeItem.isRoot()) {
			GeoNaFormCardModel nodeCard = rootNodeItem.getGeoNaFormCardModel();
			GeoNaFormDataObject gfdo = new GeoNaFormDataObject(
					Arrays.asList(nodeCard.getMetadataForm().getFormDataBean()), nodeCard.getGcubeProfile());
			tNode = new Tree_Node<GeoNaFormDataObject>(nodeCard.getGcubeProfile().getSectionTitle(), gfdo);
		} else {
			// root of the tree
			tNode = new Tree_Node<GeoNaFormDataObject>(root.getText(), null);
			tNode.setRoot(true);
		}

		// Total children count
		int total = root.getChildCount();
		// All the children
		for (int i = 0; i < total; i++) {
			Tree_Node<GeoNaFormDataObject> childCloned = buildTree(root.getChild(i));
			tNode.addChild(childCloned);
		}

		return tNode;

	}

	/**
	 * Show alert on save action.
	 *
	 * @param text            the text
	 * @param type            the type
	 * @param hideAfterAWhile the hide after A while
	 */
	public void showAlertOnSaveAction(String text, AlertType type, boolean hideAfterAWhile) {

		// Window.alert("Called alertOnCreate");
		alertFormAction.setText(text);
		alertFormAction.setType(type);
		alertFormAction.setVisible(true);
		// goBackButtonSecondStep.setEnabled(true);

		if (hideAfterAWhile) {
			// hide after some seconds
			Timer t = new Timer() {

				@Override
				public void run() {

					alertFormAction.setVisible(false);

				}
			};
			t.schedule(10000);
		}
	}

	/**
	 * Enable button save.
	 *
	 * @param enabled the enabled
	 */
	public void enableButtonDuplicateSection(boolean enabled) {
		duplicateSelected.setEnabled(enabled);
	}

	/**
	 * Enable button remove section.
	 *
	 * @param enabled the enabled
	 */
	public void enableButtonRemoveSection(boolean enabled) {
		removeSelected.setEnabled(enabled);
	}

	/**
	 * Enable button save.
	 *
	 * @param enabled the enabled
	 */
	public void enableButtonSave(boolean enabled) {
		buttonSave.setEnabled(enabled);
	}

	/**
	 * Sets the ative after.
	 *
	 * @param index the index
	 * @param bool  the bool
	 */
	public void setAtiveAfter(int index, boolean bool) {

//		if(listTabs.size()<index) {
//			for (int i = index; i < listTabs.size(); i++) {
//				listTabs.get(i).setEnabled(bool);
//			}
//		}
	}

	/**
	 * Sets the visible form actions.
	 *
	 * @param bool the new visible form actions
	 */
	public void setVisibleFormActions(boolean bool) {
		formActions.setVisible(bool);
	}

}
