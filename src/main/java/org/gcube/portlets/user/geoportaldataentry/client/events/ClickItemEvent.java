package org.gcube.portlets.user.geoportaldataentry.client.events;

import java.util.List;

import com.google.gwt.event.shared.GwtEvent;

/**
 * The Class ClickItemEvent.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 * Jun 22, 2021
 * @param <T> the generic type
 */
public class ClickItemEvent<T> extends GwtEvent<ClickItemEventHandler> {
	public static Type<ClickItemEventHandler> TYPE = new Type<ClickItemEventHandler>();
	private List<T> selectItems;

	/**
	 * Instantiates a new click item event.
	 *
	 * @param selectItems the select items
	 */
	public ClickItemEvent(List<T> selectItems) {
		this.selectItems = selectItems;
	}

	/**
	 * Gets the associated type.
	 *
	 * @return the associated type
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.google.gwt.event.shared.GwtEvent#getAssociatedType()
	 */
	@Override
	public Type<ClickItemEventHandler> getAssociatedType() {
		return TYPE;
	}

	/**
	 * Dispatch.
	 *
	 * @param handler the handler
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.google.gwt.event.shared.GwtEvent#dispatch(com.google.gwt.event.shared.
	 * EventHandler)
	 */
	@Override
	protected void dispatch(ClickItemEventHandler handler) {
		handler.onClick(this);
	}

	/**
	 * Gets the select items.
	 *
	 * @return the select items
	 */
	public List<T> getSelectItems() {
		return selectItems;
	}
}
