package org.gcube.portlets.user.geoportaldataentry.client.ui.utils;

public class UUIDUtil {

    public static String generateUUID() {
        return nativeGenerateUUID();
    }

    // Usa una funzione JavaScript per generare un UUID
    private static native String nativeGenerateUUID() /*-{
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    }-*/;
}
