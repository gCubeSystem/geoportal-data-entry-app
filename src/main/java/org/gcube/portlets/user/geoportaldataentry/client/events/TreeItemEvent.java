package org.gcube.portlets.user.geoportaldataentry.client.events;

import org.gcube.portlets.user.geoportaldataentry.client.ui.tree.NodeItem;

import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.user.client.ui.TreeItem;


/**
 * The Class TreeItemEvent.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 * Jul 4, 2022
 * @param <T> the generic type
 */
public class TreeItemEvent extends GwtEvent<TreeItemEventHandler> {
	public static Type<TreeItemEventHandler> TYPE = new Type<TreeItemEventHandler>();
	private TreeItem treeItem;
	private ACTION actionPerformed;

	/**
	 * The Enum ACTION.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
	 * 
	 * Jul 4, 2022
	 */
	public enum ACTION {
		SELECTED, DELETE, DUPLICATE
	}

	/**
	 * Instantiates a new click item event.
	 *
	 * @param selectItems the select items
	 * @param action the action
	 */
	public TreeItemEvent(TreeItem treeItem, ACTION action) {
		this.treeItem = treeItem;
		this.actionPerformed = action;
	}

	/**
	 * Gets the associated type.
	 *
	 * @return the associated type
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.google.gwt.event.shared.GwtEvent#getAssociatedType()
	 */
	@Override
	public Type<TreeItemEventHandler> getAssociatedType() {
		return TYPE;
	}

	/**
	 * Dispatch.
	 *
	 * @param handler the handler
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.google.gwt.event.shared.GwtEvent#dispatch(com.google.gwt.event.shared.
	 * EventHandler)
	 */
	@Override
	protected void dispatch(TreeItemEventHandler handler) {
		handler.onTreeItemEvent(this);
	}

	/**
	 * Gets the select items.
	 *
	 * @return the select items
	 */
	public TreeItem getSelectItem() {
		return treeItem;
	}
	
	public NodeItem selectedAsNodeItem(){
		if(treeItem!=null) {
			return (NodeItem) treeItem.getWidget();
		}
		
		return null;
	}

	/**
	 * Gets the action performed.
	 *
	 * @return the action performed
	 */
	public ACTION getActionPerformed() {
		return actionPerformed;
	}
}
