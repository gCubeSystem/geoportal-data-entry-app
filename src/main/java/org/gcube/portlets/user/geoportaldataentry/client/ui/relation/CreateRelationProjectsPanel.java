package org.gcube.portlets.user.geoportaldataentry.client.ui.relation;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.gcube.application.geoportalcommon.shared.geoportal.ResultDocumentDV;
import org.gcube.application.geoportalcommon.shared.geoportal.project.TemporalReferenceDV;
import org.gcube.application.geoportalcommon.shared.geoportal.ucd.RelationshipDefinitionDV;
import org.gcube.portlets.user.geoportaldataentry.client.GeoPortalDataEntryApp;
import org.gcube.portlets.user.geoportaldataentry.client.GeoportalDataEntryServiceAsync;
import org.gcube.portlets.user.geoportaldataentry.client.events.CloseCreateRelationGUIEvent;
import org.gcube.portlets.user.geoportaldataentry.client.events.RelationActionHandlerEvent;
import org.gcube.portlets.user.geoportaldataentry.client.resource.Images;
import org.gcube.portlets.user.geoportaldataentry.client.ui.report.ReportTemplateToHTML;
import org.gcube.portlets.user.geoportaldataentry.client.ui.utils.HTMLUtil;

import com.github.gwtbootstrap.client.ui.Alert;
import com.github.gwtbootstrap.client.ui.Button;
import com.github.gwtbootstrap.client.ui.ListBox;
import com.github.gwtbootstrap.client.ui.constants.ButtonType;
import com.github.gwtbootstrap.client.ui.constants.IconSize;
import com.github.gwtbootstrap.client.ui.constants.IconType;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.ComplexPanel;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Widget;

public class CreateRelationProjectsPanel extends Composite {

	private static CreateRelationProjectsPanelUiBinder uiBinder = GWT.create(CreateRelationProjectsPanelUiBinder.class);

	interface CreateRelationProjectsPanelUiBinder extends UiBinder<Widget, CreateRelationProjectsPanel> {
	}

	@UiField
	HTMLPanel firstPanelContainer;

	@UiField
	HTMLPanel panelTitle;

	@UiField
	HTMLPanel secondPanelContainer;

	@UiField
	Alert firstItemAlert;

	@UiField
	Alert secondItemAlert;

	@UiField
	Button buttFirtProjectReset;

	@UiField
	Button buttSecondProjectReset;

	@UiField
	FlowPanel firstProjectPanelContainer;

	@UiField
	FlowPanel secondProjectPanelContainer;

	@UiField
	ListBox listBoxRelationNames;

	@UiField
	Button closeButton;

	@UiField
	Button buttCreateRelation;

	@UiField
	Alert alertMessage;

	private Map<String, RelationshipDefinitionDV> mapRelationsDefiniton = new HashMap<String, RelationshipDefinitionDV>();

	private HashMap<Integer, ResultDocumentDV> selectedProjects = new HashMap<Integer, ResultDocumentDV>(2);

	private HandlerManager appManagerBus;

	public CreateRelationProjectsPanel(HandlerManager appManagerBus, ResultDocumentDV project) {
		initWidget(uiBinder.createAndBindUi(this));
		this.appManagerBus = appManagerBus;

		closeButton.setType(ButtonType.LINK);
		closeButton.setIcon(IconType.REMOVE);
		closeButton.setIconSize(IconSize.LARGE);

		closeButton.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				appManagerBus.fireEvent(new CloseCreateRelationGUIEvent());
			}
		});

		listBoxRelationNames.setWidth("90%");

		GeoPortalDataEntryApp.geoportalDataEntryService.getRelationshipsDefinition(project.getProfileID(),
				new AsyncCallback<List<RelationshipDefinitionDV>>() {

					@Override
					public void onSuccess(List<RelationshipDefinitionDV> relationshipNames) {

						if (relationshipNames == null || relationshipNames.size() == 0) {
							buttCreateRelation.setEnabled(false);
							alertMessage
									.setText("You cannot create a relationship. No configuration found in the UCD for: "
											+ project.getProfileID());
							alertMessage.setVisible(true);
							return;
						}

						for (RelationshipDefinitionDV relDef : relationshipNames) {
							listBoxRelationNames.addItem(relDef.getLabel(), relDef.getId());
							mapRelationsDefiniton.put(relDef.getId(), relDef);
						}

					}

					@Override
					public void onFailure(Throwable caught) {
						alertMessage.setText("Error on reading the relationship names. Error: " + caught.getMessage());
						return;

					}
				});

		firstItemAlert.add(new Image(Images.ICONS.oneFingerIcon()));
		firstItemAlert.setClose(false);
		secondItemAlert.add(new Image(Images.ICONS.oneFingerIcon()));
		secondItemAlert.setClose(false);

		buttFirtProjectReset.setType(ButtonType.LINK);
		buttFirtProjectReset.setIcon(IconType.REMOVE_SIGN);
		buttFirtProjectReset.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				selectedProjects.put(0, null);
				firstItemAlert.setVisible(true);
				firstProjectPanelContainer.clear();

			}
		});

		buttSecondProjectReset.setType(ButtonType.LINK);
		buttSecondProjectReset.setIcon(IconType.REMOVE_SIGN);

		buttSecondProjectReset.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				selectedProjects.put(1, null);
				secondItemAlert.setVisible(true);
				secondProjectPanelContainer.clear();

			}
		});

		buttCreateRelation.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				boolean isValidForm = checkFormPassed();
				if (isValidForm) {
					buttCreateRelation.setEnabled(false);

					RelationshipDefinitionDV relationSelected = mapRelationsDefiniton
							.get(listBoxRelationNames.getSelectedValue());

					appManagerBus.fireEvent(new RelationActionHandlerEvent(selectedProjects.get(0), relationSelected,
							selectedProjects.get(1)));
				}

			}
		});

		selectedProject(project);

	}

	public boolean checkFormPassed() {

		alertMessage.setVisible(false);

		ResultDocumentDV first = selectedProjects.get(0);
		if (first == null) {
			alertMessage.setVisible(true);
			alertMessage.setText("You must select the first project from the 'List of Project' Table");
			return false;
		}

		ResultDocumentDV second = selectedProjects.get(1);
		if (second == null) {
			alertMessage.setVisible(true);
			alertMessage.setText("You must select the second project from the 'List of Project' Table");
			return false;
		}

		if (first.getId().compareTo(second.getId()) == 0) {
			alertMessage.setVisible(true);
			alertMessage.setText("First and Second project cannot be the same project");
			return false;
		}

		return true;

	}

	public void selectedProject(ResultDocumentDV project) {

		ResultDocumentDV first = selectedProjects.get(0);
		
		String htmlMsg = "Selected: <b>" + project.getFirstEntryOfMap().getValue() + "</b> (id: " + project.getId()
				+ ")";

		if (first == null) {
			firstItemAlert.setVisible(false);
			selectedProjects.put(0, project);
			FlexTable flex = new FlexTable();
			flex.setWidget(0, 0, new HTML(htmlMsg));
			FlowPanel temporalContainer = new FlowPanel();
			flex.setWidget(1, 0, temporalContainer);
			firstProjectPanelContainer.add(flex);
			addTemportalReferenceToPanel(temporalContainer, project.getProfileID(), project.getProjectID());
			ReportTemplateToHTML rtth = new ReportTemplateToHTML("", project.getDocumentAsJSON(), false, false);
			rtth.showAsJSON(false);
			firstProjectPanelContainer.add(rtth);

			return;
		}

		ResultDocumentDV second = selectedProjects.get(1);

		if (second == null) {
			secondItemAlert.setVisible(false);
			selectedProjects.put(1, project);
			FlexTable flex = new FlexTable();
			flex.setWidget(0, 0, new HTML(htmlMsg));
			FlowPanel temporalContainer = new FlowPanel();
			flex.setWidget(1, 0, temporalContainer);
			addTemportalReferenceToPanel(temporalContainer, project.getProfileID(), project.getProjectID());
			secondProjectPanelContainer.add(flex);
			ReportTemplateToHTML rtth = new ReportTemplateToHTML("", project.getDocumentAsJSON(), false, false);
			rtth.showAsJSON(false);
			secondProjectPanelContainer.add(rtth);
		}

	}

	private void addTemportalReferenceToPanel(final ComplexPanel panelContainer, String profileID, String projectID) {

		GeoportalDataEntryServiceAsync.Util.getInstance().temporalReferenceForProject(profileID, projectID,
				new AsyncCallback<TemporalReferenceDV>() {

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub

					}

					@Override
					public void onSuccess(TemporalReferenceDV result) {
						if (result != null) {
							panelContainer.add(new HTML(HTMLUtil.toHTMLCode(result)));
						}

					}
				});
	}

}
