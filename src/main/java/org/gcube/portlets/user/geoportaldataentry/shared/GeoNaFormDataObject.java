package org.gcube.portlets.user.geoportaldataentry.shared;

import java.io.Serializable;
import java.util.List;

import org.gcube.application.geoportalcommon.shared.geoportal.config.GcubeProfileDV;
import org.gcube.portlets.widgets.mpformbuilder.shared.GenericDatasetBean;

public class GeoNaFormDataObject implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1084036362952974474L;
	private List<GenericDatasetBean> listGDB;
	private GcubeProfileDV gcubeProfileDV;

	public GeoNaFormDataObject() {

	}

	public GeoNaFormDataObject(List<GenericDatasetBean> listGDB, GcubeProfileDV gcubeProfileDV) {
		super();
		this.listGDB = listGDB;
		this.gcubeProfileDV = gcubeProfileDV;
	}

	public List<GenericDatasetBean> getListGDB() {
		return listGDB;
	}

	public void setListGDB(List<GenericDatasetBean> listGDB) {
		this.listGDB = listGDB;
	}

	public GcubeProfileDV getGcubeProfileDV() {
		return gcubeProfileDV;
	}

	public void setGcubeProfileDV(GcubeProfileDV gcubeProfileDV) {
		this.gcubeProfileDV = gcubeProfileDV;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("GeoNaFormDataObject [listGDB=");
		builder.append(listGDB);
		builder.append(", gcubeProfileDV=");
		builder.append(gcubeProfileDV);
		builder.append("]");
		return builder.toString();
	}

}
