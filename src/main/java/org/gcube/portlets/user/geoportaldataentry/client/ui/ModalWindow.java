package org.gcube.portlets.user.geoportaldataentry.client.ui;

import com.github.gwtbootstrap.client.ui.Alert;
import com.github.gwtbootstrap.client.ui.Button;
import com.github.gwtbootstrap.client.ui.Modal;
import com.github.gwtbootstrap.client.ui.ModalFooter;
import com.github.gwtbootstrap.client.ui.constants.AlertType;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;

public class ModalWindow {

	private HorizontalPanel hp = new HorizontalPanel();
	private Modal modal;

	public ModalWindow(Image icon, String title, String msg, AlertType alertType) {

		modal = new Modal(true,true);
		modal.hide(false);
		modal.setTitle(title);
		modal.setCloseVisible(true);
		

		Alert alert = new Alert();
		alert.setType(alertType);
		alert.setClose(false);
		alert.setText(msg);
		alert.getElement().getStyle().setMarginLeft(10, Unit.PX);
		
		
		if (icon != null)
			hp.add(icon);
		hp.add(alert);

		ModalFooter modalFooter = new ModalFooter();
		final Button buttClose = new Button("Close");
		modalFooter.add(buttClose);

		buttClose.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				modal.hide();
			}
		});

		modal.add(hp);
		modal.add(modalFooter);
	}

	public void show() {
		modal.show();
	}

}
