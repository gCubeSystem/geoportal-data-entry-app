package org.gcube.portlets.user.geoportaldataentry.shared;

import java.io.Serializable;

public class DocumentConfigDV implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7538079431272352662L;
	private String id;
	private String type;
	private String itemType;
	private ConfigHandlerDV configuration;

	public DocumentConfigDV() {
	}

	public String getId() {
		return id;
	}

	public String getType() {
		return type;
	}

	public ConfigHandlerDV getConfiguration() {
		return configuration;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getItemType() {
		return itemType;
	}

	public void setItemType(String itemType) {
		this.itemType = itemType;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setConfiguration(ConfigHandlerDV configuration) {
		this.configuration = configuration;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("DocumentConfigDV [id=");
		builder.append(id);
		builder.append(", type=");
		builder.append(type);
		builder.append(", itemType=");
		builder.append(itemType);
		builder.append(", configuration=");
		builder.append(configuration);
		builder.append("]");
		return builder.toString();
	}

}
