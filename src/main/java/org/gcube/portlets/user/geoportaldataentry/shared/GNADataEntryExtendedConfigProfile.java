package org.gcube.portlets.user.geoportaldataentry.shared;

import java.io.Serializable;

import org.gcube.application.geoportalcommon.shared.GNADataEntryConfigProfile;


/**
 * The Class GNADataEntryExtendedConfigProfile.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 * Sep 26, 2022
 */
public class GNADataEntryExtendedConfigProfile extends GNADataEntryConfigProfile implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3185317083819447952L;
	private UserRights userRights = null;

	/**
	 * Instantiates a new GNA data entry config profile.
	 */
	public GNADataEntryExtendedConfigProfile() {

	}

	/**
	 * Gets the user rights.
	 *
	 * @return the user rights
	 */
	public UserRights getUserRights() {
		return userRights;
	}

	/**
	 * Sets the user rights.
	 *
	 * @param userRights the new user rights
	 */
	public void setUserRights(UserRights userRights) {
		this.userRights = userRights;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("GNADataEntryExtendedConfigProfile [userRights=");
		builder.append(userRights);
		builder.append("]");
		return builder.toString();
	}

}
