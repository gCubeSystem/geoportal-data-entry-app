package org.gcube.portlets.user.geoportaldataentry.client.ui.action;

import org.gcube.application.geoportalcommon.shared.geoportal.ResultDocumentDV;
import org.gcube.application.geoportalcommon.shared.geoportal.config.ActionDefinitionDV;
import org.gcube.portlets.user.geoportaldataentry.client.ui.report.ReportTemplateToHTML;

import com.github.gwtbootstrap.client.ui.AccordionGroup;
import com.github.gwtbootstrap.client.ui.TextArea;
import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Widget;

public class StepViewPanel extends Composite {

	private static StepViewPanelUiBinder uiBinder = GWT.create(StepViewPanelUiBinder.class);

	interface StepViewPanelUiBinder extends UiBinder<Widget, StepViewPanel> {
	}

	@UiField
	FlowPanel principalPanel;

	@UiField
	AccordionGroup accordionProjectDetails;

	@UiField
	FlowPanel confirmProceedPanel;

	@UiField
	TextArea txtOptionalMessage;

	private ResultDocumentDV resultDocumentDV;

	private ActionDefinitionDV actionDefinitionDV;

	public StepViewPanel(ResultDocumentDV resultDocumentDV, ActionDefinitionDV actionDefinitionDV) {
		initWidget(uiBinder.createAndBindUi(this));
		this.resultDocumentDV = resultDocumentDV;
		this.actionDefinitionDV = actionDefinitionDV;
		builtUI();
	}

	private void builtUI() {
		String htmlMsg = "<p style='font-size: 18px'>Going to perform the step/s <i><b>" + actionDefinitionDV.getCallSteps() + "</b></i> on the: </p>";
		// TODO Auto-generated method stub
		principalPanel.add(new HTML(htmlMsg));
		ReportTemplateToHTML rt = new ReportTemplateToHTML("Project", resultDocumentDV.getDocumentAsJSON(), false);
		rt.showAsJSON(false);
		principalPanel.add(rt);

		String projectDetails = "<ul>";
		projectDetails += "<li>id: " + resultDocumentDV.getId() + "</li>";
		projectDetails += "<li>profile: " + resultDocumentDV.getProfileID() + "</li>";
		projectDetails += "<li>" + resultDocumentDV.getFirstEntryOfMap().getKey() + ": "
				+ resultDocumentDV.getFirstEntryOfMap().getValue() + "</li>";
		projectDetails += "</ul>";

		accordionProjectDetails.add(new HTML(projectDetails));

		String confirmMessage = "<b>Would you like to proceed?</b>";
		confirmProceedPanel.add(new HTML(confirmMessage));
	}

	public String getOptionalMessage() {
		return txtOptionalMessage.getText();
	}

}
