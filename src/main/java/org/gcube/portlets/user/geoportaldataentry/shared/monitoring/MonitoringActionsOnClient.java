package org.gcube.portlets.user.geoportaldataentry.shared.monitoring;

import java.io.Serializable;
import java.util.LinkedHashMap;

import org.gcube.portlets.user.geoportaldataentry.shared.CommitReport;

public class MonitoringActionsOnClient implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8517260295153833253L;

	private LinkedHashMap<Integer, MonitoringAction> monitor = new LinkedHashMap<Integer, MonitoringAction>();

	private Exception exception;

	private String monitorUUID;

	private CommitReport commitReport;

	private boolean monitoringTerminatedOnServer;

	public MonitoringActionsOnClient() {
	}

	public LinkedHashMap<Integer, MonitoringAction> getMonitor() {
		return monitor;
	}

	public Exception getException() {
		return exception;
	}

	public String getMonitorUUID() {
		return monitorUUID;
	}

	public CommitReport getCommitReport() {
		return commitReport;
	}

	public boolean isMonitoringTerminatedOnServer() {
		return monitoringTerminatedOnServer;
	}

	public void setMonitor(LinkedHashMap<Integer, MonitoringAction> monitor) {
		this.monitor = monitor;
	}

	public void setException(Exception exception) {
		this.exception = exception;
	}

	public void setMonitorUUID(String monitorUUID) {
		this.monitorUUID = monitorUUID;
	}

	public void setCommitReport(CommitReport commitReport) {
		this.commitReport = commitReport;
	}

	public void setMonitoringTerminatedOnServer(boolean monitoringTerminatedOnServer) {
		this.monitoringTerminatedOnServer = monitoringTerminatedOnServer;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("MonitoringActionsOnClient [monitor=");
		builder.append(monitor);
		builder.append(", exception=");
		builder.append(exception);
		builder.append(", monitorUUID=");
		builder.append(monitorUUID);
		builder.append(", commitReport=");
		builder.append(commitReport);
		builder.append(", monitoringTerminatedOnServer=");
		builder.append(monitoringTerminatedOnServer);
		builder.append("]");
		return builder.toString();
	}
	
	

}
