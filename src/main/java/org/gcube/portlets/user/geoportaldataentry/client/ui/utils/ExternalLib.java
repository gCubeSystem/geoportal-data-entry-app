package org.gcube.portlets.user.geoportaldataentry.client.ui.utils;

/**
 * The Class ExternalLib.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Sep 17, 2021
 */
public class ExternalLib {

	/**
	 * To pretty print JSON. calls the externa js lib 'pretty-print-json'
	 * 
	 * @param jsonData the json data
	 * @return the string
	 */
	public static native String toPrettyPrintJSON(String jsonData)/*-{
		//console.log(jsonData);
		var objJSON = JSON.parse(jsonData);
		return $wnd.prettyPrintJson.toHtml(objJSON);
	}-*/;
}
