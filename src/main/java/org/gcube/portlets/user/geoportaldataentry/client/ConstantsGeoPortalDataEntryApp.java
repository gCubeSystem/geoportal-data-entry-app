package org.gcube.portlets.user.geoportaldataentry.client;

import java.util.Arrays;
import java.util.List;

import org.gcube.application.geoportalcommon.shared.geoportal.WORKFLOW_PHASE;
import org.gcube.application.geoportalcommon.shared.geoportal.ucd.GEOPORTAL_DATA_HANDLER;
import org.gcube.application.geoportalcommon.shared.geoportal.ucd.HandlerDeclarationDV;
import org.gcube.application.geoportalcommon.shared.geoportal.ucd.UseCaseDescriptorDV;
import org.gcube.portlets.widgets.mpformbuilder.client.ConstantsMPFormBuilder;

import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.RootPanel;

/**
 * The Class ConstantsGeoPortalDataEntryApp.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Aug 6, 2021
 */
public class ConstantsGeoPortalDataEntryApp {

	public static final String DIV_PORTLET_ID = "geoportal-data-entry";

	public static final String DIV_LOADERS_ID = "geoportal-loaders";
	
	public static final String GET_PARAMETER_ITEM_TYPE = "git";
	
	public static final String GET_PARAMETER_ITEM_ID = "gid";
	
	public static final String GET_PARAMETER_ACTION = "act";

	public static final String DATE_FORMAT = ConstantsMPFormBuilder.DATE_FORMAT;

	public static final String TIME_FORMAT = ConstantsMPFormBuilder.TIME_FORMAT;

	public static final String HOURS_MINUTES_SEPARATOR = ConstantsMPFormBuilder.HOURS_MINUTES_SEPARATOR;

	public static final int MAX_COLUMN_DISPLAYED_IN_THE_TABLE = 5;

	public static final int PAGINATION_SIZE = 20;

	public static final String DEFAULT_DOCUMENT_PROJECTION_NAME = "_theDocument";

	public static final String CSS_CLASS_ANIMATE_FADE_IN_OUT = "animate-fadeInOut";

	public static final RootPanel ROOT_PANEL_DIV_LOADERS = RootPanel.get(ConstantsGeoPortalDataEntryApp.DIV_LOADERS_ID);

	public static final RootPanel ROOT_PANEL_DIV_PORTLET = RootPanel.get(ConstantsGeoPortalDataEntryApp.DIV_PORTLET_ID);

	public static final String CSS_CLASS_GEOPORTAL_LOADERS_CENTER = "geoportal-loaders-center";

	public static final String WORKFLOW_ACTION_POST_CREATION_ACTION_ID = "post_creation_action";

	public static final DateTimeFormat DATE_TIME_FORMAT = DateTimeFormat.getFormat("dd MMMM yyyy");
	
	public static final String SHOW_ON_MAP_NOT_AVAILABLE_IN_DRAFT = "The 'Show on Map' facility is not available in DRAFT phase";

	public static final String ALERT_MESSAGE_PROJECT_NOT_EDITABLE = "A Project can only be updated in "
			+ WORKFLOW_PHASE.DRAFT.name()
			+ " phase. You need to perform the step 'Reject' or 'UnPublish' to take back the project in "
			+ WORKFLOW_PHASE.DRAFT.name() + " phase.";

	public static final String ALERT_MESSAGE_CREATE_RELATION_FORBIDDEN = "The Create Relation operation can be performed only in "
			+ WORKFLOW_PHASE.DRAFT.name()
			+ " phase. You need to perform the step 'Reject' or 'UnPublish' to take back the project in "
			+ WORKFLOW_PHASE.DRAFT.name() + " phase.";
	
	
	public static final String ALERT_MESSAGE_DELETE_RELATION_FORBIDDEN = "The Delete Relation operation can be performed only in "
			+ WORKFLOW_PHASE.DRAFT.name()
			+ " phase. You need to perform the step 'Reject' or 'UnPublish' to take back the project in "
			+ WORKFLOW_PHASE.DRAFT.name() + " phase.";

	/**
	 * The Enum ACTION_PERFORMED_ON_ITEM.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
	 * 
	 *         Nov 25, 2021
	 */
	public enum ACTION_PERFORMED_ON_ITEM {
		UPDATED_PROJECT
	}

	public static final List<String> HANDLERS_IDS = Arrays.asList(GEOPORTAL_DATA_HANDLER.geoportal_data_list.getId(),
			GEOPORTAL_DATA_HANDLER.geoportal_data_entry.getId());

	public static void printUCDs(List<UseCaseDescriptorDV> listUCDescriptors) {
		GWT.log("print - UCDs");
		for (UseCaseDescriptorDV useCaseDescriptorDV : listUCDescriptors) {
			GWT.log("print UCD name: " + useCaseDescriptorDV.getName() + ", profileID: "
					+ useCaseDescriptorDV.getProfileID());
			for (HandlerDeclarationDV handler : useCaseDescriptorDV.getHandlers()) {
				GWT.log("\t print handler getDataHandlerType: " + handler.getDataHandlerType() + ", type: "
						+ handler.getType());
			}
		}
	}
}
