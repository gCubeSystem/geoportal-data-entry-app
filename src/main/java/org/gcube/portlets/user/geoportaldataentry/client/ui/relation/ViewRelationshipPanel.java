package org.gcube.portlets.user.geoportaldataentry.client.ui.relation;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.gcube.application.geoportalcommon.shared.config.OPERATION_ON_ITEM;
import org.gcube.application.geoportalcommon.shared.geoportal.ResultDocumentDV;
import org.gcube.application.geoportalcommon.shared.geoportal.WORKFLOW_PHASE;
import org.gcube.application.geoportalcommon.shared.geoportal.project.RelationshipDV;
import org.gcube.application.geoportalcommon.shared.geoportal.project.TemporalReferenceDV;
import org.gcube.portlets.user.geoportaldataentry.client.ConstantsGeoPortalDataEntryApp;
import org.gcube.portlets.user.geoportaldataentry.client.GeoportalDataEntryServiceAsync;
import org.gcube.portlets.user.geoportaldataentry.client.events.CloseCreateRelationGUIEvent;
import org.gcube.portlets.user.geoportaldataentry.client.events.RelationActionHandlerEvent;
import org.gcube.portlets.user.geoportaldataentry.client.resource.Images;
import org.gcube.portlets.user.geoportaldataentry.client.ui.ModalWindow;
import org.gcube.portlets.user.geoportaldataentry.client.ui.report.ReportTemplateToHTML;
import org.gcube.portlets.user.geoportaldataentry.client.ui.utils.HTMLUtil;
import org.gcube.portlets.user.geoportaldataentry.client.ui.utils.LoaderIcon;

import com.github.gwtbootstrap.client.ui.Button;
import com.github.gwtbootstrap.client.ui.Label;
import com.github.gwtbootstrap.client.ui.Modal;
import com.github.gwtbootstrap.client.ui.constants.AlertType;
import com.github.gwtbootstrap.client.ui.constants.ButtonType;
import com.github.gwtbootstrap.client.ui.constants.IconSize;
import com.github.gwtbootstrap.client.ui.constants.IconType;
import com.github.gwtbootstrap.client.ui.constants.LabelType;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.ComplexPanel;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Widget;

public class ViewRelationshipPanel extends Composite {

	private static ViewRelationshipPanelUiBinder uiBinder = GWT.create(ViewRelationshipPanelUiBinder.class);

	interface ViewRelationshipPanelUiBinder extends UiBinder<Widget, ViewRelationshipPanel> {
	}

	private String profileID;

	@UiField
	HTMLPanel firstPanelContainer;

	@UiField
	HTMLPanel panelTitle;

	@UiField
	HTMLPanel secondPanelContainer;

	@UiField
	FlowPanel firstProjectPanelContainer;

	@UiField
	FlowPanel secondProjectPanelContainer;

	@UiField
	Button closeButton;

	@UiField
	Button buttonExpand;

	private HashMap<Integer, ResultDocumentDV> selectedProjects = new HashMap<Integer, ResultDocumentDV>(2);

	private HandlerManager appManagerBus;

	private Map<String, ResultDocumentDV> mapOfTargetProjectForId = new HashMap<String, ResultDocumentDV>();

	private ResultDocumentDV fromTheProject;

	public ViewRelationshipPanel(HandlerManager appManagerBus, final ResultDocumentDV fromProject, boolean showExpand) {
		initWidget(uiBinder.createAndBindUi(this));
		this.appManagerBus = appManagerBus;
		this.fromTheProject = fromProject;
		closeButton.setType(ButtonType.LINK);
		closeButton.setIcon(IconType.REMOVE);
		closeButton.setIconSize(IconSize.LARGE);

		closeButton.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				appManagerBus.fireEvent(new CloseCreateRelationGUIEvent());
			}
		});

		buttonExpand.setType(ButtonType.LINK);
		buttonExpand.setIcon(IconType.EXPAND);
		buttonExpand.setTitle("Show this view in new Window");
		buttonExpand.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {

				Modal mw = new Modal(true, true);
				mw.setTitle("Relationships");
				mw.getElement().addClassName("no_modal_body_max_height");

				int width = 0;
				int heigth = 0;
				try {
					width = Window.getClientWidth() * 75 / 100;
					heigth = Window.getClientHeight() * 75 / 100;
				} catch (Exception e) {
					// TODO: handle exception
				}

				if (width > 800) {
					mw.setWidth(width);
				}
				
				if(heigth > 600) {
					mw.setHeight(heigth+"px");
				}

				mw.add(new ViewRelationshipPanel(appManagerBus, fromTheProject, false));
				mw.show();

			}
		});

		if (!showExpand) {
			buttonExpand.setVisible(false);
			panelTitle.setVisible(false);
			closeButton.setVisible(false);
		}

		showRelationsOf(fromProject);
	}

	public void showRelationsOf(ResultDocumentDV project) {
		firstProjectPanelContainer.clear();
		secondProjectPanelContainer.clear();
		this.fromTheProject = project;

		Entry<String, Object> firstEntrySet = project.getFirstEntryOfMap();
		String htmlMsg = firstEntrySet.getKey() + ": <b>" + firstEntrySet.getValue() + "</b> (id: " + project.getId()
				+ ")";
		
		FlexTable flex = new FlexTable();
		flex.setWidget(0, 0, new HTML(htmlMsg));
		FlowPanel fromTemporalContainer = new FlowPanel();
		flex.setWidget(1, 0, fromTemporalContainer);
		firstProjectPanelContainer.add(flex);
		addTemportalReferenceToPanel(fromTemporalContainer, project.getProfileID(), project.getProjectID());

		//firstProjectPanelContainer.add(new HTML(htmlMsg));
		ReportTemplateToHTML rtth = new ReportTemplateToHTML("", project.getDocumentAsJSON(), false, false);
		rtth.showAsJSON(false);
		firstProjectPanelContainer.add(rtth);

		HTML labelNoRelations = new HTML("No relationship/s found");
		secondProjectPanelContainer.add(labelNoRelations);

		if (project.getListRelationshipDV() != null && project.getListRelationshipDV().size() > 0) {

			secondProjectPanelContainer.clear();

			for (RelationshipDV relationDV : project.getListRelationshipDV()) {

				final FlexTable flexTable = new FlexTable();
				flexTable.getElement().addClassName("box-table-diplay-project");
				Label label = new Label();
				label.setType(LabelType.INFO);
				label.setText(relationDV.getRelationshipName());
				
				FlowPanel panelContainer = new FlowPanel();
				Button deleteRelation = new Button("", IconType.TRASH);
				deleteRelation.setTitle("Delete this relation");
				deleteRelation.setType(ButtonType.LINK);
				deleteRelation.addClickHandler(new ClickHandler() {

					@Override
					public void onClick(ClickEvent event) {
						
						// #24571
						boolean isNotInDRAFT = false;

						if (fromTheProject.getLifecycleInfo() != null) {
							String phase = fromTheProject.getLifecycleInfo().getPhase();
							// IF the project is not in DRAFT, showing an alert and the no Update Mode will
							// be activated
							if (phase != null && phase.compareToIgnoreCase(WORKFLOW_PHASE.DRAFT.getLabel()) != 0) {

								String msg = ConstantsGeoPortalDataEntryApp.ALERT_MESSAGE_DELETE_RELATION_FORBIDDEN;
								ModalWindow modalW = new ModalWindow(new Image(Images.ICONS.accessDenied()),
										"Forbidden: " + OPERATION_ON_ITEM.DELETE_RELATION, msg, AlertType.WARNING);
								modalW.show();

								isNotInDRAFT = true;
							}
						}
						
						//If the project is in DRAFT, going to delete the releation after confirm
						if(!isNotInDRAFT) {
							ResultDocumentDV toProject = mapOfTargetProjectForId.get(relationDV.getTargetUCD());
							appManagerBus.fireEvent(
									new RelationActionHandlerEvent(project, relationDV.getRelationshipName(), toProject));
						}

					}
				});

				panelContainer.add(label);
				panelContainer.add(deleteRelation);
				flexTable.setWidget(0, 0, panelContainer);

				flexTable.setWidget(1, 0, new LoaderIcon("loading project.."));
				GeoportalDataEntryServiceAsync.Util.getInstance().getResultDocumentFoProjectByID(
						relationDV.getTargetUCD(), relationDV.getTargetID(), new AsyncCallback<ResultDocumentDV>() {

							@Override
							public void onFailure(Throwable caught) {
								flexTable.setWidget(1, 0, new HTML(caught.getMessage()));

							}

							@Override
							public void onSuccess(ResultDocumentDV result) {
								mapOfTargetProjectForId.put(relationDV.getTargetUCD(), result);
								Entry<String, Object> firstEntrySet = result.getFirstEntryOfMap();
								String htmlMsg = firstEntrySet.getKey() + ": <b>" + firstEntrySet.getValue()
										+ "</b> (id: " + result.getId() + ")";

								flexTable.setWidget(1, 0, new HTML(htmlMsg));
								FlowPanel toTemporalContainer = new FlowPanel();
								flexTable.setWidget(2, 0, toTemporalContainer);
								addTemportalReferenceToPanel(toTemporalContainer, result.getProfileID(), result.getProjectID());

								ReportTemplateToHTML rtth2 = new ReportTemplateToHTML("", result.getDocumentAsJSON(),
										false, false);
								rtth2.showAsJSON(false);

								flexTable.setWidget(3, 0, rtth2);
							}
						});

				secondProjectPanelContainer.add(flexTable);
			}

		}

	}
	
	private void addTemportalReferenceToPanel(final ComplexPanel panelContainer, String profileID, String projectID) {

		GeoportalDataEntryServiceAsync.Util.getInstance().temporalReferenceForProject(profileID, projectID,
				new AsyncCallback<TemporalReferenceDV>() {

					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub

					}

					@Override
					public void onSuccess(TemporalReferenceDV result) {
						if (result != null) {
							panelContainer.add(new HTML(HTMLUtil.toHTMLCode(result)));
						}

					}
				});
	}

}
