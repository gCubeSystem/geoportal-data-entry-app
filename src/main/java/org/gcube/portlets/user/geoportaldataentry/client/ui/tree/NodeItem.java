package org.gcube.portlets.user.geoportaldataentry.client.ui.tree;

import org.gcube.portlets.user.geoportaldataentry.client.ui.card.GeoNaFormCardModel;
import org.gcube.portlets.widgets.mpformbuilder.client.form.generic.CreateMetadataForm;
import org.gcube.portlets.widgets.mpformbuilder.client.form.generic.GenericFormEvents.GenericFormEventsListener;
import org.gcube.portlets.widgets.mpformbuilder.shared.GenericDatasetBean;

import com.github.gwtbootstrap.client.ui.Button;
import com.github.gwtbootstrap.client.ui.Icon;
import com.github.gwtbootstrap.client.ui.constants.IconType;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.TreeItem;
import com.google.gwt.user.client.ui.Widget;

public class NodeItem extends Composite {

	private static NodeItemUiBinder uiBinder = GWT.create(NodeItemUiBinder.class);

	interface NodeItemUiBinder extends UiBinder<Widget, NodeItem> {
	}

	@UiField
	Icon iconField;

	@UiField
	HTML htmlNode;

	@UiField
	Button buttonAdd;

	@UiField
	Button buttonRemove;

	private GeoNaFormCardModel geoNaFormCardModel;

	private TreeItem parentTreeItem;

	private String jsonSectionFullPath;

	private boolean canBeDuplicated;

	private boolean canBeDeleted;

	private boolean isRoot = false;

	private MetadataFormCardEventHandler formCardEventHandler = new MetadataFormCardEventHandler();

	private String nodeHTMLLabel;

	public NodeItem(TreeItem parent, String nodeHTMLLabel, GeoNaFormCardModel geoNaFormCardModel,
			boolean canBeDuplicated, boolean canBeDeleted, String jsonSectionFullPath) {
		initWidget(uiBinder.createAndBindUi(this));
		this.geoNaFormCardModel = geoNaFormCardModel;
		this.parentTreeItem = parent;
		this.jsonSectionFullPath = jsonSectionFullPath;
		this.canBeDuplicated = canBeDuplicated;
		this.canBeDeleted = canBeDeleted;
		this.nodeHTMLLabel = nodeHTMLLabel;
		htmlNode.setHTML(this.nodeHTMLLabel);
		buttonAdd.setVisible(false);
		buttonRemove.setVisible(false);

		if (canBeDuplicated) {
			buttonAdd.setIcon(IconType.COPY);
			// buttonAdd.getElement().getStyle().setProperty("pointerEvents", "none");
			buttonAdd.getElement().getStyle().setProperty("textDecoration", "none");
			buttonAdd.getElement().getStyle().setColor("#555");
			buttonAdd.setVisible(true);

		}

		if (canBeDeleted) {
			buttonRemove.setIcon(IconType.TRASH);
			// buttonRemove.getElement().getStyle().setProperty("pointerEvents", "none");
			buttonRemove.getElement().getStyle().setProperty("textDecoration", "none");
			buttonRemove.getElement().getStyle().setColor("#555");
			buttonRemove.setVisible(true);
		}

		htmlNode.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				// TODO Auto-generated method stub

			}
		});

		// Is not the root node
		if (geoNaFormCardModel != null) {
			geoNaFormCardModel.getMetadataForm().addListener(formCardEventHandler);
			resetNodeStatus();
		}
	}

	/**
	 * The Class MetadataFormCardEventHandler.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
	 * 
	 *         Oct 12, 2020
	 */
	private class MetadataFormCardEventHandler implements GenericFormEventsListener {

		/**
		 * On form data valid.
		 *
		 * @param genericDatasetBean the generic dataset bean
		 */
		@Override
		public void onFormDataValid(GenericDatasetBean genericDatasetBean) {
			setNodeStatus();

		}

		/**
		 * On form data edit.
		 */
		@Override
		public void onFormDataEdit() {
			resetNodeStatus();

		}

		/**
		 * On form aborted.
		 */
		@Override
		public void onFormAborted() {
			// TODO Auto-generated method stub

		}

		/**
		 * On validation error.
		 *
		 * @param throwable the throwable
		 * @param errorMsg  the error msg
		 */
		@Override
		public void onValidationError(Throwable throwable, String errorMsg) {
			// TODO Auto-generated method stub

		}
	}

	/**
	 * Validate form.
	 *
	 * @return true, if successful
	 */
	public boolean validateForm() {

		if (isRoot) {
			return true;
		}

		CreateMetadataForm createMetadataForm = geoNaFormCardModel.getMetadataForm();
		boolean isFormDataValid = createMetadataForm.isFormDataValid();
		GWT.log("Is form data valid: " + isFormDataValid);
		if (!isFormDataValid) {
			return false;
		}

		return true;
	}

	/**
	 * Sets the valid card.
	 *
	 * @param bool the new valid card
	 */
	public void setValidCard(boolean bool) {
		if (bool) {
			iconField.setIcon(IconType.OK_SIGN);
			iconField.getElement().removeClassName("red-text");
			iconField.getElement().addClassName("green-text");
			htmlNode.getElement().removeClassName("red-text");
			htmlNode.getElement().addClassName("green-text");
		} else {
			iconField.setIcon(IconType.MINUS_SIGN);
			iconField.getElement().removeClassName("green-text");
			iconField.getElement().addClassName("red-text");
			htmlNode.getElement().removeClassName("green-text");
			htmlNode.getElement().addClassName("red-text");
		}

	}

	/**
	 * Sets the tab status.
	 */
	private void setNodeStatus() {
		boolean isValid = validateForm();
		if (isValid) {
			iconField.setIcon(IconType.OK_SIGN);
			iconField.getElement().removeClassName("red-text");
			iconField.getElement().addClassName("green-text");
			htmlNode.getElement().removeClassName("red-text");
			htmlNode.getElement().addClassName("green-text");
		} else {
			iconField.setIcon(IconType.MINUS_SIGN);
			iconField.getElement().removeClassName("green-text");
			iconField.getElement().addClassName("red-text");
			htmlNode.getElement().removeClassName("green-text");
			htmlNode.getElement().addClassName("red-text");
		}
	}

	public boolean isRoot() {
		return isRoot;
	}

	public void setRoot(boolean isRoot) {
		this.isRoot = isRoot;
	}

	private void resetNodeStatus() {
		iconField.setIcon(null);
	}

	public TreeItem getParentTreeItem() {
		return parentTreeItem;
	}

	public GeoNaFormCardModel getGeoNaFormCardModel() {
		return geoNaFormCardModel;
	}

	/**
	 * Gets the json section full path.
	 *
	 * @return the json section full path
	 */
	public String getJsonSectionFullPath() {
		return jsonSectionFullPath;
	}

	public boolean isCanBeDeleted() {
		return canBeDeleted;
	}

	public boolean isCanBeDuplicated() {
		return canBeDuplicated;
	}

	public HTML getHtmlNode() {
		return htmlNode;
	}

	public String getNodeHTMLLabel() {
		return nodeHTMLLabel;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("NodeItem [jsonSectionFullPath=");
		builder.append(jsonSectionFullPath);
		builder.append(", canBeDuplicated=");
		builder.append(canBeDuplicated);
		builder.append(", canBeDeleted=");
		builder.append(canBeDeleted);
		builder.append(", isRoot=");
		builder.append(isRoot);
		builder.append(", nodeHTMLLabel=");
		builder.append(nodeHTMLLabel);
		builder.append("]");
		return builder.toString();
	}

}
