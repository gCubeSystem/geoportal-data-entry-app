package org.gcube.portlets.user.geoportaldataentry.client.events;

import org.gcube.portlets.user.geoportaldataentry.shared.GeoNaFormDataObject;
import org.gcube.portlets.user.geoportaldataentry.shared.Tree_Node;

import com.google.gwt.event.shared.GwtEvent;

// TODO: Auto-generated Javadoc
/**
 * The Class SaveGeonaDataFormsEvent.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 *         Oct 20, 2020
 */
public class SaveGeonaDataFormsEvent extends GwtEvent<SaveGeonaDataFormsHandler> {

	/** The type. */
	public static Type<SaveGeonaDataFormsHandler> TYPE = new Type<SaveGeonaDataFormsHandler>();
	private Tree_Node<GeoNaFormDataObject> treeNode;
	private String profileID;

	/**
	 * Instantiates a new save geona data forms event.
	 *
	 * @param profileID the profile ID
	 * @param treeNode  the tree node
	 */
	public SaveGeonaDataFormsEvent(String profileID, Tree_Node<GeoNaFormDataObject> treeNode) {
		this.treeNode = treeNode;
		this.profileID = profileID;
	}

	/**
	 * Gets the associated type.
	 *
	 * @return the associated type
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.google.gwt.event.shared.GwtEvent#getAssociatedType()
	 */
	@Override
	public Type<SaveGeonaDataFormsHandler> getAssociatedType() {
		return TYPE;
	}

	/**
	 * Dispatch.
	 *
	 * @param handler the handler
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.google.gwt.event.shared.GwtEvent#dispatch(com.google.gwt.event.shared.
	 * EventHandler)
	 */
	@Override
	protected void dispatch(SaveGeonaDataFormsHandler handler) {
		handler.onSave(this);
	}

	public Tree_Node<GeoNaFormDataObject> getTreeNode() {
		return treeNode;
	}

	/**
	 * Gets the profile ID.
	 *
	 * @return the profile ID
	 */
	public String getProfileID() {
		return profileID;
	}

}
