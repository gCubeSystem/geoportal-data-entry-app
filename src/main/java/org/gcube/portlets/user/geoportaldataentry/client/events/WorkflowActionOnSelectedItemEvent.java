package org.gcube.portlets.user.geoportaldataentry.client.events;

import org.gcube.application.geoportalcommon.shared.geoportal.DocumentDV;
import org.gcube.application.geoportalcommon.shared.geoportal.config.ActionDefinitionDV;

import com.google.gwt.event.shared.GwtEvent;

public class WorkflowActionOnSelectedItemEvent<T extends DocumentDV> extends GwtEvent<WorkflowActionOnSelectedItemEventHandler> {
	public static Type<WorkflowActionOnSelectedItemEventHandler> TYPE = new Type<WorkflowActionOnSelectedItemEventHandler>();
	private ActionDefinitionDV action;

	public WorkflowActionOnSelectedItemEvent(ActionDefinitionDV doAction) {
		this.action = doAction;
	}

	/**
	 * Gets the associated type.
	 *
	 * @return the associated type
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.google.gwt.event.shared.GwtEvent#getAssociatedType()
	 */
	@Override
	public Type<WorkflowActionOnSelectedItemEventHandler> getAssociatedType() {
		return TYPE;
	}

	/**
	 * Dispatch.
	 *
	 * @param handler the handler
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.google.gwt.event.shared.GwtEvent#dispatch(com.google.gwt.event.shared.
	 * EventHandler)
	 */
	@Override
	protected void dispatch(WorkflowActionOnSelectedItemEventHandler handler) {
		handler.onDoActionFired(this);
	}

	/**
	 * Gets the action.
	 *
	 * @return the action
	 */
	public ActionDefinitionDV getAction() {
		return action;
	}

}
