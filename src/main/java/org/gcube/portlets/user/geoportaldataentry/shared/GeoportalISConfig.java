package org.gcube.portlets.user.geoportaldataentry.shared;

import java.io.Serializable;

/**
 * The Class GeoportalISConfig.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Sep 26, 2022
 */
public class GeoportalISConfig implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4187839053354198268L;
	private String genericResSecondaryType;
	private String scope;

	/**
	 * Instantiates a new geona IS config.
	 */
	public GeoportalISConfig() {
	}

	/**
	 * Instantiates a new geona IS config.
	 *
	 * @param gRSecondaryType the g R secondary type
	 * @param scope           the scope
	 */
	public GeoportalISConfig(String gRSecondaryType, String scope) {
		super();
		this.genericResSecondaryType = gRSecondaryType;
		this.scope = scope;
	}

	/**
	 * Gets the generic resource secondary type.
	 *
	 * @return the generic resource secondary type
	 */
	public String getGenericResourceSecondaryType() {
		return genericResSecondaryType;
	}

	/**
	 * Sets the generic resource secondary type.
	 *
	 * @param gRSecondaryType the new generic resource secondary type
	 */
	public void setGenericResourceSecondaryType(String gRSecondaryType) {
		this.genericResSecondaryType = gRSecondaryType;
	}

	/**
	 * Gets the scope.
	 *
	 * @return the scope
	 */
	public String getScope() {
		return scope;
	}

	/**
	 * Sets the scope.
	 *
	 * @param scope the new scope
	 */
	public void setScope(String scope) {
		this.scope = scope;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("GeoportalISConfig [genericResSecondaryType=");
		builder.append(genericResSecondaryType);
		builder.append(", scope=");
		builder.append(scope);
		builder.append("]");
		return builder.toString();
	}

}
