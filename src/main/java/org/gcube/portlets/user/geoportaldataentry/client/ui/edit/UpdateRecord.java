package org.gcube.portlets.user.geoportaldataentry.client.ui.edit;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.gcube.application.geoportalcommon.shared.geoportal.DocumentDV;
import org.gcube.application.geoportalcommon.shared.geoportal.ResultDocumentDV;
import org.gcube.application.geoportalcommon.shared.geoportal.config.FilePathDV;
import org.gcube.application.geoportalcommon.shared.geoportal.config.GcubeProfileDV;
import org.gcube.application.geoportaldatamapper.shared.MetaDataProfileBeanExt;
import org.gcube.application.geoportaldatamapper.shared.ProjectEdit;
import org.gcube.portlets.user.geoportaldataentry.client.ConstantsGeoPortalDataEntryApp.ACTION_PERFORMED_ON_ITEM;
import org.gcube.portlets.user.geoportaldataentry.client.GeoPortalDataEntryApp;
import org.gcube.portlets.user.geoportaldataentry.client.GeoportalDataEntryServiceAsync;
import org.gcube.portlets.user.geoportaldataentry.client.events.OperationPerformedOnItemEvent;
import org.gcube.portlets.user.geoportaldataentry.client.ui.card.GeoNaFormCardModel;
import org.gcube.portlets.user.geoportaldataentry.client.ui.report.ReportTemplateToHTML;
import org.gcube.portlets.user.geoportaldataentry.client.ui.utils.LoaderIcon;
import org.gcube.portlets.user.geoportaldataentry.shared.CommitReport;
import org.gcube.portlets.user.geoportaldataentry.shared.GeoNaFormDataObject;
import org.gcube.portlets.widgets.mpformbuilder.client.form.generic.CreateMetadataForm.OPERATION;
import org.gcube.portlets.widgets.mpformbuilder.client.form.generic.GenericFormEvents.GenericFormEventsListener;
import org.gcube.portlets.widgets.mpformbuilder.shared.GenericDatasetBean;

import com.github.gwtbootstrap.client.ui.Alert;
import com.github.gwtbootstrap.client.ui.Button;
import com.github.gwtbootstrap.client.ui.ControlGroup;
import com.github.gwtbootstrap.client.ui.ListBox;
import com.github.gwtbootstrap.client.ui.Modal;
import com.github.gwtbootstrap.client.ui.constants.AlertType;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NodeList;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.Widget;

public class UpdateRecord extends Composite {

	private static UpdateRecordUiBinder uiBinder = GWT.create(UpdateRecordUiBinder.class);

	interface UpdateRecordUiBinder extends UiBinder<Widget, UpdateRecord> {
	}

	@UiField
	ListBox listBoxSections;

	@UiField
	ScrollPanel scrollSectionContent;

	@UiField
	HTMLPanel htmlPanelContainer;

	@UiField
	HTMLPanel alertHTMLPanel;

	@UiField
	ControlGroup controlsControlGroup;

	@UiField
	Button buttonUpdate;

	public static final String PLACEHOLDER_LIST_BOX = "Select section...";

	private LoaderIcon loaderProjectSections = new LoaderIcon("Loading Project sections... please wait");

	private GeoNaFormCardModel currentCardSelected;

	private String profileID;

	private String projectID;

	private HashMap<String, List<FilePathDV>> sectionPathFilePaths = new HashMap<>();

	private MetadataFormCardEventHandler formCardEventHandler = new MetadataFormCardEventHandler();

	private ProjectEdit projectEditDTO;
	
	private HandlerManager appManagerBus;

	public UpdateRecord(HandlerManager appManagerBus, String profileID, String projectID, int modalWidth, int modalHeight) {
		initWidget(uiBinder.createAndBindUi(this));
		this.appManagerBus = appManagerBus;
		this.profileID = profileID;
		this.projectID = projectID;
		setUpdateButtonEnabled(false);

		htmlPanelContainer.setVisible(false);

		alertHTMLPanel.add(loaderProjectSections);

		scrollSectionContent.setHeight((modalHeight-350)+"px");
		
		listBoxSections.setWidth((modalWidth-50)+"px");

		GeoportalDataEntryServiceAsync.Util.getInstance().getProjectEdit(profileID, projectID,
				new AsyncCallback<ProjectEdit>() {

					@Override
					public void onSuccess(ProjectEdit result) {
						projectEditDTO = result;
						htmlPanelContainer.setVisible(true);

						try {
							alertHTMLPanel.remove(loaderProjectSections);
						} catch (Exception e) {

						}

						listBoxSections.addItem(PLACEHOLDER_LIST_BOX, PLACEHOLDER_LIST_BOX);
						listBoxSections.getElement().getElementsByTagName("option").getItem(0).setAttribute("disabled",
								"disabled");
						listBoxSections.setSelectedValue(PLACEHOLDER_LIST_BOX);

						int sectionArray = 0;

						for (final MetaDataProfileBeanExt profileBean : result.getTheProfileBeans()) {

							GcubeProfileDV profileDV = profileBean.getGcubeProfileDV();

							String sectionPath = profileDV.getParentName() != null ? profileDV.getParentName() : "";

							sectionPath += profileDV.getSectionName();

							// increment section stored as array
							if (profileDV.getMaxOccurs() == 0 || profileDV.getMaxOccurs() > 1) {
								sectionPath += "[" + sectionArray + "]";
								sectionArray++;
							} else {
								sectionArray = 0;
							}

							List<FilePathDV> filePaths = profileDV.getFilePaths();

							if (filePaths != null)
								sectionPathFilePaths.put(sectionPath, filePaths);

							GWT.log("Adding type: " + profileBean.getType() + ", in the section path: " + sectionPath);

							listBoxSections.addItem(profileBean.getType(), sectionPath);
						}

						listBoxSections.addChangeHandler(new ChangeHandler() {

							@Override
							public void onChange(ChangeEvent event) {
								setUpdateButtonEnabled(false);
								
								// -1 because the first element is the PLACEHOLDER "Select section..."
								int selectedIndex = listBoxSections.getSelectedIndex() - 1;
								
								MetaDataProfileBeanExt selectedBean = result.getTheProfileBeans()
										.get(selectedIndex); 
								GWT.log("Change handler fired " + selectedBean);
								controlsControlGroup.setVisible(true);
								scrollSectionContent.clear();

								GcubeProfileDV gcubeProfile = selectedBean.getGcubeProfileDV();

								currentCardSelected = GeoPortalDataEntryApp.buildNewFormCardModelFromProfile(
										gcubeProfile, 1, selectedBean, OPERATION.UPDATE, appManagerBus);

								currentCardSelected.getMetadataForm().addListener(formCardEventHandler);

								scrollSectionContent.add(currentCardSelected.getMetadataForm());

							}
						});

					}

					@Override
					public void onFailure(Throwable caught) {
						projectEditDTO = null;
						htmlPanelContainer.setVisible(true);
						alertHTMLPanel.remove(loaderProjectSections);

						String errorMsg = caught.getMessage();
						Alert alert = new Alert(errorMsg, AlertType.ERROR);
						alert.setClose(false);
						try {
							alertHTMLPanel.remove(loaderProjectSections);
						} catch (Exception e) {

						}
						alertHTMLPanel.add(alert);
						Window.alert(errorMsg);

					}
				});

		bindEvents();
	}

	private void setUpdateButtonEnabled(boolean bool) {
		buttonUpdate.setEnabled(bool);
	}

	private void bindEvents() {

		buttonUpdate.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {

				alertHTMLPanel.clear();
				boolean isFormValid = currentCardSelected.getMetadataForm().isFormDataValid();

				if (!isFormValid) {
					Alert alert = new Alert("Error/s detected, please check your data entry...", AlertType.WARNING);
					alert.setClose(true);
					alertHTMLPanel.add(alert);
					return;
				}

				GeoNaFormDataObject gfdo = new GeoNaFormDataObject(
						Arrays.asList(currentCardSelected.getMetadataForm().getFormDataBean()),
						currentCardSelected.getGcubeProfile());

				String sectionPath = listBoxSections.getSelectedValue();

				GWT.log("sectionPath is: " + sectionPath);

				List<FilePathDV> listFilePaths = sectionPathFilePaths.get(sectionPath);

				final Modal modal = new Modal(true, true);
				DocumentDV theDocument = projectEditDTO.getTheProjectDV().getTheDocument();

				modal.setTitle("Updating...");

				final FlowPanel panelContainer = new FlowPanel();

				LoaderIcon loader = new LoaderIcon("Operation in progress... please wait");
				modal.add(loader);
				String htmlMsg = "Updating the section <b>"+listBoxSections.getSelectedItemText()+"</b> of the project with:";
				htmlMsg += "<ul>";
				htmlMsg += "<li>id: " + projectID + "</li>";
				htmlMsg += "<li>" + theDocument.getFirstEntryOfMap().getKey() + ": "
						+ theDocument.getFirstEntryOfMap().getValue() + "</li>";
				htmlMsg += "</ul>";
				htmlMsg += "<br>";
				panelContainer.add(new HTML(htmlMsg));
				panelContainer.add(loader);
				panelContainer.add(new HTML("<br><br>"));

				modal.add(panelContainer);
//				modal3.setWidth(950);
//				modal3.setHeight("700px");
				modal.setCloseVisible(false);

				GeoportalDataEntryServiceAsync.Util.getInstance().updateGeportalDataForm(profileID, projectID, gfdo,
						sectionPath, listFilePaths, new AsyncCallback<CommitReport>() {

							@Override
							public void onFailure(Throwable caught) {
								modal.setCloseVisible(true);
								modal.setTitle("Error :-(");
								panelContainer.clear();
								String errorMsg = "Sorry, an error occurred when updating the project with id: "+projectID+". Please, try again. If the problem persists, please contact the support";
								Alert alert = new Alert(errorMsg, AlertType.ERROR);
								alert.setClose(false);
								modal.add(alert);

							}

							@Override
							public void onSuccess(CommitReport result) {
								modal.setCloseVisible(true);
								modal.setTitle("Project updated!");
								panelContainer.clear();
								Alert alert = new Alert();
								alert.setClose(false);
								alert.setType(AlertType.SUCCESS);
								String htmlMsg = "The project with:";
								htmlMsg += "<ul>";
								htmlMsg += "<li>id: " + projectID + "</li>";
								htmlMsg += "<li>" + theDocument.getFirstEntryOfMap().getKey() + ": "
										+ theDocument.getFirstEntryOfMap().getValue() + "</li>";
								htmlMsg += "</ul>";
								htmlMsg += "<br>";
								htmlMsg += "has been updated successfully!";
								alert.setHTML(htmlMsg);

								ReportTemplateToHTML rtth2 = new ReportTemplateToHTML("Project", result.getProjectAsJSON(),
										false, false);
								rtth2.showAsJSON(false);

								panelContainer.add(alert);
								panelContainer.add(rtth2);
								
								appManagerBus.fireEvent(new OperationPerformedOnItemEvent<ResultDocumentDV>(
										profileID, null, ACTION_PERFORMED_ON_ITEM.UPDATED_PROJECT));

							}
						});

				modal.show();
			}
		});

	}

	public void noUpdateMode() {
		buttonUpdate.setVisible(false);
	}

	/**
	 * The Class MetadataFormCardEventHandler.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
	 * 
	 *         Oct 12, 2020
	 */
	private class MetadataFormCardEventHandler implements GenericFormEventsListener {

		/**
		 * On form data valid.
		 *
		 * @param genericDatasetBean the generic dataset bean
		 */
		@Override
		public void onFormDataValid(GenericDatasetBean genericDatasetBean) {
			setUpdateButtonEnabled(true);
			//Disabling option not selected
			int selectedIndex = listBoxSections.getSelectedIndex();
			NodeList<Element> elementOption = listBoxSections.getElement().getElementsByTagName("option");
			for (int i = 0; i < listBoxSections.getItemCount(); i++) {
				if (i != selectedIndex) {
					elementOption.getItem(i).setAttribute("disabled", "disabled");
				}
			}

		}

		/**
		 * On form data edit.
		 */
		@Override
		public void onFormDataEdit() {
			setUpdateButtonEnabled(false);
			NodeList<Element> elementOption = listBoxSections.getElement().getElementsByTagName("option");
			//i==0 is the PLACEHOLDER
			for (int i = 1; i < listBoxSections.getItemCount(); i++) {
				elementOption.getItem(i).removeAttribute("disabled");
				
			}

		}

		/**
		 * On form aborted.
		 */
		@Override
		public void onFormAborted() {

		}

		/**
		 * On validation error.
		 *
		 * @param throwable the throwable
		 * @param errorMsg  the error msg
		 */
		@Override
		public void onValidationError(Throwable throwable, String errorMsg) {

		}
	}

}
