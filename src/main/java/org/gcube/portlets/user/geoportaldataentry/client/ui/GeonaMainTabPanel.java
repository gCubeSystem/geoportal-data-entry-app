package org.gcube.portlets.user.geoportaldataentry.client.ui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.gcube.application.geoportalcommon.shared.SearchingFilter;
import org.gcube.application.geoportalcommon.shared.config.GcubeUserRole;
import org.gcube.application.geoportalcommon.shared.config.OPERATION_ON_ITEM;
import org.gcube.application.geoportalcommon.shared.geoportal.ConfigurationDV;
import org.gcube.application.geoportalcommon.shared.geoportal.DocumentDV;
import org.gcube.application.geoportalcommon.shared.geoportal.ResultDocumentDV;
import org.gcube.application.geoportalcommon.shared.geoportal.config.ActionDefinitionDV;
import org.gcube.application.geoportalcommon.shared.geoportal.config.GcubeProfileDV;
import org.gcube.application.geoportalcommon.shared.geoportal.ucd.GEOPORTAL_DATA_HANDLER;
import org.gcube.application.geoportalcommon.shared.geoportal.ucd.HandlerDeclarationDV;
import org.gcube.application.geoportalcommon.shared.geoportal.ucd.UseCaseDescriptorDV;
import org.gcube.application.geoportalcommon.shared.guipresentation.BodyWelcome;
import org.gcube.application.geoportalcommon.shared.guipresentation.DataEntryGUIPresentationConfig;
import org.gcube.application.geoportalcommon.shared.guipresentation.Header;
import org.gcube.portlets.user.geoportaldataentry.client.ConstantsGeoPortalDataEntryApp;
import org.gcube.portlets.user.geoportaldataentry.client.GeoPortalClientCaches.CacheSearchingFilterParametersFromConfig;
import org.gcube.portlets.user.geoportaldataentry.client.events.CreateNewProjectEvent;
import org.gcube.portlets.user.geoportaldataentry.client.events.OperationOnItemEvent;
import org.gcube.portlets.user.geoportaldataentry.client.ui.form.GeonaDataEntryMainForm;
import org.gcube.portlets.user.geoportaldataentry.client.ui.projects.ListOfProjectTablePanel;
import org.gcube.portlets.user.geoportaldataentry.client.ui.projects.ListOfProjectWorkflowActionsPanel;
import org.gcube.portlets.user.geoportaldataentry.client.ui.relation.CreateRelationProjectsPanel;
import org.gcube.portlets.user.geoportaldataentry.client.ui.relation.ViewRelationshipPanel;
import org.gcube.portlets.user.geoportaldataentry.client.ui.table.ItemsTable.DEFAULT_DISPLAYING_COLUMN_NAME;
import org.gcube.portlets.user.geoportaldataentry.client.ui.utils.DialogInform;
import org.gcube.portlets.user.geoportaldataentry.client.ui.utils.LoaderIcon;
import org.gcube.portlets.user.geoportaldataentry.client.ui.utils.UCD_Util;

import com.github.gwtbootstrap.client.ui.Dropdown;
import com.github.gwtbootstrap.client.ui.Heading;
import com.github.gwtbootstrap.client.ui.Hero;
import com.github.gwtbootstrap.client.ui.NavLink;
import com.github.gwtbootstrap.client.ui.PageHeader;
import com.github.gwtbootstrap.client.ui.Paragraph;
import com.github.gwtbootstrap.client.ui.Tab;
import com.github.gwtbootstrap.client.ui.TabPanel;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * The Class GeonaMainTabPanel.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Sep 21, 2022
 */
public class GeonaMainTabPanel extends Composite {

	private static GeonaMainTabPanelUiBinder uiBinder = GWT.create(GeonaMainTabPanelUiBinder.class);

	/**
	 * The Interface GeonaMainTabPanelUiBinder.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
	 * 
	 *         Sep 21, 2022
	 */
	interface GeonaMainTabPanelUiBinder extends UiBinder<Widget, GeonaMainTabPanel> {
	}

	/** The geona main form panel. */
	@UiField
	HTMLPanel geonaMainFormPanel;

	/** The geona main form panel. */
	@UiField
	HTMLPanel contTabNewProject;

	/** The loader. */
	@UiField
	LoaderIcon loader;

	@UiField
	TabPanel tabPanel;

	@UiField
	Tab tabNewProject;

	@UiField
	Tab tabGetListOfProjects;

	@UiField
	NavLink navViewAsDocument;

	@UiField
	NavLink navViewAsJSON;

	@UiField
	NavLink navShowOnMap;
	
	@UiField
	NavLink navShareLink;

	@UiField
	NavLink navShowReport;

	@UiField
	NavLink navEditMode;

	@UiField
	NavLink navDelete;

	@UiField
	NavLink navCreateRelation;

	@UiField
	NavLink navViewRelations;

	@UiField
	Label roleLabel;

	@UiField
	Dropdown ddCreateNewProject;

	@UiField
	PageHeader pageHeaderDataEntry;

	@UiField
	Hero introGNAHero;

	@UiField
	DockLayoutPanel theDockLayoutPanel;

	@UiField
	PageHeader pageHeader;

	@UiField
	Heading welcomeTitle;

	@UiField
	Paragraph welcomeDescription;

	private HandlerManager appManagerBus;

	private GeonaRecordsPaginatedView grpw = null;

	private List<UseCaseDescriptorDV> ucdProjectTypesForListingDataView = new ArrayList<UseCaseDescriptorDV>();

	private CacheSearchingFilterParametersFromConfig cacheSearchingFilterParameters;

	private GcubeUserRole userRole;

	private CreateRelationProjectsPanel createRelationProjectsPanel;

	private ViewRelationshipPanel viewRelationshipPanel;

	private ScrollPanel detailsPanel = new ScrollPanel();

	private ListOfProjectTablePanel listOfProjectTablePanel;

	private ListOfProjectWorkflowActionsPanel listOfProjectWorkflowActionsPanel;

	private DataEntryGUIPresentationConfig dataGUIPresentation;

	private Map<String, String[]> mapProfileIdForPostCreationActionSteps = new HashMap<String, String[]>();

	/**
	 * Instantiates a new geona main tab panel.
	 *
	 * @param appManagerBus the app manager bus
	 */
	public GeonaMainTabPanel(HandlerManager appManagerBus) {
		initWidget(uiBinder.createAndBindUi(this));
		this.appManagerBus = appManagerBus;
		bindEvents();

		listOfProjectWorkflowActionsPanel = new ListOfProjectWorkflowActionsPanel(appManagerBus);

		// listOfProjectWorkflowActionsPanel.setHeight("40px");
		theDockLayoutPanel.addNorth(listOfProjectWorkflowActionsPanel, 40);
		theDockLayoutPanel.setWidgetHidden(listOfProjectWorkflowActionsPanel, true);

		detailsPanel.addStyleName("theDetailsPanel");
		theDockLayoutPanel.addEast(detailsPanel, 450);
		theDockLayoutPanel.setWidgetHidden(detailsPanel, true);

		listOfProjectTablePanel = new ListOfProjectTablePanel(appManagerBus);
		listOfProjectWorkflowActionsPanel.setHeight("100%");
		theDockLayoutPanel.add(listOfProjectTablePanel);

		setLoaderVisible("", false);

	}

	/**
	 * Sets the filtering parameters.
	 *
	 * @param sfp the sfp
	 */
	public void setFilteringParameters(CacheSearchingFilterParametersFromConfig sfp) {
		GWT.log("setFilteringParameters instancied");
		this.cacheSearchingFilterParameters = sfp;
		this.listOfProjectTablePanel.setFilteringParameters(cacheSearchingFilterParameters);

	}

	/**
	 * Sets the internal height.
	 *
	 * @param height the new internal height
	 */
	public void setInternalHeight(int height) {
		contTabNewProject.asWidget().setHeight(height + "px");
		theDockLayoutPanel.setHeight((height - 40) + "px");
		detailsPanel.setHeight((height - 90) + "px");
	}

	/**
	 * Bind events.
	 */
	private void bindEvents() {

		tabGetListOfProjects.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {

				// instanceAndShowListOfProjects(false);
			}
		});

		navShowOnMap.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				List<DocumentDV> listDocuments = null;
				if (grpw != null && grpw.getSelectItems() != null) {
					listDocuments = grpw.getSelectItems();
				}

				appManagerBus
						.fireEvent(new OperationOnItemEvent<DocumentDV>(listDocuments, OPERATION_ON_ITEM.VIEW_ON_MAP));

			}
		});
		
		navShareLink.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				List<DocumentDV> listDocuments = null;
				if (grpw != null && grpw.getSelectItems() != null) {
					listDocuments = grpw.getSelectItems();
				}

				appManagerBus
						.fireEvent(new OperationOnItemEvent<DocumentDV>(listDocuments, OPERATION_ON_ITEM.GET_SHAREABLE_LINK));

			}
		});

		navViewAsDocument.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				List<DocumentDV> listDocuments = null;
				if (grpw != null && grpw.getSelectItems() != null) {
					listDocuments = grpw.getSelectItems();
				}

				appManagerBus.fireEvent(new OperationOnItemEvent<DocumentDV>(listDocuments,
						OPERATION_ON_ITEM.VIEW_PROJECT_AS_DOCUMENT));

			}
		});

		navViewAsJSON.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				List<DocumentDV> listDocuments = null;
				if (grpw != null && grpw.getSelectItems() != null) {
					listDocuments = grpw.getSelectItems();
				}

				appManagerBus.fireEvent(
						new OperationOnItemEvent<DocumentDV>(listDocuments, OPERATION_ON_ITEM.VIEW_PROJECT_AS_JSON));

			}
		});

		navShowReport.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				List<DocumentDV> listDocuments = null;
				if (grpw != null && grpw.getSelectItems() != null) {
					listDocuments = grpw.getSelectItems();
				}

				appManagerBus
						.fireEvent(new OperationOnItemEvent<DocumentDV>(listDocuments, OPERATION_ON_ITEM.VIEW_REPORT));

			}
		});

		navEditMode.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {

				List<DocumentDV> listDocuments = null;
				if (grpw != null && grpw.getSelectItems() != null) {
					listDocuments = grpw.getSelectItems();
				}

				appManagerBus
						.fireEvent(new OperationOnItemEvent<DocumentDV>(listDocuments, OPERATION_ON_ITEM.EDIT_PROJECT));
			}
		});

		navDelete.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				List<DocumentDV> listDocuments = null;
				if (grpw != null && grpw.getSelectItems() != null) {
					listDocuments = grpw.getSelectItems();
				}

				appManagerBus.fireEvent(
						new OperationOnItemEvent<DocumentDV>(listDocuments, OPERATION_ON_ITEM.DELETE_PROJECT));
			}
		});

		navCreateRelation.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				List<DocumentDV> listDocuments = null;
				if (grpw != null && grpw.getSelectItems() != null) {
					listDocuments = grpw.getSelectItems();
				}

				OperationOnItemEvent oIE = new OperationOnItemEvent<DocumentDV>(listDocuments,
						OPERATION_ON_ITEM.CREATE_RELATION);
				oIE.setSourceElement(navCreateRelation.getElement());
				appManagerBus.fireEvent(
						new OperationOnItemEvent<DocumentDV>(listDocuments, OPERATION_ON_ITEM.CREATE_RELATION));
			}
		});

		navViewRelations.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				List<DocumentDV> listDocuments = null;
				if (grpw != null && grpw.getSelectItems() != null) {
					listDocuments = grpw.getSelectItems();
				}

				OperationOnItemEvent oIE = new OperationOnItemEvent<DocumentDV>(listDocuments,
						OPERATION_ON_ITEM.VIEW_RELATIONSHIPS);
				oIE.setSourceElement(navViewRelations.getElement());
				appManagerBus.fireEvent(
						new OperationOnItemEvent<DocumentDV>(listDocuments, OPERATION_ON_ITEM.VIEW_RELATIONSHIPS));
			}
		});

	}

	/**
	 * Sets the role label.
	 *
	 * @param userRole the user role
	 */
	public void setRole(GcubeUserRole userRole) {
		this.userRole = userRole;
		String msg = "Logged in as ";
		if (userRole != null && userRole.getName() != null) {
			msg += userRole.getName().substring(userRole.getName().indexOf("-") + 1, userRole.getName().length());
		} else
			msg += "Member";
		roleLabel.setText(msg);
	}

	/**
	 * Adds the form panel.
	 *
	 * @param formPanel the form panel
	 */
	public void addFormPanel(GeonaDataEntryMainForm formPanel) {
		geonaMainFormPanel.add(formPanel);
	}

	/**
	 * Show list of projects view.
	 *
	 * @param grpw the grpw
	 */
	public void showListOfProjectsView(GeonaRecordsPaginatedView grpw) {
		this.grpw = grpw;
		listOfProjectTablePanel.showListOfProjectsView(grpw);
	}

	/**
	 * Sets the loader visible.
	 *
	 * @param txtHTML the txt HTML
	 * @param visible the visible
	 */
	public void setLoaderVisible(String txtHTML, boolean visible) {
		loader.setText(txtHTML);
		loader.setVisible(visible);
	}

	/**
	 * Gets the current projects searching filter.
	 *
	 * @return the current projects searching filter
	 */
	public SearchingFilter getCurrentProjectsSearchingFilter() {
		return listOfProjectTablePanel.builtSearchingFilter();
	}

	/**
	 * Removes the tab.
	 *
	 * @param index the index
	 */
	public void removeTab(int index) {
		tabPanel.remove(index);
	}

	/**
	 * Sets the tab active.
	 *
	 * @param index the new tab active
	 */
	public void setTabActive(int index) {
		tabPanel.selectTab(index);
	}


	/**
	 * Dirty solution. I created this one because I had problem on firing click
	 * event for Tab element
	 * 
	 */
	public void instanceAndShowListOfProjects() {
		GWT.log("instanceAndShowListOfProjects called");
		listOfProjectTablePanel.instanceAndShowListOfProjects(ucdProjectTypesForListingDataView);
	}

	/**
	 * Inits the main according to list use case descriptors.
	 *
	 * @param listUCDescriptors the list UC descriptors
	 */
	public void initMainAccordingToListUseCaseDescriptors(List<UseCaseDescriptorDV> listUCDescriptors) {
		GWT.log("initMainAccordingToListUseCaseDescriptors: " + listUCDescriptors);
		// this.listUCDescriptors = listUCDescriptors;
		ddCreateNewProject.clear();
		listOfProjectTablePanel.projectTypeReset();

		ucdProjectTypesForListingDataView.clear();

		for (final UseCaseDescriptorDV ucd : listUCDescriptors) {

			HandlerDeclarationDV dataEntryHandler = UCD_Util.getHandlerDeclarationFor(ucd,
					GEOPORTAL_DATA_HANDLER.geoportal_data_entry);

			if (dataEntryHandler != null) {
				ConfigurationDV<?> config = dataEntryHandler.getConfiguration();
				List<GcubeProfileDV> listGcubeProfiles = toListGcubeProfiles(config);

				final List<GcubeProfileDV> listProfiles = listGcubeProfiles;

				NavLink link = new NavLink(ucd.getName());
				link.addClickHandler(new ClickHandler() {

					@Override
					public void onClick(ClickEvent event) {

						if (listProfiles == null || listProfiles.size() == 0) {
							DialogInform di = new DialogInform(null, "No configuration found",
									"No GcubeProfile found in the Configuration for " + config.getConfiguration());
							di.center();
							return;
						}

						appManagerBus.fireEvent(
								new CreateNewProjectEvent(ucd.getProfileID(), dataEntryHandler, listProfiles));
					}
				});

				ddCreateNewProject.add(link);
			}

			// Setting Project type having HANDLER DATA LIST
			HandlerDeclarationDV dataListHandler = UCD_Util.getHandlerDeclarationFor(ucd,
					GEOPORTAL_DATA_HANDLER.geoportal_data_list);

			if (dataListHandler != null) {
				ucdProjectTypesForListingDataView.add(ucd);
				listOfProjectTablePanel.addProjectType(ucd);

			}

			initActionListPanel(ucd);

		}

		// IF one type of document is available, fire
		// CreateNewProjectEvent(documentConfigDV));
		if (listUCDescriptors.size() == 1) {
			Scheduler.get().scheduleDeferred(new ScheduledCommand() {

				@Override
				public void execute() {
					GWT.log("One Config loaded. Firing CreateNewProjectEvent");
					UseCaseDescriptorDV useCaseDescriptor = listUCDescriptors.get(0);
					HandlerDeclarationDV dataEntryHandler = UCD_Util.getHandlerDeclarationFor(useCaseDescriptor,
							GEOPORTAL_DATA_HANDLER.geoportal_data_entry);
					if (dataEntryHandler != null) {
						ConfigurationDV<?> config = dataEntryHandler.getConfiguration();
						List<GcubeProfileDV> listGcubeProfiles = toListGcubeProfiles(config);
						appManagerBus.fireEvent(new CreateNewProjectEvent(useCaseDescriptor.getProfileID(),
								dataEntryHandler, listGcubeProfiles));
					}

				}

			});

		}
	}

	/**
	 * Sets the GUI presentation.
	 *
	 * @param dataEntryGUIPresentation the new GUI presentation
	 */
	public void setGUIPresentation(DataEntryGUIPresentationConfig dataEntryGUIPresentation) {
		this.dataGUIPresentation = dataEntryGUIPresentation;
		GWT.log("Setting: " + dataEntryGUIPresentation);

		Header header = dataGUIPresentation.getHeader();
		if (header != null) {
			if (header.getTitle() != null) {
				pageHeader.setText(header.getTitle());
			}
			if (header.getSubtitle() != null) {
				pageHeader.setSubtext(header.getSubtitle());
			}
		}

		BodyWelcome bodyWelcome = dataGUIPresentation.getBodyWelcome();
		if (bodyWelcome != null) {
			if (bodyWelcome.getTitle() != null) {
				welcomeTitle.setText(bodyWelcome.getTitle());
			}
			if (bodyWelcome.getDescription() != null) {
				welcomeDescription.setText(bodyWelcome.getDescription());
			}
		}

	}

	/**
	 * Inits the action list panel.
	 *
	 * @param ucd the ucd
	 */
	public void initActionListPanel(UseCaseDescriptorDV ucd) {
		GWT.log("Init ActionListPanel for UCD: " + ucd);
		listOfProjectWorkflowActionsPanel.initActionListPanel(ucd);

		// Setting Project type having HANDLER WORKFLOW ACTION
		HandlerDeclarationDV dataListHandler = UCD_Util.getHandlerDeclarationFor(ucd,
				GEOPORTAL_DATA_HANDLER.geoportal_workflow_action_list);

		String[] callSteps = null;
		if (dataListHandler != null) {
			ConfigurationDV<?> config = dataListHandler.getConfiguration();
			List<ActionDefinitionDV> listActionDef = (List<ActionDefinitionDV>) config.getConfiguration();
			for (ActionDefinitionDV actionDefinitionDV : listActionDef) {
				// skipping the special workflow action
				if (actionDefinitionDV.getId()
						.equals(ConstantsGeoPortalDataEntryApp.WORKFLOW_ACTION_POST_CREATION_ACTION_ID)) {
					callSteps = actionDefinitionDV.getCallSteps();
					mapProfileIdForPostCreationActionSteps.put(ucd.getProfileID(), callSteps);
				}
			}
		}
	}

	/**
	 * Gets the post creation action on save.
	 *
	 * @param profileID the profile ID
	 * @return the post creation action on save
	 */
	public String[] getPostCreationActionOnSave(String profileID) {
		return mapProfileIdForPostCreationActionSteps.get(profileID);
	}

	/**
	 * Show actions on selected.
	 *
	 * @param <T>         the generic type
	 * @param selectItems the select items
	 */
	public <T> void showActionsOnSelected(List<T> selectItems) {

		theDockLayoutPanel.setWidgetHidden(listOfProjectWorkflowActionsPanel, false);

		listOfProjectWorkflowActionsPanel.showActionsOnSelected(selectItems, userRole);

		if (createRelationProjectsPanel != null && createRelationProjectsPanel.isVisible()) {
			createRelationProjectsPanel.selectedProject((ResultDocumentDV) selectItems.get(0));
		}

		if (viewRelationshipPanel != null && viewRelationshipPanel.isVisible()) {
			viewRelationshipPanel.showRelationsOf((ResultDocumentDV) selectItems.get(0));
		}

	}

	/**
	 * To list gcube profiles.
	 *
	 * @param config the config
	 * @return the list
	 */
	private List<GcubeProfileDV> toListGcubeProfiles(ConfigurationDV<?> config) {

		try {
			return (List<GcubeProfileDV>) config.getConfiguration();
		} catch (Exception e) {
			GWT.log("Error on casting " + ConfigurationDV.class.getName() + " to List of "
					+ GcubeProfileDV.class.getName());
		}

		return null;
	}

	/**
	 * Sets the page header.
	 *
	 * @param hDV the new page header
	 */
	public void setPageHeader(HandlerDeclarationDV hDV) {
		introGNAHero.setVisible(false);
		this.pageHeaderDataEntry.setVisible(true);
		this.pageHeaderDataEntry.setText("");
		this.pageHeaderDataEntry.setSubtext("New: " + hDV.getItemType());
	}

	/**
	 * Show create relation panel.
	 *
	 * @param bool             the bool
	 * @param selectedDocument the document DV
	 */
	public void showCreateRelationPanel(boolean bool, ResultDocumentDV selectedDocument) {
		detailsPanel.clear();
		// theDetailsPanel.setVisible(bool);
		theDockLayoutPanel.setWidgetHidden(detailsPanel, !bool);
		// if hiding, returns
		if (!bool) {
			return;
		}
		createRelationProjectsPanel = new CreateRelationProjectsPanel(appManagerBus, selectedDocument);
		detailsPanel.add(createRelationProjectsPanel);
	}

	/**
	 * Show view project relations panel.
	 *
	 * @param bool             the bool
	 * @param selectedDocument the selected document
	 */
	public void showViewProjectRelationsPanel(boolean bool, ResultDocumentDV selectedDocument) {
		detailsPanel.clear();
		// theDetailsPanel.setVisible(bool);
		theDockLayoutPanel.setWidgetHidden(detailsPanel, !bool);
		// if hiding, returns
		if (!bool) {
			return;
		}
		viewRelationshipPanel = new ViewRelationshipPanel(appManagerBus, selectedDocument, true);
		detailsPanel.add(viewRelationshipPanel);
	}

	/**
	 * Enable relatioship facilities.
	 *
	 * @param bool the bool
	 */
	public void enableRelatioshipFacilities(boolean bool) {
		navCreateRelation.setVisible(bool);
		if (!bool) {
			grpw.removeColumn(DEFAULT_DISPLAYING_COLUMN_NAME.RELATIONSHIPS);
		}
	}
	
	public ListOfProjectTablePanel getListOfProjectTablePanel() {
		return listOfProjectTablePanel;
	}
	
	public List<UseCaseDescriptorDV> getUcdProjectTypesForListingDataView() {
		return ucdProjectTypesForListingDataView;
	}

}
