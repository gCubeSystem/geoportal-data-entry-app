package org.gcube.portlets.user.geoportaldataentry.client.events;

import com.google.gwt.event.shared.EventHandler;

/**
 * The Interface CreateNewProjectEventHandler.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 * Oct 13, 2020
 */
public interface CreateNewProjectEventHandler extends EventHandler {

	/**
	 * On create new project.
	 *
	 * @param newProjectEvent the new project event
	 */
	void onCreateNewProject(CreateNewProjectEvent newProjectEvent);
}