/**
 *
 */
package org.gcube.portlets.user.geoportaldataentry.client.ui.utils;

import org.gcube.portlets.user.geoportaldataentry.client.resource.Images;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.DockPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * The Class DialogInform.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Oct 5, 2021
 */
public class DialogInform extends DialogBox implements ClickHandler {

	private DockPanel dock = new DockPanel();
	private Button okButton;
	private VerticalPanel vpContainer = new VerticalPanel();
	private ImageResource loading = Images.ICONS.loading();
	private HorizontalPanel hpButtons = new HorizontalPanel();
	private HorizontalPanel hpMask = new HorizontalPanel();
	private DialogInform instance = this;

	/**
	 * Instantiates a new dialog inform.
	 *
	 * @param img     the img
	 * @param caption the caption
	 * @param msg     the msg
	 */
	public DialogInform(Image img, String caption, String msg) {
		// getElement().setClassName("gwt-DialogBoxNew");
		dock.setSpacing(4);
		dock.setWidth("100%");
		setText(caption);
//		setHeading(caption);

		okButton = new Button("OK");

		vpContainer.getElement().getStyle().setMargin(20.0, Unit.PX);
		vpContainer.add(new HTML(msg));
		hpButtons = new HorizontalPanel();
		hpButtons.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
//		hpButtons.getElement().getStyle().setMarginTop(20.0, Unit.PX);
		hpButtons.setSpacing(3);
		okButton.getElement().getStyle().setMarginRight(20.0, Unit.PX);
		hpButtons.add(okButton);

		okButton.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				instance.hide();
			}
		});

		dock.add(hpButtons, DockPanel.SOUTH);
		dock.setCellHorizontalAlignment(hpButtons, DockPanel.ALIGN_CENTER);

		if (img != null)
			dock.add(img, DockPanel.WEST);

		vpContainer.add(hpMask);
		dock.add(vpContainer, DockPanel.CENTER);
		setWidget(dock);
	}

	/**
	 * Sets the msg.
	 *
	 * @param msg the new msg
	 */
	public void setMsg(String msg) {
		vpContainer.clear();
		vpContainer.add(new HTML(msg));
		hpButtons.setVisible(true);
	}

	/**
	 * On click.
	 *
	 * @param event the event
	 */
	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.google.gwt.event.dom.client.ClickHandler#onClick(com.google.gwt.event
	 * .dom.client.ClickEvent)
	 */
	@Override
	public void onClick(ClickEvent event) {
//		hide();
	}

	/**
	 * Hide loader.
	 */
	public void hideLoader() {
		hpMask.clear();
		hpButtons.setVisible(true);
	}

	/**
	 * Loader.
	 *
	 * @param message the message
	 */
	public void showLoader(String message) {
		try {
			hpButtons.setVisible(false);
		} catch (Exception e) {
		}
		hpMask.clear();
		hpMask.add(new Image(loading));
		HTML html = new HTML(message);
		html.getElement().getStyle().setMarginLeft(5, Unit.PX);
		hpMask.add(html);
	}

	/**
	 * Adds the to center panel.
	 *
	 * @param w the w
	 */
	public void addToCenterPanel(Widget w) {
		vpContainer.add(w);
	}

	/**
	 * Gets the dock.
	 *
	 * @return the dock
	 */
	public DockPanel getDock() {
		return dock;
	}

	/**
	 * Gets the yes button.
	 *
	 * @return the yes button
	 */
	public Button getOKButton() {
		return okButton;
	}

	/**
	 * Sets the z index.
	 *
	 * @param value the new z index
	 */
	public void setZIndex(int value) {
		this.getElement().getStyle().setZIndex(value);
	}

}