package org.gcube.portlets.user.geoportaldataentry.client.ui.table;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.gcube.portlets.user.geoportaldataentry.client.events.ClickItemEvent;

import com.github.gwtbootstrap.client.ui.Pagination;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.dom.client.Style.BorderStyle;
import com.google.gwt.event.dom.client.ContextMenuEvent;
import com.google.gwt.event.dom.client.ContextMenuHandler;
import com.google.gwt.event.dom.client.DoubleClickEvent;
import com.google.gwt.event.dom.client.DoubleClickHandler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.cellview.client.SimplePager;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.MenuBar;
import com.google.gwt.user.client.ui.MenuItem;
import com.google.gwt.view.client.AbstractDataProvider;
import com.google.gwt.view.client.AsyncDataProvider;
import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.MultiSelectionModel;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SelectionChangeEvent.Handler;
import com.google.gwt.view.client.SelectionModel;
import com.google.gwt.view.client.SingleSelectionModel;


/**
 * The Class AbstractItemsCellTable.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it Jul 11, 2017
 * @param <T> the generic type
 */
public abstract class AbstractItemsCellTable<T> {

	protected SortedCellTable<T> sortedCellTable;
	protected T itemContextMenu = null;
	protected boolean fireEventOnClick = true;
	protected SelectionModel<T> theSelectionModel;
	protected HandlerManager eventBus;

	/**
	 * Inits the table.
	 *
	 * @param pager        the pager
	 * @param pagination   the pagination
	 * @param dataProvider the data provider
	 */
	public abstract void initTable(final SimplePager pager, final Pagination pagination,
			AbstractDataProvider<T> dataProvider);

	/**
	 * Inits the abstract table.
	 *
	 * @param eventBus       the event bus
	 * @param fireOnClick    the fire on click
	 * @param dataProvider   the data provider
	 * @param selectionModel the selection model
	 * @param pageSize       the page size
	 */
	protected void initAbstractTable(HandlerManager eventBus, boolean fireOnClick, AbstractDataProvider<T> dataProvider,
			SelectionModel<T> selectionModel, int pageSize) {
		this.eventBus = eventBus;
		this.fireEventOnClick = fireOnClick;
		this.theSelectionModel = selectionModel;
		sortedCellTable = new SortedCellTable<T>(pageSize, dataProvider);
		sortedCellTable.addStyleName("table-glor");
		sortedCellTable.addStyleName("table-glor-vertical-middle");
		//sortedCellTable.setStriped(true);
		sortedCellTable.setCondensed(true);
		sortedCellTable.setWidth("100%", true);

		sortedCellTable.setAutoHeaderRefreshDisabled(true);
		sortedCellTable.setAutoFooterRefreshDisabled(true);
		
//		dataProvider.addDataDisplay(sortedCellTable);
//		initTable(cellTable, null, null);
		//sortedCellTable.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);

//		DefaultSelectionEventManager<T> checkBoxManager = DefaultSelectionEventManager.<T> createCheckboxManager();
//		sortedCellTable.setSelectionModel(theSelectionModel,checkBoxManager);
		sortedCellTable.setSelectionModel(theSelectionModel);

		theSelectionModel.addSelectionChangeHandler(new Handler() {
			@Override
			public void onSelectionChange(final SelectionChangeEvent event) {

				if (theSelectionModel instanceof SingleSelectionModel) {
					SingleSelectionModel<T> ssm = (SingleSelectionModel<T>) theSelectionModel;
					final T selectedObject = ssm.getSelectedObject();
					if (selectedObject != null) {
						GWT.log("Clicked: " + selectedObject);
//				        selectedItem(selectedObject);
						if (fireEventOnClick)
							AbstractItemsCellTable.this.eventBus.fireEvent(new ClickItemEvent<T>(Arrays.asList(selectedObject)));
					}
				}else if (theSelectionModel instanceof MultiSelectionModel) {
					Set<T> selected = ((MultiSelectionModel<T>) theSelectionModel).getSelectedSet();
					GWT.log("Selected are:" +selected);
					if (fireEventOnClick)
						AbstractItemsCellTable.this.eventBus.fireEvent(new ClickItemEvent<T>(new ArrayList<T>(selected)));
				} 
			}
		});

		sortedCellTable.addDomHandler(new DoubleClickHandler() {

			@Override
			public void onDoubleClick(final DoubleClickEvent event) {
				if (theSelectionModel instanceof SingleSelectionModel) {
					SingleSelectionModel<T> ssm = (SingleSelectionModel<T>) theSelectionModel;
					T selected = ssm.getSelectedObject();
					if (selected != null) {
						GWT.log("Double Click: " + selected);
						// AbstractItemsCellTable.this.eventBus.fireEvent(new
						// org.gcube.portlets.widgets.wsexplorer.client.event.LoadFolderEvent<T>(selected));
					}
				}

			}
		}, DoubleClickEvent.getType());

		MenuBar options = new MenuBar(true);
		ScheduledCommand openCommand = new ScheduledCommand() {

			@Override
			public void execute() {
				GWT.log("Context menu shown: " + itemContextMenu);
				// AbstractItemsCellTable.this.eventBus.fireEvent(new
				// org.gcube.portlets.widgets.wsexplorer.client.event.LoadFolderEvent<T>(itemContextMenu));
			}
		};

		MenuItem openItem = new MenuItem("Open", openCommand);
		options.addItem(openItem);
		final DialogBox menuWrapper = new DialogBox(true);
		menuWrapper.getElement().getStyle().setBorderStyle(BorderStyle.NONE);
		menuWrapper.getElement().getStyle().setZIndex(10000);
		menuWrapper.add(options);
		sortedCellTable.sinkEvents(Event.ONCONTEXTMENU);

		sortedCellTable.addHandler(new ContextMenuHandler() {
			@Override
			public void onContextMenu(ContextMenuEvent event) {
			}
		}, ContextMenuEvent.getType());

	}

	/**
	 * Gets the cell tables.
	 *
	 * @return the cell tables
	 */
	public SortedCellTable<T> getCellTable() {
		return sortedCellTable;
	}

	/**
	 * Checks if is fire event on click.
	 *
	 * @return the fireEventOnClick
	 */
	public boolean isFireEventOnClick() {

		return fireEventOnClick;
	}

	/**
	 * Sets the fire event on click.
	 *
	 * @param fireEventOnClick the fireEventOnClick to set
	 */
	public void setFireEventOnClick(boolean fireEventOnClick) {

		this.fireEventOnClick = fireEventOnClick;
	}

	/**
	 * Adds the items.
	 *
	 * @param items the items
	 */
	public void addItems(List<T> items) {

		AbstractDataProvider<T> dataProvider = sortedCellTable.getDataProvider();

		if (dataProvider instanceof ListDataProvider) {
			List<T> ldp = ((ListDataProvider<T>) dataProvider).getList();
			for (int i = 0; i < items.size(); i++) {
				ldp.add(i, items.get(i));
			}

			sortedCellTable.setPageSize(items.size() + 1);
			sortedCellTable.redraw();
		} else if (dataProvider instanceof AsyncDataProvider) {

			// TODO ???

		}
	}
}
