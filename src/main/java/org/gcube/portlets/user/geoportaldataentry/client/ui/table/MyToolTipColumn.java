/**
 *
 */

package org.gcube.portlets.user.geoportaldataentry.client.ui.table;

import com.google.gwt.cell.client.Cell;
import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.core.client.GWT;
import com.google.gwt.safehtml.client.SafeHtmlTemplates;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.user.cellview.client.Column;


/**
 * The Class MyToolTipColumn.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Jun 15, 2021
 * @param <T> the generic type
 * @param <C> the generic type
 */
public abstract class MyToolTipColumn<T, C> extends Column<T, C> {

	/**
	 * The Interface Templates.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
	 * 
	 *         Jun 15, 2021
	 */
	interface Templates extends SafeHtmlTemplates {

		/**
		 * Start tool tip.
		 *
		 * @param toolTipText the tool tip text
		 * @return the safe html
		 */
		@Template("<span title=\"{0}\">")
		SafeHtml startToolTip(String toolTipText);

		/**
		 * End tool tip.
		 *
		 * @return the safe html
		 */
		@Template("</span>")
		SafeHtml endToolTip();
	}

	private static final Templates TEMPLATES = GWT.create(Templates.class);
	private final String toolTipText;

	/**
	 * Instantiates a new my tool tip column.
	 *
	 * @param cell        the cell
	 * @param toolTipText the tool tip text
	 */
	public MyToolTipColumn(final Cell<C> cell, final String toolTipText) {

		super(cell);
		this.toolTipText = toolTipText;
	}

	/**
	 * Render.
	 *
	 * @param context the context
	 * @param object  the object
	 * @param sb      the sb
	 */
	@Override
	public void render(final Context context, final T object, final SafeHtmlBuilder sb) {
		sb.append(TEMPLATES.startToolTip(toolTipText));
		super.render(context, object, sb);
		sb.append(TEMPLATES.endToolTip());
	}
}
