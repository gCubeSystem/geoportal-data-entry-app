package org.gcube.portlets.user.geoportaldataentry.client.ui.utils;

import java.util.List;

import org.gcube.application.geoportalcommon.shared.geoportal.ucd.GEOPORTAL_DATA_HANDLER;
import org.gcube.application.geoportalcommon.shared.geoportal.ucd.HandlerDeclarationDV;
import org.gcube.application.geoportalcommon.shared.geoportal.ucd.UseCaseDescriptorDV;

public class UCD_Util {

	public static HandlerDeclarationDV getHandlerDeclarationFor(List<UseCaseDescriptorDV> listUCDescriptors,
			String profileID, GEOPORTAL_DATA_HANDLER dataHandler) {

		for (UseCaseDescriptorDV useCaseDescriptor : listUCDescriptors) {

			if (useCaseDescriptor.getProfileID().compareTo(profileID) == 0) {

				for (HandlerDeclarationDV handler : useCaseDescriptor.getHandlers()) {
					GEOPORTAL_DATA_HANDLER dataHandlerType = handler.getDataHandlerType();

					if (dataHandlerType != null && dataHandlerType.equals(dataHandler)) {
						return handler;
					}
				}

			}
		}

		return null;
	}

	public static HandlerDeclarationDV getHandlerDeclarationFor(UseCaseDescriptorDV useCaseDescriptor,
			GEOPORTAL_DATA_HANDLER dataHandler) {

		if (useCaseDescriptor == null)
			return null;

		for (HandlerDeclarationDV handler : useCaseDescriptor.getHandlers()) {
			GEOPORTAL_DATA_HANDLER dataHandlerType = handler.getDataHandlerType();
			
			if (dataHandlerType != null && dataHandlerType.equals(dataHandler)) {
				return handler;
			}
		}

		return null;

	}

}
