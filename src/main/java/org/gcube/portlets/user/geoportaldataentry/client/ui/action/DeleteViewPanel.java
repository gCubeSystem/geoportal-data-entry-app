package org.gcube.portlets.user.geoportaldataentry.client.ui.action;

import org.gcube.application.geoportalcommon.shared.geoportal.ResultDocumentDV;
import org.gcube.portlets.user.geoportaldataentry.client.ui.report.ReportTemplateToHTML;

import com.github.gwtbootstrap.client.ui.AccordionGroup;
import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Widget;

public class DeleteViewPanel extends Composite {

	private static DeleteViewPanelUiBinder uiBinder = GWT.create(DeleteViewPanelUiBinder.class);

	interface DeleteViewPanelUiBinder extends UiBinder<Widget, DeleteViewPanel> {
	}

	@UiField
	FlowPanel principalPanel;

	@UiField
	AccordionGroup accordionProjectDetails;

	@UiField
	FlowPanel confirmProceedPanel;

	private ResultDocumentDV resultDocumentDV;

	public DeleteViewPanel(ResultDocumentDV resultDocumentDV) {
		initWidget(uiBinder.createAndBindUi(this));
		this.resultDocumentDV = resultDocumentDV;
		builtUI();
	}

	private void builtUI() {
		String htmlMsg = "<p style='font-size: 18px'>Going to permanently delete the:</p>";
		// TODO Auto-generated method stub
		principalPanel.add(new HTML(htmlMsg));
		ReportTemplateToHTML rt = new ReportTemplateToHTML("Project", resultDocumentDV.getDocumentAsJSON(), false);
		rt.showAsJSON(false);
		principalPanel.add(rt);

		String projectDetails = "<ul>";
		projectDetails += "<li>id: " + resultDocumentDV.getId() + "</li>";
		projectDetails += "<li>profile: " + resultDocumentDV.getProfileID() + "</li>";
		projectDetails += "<li>" + resultDocumentDV.getFirstEntryOfMap().getKey() + ": "
				+ resultDocumentDV.getFirstEntryOfMap().getValue() + "</li>";
		projectDetails += "</ul>";

		accordionProjectDetails.add(new HTML(projectDetails));

		String confirmMessage = "<b>This operation cannot be undone. Would you like to proceed?</b>";
		confirmProceedPanel.add(new HTML(confirmMessage));
	}

}
