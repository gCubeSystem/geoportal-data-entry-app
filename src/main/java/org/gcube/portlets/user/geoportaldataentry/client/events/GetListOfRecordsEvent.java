package org.gcube.portlets.user.geoportaldataentry.client.events;

import org.gcube.application.geoportalcommon.shared.SearchingFilter;

import com.google.gwt.event.shared.GwtEvent;

/**
 * The Class CreateNewProjectEvent.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 *         Oct 13, 2020
 */
public class GetListOfRecordsEvent extends GwtEvent<GetListOfRecordsEventHandler> {

	/** The type. */
	public static Type<GetListOfRecordsEventHandler> TYPE = new Type<GetListOfRecordsEventHandler>();
	private SearchingFilter searchingFilter;
	private String profileID;
	private String projectName;
	private Boolean reloadFilteringParameters = false;
	private boolean onApplicationInit = false;

	/**
	 * Instantiates a new gets the list of records event.
	 *
	 * @param onApplicationInit         the on application init
	 * @param profileID                 the profile ID
	 * @param searchingFilter           the searching filter
	 * @param reloadFilteringParameters the reload filtering parameters
	 */
	public GetListOfRecordsEvent(boolean onApplicationInit, String profileID, SearchingFilter searchingFilter,
			Boolean reloadFilteringParameters) {
		this.onApplicationInit = onApplicationInit;
		this.searchingFilter = searchingFilter;
		this.profileID = profileID;
		this.reloadFilteringParameters = reloadFilteringParameters;
	}

	/**
	 * Gets the associated type.
	 *
	 * @return the associated type
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.google.gwt.event.shared.GwtEvent#getAssociatedType()
	 */
	@Override
	public Type<GetListOfRecordsEventHandler> getAssociatedType() {
		return TYPE;
	}

	/**
	 * Dispatch.
	 *
	 * @param handler the handler
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.google.gwt.event.shared.GwtEvent#dispatch(com.google.gwt.event.shared.
	 * EventHandler)
	 */
	@Override
	protected void dispatch(GetListOfRecordsEventHandler handler) {
		handler.onGetList(this);
	}

	/**
	 * Gets the searching filter.
	 *
	 * @return the searching filter
	 */
	public SearchingFilter getSearchingFilter() {
		return searchingFilter;
	}

	/**
	 * Gets the profile ID.
	 *
	 * @return the profile ID
	 */
	public String getProfileID() {
		return profileID;
	}

	/**
	 * Checks if is on application init.
	 *
	 * @return true, if is on application init
	 */
	public boolean isOnApplicationInit() {
		return onApplicationInit;
	}

	/**
	 * Gets the reload filtering parameters.
	 *
	 * @return the reload filtering parameters
	 */
	public boolean isReloadFilteringParameters() {
		return reloadFilteringParameters;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("GetListOfRecordsEvent [searchingFilter=");
		builder.append(searchingFilter);
		builder.append(", profileID=");
		builder.append(profileID);
		builder.append(", projectName=");
		builder.append(projectName);
		builder.append(", reloadFilteringParameters=");
		builder.append(reloadFilteringParameters);
		builder.append(", onApplicationInit=");
		builder.append(onApplicationInit);
		builder.append("]");
		return builder.toString();
	}

}
