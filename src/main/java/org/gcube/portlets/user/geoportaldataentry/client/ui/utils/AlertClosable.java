package org.gcube.portlets.user.geoportaldataentry.client.ui.utils;

import com.github.gwtbootstrap.client.ui.Alert;
import com.google.gwt.user.client.Timer;

/**
 * The Class AlertClosable.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Mar 4, 2022
 */
public class AlertClosable extends Alert {

	private AlertClosable instance = this;

	/**
	 * Instantiates a new alert closable.
	 */
	public AlertClosable() {
	}

	/**
	 * Close after.
	 *
	 * @param depayMillis the depay millis
	 */
	public void closeAfter(int depayMillis) {
		Timer timer = new Timer() {

			@Override
			public void run() {
				instance.close();

			}
		};

		timer.schedule(depayMillis);
	}
}
