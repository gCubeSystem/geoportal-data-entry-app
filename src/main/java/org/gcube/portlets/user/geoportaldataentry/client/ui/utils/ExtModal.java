package org.gcube.portlets.user.geoportaldataentry.client.ui.utils;

import com.github.gwtbootstrap.client.ui.Modal;
import com.github.gwtbootstrap.client.ui.event.HideEvent;
import com.github.gwtbootstrap.client.ui.event.HideHandler;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.dom.client.Element;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.EventListener;
import com.google.gwt.user.client.Random;

/**
 * The Class ExtModal.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Apr 29, 2021
 * 
 *         This Class extends the {{@link Modal} preventing the default close of
 *         the Modal Bootstrap when clicking outside of the modal Window
 */
public class ExtModal extends Modal {

	private String elementId;
	private String closeElementId;
	private Boolean closeButtoClicked = false;
	final private ExtModal INSTANCE = this;

	/**
	 * Instantiates a new ext modal.
	 */
	public ExtModal() {
		super();
		setElementIds();
		addDeferredCommandToPreventModalClose();
	}

	/**
	 * Creates an empty, hidden widget with specified show behavior.
	 *
	 * @param animated <code>true</code> if the widget should be animated.
	 */
	public ExtModal(boolean animated) {
		super(animated, false);
		setElementIds();
		addDeferredCommandToPreventModalClose();
	}

	/**
	 * Creates an empty, hidden widget with specified show behavior.
	 *
	 * @param animated    <code>true</code> if the widget should be animated.
	 * @param dynamicSafe <code>true</code> removes from RootPanel when hidden
	 */
	public ExtModal(boolean animated, boolean dynamicSafe) {
		super(animated, dynamicSafe);
		setElementIds();
		addDeferredCommandToPreventModalClose();
	}

	private void addDeferredCommandToPreventModalClose() {
		Scheduler.get().scheduleDeferred(new ScheduledCommand() {

			@Override
			public void execute() {
				((Element) INSTANCE.getElement().getChildNodes().getItem(0)).getFirstChildElement()
						.setId(closeElementId);
				preventModalCloseWhenClickingOutside();

			}
		});

	}

	/**
	 * Sets the element id.
	 */
	private void setElementIds() {
		this.elementId = "my-modal-publish-meta" + Random.nextInt();
		this.getElement().setId(elementId);

		this.closeElementId = "my-modal-close-button" + Random.nextInt();
	}

	/**
	 * Prevent modal close when clicking outside.
	 */
	private void preventModalCloseWhenClickingOutside() {

		com.google.gwt.user.client.Element buttonCloseElement = DOM.getElementById(this.closeElementId);

		if (buttonCloseElement != null) {

			Event.sinkEvents(buttonCloseElement, Event.ONCLICK);
			Event.setEventListener(buttonCloseElement, new EventListener() {

				@Override
				public void onBrowserEvent(Event event) {
					if (Event.ONCLICK == event.getTypeInt()) {
						GWT.log("close event clicked");
						closeButtoClicked = true;
					}

				}
			});
		} else {
			GWT.log("button close element not found");
			closeButtoClicked = true;
		}

		// hide any popup panel opened
		this.addHideHandler(new HideHandler() {

			@Override
			public void onHide(HideEvent hideEvent) {
				GWT.log("HideEvent on modal fired");
				GWT.log(hideEvent.toDebugString());
				GWT.log("CloseButtonClicked is: " + closeButtoClicked);

				if (!closeButtoClicked) {
					hideEvent.preventDefault();
					hideEvent.stopPropagation();
				}
			}
		});

	}

}
