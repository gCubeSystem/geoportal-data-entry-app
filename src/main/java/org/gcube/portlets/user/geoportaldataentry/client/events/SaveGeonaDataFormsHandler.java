package org.gcube.portlets.user.geoportaldataentry.client.events;

import com.google.gwt.event.shared.EventHandler;


// TODO: Auto-generated Javadoc
/**
 * The Interface SaveGeonaDataFormsHandler.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 * Oct 20, 2020
 */
public interface SaveGeonaDataFormsHandler extends EventHandler {

	/**
	 * On save.
	 *
	 * @param saveGeonaDataFormsEvent the save geona data forms event
	 */
	void onSave(SaveGeonaDataFormsEvent saveGeonaDataFormsEvent);
}