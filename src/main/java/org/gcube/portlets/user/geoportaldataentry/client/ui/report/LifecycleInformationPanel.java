package org.gcube.portlets.user.geoportaldataentry.client.ui.report;

import org.gcube.application.geoportalcommon.shared.GeoportalItemReferences;
import org.gcube.application.geoportalcommon.shared.geoportal.project.LifecycleInformationDV;
import org.gcube.portlets.user.geoportaldataentry.client.GeoportalDataEntryServiceAsync;
import org.gcube.portlets.user.geoportaldataentry.client.ui.utils.HTMLUtil;
import org.gcube.portlets.user.geoportaldataentry.client.ui.utils.HTMLUtil.HTML_TAG;
import org.gcube.portlets.user.geoportaldataentry.client.ui.utils.LoaderIcon;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;

/**
 * The Class LifecycleInformationPanel.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Sep 1, 2022
 */
public class LifecycleInformationPanel extends FlowPanel {

	/**
	 * Instantiates a new lifecycle information panel.
	 *
	 * @param projectID       the project ID
	 * @param profileID       the profile ID
	 * @param projectAsJSON   the project as JSON
	 * @param lcDV            the lc DV
	 * @param showGoToProject the show go to project
	 */
	public LifecycleInformationPanel(String projectID, String profileID, String projectAsJSON,
			LifecycleInformationDV lcDV, boolean showGoToProject) {

		add(new HTML("Project id: " + (projectID)));

		HTML htmlPhase = new HTML();
		htmlPhase.setHTML("Current phase: <b>" + lcDV.getPhase() + "</b>");

		HTML htmlStep = new HTML();
		htmlStep.setHTML("Last invoked step: <b>" + lcDV.getLastInvokedStep() + "</b>");

		HTML lastOperationStatus = new HTML();
		String lastOperationMsg = "Last operation status: ";
		switch (lcDV.getLastOperationStatus()) {
		case OK: {
			lastOperationMsg += HTMLUtil.getHTMLElement(HTML_TAG.span, 14, "32CD32", null,
					lcDV.getLastOperationStatus().toString());
			break;
		}
		case WARNING: {
			lastOperationMsg += HTMLUtil.getHTMLElement(HTML_TAG.span, 14, "FF8000", null,
					lcDV.getLastOperationStatus().toString());
			break;
		}
		case ERROR: {
			lastOperationMsg += HTMLUtil.getHTMLElement(HTML_TAG.span, 14, "FF0000", "bold",
					lcDV.getLastOperationStatus().toString());
			break;
		}
		default:
			lastOperationMsg += HTMLUtil.getHTMLElement(HTML_TAG.span, 14, "FF0000", "bold",
					lcDV.getLastOperationStatus().toString());
			break;
		}
		lastOperationStatus.setHTML(lastOperationMsg);

		add(htmlPhase);
		add(htmlStep);
		add(lastOperationStatus);

		HTML htmlEvent = new HTML();
		htmlEvent.setHTML("Last event step: <b>" + lcDV.getLastEvent() + "</b>");

		if (showGoToProject) {
			final HorizontalPanel hpGetLink = new HorizontalPanel();
			final LoaderIcon lc = new LoaderIcon("Get link...");
			hpGetLink.add(lc);
			add(hpGetLink);
			GeoportalDataEntryServiceAsync.Util.getInstance().getLinksFor(projectID, profileID, GeoportalItemReferences.SHARE_LINK_TO.DATA_VIEWER,
					new AsyncCallback<GeoportalItemReferences>() {

						@Override
						public void onFailure(Throwable caught) {
							hpGetLink.clear();

						}

						@Override
						public void onSuccess(GeoportalItemReferences result) {
							hpGetLink.clear();
							String theURL = result.getRestrictedLink().getShortURL() != null
									? result.getRestrictedLink().getShortURL()
									: result.getRestrictedLink().getCompleteURL();
							String htmlLink = "<div>Go to project on Map: <a href=" + theURL + " target=\"_blank\">"
									+ theURL + "</a></div>";
							HTML html = new HTML(htmlLink);
							hpGetLink.add(html);
							// modal.add(html);
						}
					});
		}

		if (projectAsJSON != null)
			add(new ReportTemplateToHTML("Project", projectAsJSON, false));
		if (lcDV.getAsJSONString() != null)
			add(new ReportTemplateToHTML("Lifecycle Report", lcDV.getAsJSONString(), false));
	}

}
