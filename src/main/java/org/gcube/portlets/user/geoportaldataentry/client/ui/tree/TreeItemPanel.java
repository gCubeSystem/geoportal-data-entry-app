package org.gcube.portlets.user.geoportaldataentry.client.ui.tree;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import org.gcube.application.geoportalcommon.shared.geoportal.config.GcubeProfileDV;
import org.gcube.portlets.user.geoportaldataentry.client.events.TreeItemEvent;
import org.gcube.portlets.user.geoportaldataentry.client.events.TreeItemEvent.ACTION;
import org.gcube.portlets.user.geoportaldataentry.client.ui.card.GeoNaFormCardModel;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.ui.Tree;
import com.google.gwt.user.client.ui.TreeItem;

/**
 * The Class TreeItemPanel.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 * Jul 8, 2022
 */
public class TreeItemPanel {

	private static final String JSON_ROOT_PATH = "$.";
	private Tree tree = new Tree();
	private final TreeItem root;
	private HandlerManager appManagerBus;
	private HashMap<String, List<TreeItem>> treeItemParents = new HashMap<String, List<TreeItem>>();
	private String itemTypeTitle;
	private String profileID;

	/**
	 * Instantiates a new tree item panel.
	 *
	 * @param profileID the profile ID
	 * @param itemTypeTitle the item type title
	 * @param gnaCardsModels the gna cards models
	 * @param appManagerBus the app manager bus
	 */
	// Create a tree
	public TreeItemPanel(String profileID, String itemTypeTitle, Collection<GeoNaFormCardModel> gnaCardsModels,
			HandlerManager appManagerBus) {
		this.appManagerBus = appManagerBus;
		this.itemTypeTitle = itemTypeTitle;
		this.profileID = profileID;
		tree.setAnimationEnabled(true);
		
		NodeItem rootNode = new NodeItem(null, itemTypeTitle, null, false, false, null);
		rootNode.setRoot(true);
		root = new TreeItem(rootNode);
		
		treeItemParents.put(JSON_ROOT_PATH, Arrays.asList(root));

		for (GeoNaFormCardModel geoNaFormCardModel : gnaCardsModels) {

			GcubeProfileDV profile = geoNaFormCardModel.getGcubeProfile();
			String parentName = profile.getParentName();
			String jsonSectionFullPath = getJSONFullPathFromProfile(profile);

			if (parentName == null || parentName.isEmpty()) {
				parentName = JSON_ROOT_PATH;
			}

			GWT.log("tree getting jsonSectionFullPath: " + jsonSectionFullPath + " in " + treeItemParents.keySet());
			List<TreeItem> listParent = treeItemParents.get(parentName);
			GWT.log("tree jsonSection: " + jsonSectionFullPath + " parent " + parentName + " name: "
					+ profile.getSectionName() + " title: " + profile.getSectionTitle());

			boolean canBeDuplicated = geoNaFormCardModel.getFormCard().isInternalRepeatibleForm();
			TreeItem child = createAndAddChild(listParent.get(0), profile.getSectionTitle(), geoNaFormCardModel, canBeDuplicated, false,
					jsonSectionFullPath);

		}

		tree.addItem(root);

		tree.addSelectionHandler(new SelectionHandler<TreeItem>() {

			@Override
			public void onSelection(SelectionEvent<TreeItem> event) {
				TreeItem treeItem = event.getSelectedItem();

				NodeItem nodeItem = (NodeItem) treeItem.getWidget();

				GWT.log("TreeItem selected: " + nodeItem);
				appManagerBus.fireEvent(new TreeItemEvent(treeItem, ACTION.SELECTED));
			}
		});

		// expand the element
		root.setState(true);

	}

	/**
	 * Gets the JSON full path from profile.
	 *
	 * @param profile the profile
	 * @return the JSON full path from profile
	 */
	private String getJSONFullPathFromProfile(GcubeProfileDV profile) {

		String parentName = profile.getParentName();
		GWT.log("finding parentName: " + parentName + " in " + treeItemParents.keySet());

		String jsonSectionFullPath = null;

		if (parentName == null || parentName.isEmpty()) {
			GWT.log("tree parentName is null: " + parentName + " in " + treeItemParents.keySet());
			parentName = JSON_ROOT_PATH;
			jsonSectionFullPath = profile.getSectionName();
		} else {
			jsonSectionFullPath = parentName.endsWith(".") ? parentName + profile.getSectionName()
					: parentName + "." + profile.getSectionName();
		}

		return jsonSectionFullPath;
	}

	/**
	 * Creates the and add child.
	 *
	 * @param parent the parent
	 * @param nodeHTML the node HTML
	 * @param geoNaFormCardModel the geo na form card model
	 * @param canBeDuplicated the can be duplicated
	 * @param canBeDeleted the can be deleted
	 * @param jsonSectionFullPath the json section full path
	 * @return the tree item
	 */
	public TreeItem createAndAddChild(TreeItem parent, String nodeHTML, GeoNaFormCardModel geoNaFormCardModel, boolean canBeDuplicated,
			boolean canBeDeleted, String jsonSectionFullPath) {
		GWT.log("Creating and adding child to parent: " + parent.getText() +", card is"+geoNaFormCardModel);
		TreeItem newChild = new TreeItem(new NodeItem(parent, nodeHTML, geoNaFormCardModel, canBeDuplicated, canBeDeleted, jsonSectionFullPath));
		parent.addItem(newChild);

		fillItemParents(jsonSectionFullPath, newChild);
		return newChild;
	}

	/**
	 * Adds the child.
	 *
	 * @param parent the parent
	 * @param nextSiblingIndex the index where the item will be inserted
	 * @param item the item
	 * @return the tree item
	 */
	public TreeItem addChild(TreeItem parent, int nextSiblingIndex, TreeItem item) {
		GWT.log("Adding child "+item.getText()+" to parent: " + parent.getText());

		if(nextSiblingIndex>=0) {
			parent.insertItem(nextSiblingIndex, item);
		}
		else {
			parent.addItem(item);
		}

		NodeItem nodeItem = (NodeItem) item.getWidget();
		String jsonSectionFullPath = nodeItem.getJsonSectionFullPath();

		fillItemParents(jsonSectionFullPath, item);

		return item;
	}

	/**
	 * Fill item parents.
	 *
	 * @param jsonSectionFullPath the json section full path
	 * @param newChild the new child
	 */
	private void fillItemParents(String jsonSectionFullPath, TreeItem newChild) {
		// if the path section is not the root
		if (jsonSectionFullPath.compareTo(JSON_ROOT_PATH) != 0) {

			List<TreeItem> listNodeParents = treeItemParents.get(jsonSectionFullPath);

			if (listNodeParents == null) {
				listNodeParents = new ArrayList<TreeItem>();
			}

			listNodeParents.add(newChild);

			treeItemParents.put(jsonSectionFullPath, listNodeParents);
		}
	}

	/**
	 * Removes the child.
	 *
	 * @param parent the parent
	 * @param item the item
	 * @param jsonSectionFullPath the json section full path
	 */
	public void removeChild(TreeItem parent, TreeItem item, String jsonSectionFullPath) {
		GWT.log("Removing child to parent: " + parent.getText());
		List<TreeItem> nodes = treeItemParents.get(jsonSectionFullPath);

		if (nodes != null) {
			nodes.remove(item);
			treeItemParents.put(jsonSectionFullPath, nodes);
		}

		parent.removeItem(item);

	}

	/**
	 * Count node for full path.
	 *
	 * @param jsonSectionFullPath the json section full path
	 * @return the int
	 */
	public int countNodeForFullPath(String jsonSectionFullPath) {
		List<TreeItem> nodes = treeItemParents.get(jsonSectionFullPath);

		if (nodes != null)
			return nodes.size();

		return 0;
	}

	/**
	 * Gets the tree.
	 *
	 * @return the tree
	 */
	public Tree getTree() {
		return tree;
	}

	/**
	 * Gets the root.
	 *
	 * @return the root
	 */
	public TreeItem getRoot() {
		return root;
	}

	/**
	 * Gets the select item.
	 *
	 * @return the select item
	 */
	public TreeItem getSelectItem() {
		return tree.getSelectedItem();
	}

	/**
	 * Gets the node index.
	 *
	 * @param parent the parent
	 * @param item the item
	 * @return the node index
	 */
	public int getNodeIndex(TreeItem parent, TreeItem item) {
		return parent.getChildIndex(item);
	}

	/**
	 * Gets the item type title.
	 *
	 * @return the item type title
	 */
	public String getItemTypeTitle() {
		return itemTypeTitle;
	}

	/**
	 * Gets the profile ID.
	 *
	 * @return the profile ID
	 */
	public String getProfileID() {
		return profileID;
	}

	/**
	 * Gets the tree item parents.
	 *
	 * @return the tree item parents
	 */
	public HashMap<String, List<TreeItem>> getTreeItemParents() {
		return treeItemParents;
	}
	
	/**
	 * Select item.
	 *
	 * @param item the item
	 */
	public void selectItem(TreeItem item){
		item.setSelected(true);
	}

}
