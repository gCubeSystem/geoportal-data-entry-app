package org.gcube.portlets.user.geoportaldataentry.client.events;

import org.gcube.application.geoportalcommon.shared.geoportal.DocumentDV;

import com.google.gwt.event.shared.EventHandler;


/**
 * The Interface OperationPerformedOnItemEventHandler.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 * Nov 25, 2021
 */
public interface OperationPerformedOnItemEventHandler extends EventHandler {

	/**
	 * On do action performed fired.
	 * @param <T>
	 *
	 * @param actionPerformedOnItemEvent the action performed on item event
	 */
	<T extends DocumentDV> void onDoActionPerformedFired(OperationPerformedOnItemEvent<T> actionPerformedOnItemEvent);
}