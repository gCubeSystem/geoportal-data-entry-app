package org.gcube.portlets.user.geoportaldataentry.client.ui;

import java.util.ArrayList;
import java.util.List;

import org.gcube.application.geoportalcommon.shared.ResultSetPaginatedData;
import org.gcube.application.geoportalcommon.shared.SearchingFilter;
import org.gcube.application.geoportalcommon.shared.geoportal.DocumentDV;
import org.gcube.application.geoportalcommon.shared.geoportal.config.ItemFieldDV;
import org.gcube.application.geoportalcommon.shared.geoportal.project.ProjectDV;
import org.gcube.portlets.user.geoportaldataentry.client.ConstantsGeoPortalDataEntryApp;
import org.gcube.portlets.user.geoportaldataentry.client.GeoPortalDataEntryApp;
import org.gcube.portlets.user.geoportaldataentry.client.ui.table.ItemsTable;
import org.gcube.portlets.user.geoportaldataentry.client.ui.table.ItemsTable.DEFAULT_DISPLAYING_COLUMN_NAME;
import org.gcube.portlets.user.geoportaldataentry.client.ui.table.SortedCellTable;
import org.gcube.portlets.user.geoportaldataentry.client.ui.utils.LoaderIcon;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.cellview.client.SimplePager;
import com.google.gwt.user.cellview.client.SimplePager.TextLocation;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.AsyncDataProvider;
import com.google.gwt.view.client.HasData;
import com.google.gwt.view.client.MultiSelectionModel;
import com.google.gwt.view.client.Range;
import com.google.gwt.view.client.SelectionModel;
import com.google.gwt.view.client.SingleSelectionModel;

/**
 * The Class GeonaRecordsPaginatedView.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Aug 4, 2021
 */
public class GeonaRecordsPaginatedView {

	private static final int ITEM_START_INDEX = 0;
	private static final int ITEMS_PER_PAGE = ConstantsGeoPortalDataEntryApp.PAGINATION_SIZE;
	private VerticalPanel vPanel = new VerticalPanel();
	private FlowPanel pagerPanel = new FlowPanel();
	private Boolean initClassFirstRangeChanged = false;
	private ItemsTable<DocumentDV> itemsTable;
	private MyCustomDataProvider<DocumentDV> dataProvider = new MyCustomDataProvider<DocumentDV>();
	protected Widget orginalLoadingIndicator = null;
	private LoaderIcon loadingPanel = new LoaderIcon("Loading data...");
	private HandlerManager eventBus;
	private SearchingFilter currentSearchingFilter;
	private String profileID;

	/**
	 * Instantiates a new geona records paginated view.
	 *
	 * @param eventBus               the eventbus
	 * @param profileID              the profile ID
	 * @param displayFields          the display fields
	 * @param currentSearchingFilter the current searching filter
	 */
	public GeonaRecordsPaginatedView(HandlerManager eventBus, String profileID, List<ItemFieldDV> displayFields,
			SearchingFilter currentSearchingFilter) {
		this.profileID = profileID;
		this.currentSearchingFilter = currentSearchingFilter;
		this.initClassFirstRangeChanged = true;
		this.eventBus = eventBus;
		itemsTable = new ItemsTable<DocumentDV>(eventBus, displayFields);
		itemsTable.initTable(null, null, dataProvider);

		orginalLoadingIndicator = itemsTable.getCellTable().getLoadingIndicator();
		initPagination(ITEMS_PER_PAGE);
		loadItemsForType(profileID);
	}

	/**
	 * Gets the table data provider.
	 *
	 * @return the table data provider
	 */
	public AsyncDataProvider<DocumentDV> getTableDataProvider() {
		return (AsyncDataProvider<DocumentDV>) getCellTable().getDataProvider();
	}

	/**
	 * Gets the cell tale.
	 *
	 * @return the cell tale
	 */
	private SortedCellTable<DocumentDV> getCellTable() {
		return itemsTable.getCellTable();
	}

	/**
	 * Inits the pagination.
	 *
	 * @param itemsPerPage the items per page
	 */
	public void initPagination(int itemsPerPage) {

		SimplePager.Resources pagerResources = GWT.create(SimplePager.Resources.class);
		SimplePager pager = new SimplePager(TextLocation.CENTER, pagerResources, false, 0, true);
		pager.setDisplay(getCellTable());
		pager.setPageSize(itemsPerPage);
		pager.getElement().getStyle().setProperty("margin", "auto");
		vPanel.add(loadingPanel);
		vPanel.add(getCellTable());
		vPanel.getElement().addClassName("vPanel");
		pagerPanel.add(pager);
	}

	/**
	 * Load new page.
	 *
	 * @param profileID           the profile ID
	 * @param startIdx            the start idx
	 * @param limit               the limit
	 * @param resetStore          the reset store
	 * @param currentSearchFilter the current search filter
	 * @param invalidCache        the invalid cache
	 */
	private void loadNewPage(String profileID, final int startIdx, final int limit, final boolean resetStore,
			final SearchingFilter currentSearchFilter, final boolean invalidCache) {
		GWT.log("loadNewPage with parameters [startIdx: " + startIdx + ", limit: " + limit + ", resetStore:"
				+ resetStore + "]");
		int newStartIndex = startIdx;

		if (resetStore) {
			GWT.log("Cleaning all data...");
			newStartIndex = 0;
			GWT.log("Store reset performed start index is: " + newStartIndex);
			getTableDataProvider().updateRowCount(ITEMS_PER_PAGE, false);
		}

		loadProjects(profileID, newStartIndex, limit, currentSearchFilter, invalidCache);
	}

	/**
	 * Load items for type.
	 *
	 * @param profileID the profile ID
	 */
	private void loadItemsForType(String profileID) {
		this.profileID = profileID;
		getCellTable().setVisibleRangeAndClearData(new Range(ITEM_START_INDEX, ITEMS_PER_PAGE), false);
		loadNewPage(profileID, ITEM_START_INDEX, ITEMS_PER_PAGE, true, currentSearchingFilter, true);
	}

	/**
	 * Sets the new page result.
	 *
	 * @param result the new new page result
	 */
	private void setNewPageResult(ResultSetPaginatedData result) {
		GWT.log("setNewPageResult: " + result);
		// serverStartIndex = result.getServerEndIndex();
		SelectionModel<? super DocumentDV> sm = getCellTable().getSelectionModel();

		if (sm instanceof SingleSelectionModel) {
			SingleSelectionModel<? super DocumentDV> ssm = (SingleSelectionModel<DocumentDV>) sm;
			ssm.clear();
		} else if (sm instanceof MultiSelectionModel) {
			MultiSelectionModel<? super DocumentDV> msm = (MultiSelectionModel<DocumentDV>) sm;
			msm.clear();
		}

		getTableDataProvider().updateRowCount((int) result.getTotalItems(), true);

		// TODO MUST USE PROJECT
		getTableDataProvider().updateRowData(result.getClientStartIndex(), (List<DocumentDV>) result.getData());

		if (result.getData().size() == 0) {
			getCellTable().setLoadingIndicator(new Label("No data"));
		} else {
			getCellTable().setLoadingIndicator(orginalLoadingIndicator);
		}

		GWT.log("Updating row data startIndex: " + result.getClientStartIndex() + " children size: "
				+ result.getData().size());
		GWT.log("getAsycnDataProvider().getDataDisplays().size(): " + getCellTable().getRowCount());

		if (result.isServerSearchFinished()) {
			GWT.log("Search finished!!!");
			getTableDataProvider().updateRowCount(getCellTable().getRowCount(), true);
		}
		// initFirstRangeChanged = false;

		//printData((List<DocumentDV>) result.getData());
	}

	private void printData(List<DocumentDV> data) {
		GWT.log("printData");
		int i = 0;
		for (DocumentDV documentDV : data) {
			GWT.log(++i + ") json: " + documentDV.getDocumentAsJSON());
		}
		
	}

	/**
	 * To list document DV.
	 *
	 * @param data the data
	 * @return the list
	 */
	private List<DocumentDV> toListDocumentDV(List<ProjectDV> data) {
		if (data == null)
			return null;

		List<DocumentDV> listDocuments = new ArrayList<DocumentDV>(data.size());
		for (ProjectDV projectDV : data) {
			listDocuments.add(projectDV.getTheDocument());
		}

		return listDocuments;
	}

	/**
	 * Load projects.
	 *
	 * @param <T>           the generic type
	 * @param theProfileID  the the profile ID
	 * @param newStartIndex the new start index
	 * @param limit         the limit
	 * @param searchFilter  the search filter
	 * @param invalidCache  the invalid cache
	 */
	private <T> void loadProjects(String theProfileID, int newStartIndex, int limit, SearchingFilter searchFilter,
			boolean invalidCache) {
		showLoading(true);

		GWT.log("calling loadItemsForStatus with parameters [theProfileID: " + theProfileID + ", startIndex: "
				+ newStartIndex + ", limit: " + limit + ", searchFilter: " + searchFilter + "]");
		GeoPortalDataEntryApp.geoportalDataEntryService.getListProjects(theProfileID, newStartIndex, limit,
				searchFilter, invalidCache, new AsyncCallback<ResultSetPaginatedData>() {

					@Override
					public void onFailure(Throwable caught) {
						showLoading(false);
						Window.alert(caught.getMessage());

					}

					@Override
					public void onSuccess(ResultSetPaginatedData result) {
						showLoading(false);
						setNewPageResult(result);

					}
				});

	}

	/**
	 * Select items.
	 *
	 * @param select      the select
	 * @param limitToPage the limit to page
	 */
	public void selectItems(boolean select, boolean limitToPage) {
		SortedCellTable<DocumentDV> table = getCellTable();
		int rowSize = table.getVisibleItemCount();

		for (int i = 0; i < rowSize; i++) {
			DocumentDV item = table.getVisibleItem(i);
			itemsTable.getSelectionModel().setSelected(item, select);
		}
	}

	/**
	 * Removes the column.
	 *
	 * @param columnName the column name
	 */
	public void removeColumn(DEFAULT_DISPLAYING_COLUMN_NAME columnName) {
		try {
			itemsTable.removeColumn(columnName);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	/**
	 * Gets the select items.
	 *
	 * @return the select items
	 */
	public List<DocumentDV> getSelectItems() {
		return itemsTable.getSelectedItems();
	}

	/**
	 * A custom {@link AsyncDataProvider}.
	 *
	 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it Jul 5, 2017
	 * @param <T> the generic type
	 */
	public class MyCustomDataProvider<T> extends AsyncDataProvider<T> {

		/**
		 * {@link #onRangeChanged(HasData)} is called when the table requests a new
		 * range of data. You can push data back to the displays using
		 * {@link #updateRowData(int, List)}.
		 *
		 * @param display the display
		 */
		@Override
		public void onRangeChanged(HasData<T> display) {

			// Get the new range.
			final Range range = display.getVisibleRange();

			int start = range.getStart();
			int length = range.getLength();

			if (initClassFirstRangeChanged) {
				GWT.log("initClassFirstRangeChanged is true.. returning");
				initClassFirstRangeChanged = false;
				return;
			}
			GWT.log("Range changed: " + start + " " + length + " visible count: " + display.getVisibleItemCount());
			loadNewPage(profileID, start, length, false, currentSearchingFilter, false);
		}

	}

	/**
	 * Gets the pager panel.
	 *
	 * @return the pager panel
	 */
	public VerticalPanel getCellPanel() {
		return vPanel;
	}

	/**
	 * Set the panel in loading mode.
	 *
	 * @param show the show
	 */
	protected void showLoading(boolean show) {
		loadingPanel.setVisible(show);
	}

	/**
	 * Gets the pager panel.
	 *
	 * @return the pager panel
	 */
	public FlowPanel getPagerPanel() {
		return pagerPanel;
	}

	/**
	 * Gets the profile ID.
	 *
	 * @return the profile ID
	 */
	public String getProfileID() {
		return profileID;
	}

}
