package org.gcube.portlets.user.geoportaldataentry.client;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.TreeMap;
import java.util.stream.Collectors;

import org.gcube.application.geoportalcommon.shared.GeoportalItemReferences;
import org.gcube.application.geoportalcommon.shared.GeoportalItemReferences.SHARE_LINK_TO;
import org.gcube.application.geoportalcommon.shared.SearchingFilter;
import org.gcube.application.geoportalcommon.shared.config.GcubeUserRole;
import org.gcube.application.geoportalcommon.shared.config.OPERATION_ON_ITEM;
import org.gcube.application.geoportalcommon.shared.config.RoleRights;
import org.gcube.application.geoportalcommon.shared.config.RoleRights.OPERATION_TYPE;
import org.gcube.application.geoportalcommon.shared.geoportal.DocumentDV;
import org.gcube.application.geoportalcommon.shared.geoportal.ResultDocumentDV;
import org.gcube.application.geoportalcommon.shared.geoportal.WORKFLOW_PHASE;
import org.gcube.application.geoportalcommon.shared.geoportal.config.GcubeProfileDV;
import org.gcube.application.geoportalcommon.shared.geoportal.config.ItemFieldDV;
import org.gcube.application.geoportalcommon.shared.geoportal.materialization.innerobject.FilesetDV;
import org.gcube.application.geoportalcommon.shared.geoportal.materialization.innerobject.PayloadDV;
import org.gcube.application.geoportalcommon.shared.geoportal.project.BasicLifecycleInformationDV.Status;
import org.gcube.application.geoportalcommon.shared.geoportal.project.LifecycleInformationDV;
import org.gcube.application.geoportalcommon.shared.geoportal.step.StepPerformedResultDV;
import org.gcube.application.geoportalcommon.shared.geoportal.ucd.GEOPORTAL_DATA_HANDLER;
import org.gcube.application.geoportalcommon.shared.geoportal.ucd.HandlerDeclarationDV;
import org.gcube.application.geoportalcommon.shared.geoportal.ucd.RelationshipDefinitionDV;
import org.gcube.application.geoportalcommon.shared.geoportal.ucd.UseCaseDescriptorDV;
import org.gcube.application.geoportalcommon.shared.geoportal.view.ProjectView;
import org.gcube.application.geoportaldatamapper.shared.MetaDataProfileBeanExt;
import org.gcube.portlets.user.geoportaldataentry.client.ConstantsGeoPortalDataEntryApp.ACTION_PERFORMED_ON_ITEM;
import org.gcube.portlets.user.geoportaldataentry.client.GeoPortalClientCaches.CacheSearchingFilterParametersFromConfig;
import org.gcube.portlets.user.geoportaldataentry.client.events.ClickItemEvent;
import org.gcube.portlets.user.geoportaldataentry.client.events.ClickItemEventHandler;
import org.gcube.portlets.user.geoportaldataentry.client.events.CloseCreateRelationGUIEvent;
import org.gcube.portlets.user.geoportaldataentry.client.events.CloseCreateRelationGUIEventHandler;
import org.gcube.portlets.user.geoportaldataentry.client.events.CreateNewProjectEvent;
import org.gcube.portlets.user.geoportaldataentry.client.events.CreateNewProjectEventHandler;
import org.gcube.portlets.user.geoportaldataentry.client.events.GetListOfRecordsEvent;
import org.gcube.portlets.user.geoportaldataentry.client.events.GetListOfRecordsEventHandler;
import org.gcube.portlets.user.geoportaldataentry.client.events.OperationOnItemEvent;
import org.gcube.portlets.user.geoportaldataentry.client.events.OperationOnItemEventHandler;
import org.gcube.portlets.user.geoportaldataentry.client.events.OperationPerformedOnItemEvent;
import org.gcube.portlets.user.geoportaldataentry.client.events.OperationPerformedOnItemEventHandler;
import org.gcube.portlets.user.geoportaldataentry.client.events.RelationActionHandler;
import org.gcube.portlets.user.geoportaldataentry.client.events.RelationActionHandlerEvent;
import org.gcube.portlets.user.geoportaldataentry.client.events.RelationActionHandlerEvent.RELACTION_ACTION_TYPE;
import org.gcube.portlets.user.geoportaldataentry.client.events.SaveGeonaDataFormsEvent;
import org.gcube.portlets.user.geoportaldataentry.client.events.SaveGeonaDataFormsHandler;
import org.gcube.portlets.user.geoportaldataentry.client.events.TreeItemEvent;
import org.gcube.portlets.user.geoportaldataentry.client.events.TreeItemEventHandler;
import org.gcube.portlets.user.geoportaldataentry.client.events.WorkflowActionOnSelectedItemEvent;
import org.gcube.portlets.user.geoportaldataentry.client.events.WorkflowActionOnSelectedItemEventHandler;
import org.gcube.portlets.user.geoportaldataentry.client.resource.Images;
import org.gcube.portlets.user.geoportaldataentry.client.ui.GeonaMainTabPanel;
import org.gcube.portlets.user.geoportaldataentry.client.ui.GeonaRecordsPaginatedView;
import org.gcube.portlets.user.geoportaldataentry.client.ui.ModalWindow;
import org.gcube.portlets.user.geoportaldataentry.client.ui.action.DeleteViewPanel;
import org.gcube.portlets.user.geoportaldataentry.client.ui.action.StepViewPanel;
import org.gcube.portlets.user.geoportaldataentry.client.ui.card.GeoNaFormCardModel;
import org.gcube.portlets.user.geoportaldataentry.client.ui.edit.EditModeRecord;
import org.gcube.portlets.user.geoportaldataentry.client.ui.edit.UpdateRecord;
import org.gcube.portlets.user.geoportaldataentry.client.ui.form.GeonaDataEntryMainForm;
import org.gcube.portlets.user.geoportaldataentry.client.ui.projects.ListOfProjectTablePanel;
import org.gcube.portlets.user.geoportaldataentry.client.ui.report.LifecycleInformationPanel;
import org.gcube.portlets.user.geoportaldataentry.client.ui.tree.NodeItem;
import org.gcube.portlets.user.geoportaldataentry.client.ui.tree.TreeItemPanel;
import org.gcube.portlets.user.geoportaldataentry.client.ui.utils.DialogInform;
import org.gcube.portlets.user.geoportaldataentry.client.ui.utils.HTMLUtil;
import org.gcube.portlets.user.geoportaldataentry.client.ui.utils.HTMLUtil.HTML_TAG;
import org.gcube.portlets.user.geoportaldataentry.client.ui.utils.LoaderIcon;
import org.gcube.portlets.user.geoportaldataentry.client.ui.utils.ModalConfirm;
import org.gcube.portlets.user.geoportaldataentry.client.ui.utils.UUIDUtil;
import org.gcube.portlets.user.geoportaldataentry.shared.CommitReport;
import org.gcube.portlets.user.geoportaldataentry.shared.GNADataEntryExtendedConfigProfile;
import org.gcube.portlets.user.geoportaldataentry.shared.GeoNaFormDataObject;
import org.gcube.portlets.user.geoportaldataentry.shared.GeoportalISConfig;
import org.gcube.portlets.user.geoportaldataentry.shared.ProjectNotFoundException;
import org.gcube.portlets.user.geoportaldataentry.shared.Tree_Node;
import org.gcube.portlets.user.geoportaldataentry.shared.UserRights;
import org.gcube.portlets.user.geoportaldataentry.shared.monitoring.MonitoringAction;
import org.gcube.portlets.user.geoportaldataentry.shared.monitoring.MonitoringActionsOnClient;
import org.gcube.portlets.widgets.gdvw.client.GeoportalDataViewerWidget;
import org.gcube.portlets.widgets.gdvw.client.project.ProjectViewer;
import org.gcube.portlets.widgets.mpformbuilder.client.MetadataProfileFormBuilderServiceAsync;
import org.gcube.portlets.widgets.mpformbuilder.client.form.generic.CreateMetadataForm;
import org.gcube.portlets.widgets.mpformbuilder.client.form.generic.CreateMetadataForm.OPERATION;
import org.gcube.portlets.widgets.mpformbuilder.shared.metadata.DataTypeWrapper;
import org.gcube.portlets.widgets.mpformbuilder.shared.metadata.MetaDataProfileBean;
import org.gcube.portlets.widgets.mpformbuilder.shared.metadata.MetadataFieldWrapper;
import org.gcube.portlets.widgets.mpformbuilder.shared.upload.FilePath;
import org.gcube.portlets.widgets.mpformbuilder.shared.upload.FileUploaded;
import org.gcube.portlets.widgets.mpformbuilder.shared.upload.FileUploadedRemote;

import com.github.gwtbootstrap.client.ui.Alert;
import com.github.gwtbootstrap.client.ui.Button;
import com.github.gwtbootstrap.client.ui.Icon;
import com.github.gwtbootstrap.client.ui.Label;
import com.github.gwtbootstrap.client.ui.Modal;
import com.github.gwtbootstrap.client.ui.constants.AlertType;
import com.github.gwtbootstrap.client.ui.constants.ButtonType;
import com.github.gwtbootstrap.client.ui.constants.IconSize;
import com.github.gwtbootstrap.client.ui.constants.IconType;
import com.github.gwtbootstrap.client.ui.constants.LabelType;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ResizeEvent;
import com.google.gwt.event.logical.shared.ResizeHandler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TreeItem;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class GeoPortalDataEntryApp implements EntryPoint {

	public static final GeoportalDataEntryServiceAsync geoportalDataEntryService = GWT
			.create(GeoportalDataEntryService.class);

	private final HandlerManager appManagerBus = new HandlerManager(null);

	private LinkedHashMap<String, GeoNaFormCardModel> mapForms = new LinkedHashMap<String, GeoNaFormCardModel>();

	private GeonaDataEntryMainForm geoNaMainForm = null;

	private GeonaMainTabPanel mainTabPanel;

	private LinkedHashMap<String, Tree_Node<GeoNaFormDataObject>> savedMap = new LinkedHashMap<String, Tree_Node<GeoNaFormDataObject>>();

	private boolean projectSavedWithSuccess;

	private GeonaRecordsPaginatedView grpw = null;

	private UserRights myRights = null;

	private LoaderIcon loaderApplication = new LoaderIcon("Step 1 of 2. Loading Application facilities, please wait");

	private LoaderIcon loaderConfigurations = new LoaderIcon("Step 2 of 2. Loading Configurations, please wait");

	private GeoportalISConfig geoportalISConfigs;

	private int numberOfCards = 0;

	private int expectedCards = 0;

	private synchronized void incrementNumberOfCards() {
		numberOfCards++;
	}

	private synchronized int getNumberOfCards() {
		return numberOfCards;
	}

	private boolean dataEntryProjectCreated = false;

	private TreeItemPanel treeItemPanel;

	private GeoPortalClientCaches geoportalCaches;

	private String paramGeonaItemType;

	private String paramGeonaItemID;

	private boolean canCreateNewItem = false;

	private GNADataEntryExtendedConfigProfile gNADataEntryPresentationConfig = null;

	private MonitoringActionsOnClient monitorActionsClientInstance = null;

	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {

		final InitExecutor initExecutor = new InitExecutor();

		geoportalCaches = new GeoPortalClientCaches();

		ConstantsGeoPortalDataEntryApp.ROOT_PANEL_DIV_LOADERS.getElement()
				.addClassName(ConstantsGeoPortalDataEntryApp.CSS_CLASS_GEOPORTAL_LOADERS_CENTER);
		ConstantsGeoPortalDataEntryApp.ROOT_PANEL_DIV_LOADERS.add(loaderApplication);

		mainTabPanel = new GeonaMainTabPanel(appManagerBus);
		geoNaMainForm = new GeonaDataEntryMainForm(appManagerBus);
		mainTabPanel.addFormPanel(geoNaMainForm);

		paramGeonaItemType = Window.Location.getParameter(ConstantsGeoPortalDataEntryApp.GET_PARAMETER_ITEM_TYPE);
		paramGeonaItemID = Window.Location.getParameter(ConstantsGeoPortalDataEntryApp.GET_PARAMETER_ITEM_ID);

		ConstantsGeoPortalDataEntryApp.ROOT_PANEL_DIV_PORTLET.add(mainTabPanel);

		// ROOT_PANEL_DIV_PORTLET.getElement().getStyle().setOpacity(0.3);
		ConstantsGeoPortalDataEntryApp.ROOT_PANEL_DIV_PORTLET.getElement()
				.addClassName(ConstantsGeoPortalDataEntryApp.CSS_CLASS_ANIMATE_FADE_IN_OUT);

		// Loads Geoportal init configurations
		Command commandGeonaInitConfig = new Command() {

			@Override
			public void execute() {
				GWT.log("Execute commandGeonaInitConfig...");
				GeoportalDataEntryServiceAsync.Util.getInstance()
						.getGeonaInitConfig(new AsyncCallback<GeoportalISConfig>() {

							@Override
							public void onFailure(Throwable caught) {
								String errorMsg = "Sorry, an error occurred when loading configurations. Please, contact the support";
								Alert alert = new Alert(errorMsg, AlertType.ERROR);
								alert.setClose(false);
								try {
									ConstantsGeoPortalDataEntryApp.ROOT_PANEL_DIV_LOADERS.remove(loaderApplication);
								} catch (Exception e) {

								}
								ConstantsGeoPortalDataEntryApp.ROOT_PANEL_DIV_PORTLET.add(alert);
								Window.alert(errorMsg);
								initExecutor.execute();
							}

							@Override
							public void onSuccess(GeoportalISConfig geoportalISConfig) {
								geoportalISConfigs = geoportalISConfig;

								if (geoportalISConfig != null && geoportalISConfig.getScope() != null) {

									loadGeoportalConfigs(geoportalISConfig.getScope());
								} else
									Window.alert("Sorry, no scope found in the session. Re-login and try again");

								initExecutor.execute();

							}
						});

			}
		};

		// Loads data entry presetentation configurations
		Command commandReadDataViewerConfig = new Command() {

			@Override
			public void execute() {
				GWT.log("Execute commandReadDataViewerConfig...");
				// Loading Geoportal Configurations from IS
				GeoportalDataEntryServiceAsync.Util.getInstance()
						.readDataEntryPresentationConfig(new AsyncCallback<GNADataEntryExtendedConfigProfile>() {

							@Override
							public void onFailure(Throwable caught) {
								String errorMsg = "Sorry, an error occurred on instancing the application. Please, contact the support. Error is: "
										+ caught.getMessage();
								Alert alert = new Alert(errorMsg, AlertType.ERROR);
								alert.setClose(false);
								try {
									ConstantsGeoPortalDataEntryApp.ROOT_PANEL_DIV_LOADERS.remove(loaderApplication);
									ConstantsGeoPortalDataEntryApp.ROOT_PANEL_DIV_PORTLET.getElement().removeClassName(
											ConstantsGeoPortalDataEntryApp.CSS_CLASS_ANIMATE_FADE_IN_OUT);
								} catch (Exception e) {

								}
								ConstantsGeoPortalDataEntryApp.ROOT_PANEL_DIV_LOADERS.add(alert);
								Window.alert(errorMsg);

								initExecutor.execute();

							}

							@Override
							public void onSuccess(GNADataEntryExtendedConfigProfile gNADataEntryConfig) {
								GWT.log(GNADataEntryExtendedConfigProfile.class.getSimpleName() + " loaded: "
										+ gNADataEntryConfig);

								gNADataEntryPresentationConfig = gNADataEntryConfig;
								try {
									ConstantsGeoPortalDataEntryApp.ROOT_PANEL_DIV_LOADERS.remove(loaderApplication);
								} catch (Exception e) {

								}

								// PERMISSIONS
								myRights = gNADataEntryConfig.getUserRights();
								initGUI();
								GcubeUserRole userRole = myRights.getRoleRights().getUserRole();
								mainTabPanel.setRole(userRole);
								// GUI Presentation
								mainTabPanel.setGUIPresentation(gNADataEntryConfig.getDataEntryGUIPresentation());

								RoleRights roleRights = myRights.getRoleRights();
								canCreateNewItem = roleRights.getListPermessions().keySet()
										.contains(OPERATION_ON_ITEM.CREATE_NEW_PROJECT);

								if (!canCreateNewItem) {
									// removing Tab "Create New Project"
									mainTabPanel.removeTab(0);
									// activating Tab "List of Project"
									mainTabPanel.setTabActive(0);
									// mainTabPanel.instanceAndShowListOfProjects();
								}

								initExecutor.execute();
							}
						});

			}
		};

		Command commandInitOnItem = null;

		// Resolves the gid={ITEM_ID}&git={PROFILE_ID}
		if (paramGeonaItemType != null && paramGeonaItemID != null) {

			commandInitOnItem = new Command() {

				@Override
				public void execute() {
					GWT.log("Execute commandInitOnItem...");
					GeoportalDataEntryServiceAsync.Util.getInstance().getResultDocumentFoProjectByID(paramGeonaItemType,
							paramGeonaItemID, new AsyncCallback<ResultDocumentDV>() {

								@Override
								public void onFailure(Throwable caught) {

									if (caught instanceof ProjectNotFoundException) {
										Modal errorModal = new Modal(true, true);
										errorModal.setCloseVisible(true);
										errorModal.setTitle("Oops...");
										Image geoportalError = new Image(Images.ICONS.warningError());
										FlowPanel errorPanelMsg = new FlowPanel();
										errorPanelMsg.getElement().addClassName("general_warning");
										errorPanelMsg.add(geoportalError);
										errorPanelMsg.add(new HTML("D4S GeoPortal"));
										HTML erroMessage = new HTML(
												"Oops something went wrong, either the project does not exist or you are not authorized to access it");
										errorPanelMsg.add(erroMessage);
										errorModal.add(errorPanelMsg);
										errorModal.show();
									} else {
										Window.alert("Error " + caught.getMessage());
									}

									initExecutor.execute();

								}

								@Override
								public void onSuccess(final ResultDocumentDV result) {
									GWT.log("commandInitOnItem success: " + result);

									Scheduler.get().scheduleDeferred(new ScheduledCommand() {

										@Override
										public void execute() {

//											appManagerBus.fireEvent(new OperationOnItemEvent<DocumentDV>(
//													Arrays.asList(result), OPERATION_ON_ITEM.VIEW_PROJECT_AS_DOCUMENT));

											Timer timer = new Timer() {
												int attempts = 0;

												@Override
												public void run() {
													attempts++;
													GWT.log("commandInitOnItem run timer...");
													List<UseCaseDescriptorDV> list = mainTabPanel
															.getUcdProjectTypesForListingDataView();
													if (list.size() > 0) {
														this.cancel();

														List<UseCaseDescriptorDV> filterUCD = mainTabPanel
																.getUcdProjectTypesForListingDataView().stream()
																.filter(u -> u.getId()
																		.contentEquals(paramGeonaItemType))
																.collect(Collectors.toList());

														if (filterUCD.size() == 1) {
															GWT.log("FilterUCD");

															UseCaseDescriptorDV ucd = filterUCD.get(0);

															ListOfProjectTablePanel listProjectPanel = mainTabPanel
																	.getListOfProjectTablePanel();
															SearchingFilter searchingFilter = new SearchingFilter();
															searchingFilter.setGetForIDs(result.getProfileID(),
																	result.getProjectID());
															// searchFilter.setConditions(null);
															// searchFilter.setGetForIDs(result.getProfileID(),
															// result.getProjectID());

															GetListOfRecordsEvent event = new GetListOfRecordsEvent(
																	true, ucd.getProfileID(), searchingFilter, true);

															listProjectPanel.setSearchTypeAndFire(
																	filterUCD.get(0).getName(), event);

															Modal errorModal = new Modal(true, true);
															// errorModal.setWidth("800px");
															errorModal.setCloseVisible(true);
															errorModal.setTitle("Information...");
															Image geoportalError = new Image(Images.ICONS.info());
															geoportalError.setWidth("128px");
															FlowPanel errorPanelMsg = new FlowPanel();
															errorPanelMsg.getElement().addClassName("general_info");
															errorPanelMsg.add(geoportalError);
															errorPanelMsg.add(new HTML(gNADataEntryPresentationConfig
																	.getDataEntryGUIPresentation().getHeader()
																	.getTitle()));
															HTML erroMessage = new HTML(
																	"The <span class='general_info_facility'>List of Projects</span> only shows the <br/><br/><b>\""
																			+ result.getFirstEntryOfMap().getValue()
																			+ "\"</b><br/><br/>project resolved by shareable link<br/><br/>To view all projects select <span class='general_info_facility'>Reload Projects</span> button");
															errorPanelMsg.add(erroMessage);
															errorModal.add(errorPanelMsg);
															errorModal.show();

														}

														if (attempts > 20) {
															this.cancel();
														}
													}

												}
											};
											timer.schedule(500);
											initExecutor.execute();
										}
									});

									if (canCreateNewItem) {
										mainTabPanel.setTabActive(1);
									} else
										mainTabPanel.setTabActive(0);

								}
							});
				}

			};

		}

		try {
			initExecutor.putCommand(commandGeonaInitConfig);
			initExecutor.putCommand(commandReadDataViewerConfig);
			if (commandInitOnItem != null) {
				initExecutor.putCommand(commandInitOnItem);
			}
			initExecutor.execute();

			// Uncomment this to test the monitor
//			try {
//				new Timer() {
//					public void run() {
//						appManagerBus.fireEvent(new SaveGeonaDataFormsEvent("the id", null));
//					}
//				}.schedule(7000);
//			} catch (Exception e) {
//			}
		} catch (InterruptedException e) {
			Window.alert(
					"Error during application initialization. Please refresh and try again. If the problem persists please contact the support");
		}

	}

	private void initGUI() {
		bindEvents();
		Window.addResizeHandler(new ResizeHandler() {
			@Override
			public void onResize(ResizeEvent event) {
				GWT.log("onWindowResized width: " + event.getWidth() + " height: " + event.getHeight());
				updateSize();

			}
		});

		updateSize();
	}

	private static native void click(Element element)/*-{
		element.click();
	}-*/;

	private void loadGeoportalConfigs(final String scope) {
		GWT.log("loading GeoportalConfigsAndBuildCards in the scope: " + scope);
		// orderedCards = new ArrayList<GeoNaFormCardModel>();
		try {
			ConstantsGeoPortalDataEntryApp.ROOT_PANEL_DIV_LOADERS.insert(loaderConfigurations, 0);
		} catch (Exception e) {

		}
		GeoportalDataEntryServiceAsync.Util.getInstance().getListUseCaseDescriptors(
				ConstantsGeoPortalDataEntryApp.HANDLERS_IDS, new AsyncCallback<List<UseCaseDescriptorDV>>() {

					@Override
					public void onFailure(Throwable caught) {
						String errorMsg = "Sorry, an error occurred on loading configurations. Please, contact the support";
						Alert alert = new Alert(errorMsg, AlertType.ERROR);
						alert.setClose(false);
						try {
							ConstantsGeoPortalDataEntryApp.ROOT_PANEL_DIV_LOADERS.remove(loaderConfigurations);
							ConstantsGeoPortalDataEntryApp.ROOT_PANEL_DIV_PORTLET.getElement()
									.removeClassName(ConstantsGeoPortalDataEntryApp.CSS_CLASS_ANIMATE_FADE_IN_OUT);
						} catch (Exception e) {

						}
						ConstantsGeoPortalDataEntryApp.ROOT_PANEL_DIV_LOADERS.add(alert);
						Window.alert(errorMsg);
						GWT.log(caught.getMessage());
					}

					@Override
					public void onSuccess(List<UseCaseDescriptorDV> result) {
						GWT.log("ListUseCaseDescriptors loaded: " + result);

						if (result == null || result.size() == 0) {
							Window.alert("No Configuration found in this scope. Please contact the support");
							return;
						}

						initDataEntryAppForListUseCaseDescriptors(result);
						// After loading of UCD I can instance the "List of Project" view
						mainTabPanel.instanceAndShowListOfProjects();
						try {
							ConstantsGeoPortalDataEntryApp.ROOT_PANEL_DIV_PORTLET.getElement()
									.removeClassName(ConstantsGeoPortalDataEntryApp.CSS_CLASS_ANIMATE_FADE_IN_OUT);
							ConstantsGeoPortalDataEntryApp.ROOT_PANEL_DIV_LOADERS.remove(loaderConfigurations);
							ConstantsGeoPortalDataEntryApp.ROOT_PANEL_DIV_LOADERS.getElement()
									.removeClassName(ConstantsGeoPortalDataEntryApp.CSS_CLASS_GEOPORTAL_LOADERS_CENTER);
						} catch (Exception e) {

						}
					}
				});
	}

	private void initDataEntryAppForListUseCaseDescriptors(List<UseCaseDescriptorDV> listUCDescriptors) {
		GWT.log("initDataEntryAppForListUseCaseDescriptors with size: " + listUCDescriptors.size());
		ConstantsGeoPortalDataEntryApp.printUCDs(listUCDescriptors);

		mainTabPanel.initMainAccordingToListUseCaseDescriptors(listUCDescriptors);

		for (final UseCaseDescriptorDV ucdDV : listUCDescriptors) {
			geoportalCaches.putUCDForProfileId(ucdDV.getProfileID(), ucdDV);
		}

	}

	/**
	 * Update window size
	 */
	public void updateSize() {

		try {
			RootPanel workspace = ConstantsGeoPortalDataEntryApp.ROOT_PANEL_DIV_PORTLET;
			int topBorder = workspace.getAbsoluteTop();
			int footer = 85; // footer is bottombar + sponsor
			int headerSize = 90;
			// int headerSize = 30;
			int rootHeight = Window.getClientHeight() - topBorder - headerSize - footer;// - ((footer ==
																						// null)?0:(footer.getOffsetHeight()-15));

			GWT.log("New workspace dimension Height: " + rootHeight);
			mainTabPanel.setInternalHeight(rootHeight);
			// mainTabPanel.setlistOfProjectCenterPanelHeight(rootHeight-20+"px");
		} catch (Exception e) {

		}

	}

	private <T extends MetaDataProfileBean> void createNewDataEntyFor(String profileID,
			HandlerDeclarationDV handlerDeclarationDV, List<GcubeProfileDV> listGPs) {
		dataEntryProjectCreated = true;
		mainTabPanel.setLoaderVisible("Loading...", true);
		// orderedCards.clear();
		resetUI();
		mainTabPanel.setPageHeader(handlerDeclarationDV);

		// TODO AVOIDING THE CACHE
		// TODO I NEED TO CHECK WHY THE BY USING THE CACHE THE WEB-FORM IS FULL (EQUAL
		// TO PREVIOUS ONE) AND NOT RESET PERFORMED

//		List<GeoNaFormCardModel> cardsPerIT = geoportalCaches.getGcubeProfilePerItemType(handlerDeclarationDV.getItemType());
//		if (cardsPerIT != null) {
//			GWT.log("Profiles/Cards per Item Type are: " + cardsPerIT);
//			// orderedCards.addAll(cardsPerIT);
//			buildNewCards(profileID, handlerDeclarationDV.getItemType(), new ArrayList<GeoNaFormCardModel>(cardsPerIT));
//			return;
//		}

		numberOfCards = 0;
		expectedCards = listGPs.size();
		GWT.log("expectedCards are: " + expectedCards);
		TreeMap<Integer, GeoNaFormCardModel> treemapOrderedGNAProfiles = new TreeMap<Integer, GeoNaFormCardModel>();

		for (int i = 0; i < expectedCards; i++) {
			GcubeProfileDV gcubeProfile = listGPs.get(i);
			final int order = i;
			GWT.log("calling getProfilesInTheScope for secondaryType: " + gcubeProfile.getGcubeSecondaryType()
					+ ", name: " + gcubeProfile.getGcubeName());

			MetadataProfileFormBuilderServiceAsync.Util.getInstance().getProfilesInTheScopeForName(
					geoportalISConfigs.getScope(), gcubeProfile.getGcubeSecondaryType(), gcubeProfile.getGcubeName(),
					new AsyncCallback<List<MetaDataProfileBean>>() {

						@Override
						public void onFailure(Throwable caught) {
							mainTabPanel.setLoaderVisible("Loading...", false);
							Window.alert(caught.getMessage());

						}

						@Override
						public void onSuccess(List<MetaDataProfileBean> result) {

							incrementNumberOfCards();
							GWT.log("loaded " + getNumberOfCards() + " of " + expectedCards + " card/s");
							GWT.log("Returned " + result.size() + " profile/s");
							// orderedCards = setGeoNaFormsOrder(result);
							for (MetaDataProfileBean metaDataProfileBean : result) {

								String theMetaType = metaDataProfileBean.getType().toLowerCase();
								GWT.log("Building form card for type: " + theMetaType);

								GeoNaFormCardModel geonaForm = buildNewFormCardModelFromProfile(gcubeProfile, order,
										metaDataProfileBean, OPERATION.UPDATE, appManagerBus);

								treemapOrderedGNAProfiles.put(order, geonaForm);
							}

							if (getNumberOfCards() >= expectedCards) {
								mainTabPanel.setLoaderVisible("Loading...", false);
								// ordered values
								Collection<GeoNaFormCardModel> gnaCardsModels = treemapOrderedGNAProfiles.values();
								GWT.log("TreeMap values: " + gnaCardsModels);
								List<GeoNaFormCardModel> listGNAFormCardModel = new ArrayList<GeoNaFormCardModel>(
										gnaCardsModels);
								geoportalCaches.putGcubeProfilePerItemType(handlerDeclarationDV.getItemType(),
										listGNAFormCardModel);
								// orderedCards.addAll(new ArrayList<GeoNaFormCardModel>(gnaCardsModels));
								buildNewCards(profileID, handlerDeclarationDV.getItemType(), listGNAFormCardModel);
							}

						}
					});

		}
	}

	/**
	 * Builds the new form card model from profile.
	 *
	 * @param gcubeProfile        the gcube profile
	 * @param order               the order
	 * @param metaDataProfileBean the meta data profile bean
	 * @param operation           the operation
	 * @return the geo na form card model
	 */
	private GeoNaFormCardModel buildNewFormCardModelFromProfile(GcubeProfileDV gcubeProfile, int order,
			MetaDataProfileBean metaDataProfileBean, OPERATION operation) {

		// Managing Forms repeatability
		int minOccurs = gcubeProfile.getMinOccurs();
		minOccurs = minOccurs <= 0 ? 0 : minOccurs;
		int maxOccurs = gcubeProfile.getMaxOccurs();
		maxOccurs = maxOccurs <= 0 ? Integer.MAX_VALUE : maxOccurs;

		// TODO MUST BE MANAGED MIN_OCCURS
		ProjectFormCard cct = new ProjectFormCard(gcubeProfile.getSectionName(), gcubeProfile.getSectionTitle(), order,
				maxOccurs > 1, minOccurs, maxOccurs);

		GeoNaFormCardModel geoNaFormCardModel = new GeoNaFormCardModel(metaDataProfileBean, null, cct, gcubeProfile);

		CreateMetadataForm baseForm = new CreateMetadataForm(Arrays.asList(geoNaFormCardModel.getMetadataProfileBean()),
				appManagerBus, operation);
		geoNaFormCardModel.setMetadataForm(baseForm);

		return geoNaFormCardModel;
	}

	private void buildNewCards(String profileID, String itemTypeTitle, Collection<GeoNaFormCardModel> orderedCards) {

		projectSavedWithSuccess = false; // resetting state of saving
		mainTabPanel.setLoaderVisible("Loading...", true);
		geoNaMainForm.setVisibleFormActions(true);
		resetUI();
		// geoNaMainForm.removeTree(treeItemPanel);

		geoNaMainForm.enableButtonSave(true);
		mainTabPanel.setLoaderVisible("", false);

		treeItemPanel = new TreeItemPanel(profileID, itemTypeTitle, orderedCards, appManagerBus);
		geoNaMainForm.addTree(treeItemPanel);

		Scheduler.get().scheduleDeferred(new ScheduledCommand() {

			@Override
			public void execute() {
				TreeItem firstChild = treeItemPanel.getRoot().getChild(0);
				treeItemPanel.getTree().setSelectedItem(firstChild, true);
			}
		});

	}

	private void resetUI() {
		geoNaMainForm.resetUI();
		// geoNaMainForm.remeTree(treeItemPanel);
		mapForms.clear();
	}

	private void bindEvents() {

		appManagerBus.addHandler(SaveGeonaDataFormsEvent.TYPE, new SaveGeonaDataFormsHandler() {

			@Override
			public void onSave(final SaveGeonaDataFormsEvent saveGeonaDataFormsEvent) {

				GWT.log("SaveGeonaDataFormsEvent fired");

				// resetting last monitor
				monitorActionsClientInstance = null;

				geoNaMainForm.enableButtonSave(false);

				// TODO REMOVE THIS
				if (saveGeonaDataFormsEvent.getTreeNode() == null) {
					Window.alert("I cannot save the project. Incorrect data. Please, refresh and try again");
					geoNaMainForm.enableButtonSave(true);
					return;
				}

				final Modal modal = new Modal(true);
				modal.setCloseVisible(false);
				modal.setTitle("Saving project...");
				modal.hide(false);
				modal.setWidth(800);
				modal.setMaxHeigth("650px");
				final VerticalPanel modalContainerPanel = new VerticalPanel();
				final VerticalPanel modalContainerMonitor = new VerticalPanel();
				final LoaderIcon loader = new LoaderIcon();
				loader.setText("Trying to save the project, please wait...");
				modalContainerPanel.add(loader);
				modalContainerPanel.add(modalContainerMonitor);
				modal.add(modalContainerPanel);

				String[] listPostActionOnSave = mainTabPanel
						.getPostCreationActionOnSave(saveGeonaDataFormsEvent.getProfileID());

				List<String> stepsOnPostCreation = new ArrayList<String>();
				if (listPostActionOnSave != null) {
					for (String step : listPostActionOnSave) {
						stepsOnPostCreation.add(step);
					}
				}

				GWT.log("Calling saveGeonaDataForms, stepsOnPostCreation are " + stepsOnPostCreation);

				final String uuid = UUIDUtil.generateUUID();
				GeoportalDataEntryServiceAsync.Util.getInstance().saveGeonaDataForms(uuid,
						saveGeonaDataFormsEvent.getProfileID(), saveGeonaDataFormsEvent.getTreeNode(),
						stepsOnPostCreation, new AsyncCallback<Void>() {

							@Override
							public void onFailure(Throwable caught) {
								modalContainerPanel.clear();
								modal.setCloseVisible(true);
								try {
									modal.remove(loader);
								} catch (Exception e) {
								}
								Alert alert = new Alert(caught.getMessage());
								alert.setType(AlertType.ERROR);
								alert.setClose(false);
								modal.add(alert);
								// resetUI();
								geoNaMainForm.enableButtonSave(true);
							}

							@Override
							public void onSuccess(Void result) {
								// null
							}
						});

				modal.show();

				// Monitoring the save actions calling the timer (polling)
				String uuidString = uuid.toString();
				final Timer pollingMonitor = new Timer() {

					@Override
					public void run() {

						if (!GWT.isProdMode()) {
							GWT.log("New polling starts...");
							if (monitorActionsClientInstance != null) {
								GWT.log("New polling for: " + monitorActionsClientInstance.getMonitorUUID());
							}
						}

						// Check if the last action on monitor has been consumed on client side
						boolean monitorConsumedByClient = false;

						if (monitorActionsClientInstance != null) {

							// In case of exception or terminated monitor on server side, I'm going to
							// cancel the monitor on client side (last monitor has been consumed)
							if (monitorActionsClientInstance.getException() != null
									|| monitorActionsClientInstance.isMonitoringTerminatedOnServer()) {
								monitorConsumedByClient = true;
								// cancel the monitor client side
								GWT.log("Cancelled the monitor polling for: "
										+ monitorActionsClientInstance.getMonitorUUID());
								cancel();
								modal.setCloseVisible(true);
							}

						}

						final boolean isMonitorConsumedByClient = monitorConsumedByClient;

						GeoportalDataEntryServiceAsync.Util.getInstance().getSavingProjectMonitorStatus(uuidString,
								monitorConsumedByClient, new AsyncCallback<MonitoringActionsOnClient>() {

									@Override
									public void onFailure(Throwable caught) {

										if (isMonitorConsumedByClient) {
											return;
										}

										modalContainerPanel.clear();
										modal.setCloseVisible(true);
										try {
											modal.remove(loader);
										} catch (Exception e) {
										}
										Alert alert = new Alert(caught.getMessage());
										alert.setType(AlertType.ERROR);
										alert.setClose(false);
										modal.add(alert);
										// resetUI();
										geoNaMainForm.enableButtonSave(true);

									}

									@Override
									public void onSuccess(MonitoringActionsOnClient monitoringActions) {

										// The monitor is consumed here reporting the actions
										monitorActionsClientInstance = monitoringActions;

										if (isMonitorConsumedByClient) {
											return;
										}

										if (monitoringActions != null && monitoringActions.getMonitor() != null) {
											modalContainerMonitor.clear();
											modalContainerMonitor.getElement().getStyle().setMarginTop(15, Unit.PX);
											LinkedHashMap<Integer, MonitoringAction> monitor = monitoringActions
													.getMonitor();
											if (monitor.size() > 0) {
												for (Integer key : monitor.keySet()) {
													MonitoringAction action = monitor.get(key);
													Icon icon = null;
													if (action.getStatus() != null) {
														switch (action.getStatus()) {
														case IN_PENDING:
															icon = new Icon(IconType.HEADPHONES);
															icon.setTitle(action.getStatus().getLabel());
															icon.setSize(IconSize.TWO_TIMES);
															break;
														case IN_PROGESS:
															icon = new Icon(IconType.GEAR);
															icon.setSpin(true);
															icon.setSize(IconSize.TWO_TIMES);
															icon.setTitle(action.getStatus().getLabel());
															break;
														case DONE:
															icon = new Icon(IconType.OK);
															icon.getElement().getStyle().setColor("#008000");
															icon.setSize(IconSize.TWO_TIMES);
															icon.setTitle(action.getStatus().getLabel());
															break;
														case FAILED:
															icon = new Icon(IconType.WARNING_SIGN);
															icon.getElement().getStyle().setColor("#FF0000");
															icon.setSize(IconSize.TWO_TIMES);
															icon.setTitle(action.getStatus().getLabel());
															break;

														default:
															break;
														}
													}

													HorizontalPanel hp = new HorizontalPanel();
													hp.getElement().getStyle().setMargin(10, Unit.PX);
													hp.getElement().getStyle().setMarginLeft(32, Unit.PX);
													com.google.gwt.user.client.ui.Label label = new com.google.gwt.user.client.ui.Label(
															action.getMsg());
													icon.getElement().getStyle().setMarginRight(10, Unit.PX);

													if (icon != null)
														hp.add(icon);

													hp.add(label);

													modalContainerMonitor.add(hp);
												}
											}

											if (monitoringActions.getException() != null) {
												modalContainerPanel.clear();
												modal.setCloseVisible(true);
												try {
													modal.remove(loader);
												} catch (Exception e) {
												}
												Alert alert = new Alert(monitoringActions.getException().getMessage());
												alert.setType(AlertType.ERROR);
												alert.setClose(false);
												modal.add(alert);
												// resetUI();
												geoNaMainForm.enableButtonSave(true);
												return;
											}

											CommitReport commitReport = monitoringActions.getCommitReport();
											if (commitReport != null) {
												modalContainerPanel.clear();
												modal.setCloseVisible(true);
												modal.setTitle("Project Saved!");

												// Getting the linnk
												HorizontalPanel hLink = new HorizontalPanel();
												hLink.getElement().getStyle().setMarginBottom(5, Unit.PX);
												Label linkLabel = new Label("Link");
												linkLabel.getElement().getStyle().setMarginRight(5, Unit.PX);
												linkLabel.setType(LabelType.INFO);
												hLink.add(linkLabel);
												final LoaderIcon loaderIcon = new LoaderIcon("Getting link...");
												hLink.add(loaderIcon);
												modalContainerPanel.add(hLink);

												GeoportalDataEntryServiceAsync.Util.getInstance().getLinksFor(
														commitReport.getProjectID(), commitReport.getProfileID(),
														SHARE_LINK_TO.DATA_ENTRY,
														new AsyncCallback<GeoportalItemReferences>() {

															@Override
															public void onFailure(Throwable caught) {
																hLink.remove(loaderIcon);
																Alert alert = new Alert(caught.getMessage());
																alert.setType(AlertType.ERROR);
																alert.setClose(false);
																hLink.add(alert);

															}

															@Override
															public void onSuccess(
																	GeoportalItemReferences itemReferences) {
																hLink.remove(loaderIcon);

																if (itemReferences.getRestrictedLink() != null) {
																	String theURL = itemReferences.getRestrictedLink()
																			.getShortURL() != null
																					? itemReferences.getRestrictedLink()
																							.getShortURL()
																					: itemReferences.getRestrictedLink()
																							.getCompleteURL();
																	String htmlLink = "<a href=" + theURL
																			+ " target=\"_blank\">" + theURL + "</a>";
																	hLink.add(new HTML(htmlLink));
																} else {
																	Alert alert = new Alert(
																			"Error occurred on getting the link to the project");
																	alert.setType(AlertType.ERROR);
																	alert.setClose(false);
																	hLink.add(alert);
																}

															}
														});

												LifecycleInformationDV lcDV = commitReport.getLifecycleInformation();

												switch (lcDV.getLastOperationStatus()) {
												case OK: {
													String success = HTMLUtil.getHTMLElement(HTML_TAG.span, 14,
															"32CD32", null, "SUCCESS");
													String msg = lcDV.getLastInvokedStep() + "terminated with: "
															+ success;
													projectSavedWithSuccess = true;
													geoNaMainForm.showAlertOnSaveAction(msg, AlertType.SUCCESS, true);
													purgeFileUploaded();
													break;
												}
												case WARNING: {
													geoNaMainForm.enableButtonSave(true);
													break;
												}
												case ERROR: {
													geoNaMainForm.enableButtonSave(true);
													break;
												}
												default:
													break;
												}

												savedMap.put(commitReport.getProjectID(),
														saveGeonaDataFormsEvent.getTreeNode());

												LifecycleInformationPanel lip = new LifecycleInformationPanel(
														commitReport.getProjectID(), commitReport.getProfileID(),
														commitReport.getProjectAsJSON(), lcDV, false);

												modalContainerPanel.add(lip);
											}

										}

									}

								});

					}
				};

				pollingMonitor.scheduleRepeating(1500);
			}
		});

		appManagerBus.addHandler(TreeItemEvent.TYPE, new TreeItemEventHandler() {

			@Override
			public void onTreeItemEvent(TreeItemEvent treeItemEvent) {

				TreeItem selectedItem = treeItemEvent.getSelectItem();
				NodeItem selectedNode = treeItemEvent.selectedAsNodeItem();

				if (selectedItem == null) {
					selectedItem = geoNaMainForm.getTreeItemPanel().getSelectItem();

					if (selectedItem == null)
						return;

					selectedNode = (NodeItem) selectedItem.getWidget();
				}

				GWT.log("TreeItemEvent selected node is: " + selectedNode);

				if (selectedNode.isRoot()) {
					return;
				}

				GeoNaFormCardModel nodeCard = selectedNode.getGeoNaFormCardModel();

				switch (treeItemEvent.getActionPerformed()) {
				case SELECTED: {
					geoNaMainForm.enableButtonRemoveSection(false);
					geoNaMainForm.enableButtonDuplicateSection(false);
					geoNaMainForm.setSelectedForm(nodeCard.getMetadataForm());

//					if(nodeCount>1) {
//						geoNaMainForm.enableButtonRemoveSection(true);
//					}
//
					if (nodeCard.getFormCard().isInternalRepeatibleForm()) {
						geoNaMainForm.enableButtonDuplicateSection(true);
					}

					if (selectedNode.isCanBeDeleted()) {
						geoNaMainForm.enableButtonRemoveSection(true);
					}

					break;
				}
				case DELETE: {

					geoNaMainForm.getTreeItemPanel().removeChild(selectedItem.getParentItem(), selectedItem,
							selectedNode.getJsonSectionFullPath());
					geoNaMainForm.resetInputPanel();
					break;
				}

				case DUPLICATE: {

					GWT.log("DUPLICATING node: " + selectedNode.getHtmlNode());

					int nodeCountPerJSONFullPAth = geoNaMainForm.getTreeItemPanel()
							.countNodeForFullPath(selectedNode.getJsonSectionFullPath());

					if (nodeCountPerJSONFullPAth >= nodeCard.getFormCard().getMaxFormRepeatability()) {

						DialogInform di = new DialogInform(new Image(Images.ICONS.accessDenied()), "Maximun reached",
								"Maximun number of data reached for " + nodeCard.getFormCard().getTitle());
						di.center();
						return;
					}

					TreeItem newSubTree = cloneSubTreeItems(selectedItem);
					// TO DEBUG
					// TreeVisitUtil.postOrderVisit(newSubTree);
					TreeItem parentItem = selectedItem.getParentItem();
					int nextSiblingIndex = -1;
					if (parentItem != null) {
						nextSiblingIndex = parentItem.getChildIndex(selectedItem);
						nextSiblingIndex++;
					}
					geoNaMainForm.getTreeItemPanel().addChild(selectedItem.getParentItem(), nextSiblingIndex,
							newSubTree);
					break;
				}

				default:
					break;
				}

			}
		});

		appManagerBus.addHandler(CreateNewProjectEvent.TYPE, new CreateNewProjectEventHandler() {

			@Override
			public void onCreateNewProject(CreateNewProjectEvent newProjectEvent) {
				GWT.log("Create new project");
				if (!dataEntryProjectCreated) {
					createNewDataEntyFor(newProjectEvent.getProfileID(), newProjectEvent.getHandler(),
							newProjectEvent.getListGcubeProfiles());
					return;
				}

				// means the form was not submitted
				if (savedMap.size() == 0) {

					ModalConfirm confirm = new ModalConfirm(null, "Creating New Project...",
							"The current project (is not saved) and will be lost. Confirm?");

					confirm.getYesButton().addClickHandler(new ClickHandler() {

						@Override
						public void onClick(ClickEvent event) {
							confirm.hide();

							createNewDataEntyFor(newProjectEvent.getProfileID(), newProjectEvent.getHandler(),
									newProjectEvent.getListGcubeProfiles());
						}

					});

					confirm.show();

				} else if (projectSavedWithSuccess) {
					// means the project has been saved
					createNewDataEntyFor(newProjectEvent.getProfileID(), newProjectEvent.getHandler(),
							newProjectEvent.getListGcubeProfiles());
				} else {

					ModalConfirm confirm = new ModalConfirm(null, "Creating New Project...",
							"The data entry form/s will be cleaned. Confirm?");

					confirm.getYesButton().addClickHandler(new ClickHandler() {

						@Override
						public void onClick(ClickEvent event) {
							confirm.hide();

							createNewDataEntyFor(newProjectEvent.getProfileID(), newProjectEvent.getHandler(),
									newProjectEvent.getListGcubeProfiles());

						}

					});

					confirm.show();
				}

			}
		});

		appManagerBus.addHandler(GetListOfRecordsEvent.TYPE, new GetListOfRecordsEventHandler() {

			@Override
			public void onGetList(GetListOfRecordsEvent getListOfRecordsEvent) {
				GWT.log("Fired onGetList: " + getListOfRecordsEvent);
				SearchingFilter searchingFilter = getListOfRecordsEvent.getSearchingFilter();

				CacheSearchingFilterParametersFromConfig seachingFilterParameters = geoportalCaches
						.getFilterParametersForProfileId(getListOfRecordsEvent.getProfileID(),
								GEOPORTAL_DATA_HANDLER.geoportal_data_list);

//				CacheSearchingFilterParametersFromConfig seachingFilterParameters = geoportalCaches
//						.getFilterParametersForProfileId(getListOfRecordsEvent.getProfileID());

				final String profileID = getListOfRecordsEvent.getProfileID();
				if (profileID == null) {
					new DialogInform(null, "Error", "No Use Case Descriptor selected").center();
					return;
				}

				if (searchingFilter == null || getListOfRecordsEvent.isOnApplicationInit()
						|| getListOfRecordsEvent.isReloadFilteringParameters()) {
					mainTabPanel.setFilteringParameters(seachingFilterParameters);

					// Searching in the cache if the list of relationship definition is present
					List<RelationshipDefinitionDV> listRelationshipsDef = geoportalCaches
							.getListRelationshipDefinitionForProfileId(profileID);
					// Loading list of relationships definition ProfileID
					if (listRelationshipsDef == null || listRelationshipsDef.isEmpty()) {

						GeoPortalDataEntryApp.geoportalDataEntryService.getRelationshipsDefinition(profileID,
								new AsyncCallback<List<RelationshipDefinitionDV>>() {

									@Override
									public void onSuccess(List<RelationshipDefinitionDV> relationshipDef) {
										GWT.log("getRelationshipNames for " + profileID + " are: " + relationshipDef);
										geoportalCaches.putListRelationshipsDefinitionForProfileID(profileID,
												relationshipDef);
										mainTabPanel.enableRelatioshipFacilities(
												relationshipDef != null && !relationshipDef.isEmpty());
									}

									@Override
									public void onFailure(Throwable caught) {

									}
								});
					}

					if (searchingFilter == null) {
						searchingFilter = mainTabPanel.getCurrentProjectsSearchingFilter();
					}
				}

				searchingFilter.setProjection(seachingFilterParameters.getProjection());
				List<ItemFieldDV> displayFields = seachingFilterParameters.getDisplayFields();

				grpw = new GeonaRecordsPaginatedView(appManagerBus, profileID, displayFields, searchingFilter);
				mainTabPanel.showListOfProjectsView(grpw);

				// The Project Type is changed
				if (getListOfRecordsEvent.isReloadFilteringParameters()) {
					// Setting the action panel according to profileID
					mainTabPanel.initActionListPanel(geoportalCaches.getUCDForProfileID(profileID));
					// Displaying the create relation button only if list of relation names exits

				}

			}
		});

		appManagerBus.addHandler(ClickItemEvent.TYPE, new ClickItemEventHandler() {

			@Override
			public <T> void onClick(ClickItemEvent<T> clickItemEvent) {

				if (clickItemEvent.getSelectItems() != null) {
					mainTabPanel.showActionsOnSelected(clickItemEvent.getSelectItems());
				}

			}
		});

		appManagerBus.addHandler(RelationActionHandlerEvent.TYPE, new RelationActionHandler() {

			@Override
			public void onCreateRelation(RelationActionHandlerEvent createRelationHE) {

				if (createRelationHE.getRelactionActionType() == null)
					return;

				switch (createRelationHE.getRelactionActionType()) {
				case DELETE: {

					GcubeUserRole myRole = myRights.getRoleRights().getUserRole();

					// TODO REMOVE THIS IF AFTER ADDING ALL THE OPERATIONS IN THE IS
					// OPERATIONS/RIGHTS CONFIGURATION
					if (!myRole.equals(GcubeUserRole.DATA_MANAGER) && !myRole.equals(GcubeUserRole.DATA_EDITOR)) {
						String action = RELACTION_ACTION_TYPE.DELETE + " Relation";

						String msg = "You are not authorized to perform the action: " + action;
						ModalWindow modalW = new ModalWindow(new Image(Images.ICONS.accessDenied()),
								"Forbidden: " + action, msg, AlertType.WARNING);
						modalW.show();
						return;
					}

					if (createRelationHE.getFromProject() == null || createRelationHE.getToProject() == null) {
						DialogInform di = new DialogInform(null, "No selection", "You must select a Project");
						di.center();
						return;
					}

					String firstProject = createRelationHE.getFromProject().getFirstEntryOfMap().getKey() + ": "
							+ createRelationHE.getFromProject().getFirstEntryOfMap().getValue() + " (id:"
							+ createRelationHE.getFromProject().getId() + ")";

					final String relationName = createRelationHE.getRelationName();

					String secondProject = createRelationHE.getToProject().getFirstEntryOfMap().getKey() + ": "
							+ createRelationHE.getToProject().getFirstEntryOfMap().getValue() + " (id:"
							+ createRelationHE.getToProject().getId() + ")";

					String htmlMsg = "Going to delete the relation <b>" + relationName + "</b><br>";
					htmlMsg += "<i style='color:#333;'>From Project:</i><ul><li>" + firstProject + "</li></ul>";
					htmlMsg += "<b>" + relationName + "</b><br>";
					htmlMsg += "<i style='color:#333;'>To Project:</i><ul><li>" + secondProject + "</li></ul>";
					htmlMsg += "<br>";
					htmlMsg += "Would you like to proceed?";

					final ModalConfirm dialog = new ModalConfirm(null,
							"Deleting relationship " + relationName + ", Confirm?", htmlMsg);

					dialog.getYesButton().addClickHandler(new ClickHandler() {

						@Override
						public void onClick(ClickEvent event) {
							dialog.hide();

							final Modal modal = new Modal(true, true);
							modal.setTitle("Deleting relationship...");
							modal.setWidth(800);
							modal.setCloseVisible(false);
							String fromProfileID = createRelationHE.getFromProject().getProfileID();
							String fromProjectID = createRelationHE.getFromProject().getId();
							String toProfileID = createRelationHE.getToProject().getProfileID();
							String toProjectID = createRelationHE.getToProject().getId();
							final VerticalPanel modalContainerPanel = new VerticalPanel();
							final LoaderIcon loader = new LoaderIcon("Trying to delete the relation " + relationName
									+ " from Project ID: " + fromProjectID + " to Project ID: " + toProjectID);
							modalContainerPanel.add(loader);
							modal.add(modalContainerPanel);

							GeoportalDataEntryServiceAsync.Util.getInstance().deleteRelationship(fromProfileID,
									fromProjectID, relationName, toProfileID, toProjectID,
									new AsyncCallback<ResultDocumentDV>() {

										@Override
										public void onFailure(Throwable caught) {
											modalContainerPanel.clear();
											modal.setCloseVisible(true);
											try {
												modal.remove(loader);
											} catch (Exception e) {
											}
											Alert alert = new Alert(caught.getMessage());
											alert.setType(AlertType.ERROR);
											alert.setClose(false);
											modal.add(alert);

										}

										@Override
										public void onSuccess(ResultDocumentDV result) {
											modal.setTitle("Relationship deleted");
											modalContainerPanel.clear();
											modal.setCloseVisible(true);
											try {
												modal.remove(loader);
											} catch (Exception e) {
												// TODO: handle exception
											}
											Alert alert = new Alert();
											alert.setType(AlertType.SUCCESS);
											alert.setClose(false);
											alert.setText("Relationship " + relationName + " deleted correctly!");
											modal.add(alert);

											String htmlMsg = "In the project with:";
											htmlMsg += "<ul>";
											htmlMsg += "<li>id: " + fromProjectID + "</li>";
											htmlMsg += "<li>profile: " + fromProfileID + "</li>";
											htmlMsg += "<li>" + result.getFirstEntryOfMap().getKey() + ": "
													+ result.getFirstEntryOfMap().getValue() + "</li>";
											htmlMsg += "</ul>";
											htmlMsg += "has been deleted the relationship <b>" + relationName
													+ "</b> to the project wiht id: " + toProjectID;
											modal.add(new HTML(htmlMsg));
//											ReportTemplateToHTML rtth = new ReportTemplateToHTML("Relationship", result.getAsJSON(),
//													false, false);
//											rtth.showAsJSON(false);
//											modal.add(rtth);

											List<DocumentDV> listDocuments = Arrays.asList(result);

											appManagerBus.fireEvent(new OperationOnItemEvent<DocumentDV>(listDocuments,
													OPERATION_ON_ITEM.VIEW_RELATIONSHIPS));

											appManagerBus.fireEvent(new GetListOfRecordsEvent(false, fromProfileID,
													mainTabPanel.getCurrentProjectsSearchingFilter(), true));

										}
									});

							modal.show();
						}

					});

					dialog.show();

					break;
				}
				case CREATE: {

					if (createRelationHE.getFromProject() == null || createRelationHE.getToProject() == null) {
						DialogInform di = new DialogInform(null, "No selection",
								"You must select a Project from/to in the table");
						di.center();
						return;
					}

					String firstProject = createRelationHE.getFromProject().getFirstEntryOfMap().getKey() + ": "
							+ createRelationHE.getFromProject().getFirstEntryOfMap().getValue() + " (id:"
							+ createRelationHE.getFromProject().getId() + ")";

					String relation = createRelationHE.getRelationSelected().getLabel();

					String secondProject = createRelationHE.getToProject().getFirstEntryOfMap().getKey() + ": "
							+ createRelationHE.getToProject().getFirstEntryOfMap().getValue() + " (id:"
							+ createRelationHE.getToProject().getId() + ")";

					String htmlMsg = "Going to create the relation <b>" + relation + "</b><br>";
					htmlMsg += "<i style='color:#333;'>From Project:</i><ul><li>" + firstProject + "</li></ul>";
					htmlMsg += "<b>" + relation + "</b><br>";
					htmlMsg += "<i style='color:#333;'>To Project:</i><ul><li>" + secondProject + "</li></ul>";
					htmlMsg += "<br>";
					htmlMsg += "Would you like to proceed?";

					final ModalConfirm dialog = new ModalConfirm(null,
							"Creating relationship " + relation + ", Confirm?", htmlMsg);

					dialog.getYesButton().addClickHandler(new ClickHandler() {

						@Override
						public void onClick(ClickEvent event) {
							dialog.hide();

							final Modal modal = new Modal(true, true);
							modal.setTitle("Creating relationship...");
							modal.setWidth(800);
							modal.setCloseVisible(false);
							final RelationshipDefinitionDV relationSelected = createRelationHE.getRelationSelected();
							String fromProfileID = createRelationHE.getFromProject().getProfileID();
							String fromProjectID = createRelationHE.getFromProject().getId();
							String toProfileID = createRelationHE.getToProject().getProfileID();
							String toProjectID = createRelationHE.getToProject().getId();
							final VerticalPanel modalContainerPanel = new VerticalPanel();
							final LoaderIcon loader = new LoaderIcon(
									"Trying to create the relation " + relationSelected.getLabel()
											+ " from Project ID: " + fromProjectID + " to Project ID: " + toProjectID);
							modalContainerPanel.add(loader);
							modal.add(modalContainerPanel);

							GeoportalDataEntryServiceAsync.Util.getInstance().createRelationship(fromProfileID,
									fromProjectID, relationSelected.getId(), toProfileID, toProjectID,
									new AsyncCallback<Void>() {

										@Override
										public void onFailure(Throwable caught) {
											modalContainerPanel.clear();
											modal.setCloseVisible(true);
											try {
												modal.remove(loader);
											} catch (Exception e) {
											}
											Alert alert = new Alert(caught.getMessage());
											alert.setType(AlertType.ERROR);
											alert.setClose(false);
											modal.add(alert);

										}

										@Override
										public void onSuccess(Void result) {
											modal.setTitle("Relationship created");
											modalContainerPanel.clear();
											modal.setCloseVisible(true);
											try {
												modal.remove(loader);
											} catch (Exception e) {
												// TODO: handle exception
											}
											Alert alert = new Alert();
											alert.setType(AlertType.SUCCESS);
											alert.setClose(false);
											alert.setText("Relationship " + relationSelected.getLabel()
													+ " created correctly!");
											modal.add(alert);

											String htmlMsg = "In the project with:";
											htmlMsg += "<ul>";
											htmlMsg += "<li>id: " + fromProjectID + "</li>";
											htmlMsg += "<li>profile: " + fromProfileID + "</li>";
											htmlMsg += "<li>"
													+ createRelationHE.getFromProject().getFirstEntryOfMap().getKey()
													+ ": "
													+ createRelationHE.getFromProject().getFirstEntryOfMap().getValue()
													+ "</li>";
											htmlMsg += "</ul>";
											htmlMsg += "has been created the relationship <b>"
													+ relationSelected.getLabel() + "</b> to the project wiht id: "
													+ toProjectID;
											modal.add(new HTML(htmlMsg));
//											ReportTemplateToHTML rtth = new ReportTemplateToHTML("Relationship", result.getAsJSON(),
//													false, false);
//											rtth.showAsJSON(false);
//											modal.add(rtth);

											appManagerBus.fireEvent(new CloseCreateRelationGUIEvent());

											appManagerBus.fireEvent(new GetListOfRecordsEvent(false, fromProfileID,
													mainTabPanel.getCurrentProjectsSearchingFilter(), true));
										}
									});

							modal.show();
						}

					});

					dialog.show();

				}
				default:
					break;
				}

			}
		});

		appManagerBus.addHandler(CloseCreateRelationGUIEvent.TYPE, new CloseCreateRelationGUIEventHandler() {

			@Override
			public void onClose(CloseCreateRelationGUIEvent closeCreateRelationGUI) {
				mainTabPanel.showCreateRelationPanel(false, null);

			}
		});

		appManagerBus.addHandler(WorkflowActionOnSelectedItemEvent.TYPE,
				new WorkflowActionOnSelectedItemEventHandler() {

					@Override
					public <T extends DocumentDV> void onDoActionFired(
							final WorkflowActionOnSelectedItemEvent<T> wActionOnItem) {
						GWT.log("WorkflowActionOnSelectedItemEvent onDoActionFired item: " + wActionOnItem);

						if (grpw.getSelectItems() == null || grpw.getSelectItems().size() == 0) {
							DialogInform di = new DialogInform(null, "No selection",
									"You must select a record in the table");
							di.center();
							return;
						}

						DocumentDV item = grpw.getSelectItems().get(0);

						final ResultDocumentDV resultDocumentDV = (ResultDocumentDV) item;

						final ModalConfirm dialog = new ModalConfirm(null,
								"Step/s " + wActionOnItem.getAction().getTitle() + ", Confirm?", null);

						final StepViewPanel stvp = new StepViewPanel(resultDocumentDV, wActionOnItem.getAction());
						dialog.addToCenterPanel(stvp);

						dialog.getYesButton().addClickHandler(new ClickHandler() {

							@Override
							public void onClick(ClickEvent event) {
								dialog.hide();

								final Modal modal = new Modal(true, true);
								modal.setCloseVisible(false);
								modal.setTitle("Performing Steps...");
								modal.hide(false);
								modal.setWidth(800);
								modal.setMaxHeigth("650px");
								final VerticalPanel modalContainerPanel = new VerticalPanel();
								final LoaderIcon loader = new LoaderIcon();
								loader.setText("Trying to perfom step/s " + wActionOnItem.getAction().getCallSteps()
										+ ", please wait...");
								modalContainerPanel.add(loader);
								modal.add(modalContainerPanel);

								GeoportalDataEntryServiceAsync.Util.getInstance().performActionSteps(
										resultDocumentDV.getProfileID(), resultDocumentDV.getId(),
										stvp.getOptionalMessage(), wActionOnItem.getAction(),
										new AsyncCallback<StepPerformedResultDV>() {

											@Override
											public void onFailure(Throwable caught) {
												modal.setCloseVisible(true);
												try {
													modalContainerPanel.remove(loader);
												} catch (Exception e) {
												}
												Alert alert = new Alert(caught.getMessage());
												alert.setType(AlertType.ERROR);
												alert.setClose(false);
												modal.add(alert);
											}

											@Override
											public void onSuccess(final StepPerformedResultDV result) {
												modal.setTitle("Step/s performed");
												modal.setCloseVisible(true);
												try {
													modalContainerPanel.remove(loader);
												} catch (Exception e) {
												}

												String stepString = wActionOnItem.getAction().getCallSteps().length == 1
														? "Step"
														: "Steps";

												String stepsToString = "";
												for (String step : wActionOnItem.getAction().getCallSteps()) {
													stepsToString += step + " ";
												}

												Status status = Status.NOT_SPECIFIED;
												AlertType alertType = AlertType.INFO;
												String statusMsg = "status " + status.getLabel();
												try {
													status = result.getLifecycleInfoDV().getLastOperationStatus();
													switch (status) {
													case OK:
														alertType = AlertType.SUCCESS;
														statusMsg = "performed with " + status.getLabel().toUpperCase()
																+ "!";
														break;
													case WARNING:
														alertType = AlertType.WARNING;
														statusMsg = "performed with " + status.getLabel().toUpperCase()
																+ "!";
														break;
													case ERROR:
														alertType = AlertType.ERROR;
														statusMsg = "performed with " + status.getLabel().toUpperCase()
																+ "!";
														break;
													default:
														alertType = AlertType.INFO;
														statusMsg = "performed. Current status " + status.getLabel();
														break;
													}
												} catch (Exception e) {
													// silent
												}

												Alert alert = new Alert(
														stepString + " " + stepsToString + " " + statusMsg);
												alert.setType(alertType);
												alert.setClose(false);
												modal.add(alert);

												GeoportalDataEntryServiceAsync.Util.getInstance()
														.getResultDocumentFoProjectByID(result.getProfileID(),
																result.getProjectId(),
																new AsyncCallback<ResultDocumentDV>() {

																	@Override
																	public void onFailure(Throwable caught) {
																		Window.alert("Error: " + caught.getMessage());

																	}

																	@Override
																	public void onSuccess(
																			ResultDocumentDV theResultDocument) {
																		VerticalPanel vp = new VerticalPanel();
																		vp.getElement().getStyle().setMarginTop(20,
																				Unit.PX);
																		vp.add(new Label(
																				"Check outcome in the Publication Report"));
																		Button buttonShowPublicationReport = new Button(
																				"Show Publication Report");
																		buttonShowPublicationReport
																				.setType(ButtonType.INFO);
																		buttonShowPublicationReport
																				.addClickHandler(new ClickHandler() {

																					@Override
																					public void onClick(
																							ClickEvent event) {
																						modal.hide();
																						appManagerBus.fireEvent(
																								new OperationOnItemEvent<DocumentDV>(
																										Arrays.asList(
																												theResultDocument),
																										OPERATION_ON_ITEM.VIEW_REPORT));

																					}
																				});
																		buttonShowPublicationReport.getElement()
																				.getStyle().setMarginTop(10, Unit.PX);
																		buttonShowPublicationReport.getElement()
																				.getStyle()
																				.setMarginBottom(20, Unit.PX);
																		vp.add(buttonShowPublicationReport);

																		modal.add(vp);

																	}
																});

												appManagerBus.fireEvent(new GetListOfRecordsEvent(false,
														resultDocumentDV.getProfileID(),
														mainTabPanel.getCurrentProjectsSearchingFilter(), true));

											}
										});

								modal.show();

							}

						});

						dialog.show();

					}
				});

		appManagerBus.addHandler(OperationPerformedOnItemEvent.TYPE, new OperationPerformedOnItemEventHandler() {

			@Override
			public <T extends DocumentDV> void onDoActionPerformedFired(
					OperationPerformedOnItemEvent<T> actionPerformedOnItemEvent) {
				if (actionPerformedOnItemEvent != null) {

					ACTION_PERFORMED_ON_ITEM action = actionPerformedOnItemEvent.getAction();
					List<T> items = actionPerformedOnItemEvent.getSelectItems();

					if (items == null) {
						if (grpw != null) {
							items = (List<T>) grpw.getSelectItems();
						}
					}

					if (items == null || items.size() == 0) {
						Window.alert("No item selected");
						return;
					}

					T item = items.get(0);

					if (item == null)
						return;

					if (item instanceof DocumentDV) {
						final DocumentDV concessione = (DocumentDV) item;
						GWT.log("onDoActionPerformedFired item: " + concessione);
						switch (action) {
						case UPDATED_PROJECT:
//							appManagerBus.fireEvent(new GetListOfRecordsEvent(RECORD_TYPE.CONCESSIONE,
//									actionPerformedOnItemEvent.getProfileID(), null,
//									mainTabPanel.getCurrentSortFilter()));

							appManagerBus.fireEvent(new GetListOfRecordsEvent(false, grpw.getProfileID(),
									mainTabPanel.getCurrentProjectsSearchingFilter(), false));
							break;
						}
					}
				}

			}
		});

		appManagerBus.addHandler(OperationOnItemEvent.TYPE, new OperationOnItemEventHandler() {

			@Override
			public <T extends DocumentDV> void onDoActionFired(OperationOnItemEvent<T> showItemEvent) {

				GWT.log("Fired event: " + showItemEvent);

				if (showItemEvent != null) {

					OPERATION_ON_ITEM action = showItemEvent.getAction();
					List<T> items = showItemEvent.getSelectItems();

					if (items == null) {
						if (grpw != null) {
							items = (List<T>) grpw.getSelectItems();
						}
					}

					if (items == null || items.size() == 0) {
						DialogInform di = new DialogInform(null, "No selection",
								"You must select a record in the 'List of Projects' table");
						di.center();
						return;
					}

					T item = items.get(0);

					if (item == null)
						return;

					if (item instanceof ResultDocumentDV) {

						final ResultDocumentDV resultDocumentDV = (ResultDocumentDV) item;
						GWT.log("onDoActionFired item: " + resultDocumentDV);

						// Checking if the user is NOT authorized to perform this action in two ways:
						// 1. the map of actions allowed for the user does not contain the action;
						RoleRights roleRights = myRights.getRoleRights();
						OPERATION_TYPE operationType = roleRights.getListPermessions().get(action);
						boolean allowedOperationForUser = operationType != null ? true : false;

						GcubeUserRole myRole = myRights.getRoleRights().getUserRole();

						// TODO REMOVE THIS IF AFTER ADDING ALL THE OPERATIONS IN THE IS
						// OPERATIONS/RIGHTS CONFIGURATION
						if (!myRole.equals(GcubeUserRole.DATA_MANAGER)) {

							if (!allowedOperationForUser) {

								String msg = "You are not authorized to perform the action: " + action.getLabel();
								ModalWindow modalW = new ModalWindow(new Image(Images.ICONS.accessDenied()),
										"Forbidden: " + action, msg, AlertType.WARNING);
								modalW.show();
								return;
							}
							// 2. the user cannot access to action (e.g. the user cannot write an item of
							// another user)
							// if so an error is shown
							GcubeUserRole userRole = myRights.getRoleRights().getUserRole();
							String creatorUsername = "";
							try {
								creatorUsername = resultDocumentDV.getPublicationInfoDV().getCreationInfo()
										.getUsername();
							} catch (Exception e) {
								// TODO: handle exception
							}
							boolean allowedWriteActionOnItem = checkAccessToOperationType(userRole, operationType,
									creatorUsername);
							if (!allowedWriteActionOnItem) {
								String title = "Forbidden: missing write access";
								String msg = "You do not have write access to item: " + resultDocumentDV.getId();
								if (!userRole.isWriteOwn()) {
									msg = "You do not have write access to item: " + resultDocumentDV.getId();
								} else if (!userRole.isWriteAny()) {
									title = "Forbidden: missing ownership";
									msg = "You are not the creator of: " + resultDocumentDV.getId()
											+ ". You cannot access to it";
								}
								ModalWindow modalW = new ModalWindow(new Image(Images.ICONS.accessDenied()), title, msg,
										AlertType.WARNING);
								modalW.show();
								return;
							}
						}

						switch (action) {
						case VIEW_ON_MAP: {

							final Modal modal = new Modal(true, true);
							modal.setTitle("Show on Map the Project...");
							modal.setCloseVisible(true);
							if (resultDocumentDV.getLifecycleInfo() != null) {
								String phase = resultDocumentDV.getLifecycleInfo().getPhase();
								// IF the project is not in DRAFT phase, showing an alert and no Update Mode
								// will
								// be activated
								if (phase != null && phase.compareToIgnoreCase(WORKFLOW_PHASE.DRAFT.getLabel()) == 0) {

									Alert alert = new Alert(
											ConstantsGeoPortalDataEntryApp.SHOW_ON_MAP_NOT_AVAILABLE_IN_DRAFT);
									alert.setType(AlertType.WARNING);
									alert.setClose(false);
									modal.add(alert);
									modal.show();
									return;
								}
							}

							final HorizontalPanel hpGetLink = new HorizontalPanel();
							final LoaderIcon lc = new LoaderIcon("Just moment getting link...");
							hpGetLink.add(lc);
							modal.add(hpGetLink);

							GeoportalDataEntryServiceAsync.Util.getInstance().getLinksFor(resultDocumentDV.getId(),
									resultDocumentDV.getProfileID(), GeoportalItemReferences.SHARE_LINK_TO.DATA_VIEWER,
									new AsyncCallback<GeoportalItemReferences>() {
										@Override
										public void onFailure(Throwable caught) {
											try {
												hpGetLink.setVisible(false);
												modal.remove(hpGetLink);
											} catch (Exception e) {
												// TODO: handle exception
											}
											Alert alert = new Alert(caught.getMessage(), AlertType.ERROR);
											alert.setClose(false);
											modal.add(alert);
											// newBrowserWindow.close();
										}

										@Override
										public void onSuccess(GeoportalItemReferences result) {
											try {
												hpGetLink.setVisible(false);
												modal.remove(hpGetLink);
											} catch (Exception e) {
												// TODO: handle exception
											}
											String theURL = result.getRestrictedLink().getShortURL() != null
													? result.getRestrictedLink().getShortURL()
													: result.getRestrictedLink().getCompleteURL();
											// newBrowserWindow.setUrl(theURL);

											Anchor anchor = new Anchor(theURL);
											anchor.setHref(theURL);
											anchor.setTarget("_blank");
											anchor.setTitle(
													"Show on Map the project with id: " + resultDocumentDV.getId());
											com.github.gwtbootstrap.client.ui.Label label = new com.github.gwtbootstrap.client.ui.Label(
													"Go to Map by clicking the link");
											label.setType(LabelType.SUCCESS);

											modal.add(label);
											modal.add(new HTML("<br>"));
											modal.add(anchor);

										}
									});

							modal.show();

							break;
						}

						case GET_SHAREABLE_LINK: {

							final Modal modal = new Modal(true, true);
							modal.setTitle("Share the Project by link...");
							modal.setCloseVisible(true);
							final HorizontalPanel hpGetLink = new HorizontalPanel();
							final LoaderIcon lc = new LoaderIcon("Just moment getting link...");
							hpGetLink.add(lc);
							modal.add(hpGetLink);

							GeoportalDataEntryServiceAsync.Util.getInstance().getLinksFor(resultDocumentDV.getId(),
									resultDocumentDV.getProfileID(), GeoportalItemReferences.SHARE_LINK_TO.DATA_ENTRY,
									new AsyncCallback<GeoportalItemReferences>() {
										@Override
										public void onFailure(Throwable caught) {
											try {
												hpGetLink.setVisible(false);
												modal.remove(hpGetLink);
											} catch (Exception e) {
												// TODO: handle exception
											}
											Alert alert = new Alert(caught.getMessage(), AlertType.ERROR);
											alert.setClose(false);
											modal.add(alert);
											// newBrowserWindow.close();
										}

										@Override
										public void onSuccess(GeoportalItemReferences result) {
											try {
												hpGetLink.setVisible(false);
												modal.remove(hpGetLink);
											} catch (Exception e) {
												// TODO: handle exception
											}
											String theURL = result.getRestrictedLink().getShortURL() != null
													? result.getRestrictedLink().getShortURL()
													: result.getRestrictedLink().getCompleteURL();
											// newBrowserWindow.setUrl(theURL);

											Anchor anchor = new Anchor(theURL);
											anchor.setHref(theURL);
											anchor.setTarget("_blank");
											anchor.setTitle("Shareable link of the project with id: "
													+ resultDocumentDV.getId());
											com.github.gwtbootstrap.client.ui.Label label = new com.github.gwtbootstrap.client.ui.Label(
													"Shareable link");
											label.setType(LabelType.SUCCESS);

											modal.add(label);
											modal.add(new HTML("<br>"));
											modal.add(anchor);

										}
									});

							modal.show();

							break;
						}

						case VIEW_REPORT: {

							final Modal modal2 = new Modal(true, true);
							modal2.setTitle(
									"<span style='font-size:20px;'>Publication Report for Project ID: <span style='color:#555; font-size:20px;'>"
											+ resultDocumentDV.getId() + "</span></span>");
							modal2.setWidth(800);
							modal2.setCloseVisible(true);
							final LoaderIcon lc = new LoaderIcon("Just moment loading lifecycle info...");
							modal2.add(lc);

							GeoportalDataEntryServiceAsync.Util.getInstance().getLifecycleInfoForProjectId(
									resultDocumentDV.getProfileID(), resultDocumentDV.getId(),
									new AsyncCallback<LifecycleInformationDV>() {

										@Override
										public void onFailure(Throwable caught) {
											lc.setVisible(false);
											modal2.add(
													new HTML("No report available for: " + resultDocumentDV.getId()));
											modal2.add(new Alert(caught.getMessage(), AlertType.ERROR));
										}

										@Override
										public void onSuccess(LifecycleInformationDV result) {
											lc.setVisible(false);
											LifecycleInformationPanel lip = new LifecycleInformationPanel(
													resultDocumentDV.getId(), resultDocumentDV.getProfileID(),
													resultDocumentDV.getDocumentAsJSON(), result, false);
											modal2.add(lip);

										}
									});

							modal2.show();

							break;
						}

						case EDIT_PROJECT: {

							final Modal modal3 = new Modal(true, true);
							modal3.setTitle(
									"<span style='font-size:20px;'>Update Project with id: <span style='color:#555; font-size:20px;'>"
											+ resultDocumentDV.getId() + "</span></span>");
//							modal3.setWidth(950);
//							modal3.setHeight("700px");
							modal3.setCloseVisible(true);
							((Element) modal3.getElement().getChildNodes().getItem(1)).addClassName("modal-body-edit");
							int height = Window.getClientHeight() * 70 / 100;

							int width = Window.getClientWidth() * 70 / 100;
							modal3.setWidth(width);
							modal3.setHeight(height + "px");

							// #24569
							boolean isNotInDRAFT = false;

							if (resultDocumentDV.getLifecycleInfo() != null) {
								String phase = resultDocumentDV.getLifecycleInfo().getPhase();
								// IF the project is not in DRAFT phase, showing an alert and no Update Mode
								// will
								// be activated
								if (phase != null && phase.compareToIgnoreCase(WORKFLOW_PHASE.DRAFT.getLabel()) != 0) {

									Alert alert = new Alert(
											ConstantsGeoPortalDataEntryApp.ALERT_MESSAGE_PROJECT_NOT_EDITABLE);
									alert.setType(AlertType.WARNING);
									alert.setClose(false);
									modal3.add(alert);

									isNotInDRAFT = true;
								}
							}

							UpdateRecord ur = new UpdateRecord(appManagerBus, resultDocumentDV.getProfileID(),
									resultDocumentDV.getId(), width, height);

							if (isNotInDRAFT) {
								ur.noUpdateMode();
							}

							modal3.add(ur);
							modal3.show();
							break;
						}
						case CREATE_RELATION: {

							// #24571
							boolean isNotInDRAFT = false;

							if (resultDocumentDV.getLifecycleInfo() != null) {
								String phase = resultDocumentDV.getLifecycleInfo().getPhase();
								// IF the project is not in DRAFT, showing an alert and the no Update Mode will
								// be activated
								if (phase != null && phase.compareToIgnoreCase(WORKFLOW_PHASE.DRAFT.getLabel()) != 0) {

									String msg = ConstantsGeoPortalDataEntryApp.ALERT_MESSAGE_CREATE_RELATION_FORBIDDEN;
									ModalWindow modalW = new ModalWindow(new Image(Images.ICONS.accessDenied()),
											"Forbidden: " + action, msg, AlertType.WARNING);
									modalW.show();

									isNotInDRAFT = true;
								}
							}

							// Allowing the Create Relation only in DRAFT phase
							if (!isNotInDRAFT) {
								mainTabPanel.showCreateRelationPanel(true, resultDocumentDV);
							}

							break;
						}

						case VIEW_RELATIONSHIPS: {
							GWT.log("VIEW_RELATIONSHIPS fired");
							mainTabPanel.showViewProjectRelationsPanel(true, resultDocumentDV);

							break;
						}

						// It is the show document
						case VIEW_PROJECT_AS_DOCUMENT: {
							GWT.log("VIEW VIEW_PROJECT_AS_DOCUMENT fired");

							final Modal modal = new Modal(true, true);
							modal.setCloseVisible(true);
							final int height = Window.getClientHeight() * 70 / 100;
							int width = Window.getClientWidth() * 70 / 100;
							modal.setMaxHeigth("none");
							modal.setWidth(width);
							modal.setHeight(height + "px");
							modal.setTitle(
									"<span style='font-size:20px;'>View Document for Project ID: <span style='color:#555; font-size:20px;'>"
											+ resultDocumentDV.getId() + "</span></span>");
							final HorizontalPanel hp = new HorizontalPanel();
							final LoaderIcon lc = new LoaderIcon("Loading Project... please wait");
							hp.add(lc);
							modal.add(hp);
							GeoportalDataEntryServiceAsync.Util.getInstance().getProjectView(
									resultDocumentDV.getProfileID(), resultDocumentDV.getProjectID(),
									new AsyncCallback<ProjectView>() {

										@Override
										public void onFailure(Throwable caught) {
											hp.clear();
											modal.setTitle("Error :-(");
											Alert alert = new Alert(
													"Sorry, I cannot show the Project with id '"
															+ resultDocumentDV.getId()
															+ "' Refresh an try again. Error: " + caught.getMessage(),
													AlertType.ERROR);
											alert.setClose(false);
											hp.add(alert);

										}

										@Override
										public void onSuccess(ProjectView result) {
											hp.clear();
											GeoportalDataViewerWidget wid = new GeoportalDataViewerWidget();
											ProjectViewer viewer = wid.getProjectViewer(result);
											viewer.setTocContentVisible(true);
											viewer.setHeight((height - 80) + "px");
											modal.add(viewer);
										}
									});

							modal.show();

							break;
						}

						case VIEW_PROJECT_AS_JSON: {
							GWT.log("VIEW VIEW_PROJECT_AS_JSON fired");

							final Modal modal = new Modal(true, true);
							modal.setCloseVisible(true);
							final int height = Window.getClientHeight() * 70 / 100;
							int width = Window.getClientWidth() * 70 / 100;
							modal.setMaxHeigth("none");
							modal.setWidth(width);
							modal.setHeight(height + "px");
							modal.setTitle(
									"<span style='font-size:20px;'>View as JSON for Project ID: <span style='color:#555; font-size:20px;'>"
											+ resultDocumentDV.getId() + "</span></span>");

							EditModeRecord editMode = new EditModeRecord(appManagerBus, resultDocumentDV, height);
							modal.add(editMode);
							modal.show();

							break;
						}

						case DELETE_PROJECT: {

							DeleteViewPanel dvp = new DeleteViewPanel(resultDocumentDV);

							final ModalConfirm dialog = new ModalConfirm(null, "Delete Confirm?", null);
							dialog.addToCenterPanel(dvp);
							dialog.show();

							dialog.getYesButton().addClickHandler(new ClickHandler() {

								@Override
								public void onClick(ClickEvent event) {
									dialog.hide();
									final Modal modal = new Modal(true, true);
									modal.setCloseVisible(true);
									final HorizontalPanel hp = new HorizontalPanel();
									final LoaderIcon lc = new LoaderIcon("Deleting Project...");
									hp.add(lc);
									modal.add(hp);

									GeoportalDataEntryServiceAsync.Util.getInstance().deleteProject(
											resultDocumentDV.getProfileID(), resultDocumentDV.getId(),
											new AsyncCallback<Boolean>() {

												@Override
												public void onFailure(Throwable caught) {
													hp.clear();
													modal.setTitle("Error :-(");
													Alert alert = new Alert(
															"Sorry, I cannot delete the Project with id '"
																	+ resultDocumentDV.getId()
																	+ "' Refresh an try again. Error: "
																	+ caught.getMessage(),
															AlertType.ERROR);
													alert.setClose(false);
													hp.add(alert);

													VerticalPanel vp = new VerticalPanel();
													vp.getElement().getStyle().setMarginTop(20, Unit.PX);
													vp.add(new Label("Check outcome in the Publication Report"));
													Button buttonShowPublicationReport = new Button(
															"Show Publication Report");
													buttonShowPublicationReport.setType(ButtonType.INFO);
													buttonShowPublicationReport.addClickHandler(new ClickHandler() {

														@Override
														public void onClick(ClickEvent event) {
															modal.hide();
															appManagerBus
																	.fireEvent(new OperationOnItemEvent<DocumentDV>(
																			Arrays.asList(resultDocumentDV),
																			OPERATION_ON_ITEM.VIEW_REPORT));

														}
													});
													buttonShowPublicationReport.getElement().getStyle().setMarginTop(10,
															Unit.PX);
													buttonShowPublicationReport.getElement().getStyle()
															.setMarginBottom(20, Unit.PX);
													vp.add(buttonShowPublicationReport);

													modal.add(vp);

												}

												@Override
												public void onSuccess(Boolean result) {
													hp.clear();
													if (result) {
														modal.setTitle("Project deleted");
														Alert alert = new Alert("Project with id '"
																+ resultDocumentDV.getId() + "' deleted correclty",
																AlertType.INFO);
														alert.setClose(false);
														hp.add(alert);

														appManagerBus.fireEvent(new GetListOfRecordsEvent(false,
																resultDocumentDV.getProfileID(),
																mainTabPanel.getCurrentProjectsSearchingFilter(),
																false));
													} else {
														modal.setTitle("Error");
														Alert alert = new Alert(
																"Sorry, I cannot delete the Project with id '"
																		+ resultDocumentDV.getId()
																		+ "' Refresh an try again",
																AlertType.ERROR);
														alert.setClose(false);
														hp.add(alert);
													}

												}
											});
									modal.show();
								}

							});
							break;
						}

						default:
							break;
						}

					}

				}

			}

		});
	}

	private TreeItem cloneSubTreeItems(TreeItem root) {
		GWT.log("cloneSubTreeItems called");

		if (root == null) {
			GWT.log("cloneSubTreeItems returns, node is null");
			return null;
		}

		GWT.log("cloneSubTreeItems called on: " + root.getText());

		// Cloning
		NodeItem theRootNode = (NodeItem) root.getWidget();
		GeoNaFormCardModel nodeCard = theRootNode.getGeoNaFormCardModel();
		GeoNaFormCardModel newNodeFormCard = buildNewFormCardModelFromProfile(nodeCard.getGcubeProfile(), -1,
				nodeCard.getMetadataProfileBean(), OPERATION.UPDATE, appManagerBus);

		// create a new node with the same data as the root node
		boolean canBeDuplicated = newNodeFormCard.getFormCard().isInternalRepeatibleForm();
//		geoNaMainForm.getTreeItemPanel().createAndAddChild(theRootNode.getParentTreeItem(), newNodeFormCard,
//				canBeDuplicated, true, theRootNode.getJsonSectionFullPath());

		TreeItem newTreeItem = new TreeItem(
				new NodeItem(root.getParentItem(), nodeCard.getGcubeProfile().getSectionTitle(), newNodeFormCard,
						canBeDuplicated, true, theRootNode.getJsonSectionFullPath()));
		String parentText = root.getParentItem() != null ? root.getParentItem().getText() : null;
		GWT.log("newTreeItem created: " + newTreeItem.getText() + ", parent is: " + parentText);

		// Total children count
		int total = root.getChildCount();
		// All the children
		for (int i = 0; i < total; i++) {
			TreeItem childCloned = cloneSubTreeItems(root.getChild(i));
			newTreeItem.addItem(childCloned);
		}

		return newTreeItem;

	}

	private boolean checkAccessToOperationType(GcubeUserRole userRole, OPERATION_TYPE allowedAction,
			String creatorUsername) {

		boolean writeAnyAPassed = false;
		boolean writeOwnPassed = false;

		// Case ADMIN
		if (userRole.isWriteAny()) {
			writeAnyAPassed = true;
		}

		// Case EDITOR
		// Going to check if the user can RW the record is owner of.
		if (userRole.isWriteOwn()) {
			String ownerUser = creatorUsername;
			if (ownerUser == null || ownerUser.isEmpty()) {
				writeOwnPassed = false;
			} else if (ownerUser.compareTo(myRights.getMyUsername()) == 0) {
				// if my username is equal the owner of the project
				writeOwnPassed = true;
			}
		}

		boolean operationAllowed = false;
		switch (allowedAction) {
		case READ:
			// Access to READ operation is allowed to everyone
			operationAllowed = true;
			GWT.log(OPERATION_TYPE.READ + " ALLOWED? " + operationAllowed);
			break;
		case READ_WRITE:
			// Access to READ_WRITE operation
			operationAllowed = writeAnyAPassed || writeOwnPassed;
			GWT.log(OPERATION_TYPE.READ_WRITE + " ALLOWED? " + operationAllowed);
			break;
		case WRITE:
			// Access to WRITE operation
			operationAllowed = writeAnyAPassed || writeOwnPassed;
			GWT.log(OPERATION_TYPE.WRITE + " ALLOWED? " + operationAllowed);
			break;
		case UNKNOWN:
			operationAllowed = false;
			GWT.log(OPERATION_TYPE.UNKNOWN + " ALLOWED? " + operationAllowed);
			break;
		default:
			break;
		}

		return operationAllowed;
	}

	private void purgeFileUploaded() {

		MetadataProfileFormBuilderServiceAsync.Util.getInstance().purgeFilesUploaded(new AsyncCallback<Integer>() {

			@Override
			public void onFailure(Throwable caught) {
				// silent

			}

			@Override
			public void onSuccess(Integer result) {
				GWT.log("Purged " + result + " file/s uploaded");

			}
		});
	}

	/**
	 * Builds the new form card model from profile.
	 *
	 * @param gcubeProfile        the gcube profile
	 * @param order               the order
	 * @param metaDataProfileBean the meta data profile bean
	 * @param operation           the operation
	 * @param appManagerBus       the app manager bus
	 * @return the geo na form card model
	 */
	public static <T extends MetaDataProfileBean> GeoNaFormCardModel buildNewFormCardModelFromProfile(
			GcubeProfileDV gcubeProfile, int order, T metaDataProfileBean, OPERATION operation,
			HandlerManager appManagerBus) {

		// Managing Forms Repeatability
		int minOccurs = gcubeProfile.getMinOccurs();
		minOccurs = minOccurs <= 0 ? 0 : minOccurs;
		int maxOccurs = gcubeProfile.getMaxOccurs();
		maxOccurs = maxOccurs <= 0 ? Integer.MAX_VALUE : maxOccurs;

		// TODO MUST BE MANAGED MIN_OCCURS
		ProjectFormCard cct = new ProjectFormCard(gcubeProfile.getSectionName(), gcubeProfile.getSectionTitle(), order,
				maxOccurs > 1, minOccurs, maxOccurs);

		List<FilesetDV> fileset = null;
		if (metaDataProfileBean instanceof MetaDataProfileBeanExt) {
			MetaDataProfileBeanExt metaDataProfileBeanExt = (MetaDataProfileBeanExt) metaDataProfileBean;
			fileset = metaDataProfileBeanExt.getListFileset();
		}

		GeoNaFormCardModel geoNaFormCardModel = new GeoNaFormCardModel(metaDataProfileBean, null, cct, gcubeProfile);

		// In case of UPDATE operation, the fields of kind File will be not mandatory.
		if (operation != null && operation.equals(OPERATION.UPDATE)) {
			List<MetadataFieldWrapper> fields = geoNaFormCardModel.getMetadataProfileBean().getMetadataFields();
			for (MetadataFieldWrapper metadataFieldWrapper : fields) {
				if (metadataFieldWrapper.getMandatory()
						&& metadataFieldWrapper.getType().equals(DataTypeWrapper.File)) {
					metadataFieldWrapper.setMandatory(false);
				}
			}
		}

		CreateMetadataForm baseForm = null;
		if (fileset == null) {
			GWT.log("Instancing CreateMetadataForm without files");
			baseForm = new CreateMetadataForm(Arrays.asList(geoNaFormCardModel.getMetadataProfileBean()), appManagerBus,
					operation);
		} else {
			GWT.log("Instancing CreateMetadataForm with files");
			List<? extends FileUploaded> files = toListFileUploadedRemote(fileset);
			GWT.log("files are: " + files);
			baseForm = new CreateMetadataForm(Arrays.asList(geoNaFormCardModel.getMetadataProfileBean()), appManagerBus,
					operation, files);
		}

		geoNaFormCardModel.setMetadataForm(baseForm);

		return geoNaFormCardModel;
	}

	public static List<? extends FileUploaded> toListFileUploadedRemote(List<FilesetDV> fileset) {

		if (fileset == null || fileset.size() == 0)
			return null;

		List<FileUploadedRemote> fileUploaded = new ArrayList<FileUploadedRemote>();

		for (FilesetDV filesetDV : fileset) {
			GWT.log("filesetDV fieldName: " + filesetDV.getFilesetFieldName() + " profile: "
					+ filesetDV.getGcubeProfileFieldName());

			for (PayloadDV payload : filesetDV.getListPayload()) {
				FileUploadedRemote fu = new FileUploadedRemote();
				fu.setFileName(payload.getName());
				fu.setUrl(payload.getLink());
				fu.setMimeType(payload.getMimetype());
				// adding FilePath according to spefic file registred in the UCD
				FilePath filePath = new FilePath(filesetDV.getGcubeProfileFieldName(), filesetDV.getFilesetFieldName());
				fu.setFilePath(filePath);
				fileUploaded.add(fu);
			}
		}
		return fileUploaded;

	}

}
