package org.gcube.portlets.user.geoportaldataentry.server;

import java.io.Serializable;
import java.util.LinkedHashMap;

import javax.servlet.http.HttpServletRequest;

import org.gcube.portlets.user.geoportaldataentry.shared.CommitReport;
import org.gcube.portlets.user.geoportaldataentry.shared.monitoring.MonitoringAction;

public class MonitoringActionsOnServer implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2370892426311458184L;

	private LinkedHashMap<Integer, MonitoringAction> monitor = new LinkedHashMap<Integer, MonitoringAction>();

	private Exception exception;

	private HttpServletRequest request;

	private String monitorUUID;

	private CommitReport commitReport;

	private boolean monitoringTerminatedOnServer;

	private boolean monitoringTerminatedClientConsumed;

	public MonitoringActionsOnServer(HttpServletRequest httpServletRequest, String monitorUUID) {
		this.request = httpServletRequest;
		this.monitorUUID = monitorUUID;
	}

	/**
	 * Push action.
	 *
	 * @param action the action
	 * @return the action Id
	 */
	public synchronized MonitoringAction pushAction(MonitoringAction action) {
		final int current = monitor.size();
		int actionId = current + 1;
		action.setActionId(actionId);
		monitor.put(actionId, action);
		updateMonitorInSession();
		return action;
	}

	public synchronized void overrideAction(MonitoringAction action) {
		monitor.put(action.getActionId(), action);
		updateMonitorInSession();
	}

	public MonitoringAction getAction(Integer index) {
		if (index == null || index < 0)
			return null;

		return monitor.get(index);

	}

	public LinkedHashMap<Integer, MonitoringAction> getMonitor() {
		return monitor;
	}

	private void updateMonitorInSession() {
		SessionUtil.setMonitorStatus(request, monitorUUID, this);
	}

	public void setMonitoringTerminated(boolean bool) {
		this.monitoringTerminatedOnServer = bool;
		updateMonitorInSession();
	}

	public boolean isMonitoringTerminatedOnServer() {
		return monitoringTerminatedOnServer;
	}

	public void destroyMonitor() {
		SessionUtil.removeMonitorStatus(request, monitorUUID);
	}

	public Exception getException() {
		return exception;
	}

	public void setException(Exception exception) {
		this.exception = exception;
	}

	public void setCommitReport(CommitReport commitReport) {
		this.commitReport = commitReport;
	}

	public CommitReport getCommitReport() {
		return commitReport;
	}

	public String getMonitorUUID() {
		return monitorUUID;
	}

}
