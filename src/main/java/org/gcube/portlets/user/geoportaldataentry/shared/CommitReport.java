package org.gcube.portlets.user.geoportaldataentry.shared;

import java.io.Serializable;

import org.gcube.application.geoportalcommon.shared.geoportal.project.LifecycleInformationDV;

/**
 * The Class CommitReport.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Jul 30, 2021
 */
public class CommitReport implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3606902201347318287L;
	public String projectID;
	private LifecycleInformationDV lifecycleInformation;
	private String profileID;
	private String projectAsJSON;

	/**
	 * Instantiates a new commit report.
	 */
	public CommitReport() {

	}

	public CommitReport(String projectID, String profileID, String projectAsJSON,
			LifecycleInformationDV lifecycleInfo) {
		this.projectID = projectID;
		this.profileID = profileID;
		this.projectAsJSON = projectAsJSON;
		this.lifecycleInformation = lifecycleInfo;
	}

	public String getProjectID() {
		return projectID;
	}

	public LifecycleInformationDV getLifecycleInformation() {
		return lifecycleInformation;
	}

	public void setProjectID(String projectID) {
		this.projectID = projectID;
	}

	public void setLifecycleInformation(LifecycleInformationDV lifecycleInformation) {
		this.lifecycleInformation = lifecycleInformation;
	}

	public String getProfileID() {
		return profileID;
	}

	public void setProfileID(String profileID) {
		this.profileID = profileID;
	}

	public String getProjectAsJSON() {
		return projectAsJSON;
	}

	public void setProjectAsJSON(String projectAsJSON) {
		this.projectAsJSON = projectAsJSON;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CommitReport [projectID=");
		builder.append(projectID);
		builder.append(", lifecycleInformation=");
		builder.append(lifecycleInformation);
		builder.append(", profileID=");
		builder.append(profileID);
		builder.append(", projectAsJSON=");
		builder.append(projectAsJSON);
		builder.append("]");
		return builder.toString();
	}

}
