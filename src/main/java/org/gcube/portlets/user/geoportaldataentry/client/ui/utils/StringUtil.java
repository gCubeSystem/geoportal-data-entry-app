/**
 *
 */
package org.gcube.portlets.user.geoportaldataentry.client.ui.utils;

/**
 * The Class StringUtil.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 * Oct 12, 2021
 */
public class StringUtil {

	/**
	 * Ellipsize.
	 *
	 * @param input         the input string that may be subjected to shortening
	 * @param maxCharacters the maximum characters that must be returned for the
	 *                      input string
	 * @return the string
	 */
	public static String ellipsize(String input, int maxCharacters) {

		if (input == null)
			return "";

		if (input.length() < maxCharacters)
			return input;

		return input.substring(0, maxCharacters) + "...";
	}

}
