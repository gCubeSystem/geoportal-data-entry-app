package org.gcube.portlets.user.geoportaldataentry.client.events;

import java.util.List;

import org.gcube.application.geoportalcommon.shared.geoportal.DocumentDV;
import org.gcube.portlets.user.geoportaldataentry.client.ConstantsGeoPortalDataEntryApp.ACTION_PERFORMED_ON_ITEM;

import com.google.gwt.event.shared.GwtEvent;

/**
 * The Class OperationOnItemEvent.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Sep 15, 2021
 * @param <T> the generic type
 */
public class OperationPerformedOnItemEvent<T extends DocumentDV> extends GwtEvent<OperationPerformedOnItemEventHandler> {
	public static Type<OperationPerformedOnItemEventHandler> TYPE = new Type<OperationPerformedOnItemEventHandler>();
	private List<T> selectItems;
	private ACTION_PERFORMED_ON_ITEM action;
	private String profileID;

	/**
	 * Instantiates a new action performed on item event.
	 *
	 * @param selectItems the select items
	 * @param doAction    the do action
	 */
	public OperationPerformedOnItemEvent(String profileID, List<T> selectItems, ACTION_PERFORMED_ON_ITEM doAction) {
		this.profileID = profileID;
		this.selectItems = selectItems;
		this.action = doAction;
	}

	/**
	 * Gets the associated type.
	 *
	 * @return the associated type
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.google.gwt.event.shared.GwtEvent#getAssociatedType()
	 */
	@Override
	public Type<OperationPerformedOnItemEventHandler> getAssociatedType() {
		return TYPE;
	}

	/**
	 * Dispatch.
	 *
	 * @param handler the handler
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.google.gwt.event.shared.GwtEvent#dispatch(com.google.gwt.event.shared.
	 * EventHandler)
	 */
	@Override
	protected void dispatch(OperationPerformedOnItemEventHandler handler) {
		handler.onDoActionPerformedFired(this);
	}

	/**
	 * Gets the select items.
	 *
	 * @return the select items
	 */
	public List<T> getSelectItems() {
		return selectItems;
	}

	/**
	 * Gets the action.
	 *
	 * @return the action
	 */
	public ACTION_PERFORMED_ON_ITEM getAction() {
		return action;
	}
	
	public String getProfileID() {
		return profileID;
	}

}
