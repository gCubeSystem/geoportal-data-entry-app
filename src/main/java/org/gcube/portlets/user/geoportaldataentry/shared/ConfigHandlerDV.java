package org.gcube.portlets.user.geoportaldataentry.shared;

import java.io.Serializable;
import java.util.List;

import org.gcube.application.geoportalcommon.shared.geoportal.config.GcubeProfileDV;

public class ConfigHandlerDV implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 372838411789432170L;

	private String id;
	private String type; // is case in the JSON

	private List<GcubeProfileDV> gcubeProfiles;

	public ConfigHandlerDV() {
	}

	public String getId() {
		return id;
	}

	public String getType() {
		return type;
	}

	public List<GcubeProfileDV> getGcubeProfiles() {
		return gcubeProfiles;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setGcubeProfiles(List<GcubeProfileDV> gcubeProfiles) {
		this.gcubeProfiles = gcubeProfiles;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ConfigHandlerDV [id=");
		builder.append(id);
		builder.append(", type=");
		builder.append(type);
		builder.append(", gcubeProfiles=");
		builder.append(gcubeProfiles);
		builder.append("]");
		return builder.toString();
	}
	
	
}
