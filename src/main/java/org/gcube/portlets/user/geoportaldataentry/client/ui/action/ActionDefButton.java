package org.gcube.portlets.user.geoportaldataentry.client.ui.action;

import org.gcube.application.geoportalcommon.shared.geoportal.config.ActionDefinitionDV;

import com.github.gwtbootstrap.client.ui.Button;

public class ActionDefButton {

	private ActionDefinitionDV actionDefinitionDV;
	private Button button;

	public ActionDefButton(ActionDefinitionDV actionDefinitionDV, Button button) {
		this.actionDefinitionDV = actionDefinitionDV;
		this.button = button;
	}

	public ActionDefinitionDV getActionDefinitionDV() {
		return actionDefinitionDV;
	}

	public Button getButton() {
		return button;
	}

}
