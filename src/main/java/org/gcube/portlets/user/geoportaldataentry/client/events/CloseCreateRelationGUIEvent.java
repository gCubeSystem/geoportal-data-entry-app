package org.gcube.portlets.user.geoportaldataentry.client.events;

import com.google.gwt.event.shared.GwtEvent;


/**
 * The Class CloseCreateRelationGUIEvent.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 * Sep 19, 2022
 */
public class CloseCreateRelationGUIEvent extends GwtEvent<CloseCreateRelationGUIEventHandler> {
	public static Type<CloseCreateRelationGUIEventHandler> TYPE = new Type<CloseCreateRelationGUIEventHandler>();

	/**
	 * Instantiates a new close create relation GUI.
	 */
	public CloseCreateRelationGUIEvent() {

	}

	/**
	 * Gets the associated type.
	 *
	 * @return the associated type
	 */
	@Override
	public Type<CloseCreateRelationGUIEventHandler> getAssociatedType() {
		return TYPE;
	}

	/**
	 * Dispatch.
	 *
	 * @param handler the handler
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.google.gwt.event.shared.GwtEvent#dispatch(com.google.gwt.event.shared.
	 * EventHandler)
	 */
	@Override
	protected void dispatch(CloseCreateRelationGUIEventHandler handler) {
		handler.onClose(this);
	}

}
