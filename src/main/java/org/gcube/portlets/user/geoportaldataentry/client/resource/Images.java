package org.gcube.portlets.user.geoportaldataentry.client.resource;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;

public interface Images extends ClientBundle {

	public static final Images ICONS = GWT.create(Images.class);

	@Source("loading.gif")
	ImageResource loading();
	
	@Source("access_denied.png")
	ImageResource accessDenied();
	
	@Source("one-finger-icon.png")
	ImageResource oneFingerIcon();
	
	@Source("information_icon.png")
	ImageResource info();
	
	@Source("warning_error.png")
	ImageResource warningError();
}
