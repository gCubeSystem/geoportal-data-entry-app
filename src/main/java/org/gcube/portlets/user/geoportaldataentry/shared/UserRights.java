package org.gcube.portlets.user.geoportaldataentry.shared;

import java.io.Serializable;

import org.gcube.application.geoportalcommon.shared.config.RoleRights;

/**
 * The Class UserRights.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Nov 24, 2021
 */
public class UserRights implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -304157165851633221L;

	private String myUsername;

	private RoleRights roleRights;

	/**
	 * Instantiates a new user rights.
	 */
	public UserRights() {
		super();
	}

	public UserRights(String myUsername, RoleRights roleRights) {
		super();
		this.myUsername = myUsername;
		this.roleRights = roleRights;
	}

	public String getMyUsername() {
		return myUsername;
	}

	public RoleRights getRoleRights() {
		return roleRights;
	}

	public void setMyUsername(String myUsername) {
		this.myUsername = myUsername;
	}

	public void setRoleRights(RoleRights roleRights) {
		this.roleRights = roleRights;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("UserRights [myUsername=");
		builder.append(myUsername);
		builder.append(", roleRights=");
		builder.append(roleRights);
		builder.append("]");
		return builder.toString();
	}

}
