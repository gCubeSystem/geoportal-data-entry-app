package org.gcube.portlets.user.geoportaldataentry.shared.monitoring;

import java.io.Serializable;

public class MonitoringAction implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7129157692965636149L;
	private Integer actionId;
	private STATUS status;
	private String msg;

	public static enum STATUS {
		IN_PENDING("Pending", "Operation in Pending"),
		IN_PROGESS("Progress", "Operation in Progress"),
		FAILED("Failed", "Operation Failed"),
		DONE("Done", "Operation Done");

		String id;
		String label;

		STATUS(String id, String label) {
			this.id = id;
			this.label = label;
		}

		public String getId() {
			return id;
		}

		public String getLabel() {
			return label;
		}
	}

	public MonitoringAction() {

	}

	public MonitoringAction(STATUS status, String msg) {
		this.status = status;
		this.msg = msg;
	}

	public MonitoringAction(Integer actionId, STATUS status, String msg) {
		this(status, msg);
		this.actionId = actionId;
	}

	public STATUS getStatus() {
		return status;
	}

	public String getMsg() {
		return msg;
	}

	public void setStatus(STATUS status) {
		this.status = status;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Integer getActionId() {
		return actionId;
	}
	
	public void setActionId(Integer actionId) {
		this.actionId = actionId;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("MonitoringAction [actionId=");
		builder.append(actionId);
		builder.append(", status=");
		builder.append(status);
		builder.append(", msg=");
		builder.append(msg);
		builder.append("]");
		return builder.toString();
	}

}
