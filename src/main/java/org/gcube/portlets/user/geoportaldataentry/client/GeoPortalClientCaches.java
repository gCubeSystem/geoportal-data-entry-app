package org.gcube.portlets.user.geoportaldataentry.client;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.gcube.application.geoportalcommon.shared.geoportal.ConfigurationDV;
import org.gcube.application.geoportalcommon.shared.geoportal.config.ItemFieldDV;
import org.gcube.application.geoportalcommon.shared.geoportal.ucd.GEOPORTAL_DATA_HANDLER;
import org.gcube.application.geoportalcommon.shared.geoportal.ucd.HandlerDeclarationDV;
import org.gcube.application.geoportalcommon.shared.geoportal.ucd.RelationshipDefinitionDV;
import org.gcube.application.geoportalcommon.shared.geoportal.ucd.UseCaseDescriptorDV;
import org.gcube.portlets.user.geoportaldataentry.client.ui.card.GeoNaFormCardModel;

import com.google.gwt.core.client.GWT;

/**
 * The Class GeoPortalClientCaches.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Aug 11, 2022
 */
public class GeoPortalClientCaches {

	private TreeMap<String, List<GeoNaFormCardModel>> mapGcubeProfilePerItemType;

	private Map<String, UseCaseDescriptorDV> mapUseCaseDescriptor;

	private Map<String, CacheSearchingFilterParametersFromConfig> mapSearchingFilterParametersForProfileId;

	private Map<String, List<RelationshipDefinitionDV>> mapRelationsNamesForProfileId;

	/**
	 * Instantiates a new geo portal client caches.
	 */
	public GeoPortalClientCaches() {
		mapGcubeProfilePerItemType = new TreeMap<String, List<GeoNaFormCardModel>>();
		mapSearchingFilterParametersForProfileId = new HashMap<String, GeoPortalClientCaches.CacheSearchingFilterParametersFromConfig>();
		mapUseCaseDescriptor = new HashMap<String, UseCaseDescriptorDV>();
		mapRelationsNamesForProfileId = new HashMap<String, List<RelationshipDefinitionDV>>();
	}

	/**
	 * Put list relationships definition for profile ID.
	 *
	 * @param profileId       the profile id
	 * @param relationshipDef the relationship def
	 */
	public void putListRelationshipsDefinitionForProfileID(String profileId,
			List<RelationshipDefinitionDV> relationshipDef) {
		mapRelationsNamesForProfileId.put(profileId, relationshipDef);
	}

	/**
	 * Gets the list relationship definition for profile id.
	 *
	 * @param profileId the profile id
	 * @return the list relationship definition for profile id
	 */
	public List<RelationshipDefinitionDV> getListRelationshipDefinitionForProfileId(String profileId) {
		return mapRelationsNamesForProfileId.get(profileId);
	}

	/**
	 * Put UCD for profile id.
	 *
	 * @param profileID the profile ID
	 * @param ucdDV     the ucd DV
	 */
	public void putUCDForProfileId(String profileID, UseCaseDescriptorDV ucdDV) {
		mapUseCaseDescriptor.put(profileID, ucdDV);
	}

	/**
	 * Gets the UCD for profile ID.
	 *
	 * @param profileID the profile ID
	 * @return the UCD for profile ID
	 */
	public UseCaseDescriptorDV getUCDForProfileID(String profileID) {
		return mapUseCaseDescriptor.get(profileID);
	}

	/**
	 * Put gcube profile per item type.
	 *
	 * @param itemType          the item type
	 * @param listGcubeProfiles the list gcube profiles
	 */
	public void putGcubeProfilePerItemType(String itemType, List<GeoNaFormCardModel> listGcubeProfiles) {
		mapGcubeProfilePerItemType.put(itemType, listGcubeProfiles);
	}

	/**
	 * Gets the gcube profile per item type.
	 *
	 * @param itemType the item type
	 * @return the gcube profile per item type
	 */
	public List<GeoNaFormCardModel> getGcubeProfilePerItemType(String itemType) {
		return mapGcubeProfilePerItemType.get(itemType);
	}

	/**
	 * Gets the filter parameters for profile id.
	 *
	 * @param profileID the profile ID
	 * @return the filter parameters for profile id
	 */
	public CacheSearchingFilterParametersFromConfig getFilterParametersForProfileId(String profileID,
			GEOPORTAL_DATA_HANDLER handlerType) {
		GWT.log("getFilterParametersForProfileId for: " + profileID);

		CacheSearchingFilterParametersFromConfig searchingFilterParameters = mapSearchingFilterParametersForProfileId
				.get(profileID);

		if (searchingFilterParameters == null) {

			searchingFilterParameters = new CacheSearchingFilterParametersFromConfig();
			UseCaseDescriptorDV ucd = mapUseCaseDescriptor.get(profileID);
			
			for (HandlerDeclarationDV handler : ucd.getHandlers()) {

				if (handler.getDataHandlerType().equals(handlerType)) {
					ConfigurationDV<?> configurationDV = handler.getConfiguration();
					GWT.log("For profileID " + profileID + " and handler type " + handlerType + ", read config for : "
							+ configurationDV);

					switch (configurationDV.getConfigurationType()) {
					case item_fields: {
						List<ItemFieldDV> listItemFields = (List<ItemFieldDV>) configurationDV.getConfiguration();
						for (ItemFieldDV itemField : listItemFields) {
							if (itemField.isDisplayAsResult()) {
								searchingFilterParameters.addDisplayField(itemField);
								// adding to projection for filtering
								for (String jsonFieldPath : itemField.getJsonFields()) {
									// String mongoProjection = itemField.getProjection() + "." + jsonField;
									searchingFilterParameters.putMongoProjection(jsonFieldPath, 1);
								}
							}

							if (itemField.isSearchable()) {
								searchingFilterParameters.addSearchByField(itemField);
							}

							if (itemField.isSortable()) {
								searchingFilterParameters.addOrderByField(itemField);
							}
						}

						break;
					}
					default: {
						break;
					}
					}

					mapSearchingFilterParametersForProfileId.put(profileID, searchingFilterParameters);
				}
			}

		}

		return searchingFilterParameters;
	}

//	/**
//	 * Prints the cache handler configurations.
//	 */
//	// DEBUGGING
//	public void printCacheHandlerConfigurations() {
//		GWT.log("print - mapHandlersConfigurationsForProfileId is:");
//		for (String key : mapHandlersConfigurationsForProfileId.keySet()) {
//			GWT.log("print - key: " + key + ", value: " + mapHandlersConfigurationsForProfileId.get(key));
//		}
//	}

	/**
	 * Prints the cache gcube profile per item type.
	 */
	// DEBUGGING
	public void printCacheGcubeProfilePerItemType() {
		GWT.log("print - mapGcubeProfilePerItemType is:");
		for (String key : mapGcubeProfilePerItemType.keySet()) {
			GWT.log("print - key: " + key + ", value: " + mapGcubeProfilePerItemType.get(key));
		}
	}

	/**
	 * The Class CacheSearchingFilterParametersFromConfig.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
	 * 
	 *         Sep 13, 2022
	 */
	public class CacheSearchingFilterParametersFromConfig {

		List<ItemFieldDV> displayFields;
		List<ItemFieldDV> searchByFields;
		List<ItemFieldDV> orderByFields;

		// The projection
		LinkedHashMap<String, Object> projection;

		/**
		 * Instantiates a new cache searching filter parameters from config.
		 */
		public CacheSearchingFilterParametersFromConfig() {
			displayFields = new ArrayList<ItemFieldDV>();
			searchByFields = new ArrayList<ItemFieldDV>();
			orderByFields = new ArrayList<ItemFieldDV>();
			projection = new LinkedHashMap<String, Object>();
		}

		/**
		 * Put mongo projection.
		 *
		 * @param keyField the key field
		 * @param value    the value
		 */
		public void putMongoProjection(String keyField, Object value) {
			projection.put(keyField, value);
		}

		/**
		 * Adds the display field.
		 *
		 * @param itemField the item field
		 */
		public void addDisplayField(ItemFieldDV itemField) {
			displayFields.add(itemField);
		}

		/**
		 * Adds the search by field.
		 *
		 * @param itemField the item field
		 */
		public void addSearchByField(ItemFieldDV itemField) {
			searchByFields.add(itemField);
		}

		/**
		 * Adds the order by field.
		 *
		 * @param itemField the item field
		 */
		public void addOrderByField(ItemFieldDV itemField) {
			orderByFields.add(itemField);
		}

		/**
		 * Gets the display fields.
		 *
		 * @return the display fields
		 */
		public List<ItemFieldDV> getDisplayFields() {
			return displayFields;
		}

		/**
		 * Gets the search by fields.
		 *
		 * @return the search by fields
		 */
		public List<ItemFieldDV> getSearchByFields() {
			return searchByFields;
		}

		/**
		 * Gets the order by fields.
		 *
		 * @return the order by fields
		 */
		public List<ItemFieldDV> getOrderByFields() {
			return orderByFields;
		}

		/**
		 * Gets the projection.
		 *
		 * @return the projection
		 */
		public LinkedHashMap<String, Object> getProjection() {
			return projection;
		}

	}
}
