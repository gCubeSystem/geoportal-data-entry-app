package org.gcube.portlets.user.geoportaldataentry.client;

import java.util.LinkedHashMap;

import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.Window;

public class InitExecutor {

	private LinkedHashMap<Integer, Command> queueInitStage = new LinkedHashMap<>();

	public InitExecutor() {

	}

	public synchronized void putCommand(Command command) throws InterruptedException {
		queueInitStage.put(queueInitStage.size(), command);
	}

	private synchronized Command pollCommand() throws InterruptedException {
		for (Integer key : queueInitStage.keySet()) {
			Command command = queueInitStage.get(key);
			if(command!=null) {
				queueInitStage.put(key, null);
				return command;
			}
		}
		
		return null;
	}

	protected void execute() {
		try {
			Command command = pollCommand();
			if (command != null)
				command.execute();
		} catch (InterruptedException e) {
			Window.alert("An error occurred while initializing the application. Please contact the support. Error is: "
					+ e.getMessage());
		}
	}

}
