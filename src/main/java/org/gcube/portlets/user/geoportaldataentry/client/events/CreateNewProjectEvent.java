package org.gcube.portlets.user.geoportaldataentry.client.events;

import java.util.List;

import org.gcube.application.geoportalcommon.shared.geoportal.config.GcubeProfileDV;
import org.gcube.application.geoportalcommon.shared.geoportal.ucd.HandlerDeclarationDV;

import com.google.gwt.event.shared.GwtEvent;

/**
 * The Class CreateNewProjectEvent.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Mar 17, 2022
 */
public class CreateNewProjectEvent extends GwtEvent<CreateNewProjectEventHandler> {

	/** The type. */
	public static Type<CreateNewProjectEventHandler> TYPE = new Type<CreateNewProjectEventHandler>();
	private HandlerDeclarationDV handler;
	private List<GcubeProfileDV> listGcubeProfiles;
	private String profileID;

	/**
	 * Instantiates a new creates the new project event.
	 *
	 * @param handler           the handler
	 * @param listGcubeProfiles the list gcube profiles
	 */
	// (passing this second argument in order to avoid twice the casting as
	// List<GcubeProfileDV>)
	public CreateNewProjectEvent(String profileID, HandlerDeclarationDV handler,
			List<GcubeProfileDV> listGcubeProfiles) {
		this.profileID = profileID;
		this.handler = handler;
		this.listGcubeProfiles = listGcubeProfiles;
	}

	/**
	 * Gets the associated type.
	 *
	 * @return the associated type
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.google.gwt.event.shared.GwtEvent#getAssociatedType()
	 */
	@Override
	public Type<CreateNewProjectEventHandler> getAssociatedType() {
		return TYPE;
	}

	/**
	 * Dispatch.
	 *
	 * @param handler the handler
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.google.gwt.event.shared.GwtEvent#dispatch(com.google.gwt.event.shared.
	 * EventHandler)
	 */
	@Override
	protected void dispatch(CreateNewProjectEventHandler handler) {
		handler.onCreateNewProject(this);
	}

	public HandlerDeclarationDV getHandler() {
		return handler;
	}

	/**
	 * Gets the list gcube profiles.
	 *
	 * @return the list gcube profiles
	 */
	public List<GcubeProfileDV> getListGcubeProfiles() {
		return listGcubeProfiles;
	}

	public String getProfileID() {
		return profileID;
	}

}
