package org.gcube.portlets.user.geoportaldataentry.client.events;

import org.gcube.application.geoportalcommon.shared.geoportal.ResultDocumentDV;
import org.gcube.application.geoportalcommon.shared.geoportal.ucd.RelationshipDefinitionDV;

import com.google.gwt.event.shared.GwtEvent;

/**
 * The Class RelationActionHandlerEvent.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Oct 5, 2022
 */
public class RelationActionHandlerEvent extends GwtEvent<RelationActionHandler> {

	/** The type. */
	public static Type<RelationActionHandler> TYPE = new Type<RelationActionHandler>();
	private ResultDocumentDV fromProject;
	private RelationshipDefinitionDV relationSelected;
	private ResultDocumentDV toProject;
	private RELACTION_ACTION_TYPE relactionActionType;
	private String relationName;

	/**
	 * The Enum RELACTION_ACTION_TYPE.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
	 * 
	 *         Oct 5, 2022
	 */
	public static enum RELACTION_ACTION_TYPE {
		CREATE, DELETE
	}

	/**
	 * Instantiates a new relation action handler event.
	 *
	 * @param fromProject      the from project
	 * @param relationSelected the relation selected
	 * @param toProject        the to project
	 */
	public RelationActionHandlerEvent(ResultDocumentDV fromProject, RelationshipDefinitionDV relationSelected,
			ResultDocumentDV toProject) {
		this.fromProject = fromProject;
		this.relationSelected = relationSelected;
		this.toProject = toProject;
		this.relactionActionType = RELACTION_ACTION_TYPE.CREATE;
	}

	/**
	 * Instantiates a new relation action handler event.
	 *
	 * @param fromProject  the from project
	 * @param relationName the relation name
	 * @param toProject    the to project
	 */
	public RelationActionHandlerEvent(ResultDocumentDV fromProject, String relationName, ResultDocumentDV toProject) {
		this.fromProject = fromProject;
		this.toProject = toProject;
		this.relationName = relationName;
		this.relactionActionType = RELACTION_ACTION_TYPE.DELETE;
	}

	/**
	 * Gets the associated type.
	 *
	 * @return the associated type
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.google.gwt.event.shared.GwtEvent#getAssociatedType()
	 */
	@Override
	public Type<RelationActionHandler> getAssociatedType() {
		return TYPE;
	}

	/**
	 * Dispatch.
	 *
	 * @param handler the handler
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.google.gwt.event.shared.GwtEvent#dispatch(com.google.gwt.event.shared.
	 * EventHandler)
	 */
	@Override
	protected void dispatch(RelationActionHandler handler) {
		handler.onCreateRelation(this);
	}

	/**
	 * Gets the from project.
	 *
	 * @return the from project
	 */
	public ResultDocumentDV getFromProject() {
		return fromProject;
	}

	/**
	 * Gets the relation selected.
	 *
	 * @return the relation selected
	 */
	public RelationshipDefinitionDV getRelationSelected() {
		return relationSelected;
	}

	/**
	 * Gets the to project.
	 *
	 * @return the to project
	 */
	public ResultDocumentDV getToProject() {
		return toProject;
	}

	/**
	 * Gets the relaction action type.
	 *
	 * @return the relaction action type
	 */
	public RELACTION_ACTION_TYPE getRelactionActionType() {
		return relactionActionType;
	}

	/**
	 * Gets the relation name.
	 *
	 * @return the relation name
	 */
	public String getRelationName() {
		return relationName;
	}

}
