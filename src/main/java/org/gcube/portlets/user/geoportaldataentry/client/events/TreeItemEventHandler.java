package org.gcube.portlets.user.geoportaldataentry.client.events;

import com.google.gwt.event.shared.EventHandler;

/**
 * The Interface TreeItemEventHandler.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Jul 4, 2022
 */
public interface TreeItemEventHandler extends EventHandler {

	/**
	 * On tree item event.
	 *
	 * @param treeItemEvent the tree item event
	 */
	void onTreeItemEvent(TreeItemEvent treeItemEvent);

}