package org.gcube.portlets.user.geoportaldataentry.client.ui.edit.jseditor;

import com.google.gwt.core.client.JavaScriptObject;


/**
 * The Class JSONEditorWrapper.
 * 
 * Wrapper for https://github.com/josdejong/jsoneditor
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 * Sep 27, 2021
 */
public class JSONEditorWrapper extends JavaScriptObject {

	/**
	 * Instantiates a new JSON editor wrapper.
	 */
	protected JSONEditorWrapper() {
	}

	/**
	 * Inits the.
	 *
	 * @param divID the div ID
	 * @return the JSON editor wrapper
	 */
	public static native JSONEditorWrapper init(String divID) /*-{
		var container = $doc.getElementById(divID);
		var options = {
			modes : ['code','tree'],
			search: true,
			onError : function(error) {
				console.log("JSONEditorWrapper error: " + error)
			}
		};
		//console.log("container is: " + container);
		return new $wnd.JSONEditor(container, options);
	}-*/;

	/**
	 * Sets the.
	 *
	 * @param json the json
	 */
	public final native void set(String json) /*-{
		var toJSONObject = JSON.parse(json);
		// set json
		//console.log("displayng JSON: " + toJSONObject);
		this.set(toJSONObject);
		this.refresh();
	}-*/;

	/**
	 * Gets the text.
	 *
	 * @return the text
	 */
	public final native String getText() /*-{
		//console.log("this.get(): " +this.getText());
		return this.getText();
	}-*/;

	/**
	 * On error.
	 */
	public final native void onError() /*-{
		this.onError(error);
	}-*/;

	/**
	 * Sets the mode.
	 *
	 * @param mode the new mode
	 */
	public final native void setMode(String mode) /*-{
		this.setMode(mode);
	}-*/;

	/**
	 * Sets the name.
	 *
	 * @param rootName the new name
	 */
	public final native void setName(String rootName) /*-{
		this.setName(rootName);
	}-*/;
}