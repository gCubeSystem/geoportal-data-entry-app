package org.gcube.portlets.user.geoportaldataentry.client.ui.card;

import java.util.ArrayList;
import java.util.List;

import org.gcube.portlets.user.geoportaldataentry.client.ProjectFormCard;
import org.gcube.portlets.user.geoportaldataentry.client.resource.Images;
import org.gcube.portlets.user.geoportaldataentry.client.ui.utils.AlertClosable;
import org.gcube.portlets.user.geoportaldataentry.client.ui.utils.DialogInform;
import org.gcube.portlets.widgets.mpformbuilder.client.form.generic.CreateMetadataForm;
import org.gcube.portlets.widgets.mpformbuilder.client.form.generic.GenericFormEvents.GenericFormEventsListener;
import org.gcube.portlets.widgets.mpformbuilder.shared.GenericDatasetBean;

import com.github.gwtbootstrap.client.ui.Button;
import com.github.gwtbootstrap.client.ui.Tab;
import com.github.gwtbootstrap.client.ui.constants.AlertType;
import com.github.gwtbootstrap.client.ui.constants.IconType;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;

/**
 * The Class MetadataFormCard.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 *         Oct 12, 2020
 */
public class MetadataFormCard {

	/** The heading. */
	private String heading;

	/** The tab. */
	private Tab tab;

	/** The repeatible. */
	private boolean repeatible;

	/** The list forms. */
	private List<CreateMetadataForm> listForms = new ArrayList<CreateMetadataForm>();

	/** The form card event handler. */
	private MetadataFormCardEventHandler formCardEventHandler = new MetadataFormCardEventHandler();

	/** The geona form model. */
	private GeoNaFormCardModel geonaFormModel;

	private HandlerManager appManagerBus;

	private Integer maxFormRepeatability;

	private FlowPanel tabContainer = new FlowPanel();

	private int minFormRepeatability;

	/**
	 * The Class MetadataFormCardEventHandler.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
	 * 
	 *         Oct 12, 2020
	 */
	private class MetadataFormCardEventHandler implements GenericFormEventsListener {

		/**
		 * On form data valid.
		 *
		 * @param genericDatasetBean the generic dataset bean
		 */
		@Override
		public void onFormDataValid(GenericDatasetBean genericDatasetBean) {
			setTabStatus();

		}

		/**
		 * On form data edit.
		 */
		@Override
		public void onFormDataEdit() {
			resetTabStatus();

		}

		/**
		 * On form aborted.
		 */
		@Override
		public void onFormAborted() {
			// TODO Auto-generated method stub

		}

		/**
		 * On validation error.
		 *
		 * @param throwable the throwable
		 * @param errorMsg  the error msg
		 */
		@Override
		public void onValidationError(Throwable throwable, String errorMsg) {
			// TODO Auto-generated method stub

		}

	}

	/**
	 * Instantiates a new metadata form card.
	 *
	 * @param tab            the tab
	 * @param geonaFormModel the geona form model
	 * @param appManagerBus  the app manager bus
	 */
	public MetadataFormCard(Tab tab, GeoNaFormCardModel geonaFormModel, HandlerManager appManagerBus) {
		this.tab = tab;
		this.geonaFormModel = geonaFormModel;
		this.appManagerBus = appManagerBus;
		ProjectFormCard formCard = geonaFormModel.getFormCard();
		this.repeatible = formCard.isInternalRepeatibleForm();
		this.minFormRepeatability = formCard.getMinFormRepeatability() != null
				? geonaFormModel.getFormCard().getMinFormRepeatability()
				: 1;
		this.maxFormRepeatability = formCard.getMaxFormRepeatability() != null
				? geonaFormModel.getFormCard().getMaxFormRepeatability()
				: Integer.MAX_VALUE;

		boolean repeatibleFormIsRemovable = true;
		if (minFormRepeatability > 1) {
			repeatibleFormIsRemovable = false;
			for (int i = 1; i < minFormRepeatability; i++) {
				CreateMetadataForm newForm = new CreateMetadataForm(
						geonaFormModel.getMetadataForm().getMetadataProfiles(), appManagerBus,
						CreateMetadataForm.OPERATION.UPDATE);
				// newForm.addListener(formCardEventHandler);
				// here removableForm is false because the min > 1 must be respected
				addNewForm(newForm, repeatibleFormIsRemovable);
			}
		}

		this.buildCard(geonaFormModel.getMetadataForm(), repeatibleFormIsRemovable);
		// geonaFormModel.getMetadataForm().addListener(formCardEventHandler);
	}

	/**
	 * Builds the card.
	 *
	 * @param createMetadataForm the create metadata form
	 */
	private void buildCard(final CreateMetadataForm createMetadataForm, boolean repeatibleFormIsRemovable) {

		if (repeatible) {
			Button buttonAddNew = new Button("Add New...");
			tab.add(buttonAddNew);

			buttonAddNew.addClickHandler(new ClickHandler() {

				@Override
				public void onClick(ClickEvent event) {

					if (listForms.size() < maxFormRepeatability) {

						CreateMetadataForm newForm = new CreateMetadataForm(createMetadataForm.getMetadataProfiles(),
								appManagerBus, CreateMetadataForm.OPERATION.UPDATE);
						// newForm.addListener(formCardEventHandler);
						addNewForm(newForm, true);
					} else {
						DialogInform di = new DialogInform(new Image(Images.ICONS.accessDenied()), "Maximun reached",
								"Maximun number of data reached for " + geonaFormModel.getFormCard().getTitle());
						di.center();
					}
				}
			});
		}

		tab.add(tabContainer);
		addNewForm(createMetadataForm, repeatibleFormIsRemovable);
	}

	/**
	 * Adds the new form.
	 *
	 * @param newForm the new form
	 */
	private void addNewForm(final CreateMetadataForm newForm, boolean repeatibleFormIsRemovable) {
		tabContainer.insert(newForm, 0);
		listForms.add(newForm);

		if (listForms.size() > 1 && repeatibleFormIsRemovable) {
			final Button buttonRemoveLatestForm = new Button("Remove");
			buttonRemoveLatestForm.setIcon(IconType.TRASH);
			tabContainer.insert(buttonRemoveLatestForm, 1);

			buttonRemoveLatestForm.addClickHandler(new ClickHandler() {

				@Override
				public void onClick(ClickEvent event) {
					tabContainer.remove(newForm);
					listForms.remove(newForm);
					tabContainer.remove(buttonRemoveLatestForm);

					AlertClosable alertCloasable = new AlertClosable();
					alertCloasable.setHTML("Data Entry form removed!");
					alertCloasable.setAnimation(true);
					alertCloasable.setType(AlertType.DEFAULT);
					alertCloasable.setClose(true);
					alertCloasable.closeAfter(3000);
					alertCloasable.getElement().getStyle().setMarginTop(5, Unit.PX);
					tabContainer.insert(alertCloasable,0);

				}
			});
		}

		newForm.addListener(formCardEventHandler);
	}

	/**
	 * Gets the list forms.
	 *
	 * @return the list forms
	 */
	public List<CreateMetadataForm> getListForms() {
		return listForms;
	}

	/**
	 * Gets the heading.
	 *
	 * @return the heading
	 */
	public String getHeading() {
		return heading;
	}

	/**
	 * Validate form.
	 *
	 * @return true, if successful
	 */
	public boolean validateForm() {
		for (CreateMetadataForm createMetadataForm : listForms) {
			boolean isFormDataValid = createMetadataForm.isFormDataValid();
			GWT.log("Is form data valid: " + isFormDataValid);
			if (!isFormDataValid) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Sets the valid card.
	 *
	 * @param bool the new valid card
	 */
	public void setValidCard(boolean bool) {
		if (bool) {
			tab.setIcon(IconType.OK_SIGN);
			tab.asWidget().getElement().removeClassName("red-text");
			tab.asWidget().getElement().addClassName("green-text");
		} else {
			tab.setIcon(IconType.MINUS_SIGN);
			tab.asWidget().getElement().removeClassName("green-text");
			tab.asWidget().getElement().addClassName("red-text");
		}

	}

	/**
	 * Sets the tab status.
	 */
	private void setTabStatus() {
		boolean isValid = validateForm();
		if (isValid) {
			tab.setIcon(IconType.OK_SIGN);
			tab.asWidget().getElement().removeClassName("red-text");
			tab.asWidget().getElement().addClassName("green-text");
		} else {
			tab.setIcon(IconType.MINUS_SIGN);
			tab.asWidget().getElement().removeClassName("red-text");
			tab.asWidget().getElement().addClassName("red-text");
		}
	}

	/**
	 * Reset tab status.
	 */
	private void resetTabStatus() {
		tab.setIcon(null);
	}

	public GeoNaFormCardModel getGeonaFormModel() {
		return geonaFormModel;
	}
}
