package org.gcube.portlets.user.geoportaldataentry.client.events;

import org.gcube.application.geoportalcommon.shared.geoportal.DocumentDV;

import com.google.gwt.event.shared.EventHandler;

/**
 * The Interface OperationOnItemEventHandler.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Aug 5, 2021
 */
public interface OperationOnItemEventHandler extends EventHandler {

	/**
	 * On do action fired.
	 *
	 * @param <T>           the generic type
	 * @param showItemEvent the show item event
	 */
	<T extends DocumentDV> void onDoActionFired(OperationOnItemEvent<T> showItemEvent);
}