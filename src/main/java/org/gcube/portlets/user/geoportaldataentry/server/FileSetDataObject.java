package org.gcube.portlets.user.geoportaldataentry.server;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.gcube.application.geoportalcommon.shared.geoportal.config.FilePathDV;

public class FileSetDataObject {

	private FilePathDV filePathDV;
	private List<File> files = new ArrayList<File>();

	public FileSetDataObject() {
	}

	public void setFilePathDV(FilePathDV filePath) {
		this.filePathDV = filePath;

	}

	public void addFile(File file) {
		files.add(file);
	}

	public File[] getFileset() {
		File[] arr = new File[files.size()];
		arr = files.toArray(arr);
		return arr;
	}

	public FilePathDV getFilePathDV() {
		return filePathDV;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("FileSetDataObject [filePathDV.getFieldName()=");
		builder.append(filePathDV.getFieldName());
		builder.append(", files size=");
		builder.append(files.size());
		builder.append("]");
		return builder.toString();
	}
	
	

}
