package org.gcube.portlets.user.geoportaldataentry.client;

import java.util.List;

import org.gcube.application.geoportalcommon.shared.GeoportalItemReferences;
import org.gcube.application.geoportalcommon.shared.GeoportalItemReferences.SHARE_LINK_TO;
import org.gcube.application.geoportalcommon.shared.ResultSetPaginatedData;
import org.gcube.application.geoportalcommon.shared.SearchingFilter;
import org.gcube.application.geoportalcommon.shared.geoportal.ResultDocumentDV;
import org.gcube.application.geoportalcommon.shared.geoportal.config.ActionDefinitionDV;
import org.gcube.application.geoportalcommon.shared.geoportal.config.FilePathDV;
import org.gcube.application.geoportalcommon.shared.geoportal.project.LifecycleInformationDV;
import org.gcube.application.geoportalcommon.shared.geoportal.project.ProjectDV;
import org.gcube.application.geoportalcommon.shared.geoportal.project.TemporalReferenceDV;
import org.gcube.application.geoportalcommon.shared.geoportal.step.StepPerformedResultDV;
import org.gcube.application.geoportalcommon.shared.geoportal.ucd.RelationshipDefinitionDV;
import org.gcube.application.geoportalcommon.shared.geoportal.ucd.UseCaseDescriptorDV;
import org.gcube.application.geoportalcommon.shared.geoportal.view.ProjectView;
import org.gcube.application.geoportaldatamapper.shared.ProjectEdit;
import org.gcube.portlets.user.geoportaldataentry.shared.CommitReport;
import org.gcube.portlets.user.geoportaldataentry.shared.GNADataEntryExtendedConfigProfile;
import org.gcube.portlets.user.geoportaldataentry.shared.GeoNaFormDataObject;
import org.gcube.portlets.user.geoportaldataentry.shared.GeoportalISConfig;
import org.gcube.portlets.user.geoportaldataentry.shared.Tree_Node;
import org.gcube.portlets.user.geoportaldataentry.shared.monitoring.MonitoringActionsOnClient;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * The client side stub for the RPC service.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Aug 4, 2021
 */
@RemoteServiceRelativePath("geoportaldataentryservice")
public interface GeoportalDataEntryService extends RemoteService {

	/**
	 * Save geona data forms.
	 *
	 * @param uuid the uuid
	 * @param profileID           the profile ID
	 * @param tree_Node           the tree node
	 * @param stepsOnPostCreation the steps on post creation
	 * @return the commit report
	 * @throws Exception the exception
	 */
	void saveGeonaDataForms(String uuid, String profileID, Tree_Node<GeoNaFormDataObject> tree_Node,
			List<String> stepsOnPostCreation) throws Exception;

	/**
	 * Gets the geona init config.
	 *
	 * @return the geona init config
	 */
	GeoportalISConfig getGeonaInitConfig();

	/**
	 * Gets the links for.
	 *
	 * @param itemId    the item id is the mongoId
	 * @param profileID the profile ID
	 * @param shareLinkTo the share link to
	 * @return the links for
	 * @throws Exception the exception
	 */
	GeoportalItemReferences getLinksFor(String itemId, String profileID, SHARE_LINK_TO shareLinkTo) throws Exception;

	/**
	 * Gets the list projects.
	 *
	 * @param theProfileID      the the profile ID
	 * @param start             the start
	 * @param limit             the limit
	 * @param filter            the filter
	 * @param reloadFromService the reload from service
	 * @return the list projects
	 * @throws Exception the exception
	 */
	public ResultSetPaginatedData getListProjects(String theProfileID, Integer start, Integer limit,
			SearchingFilter filter, boolean reloadFromService) throws Exception;

	/**
	 * Gets the JSON record.
	 *
	 * @param profileID the profile ID
	 * @param projectID the project ID
	 * @return the JSON record
	 * @throws Exception the exception
	 */
	String getJSONDocumentInTheProject(String profileID, String projectID) throws Exception;

	/**
	 * Update record.
	 *
	 * @param profileID  the profile ID
	 * @param projectID  the project ID
	 * @param jsonUpdate the json update
	 * @return the project DV
	 * @throws Exception the exception
	 */
	ProjectDV updateRecord(String profileID, String projectID, String jsonUpdate) throws Exception;

	/**
	 * Read data viewer config.
	 *
	 * @return the GNA data entry ext config profile
	 * @throws Exception the exception
	 */
	GNADataEntryExtendedConfigProfile readDataEntryPresentationConfig() throws Exception;

	/**
	 * Gets the list use case descriptors.
	 *
	 * @param handlersIds the handlers ids
	 * @return the list use case descriptors
	 * @throws Exception the exception
	 */
	List<UseCaseDescriptorDV> getListUseCaseDescriptors(List<String> handlersIds) throws Exception;

	/**
	 * Gets the lifecycle info for project id.
	 *
	 * @param profileID the profile ID
	 * @param projectID the project ID
	 * @return the lifecycle info for project id
	 * @throws Exception the exception
	 */
	LifecycleInformationDV getLifecycleInfoForProjectId(String profileID, String projectID) throws Exception;

	/**
	 * Delete project.
	 *
	 * @param profileID the profile ID
	 * @param projectID the project ID
	 * @return true, if successful
	 * @throws Exception the exception
	 */
	boolean deleteProject(String profileID, String projectID) throws Exception;

	/**
	 * Perform action steps.
	 *
	 * @param profileID the profile ID
	 * @param projectID the project ID
	 * @param optionalMessage the optional message
	 * @param action the action
	 * @return the project DV
	 * @throws Exception the exception
	 */
	StepPerformedResultDV performActionSteps(String profileID, String projectID, String optionalMessage, ActionDefinitionDV action) throws Exception;

	/**
	 * Gets the relationships definition.
	 *
	 * @param profileID the profile ID
	 * @return the relationships definition
	 * @throws Exception the exception
	 */
	List<RelationshipDefinitionDV> getRelationshipsDefinition(String profileID) throws Exception;

	/**
	 * Creates the relationship.
	 *
	 * @param fromProfileID  the from profile ID
	 * @param fromProjectID  the from project ID
	 * @param relationshipID the relationship ID
	 * @param toProfileID    the to profile ID
	 * @param toProjectID    the to project ID
	 * @throws Exception the exception
	 */
	void createRelationship(String fromProfileID, String fromProjectID, String relationshipID, String toProfileID,
			String toProjectID) throws Exception;

	/**
	 * Gets the project by ID.
	 *
	 * @param profileID the profile ID
	 * @param projectID the project ID
	 * @return the project by ID
	 * @throws Exception the exception
	 */
	ProjectDV getProjectByID(String profileID, String projectID) throws Exception;

	/**
	 * Gets the result document fo project by ID.
	 *
	 * @param profileID the profile ID
	 * @param projectID the project ID
	 * @return the result document fo project by ID
	 * @throws Exception the exception
	 */
	ResultDocumentDV getResultDocumentFoProjectByID(String profileID, String projectID) throws Exception;

	/**
	 * Delete relationship.
	 *
	 * @param fromProfileID the from profile ID
	 * @param fromProjectID the from project ID
	 * @param id            the id
	 * @param toProfileID   the to profile ID
	 * @param toProjectID   the to project ID
	 * @return the result document DV
	 * @throws Exception the exception
	 */
	ResultDocumentDV deleteRelationship(String fromProfileID, String fromProjectID, String id, String toProfileID,
			String toProjectID) throws Exception;

	/**
	 * Temporal reference for project.
	 *
	 * @param profileID the profile ID
	 * @param projectID the project ID
	 * @return the temporal reference DV
	 */
	TemporalReferenceDV temporalReferenceForProject(String profileID, String projectID);

	/**
	 * Gets the project edit.
	 *
	 * @param profileID the profile ID
	 * @param projectID the project ID
	 * @return the project edit
	 * @throws Exception the exception
	 */
	ProjectEdit getProjectEdit(String profileID, String projectID) throws Exception;

	/**
	 * Update geportal data form.
	 *
	 * @param profileID     the profile ID
	 * @param projectID     the project ID
	 * @param section       the section
	 * @param sectionPath   the section path
	 * @param listFilePaths the list file paths
	 * @return the commit report
	 * @throws Exception the exception
	 */
	CommitReport updateGeportalDataForm(String profileID, String projectID, GeoNaFormDataObject section,
			String sectionPath, List<FilePathDV> listFilePaths) throws Exception;

	/**
	 * Gets the project view.
	 *
	 * @param profileID the profile ID
	 * @param projectID the project ID
	 * @return the project view
	 * @throws Exception the exception
	 */
	ProjectView getProjectView(String profileID, String projectID) throws Exception;


	/**
	 * Gets the saving project monitor status.
	 *
	 * @param monitorUUID the monitor UUID
	 * @param isMonitoringTerminatedClientConsumed the is monitoring terminated client consumed
	 * @return the saving project monitor status
	 * @throws Exception the exception
	 */
	MonitoringActionsOnClient getSavingProjectMonitorStatus(String monitorUUID,
			boolean isMonitoringTerminatedClientConsumed) throws Exception;

}
