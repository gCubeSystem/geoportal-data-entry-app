package org.gcube.portlets.user.geoportaldataentry.client.ui.utils;

import org.gcube.portlets.user.geoportaldataentry.shared.Tree_Node;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.TreeItem;

public class TreeVisitUtil {

	public static <T> void preOrderVisit(Tree_Node<T> node) {
		GWT.log("preOrderVisit called");

		if (node == null)
			return;

		Tree_Node<T> parent = node.getParent();
		String parentName = null;
		if (parent != null) {
			parentName = parent.getName();
		}

		GWT.log("preOrderVisit Node name: " + node + ", parent: " + parentName);

		for (Tree_Node<T> child : node.getChildren()) {
			preOrderVisit(child);
		}

		return;

	}

	// Function to print the postorder traversal
	// of the n-ary tree
	public static void postOrderVisit(TreeItem root) {
		// GWT.log("postOrderVisit called");

		if (root == null) {
			// GWT.log("postOrderVisit returns, node is null");
			return;
		}

		// GWT.log("postOrderVisit called on: " + root.getText());

		// Total children count
		int total = root.getChildCount();
		for (int i = 0; i < total; i++) {
			postOrderVisit(root.getChild(i));
		}

		String parentText = root.getParentItem() != null ? root.getParentItem().getText() : null;
		GWT.log("PostOrderVisit: " + root.getText() + " with parent: " + parentText);
	}

}
