package org.gcube.portlets.user.geoportaldataentry.client.events;

import com.google.gwt.event.shared.EventHandler;


/**
 * The Interface GetListOfRecordsEventHandler.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 * Aug 4, 2021
 */
public interface GetListOfRecordsEventHandler extends EventHandler {


	/**
	 * On get list.
	 *
	 * @param getListOfRecordsEvent the get list of records event
	 */
	void onGetList(GetListOfRecordsEvent getListOfRecordsEvent);
}