package org.gcube.portlets.user.geoportaldataentry.client.events;

import com.google.gwt.event.shared.EventHandler;

/**
 * The Interface RelationActionHandler.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Oct 5, 2022
 */
public interface RelationActionHandler extends EventHandler {

	/**
	 * On create relation.
	 *
	 * @param createRelationHandlerEvent the create relation handler event
	 */
	void onCreateRelation(RelationActionHandlerEvent createRelationHandlerEvent);
}