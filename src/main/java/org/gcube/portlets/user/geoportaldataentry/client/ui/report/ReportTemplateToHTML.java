package org.gcube.portlets.user.geoportaldataentry.client.ui.report;

import org.gcube.portlets.user.geoportaldataentry.client.ui.utils.ExternalLib;

import com.github.gwtbootstrap.client.ui.AccordionGroup;
import com.github.gwtbootstrap.client.ui.Label;
import com.github.gwtbootstrap.client.ui.Paragraph;
import com.github.gwtbootstrap.client.ui.constants.IconType;
import com.github.gwtbootstrap.client.ui.event.HiddenEvent;
import com.github.gwtbootstrap.client.ui.event.HiddenHandler;
import com.github.gwtbootstrap.client.ui.event.ShowEvent;
import com.github.gwtbootstrap.client.ui.event.ShowHandler;
import com.google.gwt.core.client.GWT;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.json.client.JSONValue;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * The Class ReportTemplateToHTML.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 *         Jan 25, 2021
 */
public class ReportTemplateToHTML extends Composite {

	private static ReportTemplateToHTMLUiBinder uiBinder = GWT.create(ReportTemplateToHTMLUiBinder.class);

	/**
	 * The Interface ReportTemplateToHTMLUiBinder.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
	 * 
	 *         Jan 25, 2021
	 */
	interface ReportTemplateToHTMLUiBinder extends UiBinder<Widget, ReportTemplateToHTML> {
	}

	@UiField
	VerticalPanel vpContainer;

	@UiField
	VerticalPanel htmlContainer;

	@UiField
	AccordionGroup showReportAsJSON;

	@UiField
	AccordionGroup showReportAsTable;

	@UiField
	Paragraph reportJSON;

	@UiField
	Paragraph reportTable;

	@UiField
	Label labelToReport;

	boolean toJSONERROR = false;

	/**
	 * Instantiates a new report template to HTML.
	 *
	 * @param documentLabel   the document label
	 * @param theJSONDocument the the JSON document
	 * @param openTableReport the open table report
	 * @param openJSONReport  the open JSON report
	 */
	public ReportTemplateToHTML(String documentLabel, String theJSONDocument, boolean openTableReport,
			boolean openJSONReport) {
		initWidget(uiBinder.createAndBindUi(this));
		vpContainer.setVisible(false);
		vpContainer.setWidth("95%");

		showReportAsJSON.setDefaultOpen(openJSONReport);
		showReportAsTable.setDefaultOpen(openTableReport);

		documentLabel = documentLabel == null ? "Document" : documentLabel;
		labelToReport.setText(documentLabel);
		// showReportAsJSON.setIconPosition(IconPosition.RIGHT);

		if (openTableReport) {
			showReportAsTable.setIcon(IconType.ARROW_DOWN);
		} else {
			showReportAsTable.setIcon(IconType.ARROW_RIGHT);
		}

		if (openJSONReport) {
			showReportAsJSON.setIcon(IconType.ARROW_DOWN);
		} else {
			showReportAsJSON.setIcon(IconType.ARROW_RIGHT);
		}

		showReportAsTable.addShowHandler(new ShowHandler() {

			@Override
			public void onShow(ShowEvent showEvent) {
				showReportAsTable.setIcon(IconType.ARROW_DOWN);

			}
		});

		showReportAsTable.addHiddenHandler(new HiddenHandler() {

			@Override
			public void onHidden(HiddenEvent hiddenEvent) {
				showReportAsTable.setIcon(IconType.ARROW_RIGHT);

			}
		});

		showReportAsJSON.addShowHandler(new ShowHandler() {

			@Override
			public void onShow(ShowEvent showEvent) {
				showReportAsJSON.setIcon(IconType.ARROW_DOWN);

			}
		});

		showReportAsJSON.addHiddenHandler(new HiddenHandler() {

			@Override
			public void onHidden(HiddenEvent hiddenEvent) {
				showReportAsJSON.setIcon(IconType.ARROW_RIGHT);

			}
		});

		if (theJSONDocument != null) {
			GWT.log("report is: " + theJSONDocument);
			vpContainer.setVisible(true);

			try {

				JSONValue jsonObj = JSONParser.parse(theJSONDocument);
				String toTableHTML = jsonToHTML(jsonObj.toString());
				GWT.log("HTML table: " + toTableHTML);

				reportTable.add(new HTML(toTableHTML));

			} catch (Exception e) {
				e.printStackTrace();
				GWT.log("error: " + e.getMessage());
				showReportAsTable.setVisible(false);
			}

			reportJSON.add(new HTML("<pre>" + ExternalLib.toPrettyPrintJSON(theJSONDocument) + "</pre>"));
		}
	}

	/**
	 * Instantiates a new report template to HTML. Shows the JSON Document as Table
	 * HTML by default
	 *
	 * @param documentLabel   the document label
	 * @param theJSONDocument the the JSON document
	 * @param openJSONReport  the open JSON report
	 */
	public ReportTemplateToHTML(String documentLabel, String theJSONDocument, boolean openJSONReport) {

		initWidget(uiBinder.createAndBindUi(this));
		vpContainer.setVisible(false);

		showReportAsTable.setVisible(false);

		showReportAsJSON.setDefaultOpen(openJSONReport);

		documentLabel = documentLabel == null ? "Document" : documentLabel;
		labelToReport.setText(documentLabel);
		// showReportAsJSON.setIconPosition(IconPosition.RIGHT);

		if (openJSONReport) {
			showReportAsJSON.setIcon(IconType.ARROW_DOWN);
		} else {
			showReportAsJSON.setIcon(IconType.ARROW_RIGHT);
		}

		showReportAsJSON.addShowHandler(new ShowHandler() {

			@Override
			public void onShow(ShowEvent showEvent) {
				showReportAsJSON.setIcon(IconType.ARROW_DOWN);

			}
		});

		showReportAsJSON.addHiddenHandler(new HiddenHandler() {

			@Override
			public void onHidden(HiddenEvent hiddenEvent) {
				showReportAsJSON.setIcon(IconType.ARROW_RIGHT);

			}
		});

		if (theJSONDocument != null) {
			GWT.log("report is: " + theJSONDocument);
			vpContainer.setVisible(true);

			try {

				JSONValue jsonObj = JSONParser.parse(theJSONDocument);
				String toTableHTML = jsonToHTML(jsonObj.toString());
				GWT.log("HTML table: " + toTableHTML);
				htmlContainer.add(new HTML(toTableHTML));

			} catch (Exception e) {
				e.printStackTrace();
				GWT.log("error: " + e.getMessage());
				showReportAsTable.setVisible(false);
			}

			reportJSON.add(new HTML("<pre>" + ExternalLib.toPrettyPrintJSON(theJSONDocument) + "</pre>"));
		}

	}

	public void showAsHTMLTable(boolean bool) {
		showReportAsTable.setVisible(bool);
	}

	public void showAsJSON(boolean bool) {
		showReportAsJSON.setVisible(bool);
	}

	/**
	 * Json to HTML.
	 *
	 * @param jsonTxt the json txt
	 * @return the string
	 */
	private static native String jsonToHTML(String jsonTxt)/*-{
		try {
			var jsonObj = JSON.parse(jsonTxt);

			if (jsonObj.length == undefined)
				jsonObj = [ jsonObj ]
				//console.log(jsonObj.length)

				// EXTRACT VALUE FOR HTML HEADER.
			var col = [];
			for (var i = 0; i < jsonObj.length; i++) {
				for ( var key in jsonObj[i]) {
					//console.log('key json' +key)
					if (col.indexOf(key) === -1) {
						col.push(key);
					}
				}
			}

			// CREATE DYNAMIC TABLE.
			var table = document.createElement("table");
			try {
				table.classList.add("my-html-table");

			} catch (e) {
				console.log('invalid css add', e);
			}

			// ADD JSON DATA TO THE TABLE AS ROWS.
			for (var i = 0; i < col.length; i++) {
				tr = table.insertRow(-1);
				var firstCell = tr.insertCell(-1);
				//firstCell.style.cssText="font-weight: bold; text-align: center; vertical-align: middle;";
				firstCell.innerHTML = col[i];
				for (var j = 0; j < jsonObj.length; j++) {
					var tabCell = tr.insertCell(-1);
					var theValue = jsonObj[j][col[i]];
					//console.log("the value: "+theValue);
					if (Object.prototype.toString.call(theValue) === '[object Array]') {
						var formattedValueArray = "";
						for (var k = 0; k < theValue.length; k++) {
							var theValueArray = theValue[k];
							//console.log(theValueArray);
							formattedValueArray += theValueArray + "<br>";
						}
						tabCell.innerHTML = formattedValueArray;
					} else {
						tabCell.innerHTML = theValue;
					}
				}
			}

			return table.outerHTML;
		} catch (e) {
			console.log('invalid json', e);
			return jsonTxt;
		}

	}-*/;
}
