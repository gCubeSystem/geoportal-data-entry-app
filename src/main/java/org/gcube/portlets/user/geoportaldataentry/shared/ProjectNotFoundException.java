package org.gcube.portlets.user.geoportaldataentry.shared;

public class ProjectNotFoundException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4918313646452701634L;

	public ProjectNotFoundException() {
	}
	
	public ProjectNotFoundException(String error){
		super(error);
	}
	
	public ProjectNotFoundException(Throwable error){
		super(error);
	}

}
