package org.gcube.portlets.user.geoportaldataentry.shared;

import java.io.Serializable;
import java.util.List;

import org.gcube.application.geoportal.common.model.legacy.Concessione;
import org.gcube.application.geoportalcommon.shared.SearchingFilter;

public class ResultSetSorted implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 889420364685643758L;

	private SearchingFilter searchFilter;

	private List<Concessione> data;

	ResultSetSorted() {

	}

	public ResultSetSorted(SearchingFilter searchFilter, List<Concessione> data) {
		super();
		this.searchFilter = searchFilter;
		this.data = data;
	}

	public SearchingFilter getSearchFilter() {
		return searchFilter;
	}

	public List<Concessione> getData() {
		return data;
	}

	public void setSearchFilter(SearchingFilter searchFilter) {
		this.searchFilter = searchFilter;
	}

	public void setData(List<Concessione> data) {
		this.data = data;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ResultSetSorted [searchFilter=");
		builder.append(searchFilter);
		builder.append(", data=");
		builder.append(data);
		builder.append("]");
		return builder.toString();
	}

}
