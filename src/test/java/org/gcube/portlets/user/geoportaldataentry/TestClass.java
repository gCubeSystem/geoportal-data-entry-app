//package org.gcube.portlets.user.geoportaldataentry;
//
//
//
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.HashMap;
//import java.util.Iterator;
//import java.util.List;
//import java.util.Map;
//
//import org.bson.Document;
//import org.gcube.application.geoportal.common.model.legacy.Concessione;
//import org.gcube.application.geoportal.common.model.rest.QueryRequest;
//import org.gcube.application.geoportal.common.model.rest.QueryRequest.OrderedRequest;
//import org.gcube.application.geoportal.common.model.rest.QueryRequest.OrderedRequest.Direction;
//import org.gcube.application.geoportal.common.model.rest.QueryRequest.PagedRequest;
//import org.gcube.application.geoportal.common.rest.MongoConcessioni;
//import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
//import org.gcube.common.scope.api.ScopeProvider;
//
//import com.mongodb.BasicDBList;
//import com.mongodb.BasicDBObject;
//import com.mongodb.BasicDBObjectBuilder;
//
//public class TestClass {
//
//	private static String TOKEN = "";
//	private static String CONTEXT = "/gcube/devsec/devVRE";
//	private static String USERNAME = "francesco.mangiacrapa";
//
//	//@Before
//	public void init() {
//		ScopeProvider.instance.set(CONTEXT);
//		SecurityTokenProvider.instance.set(TOKEN);
//	}
//
//	// @Test
//	public void getListConcessioniTest() throws Exception {
//		MongoConcessioni clientMongo = mongoConcessioni().build();
//		Iterator<Concessione> concessioni = clientMongo.getList();
//		List<Concessione> listOfConcessioni = new ArrayList<Concessione>();
//		if (concessioni != null) {
//			while (concessioni.hasNext()) {
//				Concessione concessione = (Concessione) concessioni.next();
//				listOfConcessioni.add(concessione);
//
//			}
//		}
//		int i = 0;
//		for (Concessione concessione : listOfConcessioni) {
//			System.out.println(++i + " " + concessione);
//		}
//	}
//
//	//@Test
//	public void queryConcessioniTest() throws Exception {
//		try {
//			MongoConcessioni clientMongo = mongoConcessioni().build();
//			int offsetIndex = 0;
//			int limitIndex = 25;
//			Direction sDirection = Direction.ASCENDING;
//			List<String> orderByFields = Arrays.asList("nome");
//
//			Map<String, Object> searchFields = new HashMap<String, Object>();
//			searchFields.put("nome", "test");
//			searchFields.put("authors", "fra");
//
//			QueryRequest request = new QueryRequest();
//			PagedRequest paging = new PagedRequest();
//			paging.setOffset(offsetIndex);
//			paging.setLimit(limitIndex);
//			request.setPaging(paging);
//
//			OrderedRequest ordering = new OrderedRequest();
//
//			ordering.setDirection(sDirection);
//
//			ordering.setFields(orderByFields);
//			request.setOrdering(ordering);
//
//			Document query = null;
//
//			if (searchFields != null) {
//
//				BasicDBObjectBuilder builder = BasicDBObjectBuilder.start();
//
//				query = new Document();
//
//				for (String key : searchFields.keySet()) {
//					// builder.append(key, new BasicDBObject("$eq", searchFields.get(key)));
//					BasicDBObject bs = new BasicDBObject();
//					bs.append("$regex", searchFields.get(key));
//					bs.append("$options", "i");
//					builder.append(key, bs);
//					// query.put(key, new BasicDBObject("$eq", searchFields.get(key)));
//					// query = new Document(key, searchFields.get(key));
//				}
//
////			query.putAll(builder.get().toMap());
////			request.setFilter(query);
//
//				BasicDBList list = new BasicDBList();
//				list.add(builder.get().toMap());
//				query.put("$or", list);
//				// or(query);
//				request.setFilter(query);
//
//				// ************************************************ AND
////			query = new Document();
////			for (String key : searchFields.keySet()) {
////				//AND
////				 BasicDBObject bs = new BasicDBObject();
////				 bs.append("$regex", searchFields.get(key));
////				 bs.append("$options", "i");
////				 query.put(key, bs);
////				 
////			}
////			request.setFilter(query);
//				// ******************************************** END AND
//			}
//
//			// OR
//			/*
//			 * query = new Document(); BasicDBObject container = new BasicDBObject();
//			 * BsonArray bArray = new BsonArray(); for (String key : searchFields.keySet())
//			 * { //AND BasicDBObject bs = new BasicDBObject(); bs.append("$regex",
//			 * searchFields.get(key)); bs.append("$options", "i"); query.put(key, bs);
//			 * 
//			 * //container.put(key, bs);
//			 * 
//			 * 
//			 * // BasicDBObject bs2 = new BasicDBObject(); // bs2.append("$regex",
//			 * searchFields.get(key)); // bs2.append("$options", "i"); // BsonDocument bsK =
//			 * new BsonDocument(); // bsK.append(key, new BsonString(bs2.toJson())); //
//			 * bArray.add(bsK);
//			 * 
//			 * } // query.put("$or", bArray);
//			 * 
//			 * BasicDBList list = new BasicDBList(); list.add(query);
//			 * 
//			 * Document orDocument = new Document(); orDocument.put("$or", list); // query =
//			 * orDocument; request.setFilter(query);
//			 */
//
//			System.out.println("Paging offset: " + offsetIndex + ", limit: " + limitIndex);
//			System.out.println("Direction: " + sDirection);
//			System.out.println("Order by Fields: " + orderByFields);
//			System.out.println("Search for: " + query);
//			System.out.println("Search for Query to JSON: " + query.toJson());
//
//			Iterator<Concessione> concessioni = clientMongo.query(request);
//
//			if (concessioni.hasNext()) {
//				System.out.println("Found concessioni, printing them...");
//			} else
//				System.out.println("No concessione found");
//
//			if (concessioni != null) {
//				while (concessioni.hasNext()) {
//					Concessione concessione = (Concessione) concessioni.next();
//					System.out.println(concessione.getNome());
//
//				}
//
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//
//	// @Test
////	public void readUserRights() throws Exception {
////		new GNARoleRitghtsConfigReader();
////		List<RoleRights> listUserRights = GNARoleRitghtsConfigReader.readRoleRightsConfig();
////		System.out.println(listUserRights);
////	}
//
//
//
//}
