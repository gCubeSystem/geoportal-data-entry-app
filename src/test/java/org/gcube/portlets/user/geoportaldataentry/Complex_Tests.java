package org.gcube.portlets.user.geoportaldataentry;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Properties;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import org.bson.Document;
import org.gcube.application.geoportal.client.utils.Serialization;
import org.gcube.application.geoportal.common.model.JSONPathWrapper;
import org.gcube.application.geoportal.common.model.document.Project;
import org.gcube.application.geoportal.common.model.document.access.Access;
import org.gcube.application.geoportal.common.model.document.access.AccessPolicy;
import org.gcube.application.geoportal.common.model.document.filesets.RegisteredFileSet;
import org.gcube.application.geoportal.common.model.useCaseDescriptor.HandlerDeclaration;
import org.gcube.application.geoportal.common.model.useCaseDescriptor.UseCaseDescriptor;
import org.gcube.application.geoportalcommon.ConvertToDataServiceModel;
import org.gcube.application.geoportalcommon.ConvertToDataValueObjectModel;
import org.gcube.application.geoportalcommon.ProjectDVBuilder;
import org.gcube.application.geoportalcommon.geoportal.GeoportalClientCaller;
import org.gcube.application.geoportalcommon.geoportal.ProjectsCaller;
import org.gcube.application.geoportalcommon.geoportal.UseCaseDescriptorCaller;
import org.gcube.application.geoportalcommon.geoportal.access.GeportalCheckAccessPolicy;
import org.gcube.application.geoportalcommon.geoportal.serdes.Payload;
import org.gcube.application.geoportalcommon.shared.geoportal.ConfigurationDV;
import org.gcube.application.geoportalcommon.shared.geoportal.config.FilePathDV;
import org.gcube.application.geoportalcommon.shared.geoportal.config.GcubeProfileDV;
import org.gcube.application.geoportalcommon.shared.geoportal.materialization.innerobject.FilesetDV;
import org.gcube.application.geoportalcommon.shared.geoportal.materialization.innerobject.PayloadDV;
import org.gcube.application.geoportalcommon.shared.geoportal.project.ProjectDV;
import org.gcube.application.geoportalcommon.shared.geoportal.ucd.GEOPORTAL_CONFIGURATION_TYPE;
import org.gcube.application.geoportalcommon.shared.geoportal.ucd.GEOPORTAL_DATA_HANDLER;
import org.gcube.application.geoportalcommon.shared.geoportal.ucd.HandlerDeclarationDV;
import org.gcube.application.geoportalcommon.shared.geoportal.view.ProjectView;
import org.gcube.application.geoportalcommon.shared.geoportal.view.SectionView;
import org.gcube.application.geoportalcommon.shared.geoportal.view.SubDocumentView;
import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.portlets.user.geoportaldataentry.server.FileSetDataObject;
import org.gcube.portlets.user.geoportaldataentry.server.FormDataObjectToJSON;
import org.gcube.portlets.user.geoportaldataentry.server.FormDataObjectToJSON.JSONObjectOrdered;
import org.gcube.portlets.user.geoportaldataentry.server.MongoServiceUtil;
import org.gcube.portlets.user.geoportaldataentry.server.json.JsonMerge;
import org.gcube.portlets.user.geoportaldataentry.server.json.JsonMerge.MERGE_OPTION;
import org.gcube.portlets.widgets.mpformbuilder.server.MetadataProfileFormBuilderServiceImpl;
import org.gcube.portlets.widgets.mpformbuilder.shared.metadata.MetaDataProfileBean;
import org.gcube.portlets.widgets.mpformbuilder.shared.metadata.MetadataFieldWrapper;
import org.gcube.portlets.widgets.mpformbuilder.shared.upload.FileUploadedRemote;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gwt.user.client.Random;
import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.spi.json.GsonJsonProvider;
import com.jayway.jsonpath.spi.json.JsonOrgJsonProvider;

public class Complex_Tests {

	private static final String GCUBE_CONFIG_PROPERTIES_FILENAME = "gcube_config.properties";
	// APP Working Directory + /src/test/resources must be the location of
	// gcube_config.properties
	private static String gcube_config_path = String.format("%s/%s",
			System.getProperty("user.dir") + "/src/test/resources", GCUBE_CONFIG_PROPERTIES_FILENAME);
	private static String CONTEXT;
	private static String TOKEN;

	private UseCaseDescriptorCaller clientUCD = null;
	private ProjectsCaller clientPrj = null;

	private static String PROFILE_ID = "profiledConcessioni";
	private static String PROJECT_ID = "644a66e944aad51c80409a3b";

	private static String MY_LOGIN = "francesco.mangiacrapa";

	public static final String JSON_$_POINTER = "$";
	private static final Logger LOG = LoggerFactory.getLogger(Complex_Tests.class);

	private LinkedHashMap<String, List<GcubeProfilesMetadataForUCD>> linkedMap_UCDId_gCubeProfiles;

	public static enum COMMON_IMAGES_FORMAT {
		gif, png, jpeg, jpg, bmp, tif, tiff, svg, avif, webp
	}

	public static class ImageDetector {

		private static String[] getNames(Class<? extends Enum<?>> e) {
			return Arrays.stream(e.getEnumConstants()).map(Enum::name).toArray(String[]::new);
		}

		public static List<String> listFormatImages;

		static {

			String[] arrayImgs = ImageDetector.getNames(COMMON_IMAGES_FORMAT.class);
			listFormatImages = Arrays.asList(arrayImgs);

		}

		public static boolean isImage(String mimeType) {
			if (mimeType == null || mimeType.isEmpty())
				return false;

			String inputImageFormat = mimeType.replaceAll("image/", "");

			return listFormatImages.contains(inputImageFormat);
		}

	}

	/**
	 * Read context settings.
	 */
	public static void readContextSettings() {

		try (InputStream input = new FileInputStream(gcube_config_path)) {

			Properties prop = new Properties();

			// load a properties file
			prop.load(input);

			CONTEXT = prop.getProperty("CONTEXT");
			TOKEN = prop.getProperty("TOKEN");
			// get the property value and print it out
			System.out.println("CONTEXT: " + CONTEXT);
			System.out.println("TOKEN: " + TOKEN);

		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	@Before
	public void init() {
		readContextSettings();
		ScopeProvider.instance.set(CONTEXT);
		SecurityTokenProvider.instance.set(TOKEN);
		clientPrj = GeoportalClientCaller.projects();
		clientUCD = GeoportalClientCaller.useCaseDescriptors();
	}

	// @Test
	public void testSectionPathInnerParse() throws Exception {
		String sectionPath = "$.abstractRelazione";

		String profileID = "profiledConcessioni";
		String projectID = "6425598a8593b215a1281e1c";

		Configuration configurationGSON = Configuration.builder().jsonProvider(new GsonJsonProvider()).build();

		ProjectsCaller client = GeoportalClientCaller.projects();

		Project proejct = client.getProjectByID(profileID, projectID);

		String jsonSourceProject = proejct.getTheDocument().toJson();

		// If the section path is the Root document, passed as "$.", fixing as "$"
		if (sectionPath.compareTo(FormDataObjectToJSON.JSON_$_POINTER + ".") == 0)
			sectionPath = FormDataObjectToJSON.JSON_$_POINTER;

		LOG.info("theString: {}", jsonSourceProject);
		com.google.gson.JsonObject currentSectionJObject = JsonPath.parse(jsonSourceProject, configurationGSON)
				.read(sectionPath);
		LOG.info("currentSectionJObject: {}", currentSectionJObject.toString());
		LOG.info("sourceSectionObject: {}", currentSectionJObject.toString());

		JSONObject targetSectionJObject = new JSONObjectOrdered().instance();
		targetSectionJObject.put("abstractIta", "Prova REJECT 1");
		targetSectionJObject.put("titolo", "Prova REJECT abstract relazione di scavo 1");
		LOG.info("targetSectionJObject: {}", targetSectionJObject.toString());

		String mergedDoc = JsonMerge.merge(targetSectionJObject.toString(), currentSectionJObject.toString(),
				MERGE_OPTION.REPLACE);
		LOG.info("output: {}", mergedDoc);

		String newDocJson;
		// Updating path is first level of the root
		if (sectionPath.equals(FormDataObjectToJSON.JSON_$_POINTER)) {
			// The merged DOC is the root Document
			newDocJson = mergedDoc;
		} else {
			// The merged DOC is a child of the root Document
			Gson gson = new Gson();
			JsonObject gsonOject = gson.fromJson(mergedDoc, JsonObject.class);
			// Putting the merged section into Document
			DocumentContext newContextDocJson = JsonPath.parse(jsonSourceProject, configurationGSON).set(sectionPath,
					gsonOject);
			newDocJson = newContextDocJson.json().toString();

		}

		Document updatedDocument = Serialization.read(newDocJson.toString(), Document.class);
		LOG.info("New document is: {}", updatedDocument.toJson());

	}

	// @Test
	public void testSectionPathRootParse() throws Exception {
		String sectionPath = "$.";

		String profileID = "profiledConcessioni";
		String projectID = "6425598a8593b215a1281e1c";

		Configuration configurationGSON = Configuration.builder().jsonProvider(new GsonJsonProvider()).build();

		ProjectsCaller client = GeoportalClientCaller.projects();

		Project proejct = client.getProjectByID(profileID, projectID);

		String jsonSourceProject = proejct.getTheDocument().toJson();

		// If the section path is the Root document, passed as "$.", fixing as "$"
		if (sectionPath.compareTo(FormDataObjectToJSON.JSON_$_POINTER + ".") == 0)
			sectionPath = FormDataObjectToJSON.JSON_$_POINTER;

		LOG.info("theString: {}", jsonSourceProject);
		com.google.gson.JsonObject currentSectionJObject = JsonPath.parse(jsonSourceProject, configurationGSON)
				.read(sectionPath);
		LOG.info("currentSectionJObject: {}", currentSectionJObject.toString());
		LOG.info("sourceSectionObject: {}", currentSectionJObject.toString());

		JSONObject targetSectionJObject = new JSONObjectOrdered().instance();
		targetSectionJObject.put("nome", proejct.getTheDocument().get("nome") + " 1");
		targetSectionJObject.put("titolo", proejct.getTheDocument().get("introduzione") + " 1");
		LOG.info("targetSectionJObject: {}", targetSectionJObject.toString());

//		JSONObject mergedSection = FormDataObjectToJSON.deepMerge(sourceSectionObject,
//				targetSectionJObject);

		String output = JsonMerge.merge(targetSectionJObject.toString(), currentSectionJObject.toString(),
				MERGE_OPTION.REPLACE);
		LOG.info("output: {}", output);

		Gson gson = new Gson();
		JsonObject gsonOject = gson.fromJson(output, JsonObject.class);

		// Putting the merged section into Document
		DocumentContext newContextDocJson = JsonPath.parse(jsonSourceProject, configurationGSON).set(sectionPath,
				gsonOject);
		// DocumentContext newDocument = JsonPath.parse(jsonSourceProject,
		// configurationGSON).set(sectionPath, new JSONObject(output));
		// String newDocJson = JsonPath.parse(jsonSourceProject).set(sectionPath, new
		// JSONObject(output)).jsonString();
		String newDocJson = newContextDocJson.json().toString();
		LOG.info("Going to call updateProject with document: {}", newDocJson);

		Document updatedDocument = Serialization.read(newDocJson.toString(), Document.class);
		LOG.info("New document is: {}", updatedDocument.toJson());

	}

	// @Before
	public void preloadgCubeProfilesForUCDs() {
		LOG.debug("preloadgCubeProfilesForUCDs called");
		try {

			List<UseCaseDescriptor> listUCDs = clientUCD.getList();

			for (UseCaseDescriptor ucd : listUCDs) {

				LOG.debug("Loaded UCD for ID: " + ucd.getId());
				GEOPORTAL_DATA_HANDLER theHandler = GEOPORTAL_DATA_HANDLER.geoportal_data_entry;
				List<HandlerDeclaration> handlers = ucd.getHandlersByType(theHandler.getType());

				if (handlers.size() == 0) {
					LOG.warn("No handler " + theHandler + "found into UCD " + ucd.getId() + ", continue...");
					continue;
				}

				// Loading Handler gcube_profiles
				HandlerDeclaration dataEntryHandler = handlers.get(0);
				HandlerDeclarationDV handlerGcubeProfiles = ConvertToDataValueObjectModel
						.toHandlerDeclarationDV(dataEntryHandler, ucd, GEOPORTAL_CONFIGURATION_TYPE.gcube_profiles);

				LOG.debug("Handler " + GEOPORTAL_CONFIGURATION_TYPE.gcube_profiles + " for PROFILE_ID: " + ucd.getId());
				LOG.debug("" + handlerGcubeProfiles);

				ConfigurationDV<?> config = handlerGcubeProfiles.getConfiguration();
				// List of gCube Profiles defined in the UCD
				List<GcubeProfileDV> listGcubeProfiles = toListGcubeProfiles(config);
				LOG.debug("List of GcubeProfileDV are: " + listGcubeProfiles);

				List<GcubeProfilesMetadataForUCD> listProfilesBean = new ArrayList<GcubeProfilesMetadataForUCD>();
				// Loading Metadata Profile from IS
				MetadataProfileFormBuilderServiceImpl metaProfileBUilder = new MetadataProfileFormBuilderServiceImpl();
				linkedMap_UCDId_gCubeProfiles = new LinkedHashMap<String, List<GcubeProfilesMetadataForUCD>>();
				for (GcubeProfileDV gcubeProfileDV : listGcubeProfiles) {
					GcubeProfilesMetadataForUCD gCubeProfileMetadataForUCD = new GcubeProfilesMetadataForUCD();
					ScopeProvider.instance.set(CONTEXT);
					SecurityTokenProvider.instance.set(TOKEN);
					List<MetaDataProfileBean> listProfiles = metaProfileBUilder.getProfilesInTheScopeForName(CONTEXT,
							gcubeProfileDV.getGcubeSecondaryType(), gcubeProfileDV.getGcubeName());

					String key = gcubeProfileDV.getGcubeSecondaryType() + gcubeProfileDV.getGcubeName();
					LOG.debug("for key: " + key + " readi profiles: " + listGcubeProfiles);
					gCubeProfileMetadataForUCD.setGcubeProfile(gcubeProfileDV);
					gCubeProfileMetadataForUCD.setListMetadataProfileBean(listProfiles);
					listProfilesBean.add(gCubeProfileMetadataForUCD);

				}
				linkedMap_UCDId_gCubeProfiles.put(ucd.getId(), listProfilesBean);

				for (String key : linkedMap_UCDId_gCubeProfiles.keySet()) {
					LOG.debug("For key '" + key + "' got profiles: " + linkedMap_UCDId_gCubeProfiles.get(key));
				}
			}

		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	//@Test
	public void deleteFileSet_ServiceTest() throws Exception {
		ScopeProvider.instance.set(CONTEXT);
		SecurityTokenProvider.instance.set(TOKEN);

		boolean ignore_errors = false;
		String path = "$.abstractRelazione.filesetIta";

		Project doc = clientPrj.getProjectByID(PROFILE_ID, PROJECT_ID);

		JSONPathWrapper wrapper = new JSONPathWrapper(doc.getTheDocument().toJson());
		List<String> matchingPaths = wrapper.getMatchingPaths(path);

		LOG.info("matchingPaths is: " + matchingPaths);

		String error = null;
		if (matchingPaths.isEmpty()) {
			error = "No Registered FileSet found at " + path;
			if (!ignore_errors) {
				throw new WebApplicationException(error, Response.Status.BAD_REQUEST);
			}
		}
		if (matchingPaths.size() > 1 && !ignore_errors) {
			error = "Multiple Fileset (" + matchingPaths.size() + ") matching " + path;
			if (!ignore_errors)
				throw new WebApplicationException(error, Response.Status.BAD_REQUEST);
		}

		if (error != null && ignore_errors) {
			LOG.info("Error detected {}. Ignoring it and returning input doc", error);

		}
		
		List<Object> listPath = wrapper.getByPath(path);
		LOG.info("List path: "+listPath);
//		RegisteredFileSet fs = Serialization.convert(listPath.get(0), RegisteredFileSet.class);
//		LOG.info("Going to delete {}", fs);

	}

	//@Test
	public void registerFileSet() throws Exception {
		ScopeProvider.instance.set(CONTEXT);
		SecurityTokenProvider.instance.set(TOKEN);

		Project theProject = clientPrj.getProjectByID(PROFILE_ID, PROJECT_ID);

		MongoServiceUtil mongoService = new MongoServiceUtil();

		Access access = new Access();
		access.setLicense("CC-BY");
		access.setPolicy(AccessPolicy.OPEN);

		String sectionJSONPath = "$.abstractRelazione";
		String fieldName = "filesetIta";
		String fieldDefinition = "$.abstractRelazione._children[?(@.filesetIta)]";

		String theFileName = "Application_Profile_for_CSW_2.0-2.pdf";
		String theFileURL = "https://data.dev.d4science.org/shub/E_bnN2aDJZZUMySy9peE9ScEVLNVFNWjBOZWx0cXQ2UUFkQ2E3Rjc1S29EelJIMEJGbDRoczBnbHVPWHczZTNQTw==";

		FileUploadedRemote file = new FileUploadedRemote();
		file.setUrl(theFileURL);
		file.setFileName(theFileName);

		File input = null;
		File output = null;
		try {
			File tempDir = Files.createTempDirectory("GEOPORTAL_REPLACE_FILES_").toFile();
			String tmpDirPath = tempDir.getAbsolutePath();
			if (file instanceof FileUploadedRemote) {
				FileUploadedRemote remote = (FileUploadedRemote) file;
				LOG.info("Uploaded file is remote: " + remote.getUrl());
				InputStream in = new URL(remote.getUrl()).openStream();
				String fileName = (remote.getFileName() == null || remote.getFileName().isEmpty())
						? "file_" + Random.nextInt()
						: remote.getFileName();
				LOG.info("the fileName is: " + fileName);
				output = new File(tmpDirPath, fileName);
				Path outputAbsolutePath = Paths.get(output.getAbsolutePath());
				Files.copy(in, outputAbsolutePath, StandardCopyOption.REPLACE_EXISTING);
				LOG.info("Remote file: " + remote.getUrl() + ", copied to new file: " + output.getName());
			} else {
				LOG.info("Uploaded file is local: " + file.getTempSystemPath());
				input = new File(file.getTempSystemPath());
				output = new File(tmpDirPath, file.getFileName());
				copyContent(input, output);
				LOG.info("Temp file: " + file.getTempSystemPath() + ", copied to new file: " + output.getName());
			}
			// tempDir.deleteOnExit();
		} catch (Exception e) {
			LOG.warn("Skipping file: " + file.getFileName() + ". Error: " + e.getMessage(), e);
		}

		File fileset = output;
		LOG.info("the fileName is: " + fileset.getName());

		mongoService.registerFileSet(theProject.getProfileID(), theProject, sectionJSONPath, fieldName, fieldDefinition,
				access, fileset);

	}

	/**
	 * Copy content.
	 *
	 * @param a the a
	 * @param b the b
	 * @throws Exception the exception
	 */
	public static void copyContent(File a, File b) throws Exception {
		FileInputStream in = new FileInputStream(a);
		FileOutputStream out = new FileOutputStream(b);

		try {

			int n;

			// read() function to read the
			// byte of data
			while ((n = in.read()) != -1) {
				// write() function to write
				// the byte of data
				out.write(n);
			}
		} finally {
			if (in != null) {

				// close() function to close the
				// stream
				in.close();
			}
			// close() function to close
			// the stream
			if (out != null) {
				out.close();
			}
		}
		LOG.debug("File Copied");
	}

	// @Test
	public void testReadProjectForUCDDataEntry() {
		ScopeProvider.instance.set(CONTEXT);
		SecurityTokenProvider.instance.set(TOKEN);

		try {

			Project theProject = clientPrj.getProjectByID(PROFILE_ID, PROJECT_ID);
			ProjectDVBuilder projectBuilder = ProjectDVBuilder.newBuilder().fullDocumentMap(true);
			ProjectDV theProjectDV = ConvertToDataValueObjectModel.toProjectDV(theProject, projectBuilder);
			String theWholeProjectAsJSON = theProject.getTheDocument().toJson();

			LOG.debug("theProjectDV as JSON: " + theWholeProjectAsJSON);
			LOG.debug("theProjectDV as MAP: " + theProjectDV.getTheDocument().getDocumentAsMap());

			ProjectView projectView = new ProjectView();
			projectView.setTheProjectDV(theProjectDV);

			// NO UCD defined, applyong default
			if (linkedMap_UCDId_gCubeProfiles.size() == 0) {
				LOG.warn("No " + GEOPORTAL_CONFIGURATION_TYPE.gcube_profiles + " found in the UCD");
				LOG.info("Applying default business logic to display the project");
				SectionView sectionView = new SectionView();
				sectionView.setSectionTitle("Document");
				SubDocumentView subDocumentView = new SubDocumentView();
				subDocumentView.setMetadataAsJSON(theProjectDV.getTheDocument().getDocumentAsJSON());
				sectionView.addSubDocument(subDocumentView);
				projectView.addSectionView(sectionView);
			}

			List<GcubeProfilesMetadataForUCD> listProfilesBean = linkedMap_UCDId_gCubeProfiles.get(PROFILE_ID);

			com.jayway.jsonpath.Configuration configuration = com.jayway.jsonpath.Configuration.builder()
					.jsonProvider(new JsonOrgJsonProvider()).build();

			// Reading the Project according to list of Profile defined in the UCD
			for (GcubeProfilesMetadataForUCD gcubeProfileMetaForUCD : listProfilesBean) {

				GcubeProfileDV gcubeProfileDV = gcubeProfileMetaForUCD.getGcubeProfile();
				SectionView sectionView = new SectionView();
				sectionView.setSectionTitle(gcubeProfileDV.getSectionTitle());
				LOG.debug("\n\nThe profile is: " + gcubeProfileDV);
				// Building JSON/section full PATH and section name
				String sectionJSONPath = "";
				String parentPathFromProfile = gcubeProfileDV.getParentName() == null ? ""
						: gcubeProfileDV.getParentName();
				String theSectionName = gcubeProfileDV.getSectionName();

				if (theSectionName.compareTo(JSON_$_POINTER) == 0
						|| theSectionName.compareTo(JSON_$_POINTER + ".") == 0) {
					sectionJSONPath = JSON_$_POINTER;
					theSectionName = "";
				} else {
					sectionJSONPath = String.format("%s%s",
							parentPathFromProfile.endsWith(".") ? parentPathFromProfile : parentPathFromProfile + ".",
							theSectionName);
				}

				LOG.debug("The sectionJSONPath is: " + sectionJSONPath);

				JsonPath theSectionJsonPath = JsonPath.compile(sectionJSONPath);
				Object data = theSectionJsonPath.read(theWholeProjectAsJSON, configuration);

				LOG.debug("Data is instace of: " + data.getClass());
				LOG.debug("data to string: " + data.toString());

				// Splitting the General Document in bson.Document according to list of
				// GcubeProfiles
				List<Document> listBSONDocument = new ArrayList<Document>();
				if (data instanceof org.json.JSONObject) {
					String jsonString = data.toString();
					LOG.debug("the JSON to string: " + jsonString);
					Document sectionDoc = Document.parse(jsonString);
					boolean isAccessibleSection = isAccessibleSectionAccordingToPolicy(sectionDoc, sectionJSONPath);
					if (isAccessibleSection) {
						listBSONDocument.add(sectionDoc);
					}

				} else if (data instanceof org.json.JSONArray) {
					org.json.JSONArray dataArray = (org.json.JSONArray) data;
					for (int i = 0; i < dataArray.length(); i++) {
						String jsonString = dataArray.get(i).toString();
						LOG.debug("the array " + i + " JSON to string: " + jsonString);
						Document sectionDoc = Document.parse(jsonString);
						boolean isAccessibleSection = isAccessibleSectionAccordingToPolicy(sectionDoc, sectionJSONPath);
						if (isAccessibleSection) {
							listBSONDocument.add(sectionDoc);
						}

					}
				}

				LOG.debug("Result for " + gcubeProfileDV.getSectionName() + " is: " + listBSONDocument);
				List<MetaDataProfileBean> theProfileBeans = gcubeProfileMetaForUCD.getListMetadataProfileBean();
				MetaDataProfileBean theProfileBean = theProfileBeans.get(0);

				// For each bson.Document creating the SubDocumentView
				for (int i = 0; i < listBSONDocument.size(); i++) {
					Document fromSectionDoc = listBSONDocument.get(i);
					SubDocumentView subDocumentView = new SubDocumentView();
					Document toSectionDoc = new Document();
					// Filling the SubDocumentView metadata with the metadataField.getFieldName()
					// read from the Profile
					for (MetadataFieldWrapper metadataField : theProfileBean.getMetadataFields()) {

						String theFieldName = metadataField.getFieldId() != null ? metadataField.getFieldId()
								: metadataField.getFieldName();
						LOG.debug("reading theFieldName: " + theFieldName);
						Object theOBJFieldValue = fromSectionDoc.get(theFieldName);

						// NB: Using ALWAYS THE metadataField.getFieldName() as LABEL
						toSectionDoc = sanitizeDocumentValue(toSectionDoc, metadataField.getFieldName(),
								theOBJFieldValue);

					}
					String subToJSON = toSectionDoc.toJson();
					LOG.debug("theSubSetionDoc is: " + subToJSON);
					subDocumentView.setMetadataAsJSON(toSectionDoc.toJson());

					// Reading filePaths
					List<FilePathDV> filePaths = gcubeProfileDV.getFilePaths();

					// READING fileset* field ACCORDING TO filePaths OF THE 'gcubeProfiles' CONFIG
					if (filePaths != null) {
						String fromSectionDocJSON = fromSectionDoc.toJson();
						List<FilesetDV> listFiles = new ArrayList<FilesetDV>();
						List<FilesetDV> listImages = new ArrayList<FilesetDV>();
						for (FilePathDV filePath : filePaths) {

							String filesetJSONPath = String.format("%s.%s", JSON_$_POINTER, filePath.getFieldName());
							List<Payload> listPayloads = readPayloadsForFileset(filesetJSONPath, fromSectionDocJSON);
							FilesetDV filesetDV = new FilesetDV();
							filesetDV.setGcubeProfileFieldName(filePath.getGcubeProfileFieldName());
							for (Payload payload : listPayloads) {
								PayloadDV payloadDV = ConvertToDataValueObjectModel.toPayloadDV(payload);
								filesetDV.addPayloadDV(payloadDV);
								boolean isImage = ImageDetector.isImage(payload.getMimetype());

								if (isImage) {
									listImages.add(filesetDV);
								} else {
									listFiles.add(filesetDV);
								}
							}

						}
						subDocumentView.setListFiles(listFiles);
						subDocumentView.setListImages(listImages);
					}

					sectionView.addSubDocument(subDocumentView);

				}

				projectView.addSectionView(sectionView);
			}

			for (SectionView section : projectView.getListSections()) {
				System.out.println("\n\n###### Section Title: " + section.getSectionTitle() + " ######");
				int i = 1;
				for (SubDocumentView subDocument : section.getListSubDocuments()) {
					System.out.println("## " + SubDocumentView.class.getSimpleName() + " n." + i);
					System.out.println("***** Metadata");
					System.out.println(prettyPrintJSON(subDocument.getMetadataAsJSON()));
					System.out.println("***** Files");
					if (subDocument.getListFiles() != null) {
						for (FilesetDV filesetDV : subDocument.getListFiles()) {
							System.out.println("******* File Fileset name: " + filesetDV.getGcubeProfileFieldName());
							for (PayloadDV payload : filesetDV.getListPayload()) {
								System.out.println("********* Payload: " + payload);
							}
						}
					}
					System.out.println("***** Images");
					if (subDocument.getListImages() != null) {
						for (FilesetDV filesetDV : subDocument.getListImages()) {
							System.out.println("******* Image Fileset name: " + filesetDV.getGcubeProfileFieldName());
							for (PayloadDV payload : filesetDV.getListPayload()) {
								System.out.println("********* Payload: " + payload);
							}
						}
					}
					i++;
				}

			}
		} catch (Exception e) {
			System.out.println("error");
			e.printStackTrace();
		}

		System.out.println("\n\n Procedure termimated!!!");
	}

	private List<Payload> readPayloadsForFileset(String filesetJSONPath, String sectionJSONDocument) {
		LOG.debug("readPayloadsForFileset called");

		List<Payload> listPayloads = new ArrayList<Payload>();
		String _payloadsJSONPath = String.format("%s.%s", filesetJSONPath, "_payloads");
		try {
			com.jayway.jsonpath.Configuration configuration = com.jayway.jsonpath.Configuration.builder()
					.jsonProvider(new JsonOrgJsonProvider()).build();

			LOG.info("Reading sectionPath at {} into section document {}", _payloadsJSONPath, sectionJSONDocument);
			JsonPath theSectionPolycJsonPath = JsonPath.compile(_payloadsJSONPath);
			Object _payloads = theSectionPolycJsonPath.read(sectionJSONDocument, configuration).toString();

			if (_payloads instanceof String) {
				String toStringPayloads = (String) _payloads;
				LOG.trace("The _payloads is a String {}", toStringPayloads);
				JSONArray jsonArray = new JSONArray(toStringPayloads);
				for (int i = 0; i < jsonArray.length(); i++) {
					Payload payloadDV = Serialization.read(jsonArray.getJSONObject(i).toString(), Payload.class);
					listPayloads.add(payloadDV);
				}
			}

			LOG.info("returning list of payloads {}", listPayloads);
		} catch (Exception e) {
			LOG.error("Error on reading the JSON Path {} in the doc {} ", _payloadsJSONPath, sectionJSONDocument);
			e.printStackTrace();
		}

		return listPayloads;

	}

	private boolean isAccessibleSectionAccordingToPolicy(Document section, String sectionJSONPath) {
		LOG.debug("isAccessibleSectionAccordingToPolicy called");
		boolean isAccessible = true;

		// Skipping the root, going to check the access_policy of subsections
		if (sectionJSONPath.compareTo(JSON_$_POINTER) != 0) {
			isAccessible = checkAccessPolicy(section.toJson(), MY_LOGIN);
		}

		return isAccessible;
	}

	private boolean checkAccessPolicy(String sectionDocumentJSON, String myLogin) {
		LOG.debug("checkAccessPolicy called");
		// CHECKING THE POLICY
		String accessPolicyPath = JSON_$_POINTER + "._access._policy";
		boolean isAccessible = true;
		try {
			com.jayway.jsonpath.Configuration configuration = com.jayway.jsonpath.Configuration.builder()
					.jsonProvider(new JsonOrgJsonProvider()).build();

			LOG.info("Reading access policy at {} into section document {}", accessPolicyPath, sectionDocumentJSON);
			JsonPath theSectionPolycJsonPath = JsonPath.compile(accessPolicyPath);
			String _policy = theSectionPolycJsonPath.read(sectionDocumentJSON, configuration).toString();
			LOG.info("The section {} has policy {}", accessPolicyPath, _policy);
			isAccessible = GeportalCheckAccessPolicy.isAccessible(_policy, myLogin);
		} catch (Exception e) {
			LOG.error(accessPolicyPath + " not found. Check OK");
		}
		LOG.info("Is the section {} accessible? {}", sectionDocumentJSON, isAccessible);
		return isAccessible;
	}

	private String prettyPrintJSON(String jsonString) {

		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		JsonObject jsonObject = new JsonParser().parse(jsonString).getAsJsonObject();
		return gson.toJson(jsonObject);
	}

	private Document sanitizeDocumentValue(Document toDoc, String fieldLabel, Object theObjectFieldValue) {

		if (theObjectFieldValue != null) {
			if (theObjectFieldValue instanceof String) {
				String toString = (String) theObjectFieldValue;
				if (toString != null && !toString.isEmpty()) {
					toDoc.append(fieldLabel, theObjectFieldValue);
				} else {
					LOG.debug("Skipping String field " + fieldLabel + " its value is null or empty");
				}

			} else if (theObjectFieldValue instanceof ArrayList) {
				ArrayList toArrayList = (ArrayList) theObjectFieldValue;
				if (toArrayList != null && !toArrayList.isEmpty()) {
					toDoc.append(fieldLabel, theObjectFieldValue);
				} else {
					LOG.debug("Skipping ArrayList field " + fieldLabel + " its value is null or empty");
				}
			} else {
				toDoc.append(fieldLabel, theObjectFieldValue);
			}
		} else {
			LOG.debug("Skipping field " + fieldLabel + " its value is null or empty");
		}

		return toDoc;
	}

	/**
	 * To list gcube profiles.
	 *
	 * @param config the config
	 * @return the list
	 */
	private List<GcubeProfileDV> toListGcubeProfiles(ConfigurationDV<?> config) {

		try {
			return (List<GcubeProfileDV>) config.getConfiguration();
		} catch (Exception e) {
			System.err.println("Error on casting " + ConfigurationDV.class.getName() + " to List of "
					+ GcubeProfileDV.class.getName());
		}

		return null;
	}

}
