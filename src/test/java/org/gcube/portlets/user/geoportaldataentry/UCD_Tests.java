package org.gcube.portlets.user.geoportaldataentry;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.gcube.application.geoportal.common.model.useCaseDescriptor.UseCaseDescriptor;
import org.gcube.application.geoportalcommon.ConvertToDataValueObjectModel;
import org.gcube.application.geoportalcommon.geoportal.GeoportalClientCaller;
import org.gcube.application.geoportalcommon.geoportal.UseCaseDescriptorCaller;
import org.gcube.application.geoportalcommon.shared.geoportal.ucd.GEOPORTAL_DATA_HANDLER;
import org.gcube.application.geoportalcommon.shared.geoportal.ucd.UseCaseDescriptorDV;
import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.common.scope.api.ScopeProvider;
import org.junit.Before;

public class UCD_Tests {

	private UseCaseDescriptorCaller client = null;

	// private static String CONTEXT = "/pred4s/preprod/preVRE";
	// private static String TOKEN = ""; //preVRE

	private static String CONTEXT = "/gcube/devsec/devVRE";
	private static String TOKEN = ""; // devVRE

	private static String PROFILE_ID = "profiledConcessioni";

	@Before
	public void getClient() {
		// assumeTrue(GCubeTest.isTestInfrastructureEnabled());
		ScopeProvider.instance.set(CONTEXT);
		SecurityTokenProvider.instance.set(TOKEN);
		client = GeoportalClientCaller.useCaseDescriptors();
	}

	// @Test
	public void getList() throws Exception {
		ScopeProvider.instance.set(CONTEXT);
		SecurityTokenProvider.instance.set(TOKEN);
		List<UseCaseDescriptor> listOfUCD = client.getList();

		int i = 0;
		for (UseCaseDescriptor useCaseDescriptor : listOfUCD) {
			System.out.println(++i + ") " + useCaseDescriptor);
		}
	}

	// @Test
	public void getItemByID() throws Exception {
		ScopeProvider.instance.set(CONTEXT);
		SecurityTokenProvider.instance.set(TOKEN);

		UseCaseDescriptor ucd = client.getUCDForId(PROFILE_ID);
		System.out.println(ucd);
	}

	// @Test
	public void convertToDVObject() throws Exception {
		ScopeProvider.instance.set(CONTEXT);
		SecurityTokenProvider.instance.set(TOKEN);
		List<UseCaseDescriptor> listOfUCD = client.getList();

		int i = 0;
		for (UseCaseDescriptor useCaseDescriptor : listOfUCD) {
			System.out.println(++i + ") " + useCaseDescriptor);
			ConvertToDataValueObjectModel.toUseCaseDescriptorDV(useCaseDescriptor, null);
		}
	}

	//@Test
	public void getUCDForHandlerIds() throws Exception {
		ScopeProvider.instance.set(CONTEXT);
		SecurityTokenProvider.instance.set(TOKEN);

//		List<String> handlersIds = Arrays.asList(GEOPORTAL_DATA_HANDLER.geoportal_data_entry.getId());
//		List<UseCaseDescriptor> listOfUCD = client
//				.getListForHandlerIds(handlersIds);
//		int i = 0;
//		for (UseCaseDescriptor useCaseDescriptor : listOfUCD) {
//			System.out.println(++i + ") " + useCaseDescriptor);
//			UseCaseDescriptorDV ucdDV = ConvertToDataValueObjectModel.toUseCaseDescriptorDV(useCaseDescriptor, null);
//			System.out.println("returned " + ucdDV);
//		}

		List<String> handlersIds = Arrays.asList(GEOPORTAL_DATA_HANDLER.geoportal_data_entry.getId());
		List<UseCaseDescriptor> listUseCaseDescriptor;
		try {
			UseCaseDescriptorCaller client = GeoportalClientCaller.useCaseDescriptors();

			if (handlersIds == null) {
				handlersIds = Arrays.asList(GEOPORTAL_DATA_HANDLER.geoportal_data_entry.getId());
				System.out.println("handlersIds is null, so using default: " + handlersIds);
			}
			listUseCaseDescriptor = client.getListForHandlerIds(handlersIds);
		} catch (Exception e) {
			String error = "Error on contacting the Geoportal service";
			System.out.println(error + " for handlers: " + handlersIds);
			throw new Exception(
					"Error when contacting the Geoportal service. Refresh and try again or contact the support", e);
		}

		if (listUseCaseDescriptor == null) {
			listUseCaseDescriptor = new ArrayList<UseCaseDescriptor>();
		}

		List<UseCaseDescriptorDV> listUCDDV = new ArrayList<UseCaseDescriptorDV>(listUseCaseDescriptor.size());
		for (UseCaseDescriptor ucd : listUseCaseDescriptor) {
			listUCDDV.add(ConvertToDataValueObjectModel.toUseCaseDescriptorDV(ucd, null));
		}
		
		System.out.println(listUCDDV);

	}

}
